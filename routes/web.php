<?php
// Redirects
Route::redirect('pages/support-us', '/donations', 301);

// Home Page
Route::get('/', 'HomeController@index');
Route::get('/index', 'HomeController@indexDev');  // To bypass holding page in development mode

// Newsletter Email
Route::get('/newsletter', 'NewsController@newsletter');

// Content Styles
Route::get('/style-guide', 'HomeController@styleGuide');

// Find a Sarcoma Specialist
Route::get('/pages/about-sarcoma/find-a-sarcoma-specialist', 'SpecialistsController@list');

// Contact Form
Route::group(['prefix' => '/contact'], function () {
    Route::get('/', 'ContactController@index');
    Route::post('save-message', 'ContactController@saveMessage');
    Route::post('save-message', 'ContactController@saveMessage');
    Route::get('success', 'ContactController@success');
	Route::post('save-message-golive', 'ContactController@saveMessageGolive');
});

// Pages Module
Route::group(['prefix' => '/pages'], function () {
    Route::get('{category}', 'PagesController@index');    
	Route::get('{category}/{page}', 'PagesController@index');    
});

// News Module
Route::group(['prefix' => '/news'], function () {
	Route::get('', 'NewsController@list'); 	
	
	Route::get('archive', 'NewsController@archive'); 
	Route::get('archive/{age}', 'NewsController@archive'); 
	
    Route::get('{category}', 'NewsController@list');    
	Route::get('{category}/{page}', 'NewsController@item'); 
	
});

// Events Module
Route::group(['prefix' => '/events'], function () {
	Route::get('', 'EventsController@list');

    Route::get('{category}', 'EventsController@list');
	Route::get('{category}/{page}', 'EventsController@item');

	Route::get('register/{page}', 'EventsController@register');
});

// Event Register Module
Route::group(['prefix' => '/event-register'], function () {	
	Route::any('paypal-success', 'EventsController@paypalSuccess');
    Route::any('paypal-cancel', 'EventsController@paypalCancel');
	Route::get('{event}', 'EventsController@register');
	Route::post('store-asm2021', 'EventsController@registerStoreAsm2021');
});

// Events Module - Abstracts
Route::group(['prefix' => '/asm-2020/abstracts'], function () {
	Route::get('', 'EventsController@abstracts');
	Route::get('{token}', 'EventsController@abstractsRegister');
});

// Events Module - Abstracts
Route::group(['prefix' => '/asm-2021/abstracts'], function () {
    Route::get('', 'EventsController@abstracts2021');
    Route::get('{token}', 'EventsController@abstractsRegister2021');
});

// Events Module - Abstracts-submit
Route::group(['prefix' => '/asm-2021/submit-abstract'], function () {
    Route::get('', 'EventsController@submitAbstracts2021');
    Route::post('store', 'EventsController@storeAbstracts2021');
});

// Faqs Module
// Faqs Module
Route::group(['prefix' => '/faqs'], function () {
	Route::get('', 'FaqsController@index'); 
    Route::get('{category}', 'FaqsController@index');    	
});

// Documents Module
Route::group(['prefix' => '/documents'], function () {
	Route::get('', 'DocumentsController@index'); 
    Route::get('{category}', 'DocumentsController@index');    	
});

// Gallery Module
Route::group(['prefix' => '/gallery'], function () {
	Route::get('', 'GalleryController@index'); 
    Route::get('{category}', 'GalleryController@detail');    	
});

// Projects Module
Route::group(['prefix' => '/members/projects'], function () {
	Route::get('', 'ProjectsController@list'); 
});

Route::group(['prefix' => '/projects'], function () {
	Route::get('', 'ProjectsController@list'); 			
	
    Route::get('{category}', 'ProjectsController@list');    
	Route::get('{category}/{page}', 'ProjectsController@item');
});

// Properties Module
Route::group(['prefix' => '/properties'], function () {
    Route::get('{category}', 'PropertiesController@list');
    Route::get('{category}/{page}', 'PropertiesController@item');
});


// Team Module
Route::group(['prefix' => '/team'], function () {
    Route::get('', 'TeamController@index');
    Route::get('{category}', 'TeamController@index');
    Route::get('{category}/{member}', 'TeamController@member');
});

// Donation Module
Route::group(['prefix' => '/donations'], function () {
    Route::get('/', 'DonationsController@index');
    Route::post('save-donation', 'DonationsController@saveDonation');
    Route::any('paypal-success', 'DonationsController@paypalSuccess');
	Route::any('paypal-cancel', 'DonationsController@paypalCancel');
    Route::middleware('cancel-link-validation')->get('cancel-recurring-payment/{id}/{hash}', 'DonationsController@cancelRecurringPayment');

    Route::get('christmas-appeal', 'DonationsController@xmas');
});

Route::post('ipn/notify','PayPalController@postNotify');



// Specialists Module
Route::group(['prefix' => '/specialists'], function () {
	Route::get('/', 'SpecialistsController@list'); 				
});

// ***********
// Members Module
// ***********
// Apply
Route::group(['prefix' => '/apply'], function () {    
	Route::get('/', 'RegisterController@register');
    Route::post('store', 'RegisterController@store');
});

// Register & Pay
Route::group(['prefix' => '/register'], function () {
	Route::any('paypal-success', 'RegisterController@paypalSuccess');
    Route::any('paypal-cancel', 'RegisterController@paypalCancel');
	Route::get('/{member}', 'RegisterController@accountSetup');
    Route::post('store', 'RegisterController@accountSetupStore');
});

// Login
Route::group(['prefix' => '/login'], function () {
    Route::get('/', 'Auth\LoginController@showLoginForm');
	Route::post('/', 'Auth\LoginController@login');
	
	
   // Route::post('process', 'MembersController@process');
	
	//Route::get('forgot', 'MembersController@forgot');
}); 

// Logout
Route::group(['prefix' => '/logout'], function () {
    Route::get('/', 'Auth\LoginController@logout');	
}); 

// Password Reset
Route::group(['prefix' => '/password'], function () {
    Route::get('forgot', 'Auth\ForgotPasswordController@showLinkRequestForm');	
	Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail');	
	
	Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm');	
	Route::post('reset/', 'Auth\ResetPasswordController@reset');	
}); 

// Members Only Section
Route::group(['prefix' => '/members-portal'], function () {
    Route::get('/', 'MembersController@index');
	
	Route::get('change-details', 'MembersController@changeDetails');
	Route::post('save-details', 'MembersController@saveDetails');

	Route::get('change-password', 'MembersController@changePassword');
	Route::post('save-password', 'MembersController@savePassword');

    Route::get('renew', 'MembersController@renew');
    Route::post('renew-process', 'MembersController@renewProcess');
    Route::any('paypal-success', 'MembersController@paypalSuccess');
    Route::any('paypal-cancel', 'MembersController@paypalCancel');
    Route::any('payments', 'MembersController@payments');
});

// ***********
// Shop Module
// ***********
Route::group(['prefix' => '/products', 'as' => 'products.'], function() {
    Route::get('', 'ProductsController@list')->name('list'); 	
	
	Route::get('{category}', 'ProductsController@list');    
	Route::get('{category}/{page}', 'ProductsController@item'); 	
});

Route::group(['prefix' => 'cart', 'as' => 'cart.'], function() {
    Route::get('show', 'CartController@show')->name('show');
    Route::post('add/{product}', 'CartController@add')->name('add');
    Route::post('remove/{cart_item}', 'CartController@remove')->name('remove');
});

Route::group(['prefix' => 'checkout', 'as' => 'checkout.'], function() {
    Route::get('show', 'CheckoutController@show')->name('show');	
    Route::post('submit', 'CheckoutController@submit')->name('submit');
		
	Route::get('complete/{order}', 'CheckoutController@complete')->name('complete');
	Route::get('cancel/{order}', 'CheckoutController@cancel')->name('cancel');
});
// ***********

// Testimonials Module
Route::group(['prefix' => '/testimonials'], function () {
    Route::get('', 'TestimonialsController@index');
});

// Site Search
Route::group(['prefix' => '/results'], function () {
    Route::any('', 'SiteSearchController@index');
});

// Email
Route::any('email-test', 'HomeController@emailTest');
