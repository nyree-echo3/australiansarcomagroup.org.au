$(document).ready(function() {

    var form = $("#frmRegister");

    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); }
    });

    var settings = {
        headerTag: "h5",
        bodyTag: "section",
        stepsOrientation: "horizontal",
        autoFocus: true,
        labels:
            {
                finish: "Update",
            },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            form.submit();
        }
    };

   
        settings.enableAllSteps = true;
        settings.startIndex = 0;
   

    form.steps(settings);

    
        $("li[role='tab']").removeClass('current').addClass('done');
        form.validate().settings.ignore = ":disabled";


    $('input[type="radio"]').iCheck({
        radioClass: 'iradio_square-grey'
    });  
});