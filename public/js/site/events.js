// JavaScript Document
function showPopup(html, w, h){
   $("#jQueryPopupDiv").dialog({
		modal: true,
		title: 'ANZSA',
		width: w,
		height: h,
		position: {
        my: "center center",
        at: "center center",
        of: window
    },
		buttons: {
			Close: function () {
				$(this).dialog('close');
			}
		},
		open: function () {
			$("#jQueryPopupDiv").html(html);
		}
	});
}
			
function btnBooking_onclick(eventSEO, sitepath)  {	
	// alert if user want to continue like guest
	var isLoggedIn = document.getElementById('hidIsMemberLoggedIn').value;
	var membersOnlyEvent = document.getElementById('hidMembersOnlyEvent').value;
	// user is not logged-in                

	if (!isLoggedIn) {		
		var html;
		if (membersOnlyEvent == "active") {               
			html = 'This is a members only event, would you like to login now?<br><br>';   
			html += "<a href='" + sitepath + '/login?redirect=event-register/' + eventSEO +  "' class='btn-popup'>Member Login</a>&nbsp;&nbsp;";
		} else {                      
			html = 'You are currently not logged in. Would you like to login as a member or continue on as a guest?<br><br>';
			html += "<a href='" + sitepath + '/login?redirect=event-register/' + eventSEO +  "' class='btn-popup'>Member Login</a>&nbsp;&nbsp;";
			html += "<a href='" + sitepath + '/event-register/' + eventSEO +  "' class='btn-popup'>Continue as Guest</a> &nbsp;&nbsp;";               
		}
		showPopup(html, 400, 250);
		return;
	}
	
	window.location.href = sitepath + "/event-register/" + eventSEO;
}
