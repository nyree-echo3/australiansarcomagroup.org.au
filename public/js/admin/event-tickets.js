// JavaScript Document
$(document).ready(function () {
	$('.money').mask('#,##0.00', {reverse: true});
	
	$("#btnTicketAdd").click(function () {
		event_row = parseInt($("#event_tickets").val()) + 1;
		
		strHtml  = '';
		
		strHtml += '<div id="ticketRow' + event_row + '">';	
		strHtml += '<div class="form-group">';	
		
		strHtml += '<div class="col-sm-6">';
		strHtml += '<div class="input-group ticket-group">';			
		strHtml += '<input type="text" name="ticket_name' + event_row + '" class="form-control" value="" placeholder="Name">';
		strHtml += '<input type="hidden" name="ticket_id' + event_row + '" value="">';
		strHtml += '</div>';	
		strHtml += '</div>';
	
		strHtml += '<div class="col-sm-3">';
		strHtml += '<div class="input-group ticket-group">';			
		strHtml += '<input type="text" name="ticket_price' + event_row + '" class="form-control money" value="" placeholder="0.00">';
		strHtml += '</div>';		
		strHtml += '</div>';
	
		strHtml += '<div class="col-sm-1">';
		strHtml += '<div class="input-group ticket-group">';			
		strHtml += '<input class="page_status" type="checkbox" name="ticket_members_only' + event_row + '">';
		strHtml += '</div>';	
		strHtml += '</div>';

		strHtml += '<div class="col-sm-1">';
		strHtml += '<div class="input-group ticket-group">';
		strHtml += '<input class="page_status" type="checkbox" name="ticket_virtual' + event_row + '">';
		strHtml += '</div>';
		strHtml += '</div>';
	
		strHtml += '<div class="col-sm-1">';
		strHtml += '<div class="input-group ticket-group">';			
		strHtml += '<a href="#" id="btnTicketDelete' + event_row + '" class="tool" data-toggle=confirmation data-title="Are you sure?" data-popout="true" data-singleton="true" data-btn-ok-label="Yes" data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>';
		strHtml += '<input type="hidden" id="ticket_is_deleted' + event_row + '" name="ticket_is_deleted' + event_row + '" value="false">';
		strHtml += '</div>';	
		strHtml += '</div>';
		
		strHtml += '</div>';
		
		strHtml += '<script>';
		strHtml += "$('#btnTicketDelete" + event_row + "').confirmation({";
		strHtml += 'onConfirm:function(e) {		';			
		strHtml += 'btnDeleteTicket(' + event_row + ');';		
     	strHtml += 'e.preventDefault();'
		strHtml += 'return false;';
		strHtml += '},';
		strHtml += 'onCancel:function(e) {';     
		strHtml += 'return false;';	
		strHtml += '}';
		strHtml += '});';
		strHtml += '</script>';
		
		$("#ticketsWrapper").append(strHtml);
		$("#event_tickets").val(event_row);					
	});			
});	

function btnDeleteTicket(ticketId)  {			
	$("#ticket_is_deleted" + ticketId).val("true");
	$("#ticketRow" + ticketId).hide();	
}