<?php
namespace App\Exports;

use App\Member;
use App\MemberPayment;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class MembersPaymentsExport implements FromView, WithEvents
{
    public function view(): View
    {
		//$payments = MemberPayment::get();
		$payments = MemberPayment::with('member')->get();
		
        return view('admin/members/export-members-payment', [
            'payments' => $payments
        ]);
    }
	
	public function registerEvents(): array
    {		
        return [
			AfterSheet::class    => function(AfterSheet $event) {     
			    $event->sheet->autoSize();
			
                $event->sheet->getDelegate()->getStyle('A1:A1')->getFont()->setSize(14);
			    
			    $event->sheet->setAutoFilter('A5:F5');
			    
			    $event->sheet->getStyle('A5:F5')->getFill()
							 ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('8db4e2'); 						   			    
            },		
        ];
    }
} 