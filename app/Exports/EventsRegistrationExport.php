<?php
namespace App\Exports;

use App\Event;
use App\EventsCategory;
use App\EventsTicket;
use App\EventsBooking;
use App\EventsBookingPayment;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class EventsRegistrationExport implements FromView, WithEvents
{
    public function view(): View
    {		
		$registrations = EventsBooking::with('event')->with('ticket')->with('payment')->orderBy('id', 'desc')->get();
		
        return view('admin/events/export-events-registrations', [
            'registrations' => $registrations
        ]);
    }
	
	public function registerEvents(): array
    {		
        return [
			AfterSheet::class    => function(AfterSheet $event) {     
			    $event->sheet->autoSize();
			
                $event->sheet->getDelegate()->getStyle('A1:A1')->getFont()->setSize(14);
			    
			    $event->sheet->setAutoFilter('A5:M5');
			    
			    $event->sheet->getStyle('A5:M5')->getFill()
							 ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('8db4e2'); 						   			    
            },		
        ];
    }
} 