<?php

namespace App\Mail;

use App\Member;
use App\MemberQualification;
use App\MemberAward;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

use App\Setting;

class SubmitAbstractMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $abstract;

    public function __construct($abstract)
    {
        $this->abstract = $abstract;
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;

        return $this->subject('Website | Abstract Submission')
                    ->from($contactEmail)
                    ->view('site/emails/submit-abstract-message-admin');

    }
}
