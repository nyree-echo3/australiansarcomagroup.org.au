<?php

namespace App\Mail;

use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubmitAbstractMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $abstract;

    public function __construct($abstract)
    {
        $this->abstract = $abstract;
    }

    public function build()
    {
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;				
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject($companyName.' | Abstract Submission')
			        ->from($contactEmail)
			        ->view('site/emails/submit-abstract-message-user', array(
						'companyName' => $companyName, 												
					));
    }
}
