<?php

namespace App\Mail;

use App\Member;
use App\MemberQualification;
use App\MemberAward;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

use App\Setting;

class MembersMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $member;
	public $qualifications;
	public $awards;

    public function __construct($member)
    {
        $this->member = $member;
		$this->qualifications = MemberQualification::where('member_id', '=', $member->id)->get();
		$this->awards = MemberAward::where('member_id', '=', $member->id)->get();
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		$member = $this->member;
		
		if ($member->cvFile != "") {
			$cvFile = "app/members/" . $member->id . "/" . $member->cvFile;	    

			return $this->subject('Website | Membership Application')
						->from($contactEmail)
						->attach(storage_path($cvFile))
						->view('site/emails/members-message-admin');
	    } else  {		   
			return $this->subject('Website | Membership Application')
						->from($contactEmail)						
						->view('site/emails/members-message-admin');
	    }
    }
}
