<?php

namespace App\Mail;

use App\Donation;
use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DonationMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $donation;
    public $next_billing_date;
    public $cancel_link_hash;

    public function __construct(Donation $donation, $next_billing_date = null, $cancel_link_hash = null)
    {
        $this->donation = $donation;
        $this->next_billing_date = $next_billing_date;
        $this->cancel_link_hash = $cancel_link_hash;
    }

    public function build()
    {
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;
		
		$setting = Setting::where('key','=','contact-details')->first();
		$contactDetails = $setting->value;
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		$payment_direct_deposit = Setting::where('key', '=', 'payment-direct-deposit')->first();
		
        return $this->subject($companyName . " | Donation")
			        ->from($contactEmail)
			        ->view('site/emails/donation-message-user', array(
						'companyName' => $companyName, 
						'contactDetails' => $contactDetails, 
						'payment_direct_deposit' => $payment_direct_deposit->value
					));
    }
}
