<?php

namespace App\Mail;

use App\Donation;
use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DonationCancelMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $donation;

    public function __construct(Donation $donation)
    {
        $this->donation = $donation;
    }

    public function build()
    {
        $setting = Setting::where('key','=','company-name')->first();
        $companyName = $setting->value;

        $setting = Setting::where('key','=','contact-details')->first();
        $contactDetails = $setting->value;

        $setting = Setting::where('key','=','contact-email')->first();
        $contactEmail = $setting->value;

        return $this->subject($companyName.' - Cancel Regular Donation')
            ->from($contactEmail)
            ->view('site/emails/donation-cancel-message-user', array(
                'companyName' => $companyName,
                'contactDetails' => $contactDetails,
            ));
    }
}
