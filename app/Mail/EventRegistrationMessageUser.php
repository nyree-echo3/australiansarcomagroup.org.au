<?php

namespace App\Mail;

use App\Events;
use App\EventsCategory;
use App\EventsTicket;
use App\EventsBooking;
use App\EventsBookingPayment;
use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventRegistrationMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $event;
	public $ticket;
	public $event_booking;
	public $event_payment;
	
    public function __construct($event, $ticket, $event_booking, $event_payment)
    {
        $this->event = $event;
        $this->ticket = $ticket;
		$this->event_booking = $event_booking;
		$this->event_payment = $event_payment;
    }

    public function build()
    {
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;				
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;

        $payment_direct_deposit_events = Setting::where('key', '=', 'payment-direct-deposit-events')->first();
		
        return $this->subject($companyName.' | Event Registration')
			        ->from($contactEmail)
			        ->view('site/emails/event-registration-message-user', array(
						'companyName' => $companyName, 		
						'payment_direct_deposit_events' => $payment_direct_deposit_events->value
					));
    }
}
