<?php

namespace App\Mail;

use App\Donation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class DonationMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $donation;
    public $next_billing_date;

    public function __construct(Donation $donation, $next_billing_date = null)
    {
        $this->donation = $donation;
        $this->next_billing_date = $next_billing_date;
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject('Website | Donation')
			        ->from($contactEmail)
			        ->view('site/emails/donation-message-admin');
    }
}
