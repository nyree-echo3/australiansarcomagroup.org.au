<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;
use Laravel\Scout\Searchable;

class Page extends Model
{
    use SortableTrait, Sortable, Searchable;

    protected $table = 'pages';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        unset($array['child_pages']);
        return $array;
    }

    public function category()
    {
        return $this->belongsTo(PageCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(PageCategory::class,'id','category_id');
    }

    public function parentPage()
    {
        return $this->belongsTo(Page::class, 'parent_page_id');
    }

    public function parentpagesort()
    {
        return $this->hasOne(Page::class, 'id');
    }

    public function childPages()
    {
        return $this->hasMany(Page::class, 'parent_page_id');
    }

    public function getBodyCleanAttribute()
    {
        $body = strip_tags($this->attributes['body']);
        $body = str_replace('&nbsp;','', $body);
        $body = str_replace("\r\n",'', $body);
        $body = str_replace("\t",'', $body);
        $body = str_replace("&quot;",'"', $body);
        $body = str_replace("&#39;","'", $body);
        $body = str_replace("&lt;","<", $body);
        $body = str_replace("&gt;",">", $body);
        $body = str_replace("&amp;",'&', $body);
        $body = str_limit($body, 320);

        return $body;
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('pages-filter');
        $select = $query;

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','pages')->where('type','=','item')->first();
        if($special_url){
            return $special_url->url;
        }


        return 'pages/'.$this->category->slug.'/'.$this->attributes['slug'];
    }

}
