<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;
use Laravel\Scout\Searchable;

class TeamMember extends Model
{
    use SortableTrait, Sortable, Searchable;

    protected $table = 'team_members';

    public $sortable = ['name', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        return $array;
    }

    public function category()
    {
        return $this->belongsTo(TeamCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(TeamCategory::class,'id','category_id');
    }

    public function getBodyCleanAttribute()
    {
        $body = strip_tags($this->attributes['body']);
        $body = str_replace('&nbsp;','', $body);
        $body = str_replace("\r\n",'', $body);
        $body = str_replace("\t",'', $body);
        $body = str_replace("&quot;",'"', $body);
        $body = str_replace("&#39;","'", $body);
        $body = str_replace("&lt;","<", $body);
        $body = str_replace("&gt;",">", $body);
        $body = str_replace("&amp;",'&', $body);
        $body = str_limit($body, 320);

        return $body;
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('team-filter');
        $select = $query;

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('name','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','team')->where('type','=','item')->first();
        if($special_url){
            return $special_url->url;
        }


        return 'team/'.$this->category->slug.'/'.$this->attributes['slug'];
    }
}
