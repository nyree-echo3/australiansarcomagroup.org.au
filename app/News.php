<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Laravel\Scout\Searchable;

class News extends Model
{
    use Sortable, Searchable;

    protected $table = 'news';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        return $array;
    }

    public function category()
    {
        return $this->belongsTo(NewsCategory::class, 'category_id');
    }
	public function categoryactive()
    {
        return $this->belongsTo(NewsCategory::class, 'category_id')->where('status', '=', 'active');
    }
	public function categorypublic()
    {
        return $this->belongsTo(NewsCategory::class, 'category_id')->where('members_only', '=', 'passive')->where('status', '=', 'active');
    }	
    public function categorysort()
    {
        return $this->hasOne(NewsCategory::class,'id','category_id');
    }

    public function getBodyCleanAttribute()
    {
        $body = strip_tags($this->attributes['body']);
        $body = str_replace('&nbsp;','', $body);
        $body = str_replace("\r\n",'', $body);
        $body = str_replace("\t",'', $body);
        $body = str_replace("&quot;",'"', $body);
        $body = str_replace("&#39;","'", $body);
        $body = str_replace("&lt;","<", $body);
        $body = str_replace("&gt;",">", $body);
        $body = str_replace("&amp;",'&', $body);
        $body = str_limit($body, 320);

        return $body;
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('news-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','news')->where('type','=','item')->first();
        if($special_url){
            return $special_url->url;
        }


        return 'news/'.$this->category->slug.'/'.$this->attributes['slug'];
    }
}
