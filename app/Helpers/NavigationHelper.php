<?php
namespace App\Helpers;

use App\Module;
use App\Navigation;
use App\Page;
use App\PageCategory;

class NavigationHelper {


    public function navigationItems($mode, $type, $module_slug, $slug, $href=null, $new_name=null, $new_slug=null, $children=null, $menu_type = "main")
    {
		switch ($menu_type)  {
			case "members": $menu_id = 2; break;	
			default: $menu_id = 1; 		
		}
		
		
        $navigation_raw = Navigation::where('id', '=', $menu_id)->first();
        $navigation = json_decode($navigation_raw->navigation);

        $existence_control[0] = false;
        $existence_control[1] = '';

        foreach($navigation as $key => $element){

            //lvl 1
            //echo $element->text."<br>";
            if($element->type==$type && $element->module==$module_slug && $element->slug==$slug){
                $existence_control[0] = true;
                $existence_control[1] = $element;

                if($mode=='change-href'){
                    $element->href = $href;
                }

                if($mode=='delete-item'){					
                    array_splice($navigation, $key, 1);
                }

                if($mode=='update'){
                    $element->text = $new_name;
                    $element->slug = $new_slug;
                }

                if($mode=='add-pages'){
                    $element->children = json_decode(json_encode($children));
                }
            }

            if($mode=='side-nav') {

                if($element->href==$href){

                    if(isset($element->children) && sizeof($element->children) > 1){
                        return $element->children;
                    }
                    return null;
                }
            }

            if(isset($element->children)){

                foreach ($element->children as $key_lvl1 => $children_lvl1){

                    //lvl2
                    //echo '-'.$children_lvl1->text."<br>";
                    if($children_lvl1->type==$type && $children_lvl1->module==$module_slug && $children_lvl1->slug==$slug){
                        $existence_control[0] = true;
                        $existence_control[1] = $children_lvl1;

                        if($mode=='change-href'){
                            $children_lvl1->href = $href;
                        }

                        if($mode=='delete-item'){
                            array_splice($element->children, $key_lvl1, 1);

                            if(count($element->children)==0){
                                unset($element->children);
                            }
                        }

                        if($mode=='update'){
                            $children_lvl1->text = $new_name;
                            $children_lvl1->slug = $new_slug;
                        }

                        if($mode=='add-pages'){
                            $children_lvl1->children = json_decode(json_encode($children));
                        }

                    }

                    if($mode=='side-nav') {
                        if($children_lvl1->href==$href){
                            return $element->children;
                        }
                    }

                    if(isset($children_lvl1->children)){

                        foreach($children_lvl1->children as $key_lvl2 => $children_lvl2){

                            //lvl3
                            //echo '--'.$children_lvl2->text."<br>";
                            if($children_lvl2->type==$type && $children_lvl2->module==$module_slug && $children_lvl2->slug==$slug){
                                $existence_control[0] = true;
                                $existence_control[1] = $children_lvl2;

                                if($mode=='change-href'){
                                    $children_lvl2->href = $href;
                                }

                                if($mode=='delete-item'){
                                    array_splice($children_lvl1->children, $key_lvl2, 1);

                                    if(count($children_lvl1->children)==0){
                                        unset($children_lvl1->children);
                                    }
                                }

                                if($mode=='update'){
                                    $children_lvl2->text = $new_name;
                                    $children_lvl2->slug = $new_slug;
                                }

                                if($mode=='add-pages'){
                                    $children_lvl2->children = json_decode(json_encode($children));
                                }
                            }

                            if($mode=='side-nav') {
                                if($children_lvl2->href==$href){
                                    return $element->children;
                                }
                            }

                            if(isset($children_lvl2->children)){

                                foreach($children_lvl2->children as $key_lvl3 => $children_lvl3){

                                    //lvl4
                                    //echo '---'.$children_lvl3->text."<br>";
                                    if($children_lvl3->type==$type && $children_lvl3->module==$module_slug && $children_lvl3->slug==$slug){
                                        $existence_control[0] = true;
                                        $existence_control[1] = $children_lvl3;

                                        if($mode=='change-href'){
                                            $children_lvl3->href = $href;
                                        }

                                        if($mode=='delete-item'){
                                            array_splice($children_lvl2->children, $key_lvl3, 1);

                                            if(count($children_lvl2->children)==0){
                                                unset($children_lvl2->children);
                                            }
                                        }

                                        if($mode=='update'){
                                            $children_lvl3->text = $new_name;
                                            $children_lvl3->slug = $new_slug;
                                        }

                                        if($mode=='add-pages'){
                                            $children_lvl3->children = json_decode(json_encode($children));
                                        }
                                    }

                                    if($mode=='side-nav') {
                                        if($children_lvl3->href==$href){
                                            return $element->children;
                                        }
                                    }

                                    if(isset($children_lvl3->children)){

                                        foreach($children_lvl3->children as $key_lvl4 => $children_lvl4){

                                            //lvl5
                                            //echo '-----'.$children_lvl4->text."<br>";
                                            if($children_lvl4->type==$type && $children_lvl4->module==$module_slug && $children_lvl4->slug==$slug){
                                                $existence_control[0] = true;
                                                $existence_control[1] = $children_lvl4;

                                                if($mode=='change-href'){
                                                    $children_lvl4->href = $href;
                                                }

                                                if($mode=='delete-item'){
                                                    array_splice($children_lvl3->children, $key_lvl4, 1);

                                                    if(count($children_lvl3->children)==0){
                                                        unset($children_lvl3->children);
                                                    }
                                                }

                                                if($mode=='update'){
                                                    $children_lvl4->text = $new_name;
                                                    $children_lvl4->slug = $new_slug;
                                                }
                                            }

                                            if($mode=='side-nav') {
                                                if($children_lvl4->href==$href){
                                                    return $element->children;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if($mode=='change-href' || $mode=='delete-item' || $mode=='update' || $mode=='add-pages'){

            $navigation_raw->navigation = json_encode($navigation);
            $navigation_raw->save();
        }

        return $existence_control;
    }

    public function navigationAddItem($text, $href, $id, $module, $type, $slug, $menu_type = "main")
    {
        switch ($menu_type)  {
			case "members": $menu_id = 2; break;	
			default: $menu_id = 1; 		
		}
		
        $module_db = Module::where('slug','=', $module)->first();
        $existence = $this->navigationItems('existence', $type, $module_db->slug, $slug, NULL, NULL, NULL, NULL, $menu_type);

        if($existence[0]!=true){

            $navigation_raw = Navigation::where('id', '=', $menu_id)->first();
            $navigation = json_decode($navigation_raw->navigation);

            $navigation[] = array(
                'text' => $text,
                'icon' => '',
                'href' => $href,
                'target' => 'self',
                'title' => '',
                'id' => $id,
                'module' => $module,
                'type' => $type,
                'slug' => $slug,
            );

            $navigation_raw->navigation = json_encode($navigation);
            $navigation_raw->save();
        }
    }

    public function navigationAddPagesCategory($text, $href, $id, $slug, $menu_type = "main")
    {
		switch ($menu_type)  {
			case "members": $menu_id = 2; break;	
			default: $menu_id = 1; 		
		}
		
        $module = 'pages';
        $type = 'category';

        $existence = $this->navigationItems('existence', $type, 'pages', $slug);

        if($existence[0]!=true){

            $navigation_raw = Navigation::where('id', '=', $menu_id)->first();
            $navigation = json_decode($navigation_raw->navigation);

            $navigation[] = array(
                'text' => $text,
                'icon' => '',
                'href' => $href,
                'target' => 'self',
                'title' => '',
                'id' => $id,
                'module' => $module,
                'type' => $type,
                'slug' => $slug,
            );

            $category = PageCategory::where('slug','=',$slug)->first();

            if(count($category->pages)>0){

                $pages = Page::where('category_id','=',$category->id)->where('parent_page_id','=',null)->orderBy('position')->get();

                foreach($pages as $page){

                    $children[] = array(
                        'text' => $page->title,
                        'icon' => '',
                        'href' => $page->url,
                        'target' => 'self',
                        'title' => '',
                        'id' => 'page-'.$page->id,
                        'module' => $module,
                        'type' => 'page',
                        'slug' => $page->slug,
                    );
                }

                end($navigation);
                $key = key($navigation);

                $navigation[$key]['children'] = $children;
            }

            $navigation_raw->navigation = json_encode($navigation);
            $navigation_raw->save();
        }
    }

    public function navigationUpdateCategoryPages($category_id, $menu_type = "main")
    {

        $category = PageCategory::where('id','=',$category_id)->first();
        $existence = $this->navigationItems('existence', 'category', 'pages', $category->slug, NULL, NULL, NULL, NULL, $menu_type);

        if($existence[0]==true){

            if(count($category->pages)>0){

                $pages = Page::where('category_id','=',$category_id)->where('parent_page_id','=',null)->orderBy('position')->get();

                foreach($pages as $page){

                    //check if page has a child page

                    $child_pages = Page::where('parent_page_id', '=', $page->id)->orderBy('position')->get();
                    $children_page_arr = null;

                    foreach ($child_pages as $child_page){

                        $children_page_arr[] = array(
                            'text' => $child_page->title,
                            'icon' => '',
                            'href' => $child_page->url,
                            'target' => 'self',
                            'title' => '',
                            'id' => 'page-'.$child_page->id,
                            'module' => 'pages',
                            'type' => 'page',
                            'slug' => $child_page->slug,
                        );
                    }

                    $children[] = array(
                        'text' => $page->title,
                        'icon' => '',
                        'href' => $page->url,
                        'target' => 'self',
                        'title' => '',
                        'id' => 'page-'.$page->id,
                        'module' => 'pages',
                        'type' => 'page',
                        'slug' => $page->slug,
                    );

                    if(isset($children_page_arr)){
                        if(count($children_page_arr)>0){
                            $children[count($children)-1]['children'] = $children_page_arr;
                        }
                    }
                }

                $this->navigationItems('add-pages', 'category', 'pages', $category->slug, null, null, null, $children, $menu_type);
            }
        }
    }

    public function navigationUpdatePagesPosition($menu_type = "main"){

        $page_categories =  PageCategory::orderBy('created_at', 'desc')->get();

        foreach ($page_categories as $page_category){

            $result = $this->navigationItems('existence', 'category', 'pages', $page_category->slug, NULL, NULL, NULL, NULL, $menu_type);

            if($result[0]){

                if(isset($result[1]->children)){

                    $position = 1;

                    foreach($result[1]->children as $child){
                        Page::where('slug','=',$child->slug)->update(['position' => $position]);
                        $position++;
                    }
                }
            }
        }

        //for child pages

        $pages_with_children = Page::has('childPages')->get();

        foreach ($pages_with_children as $page){

            $result = $this->navigationItems('existence', 'page', 'pages', $page->slug, $menu_type);

            if($result[0]){

                if(isset($result[1]->children)){

                    $position = 1;

                    foreach($result[1]->children as $child){
                        Page::where('slug','=',$child->slug)->update(['position' => $position]);
                        $position++;
                    }
                }
            }
        }

    }
}
?>