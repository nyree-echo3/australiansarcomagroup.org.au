<?php
namespace App\Helpers;

use App\Navigation;
use App\Http\Controllers\Site\NewsController;

class NavigationBuilder {


    public function build($menu_type = "main")
    {
		switch ($menu_type)  {
			case "members": $menu_id = 2; break;	
			default: $menu_id = 1; 		
		}
		
        $current_path = request()->path();

        $navigation_raw = Navigation::where('id', '=', $menu_id)->first();
        $navigation = json_decode($navigation_raw->navigation);

        $navigation_html = '';

        ///////active
        foreach($navigation as $key => $element){

            if($element->href==$current_path){
                $element->active=true;
            }

            if(isset($element->children)){
                foreach ($element->children as $key_lvl1 => $children_lvl1){

                    if($children_lvl1->href==$current_path){
                        $element->active=true;
                        $children_lvl1->active=true;
                    }

                    if(isset($children_lvl1->children)){
                        foreach($children_lvl1->children as $key_lvl2 => $children_lvl2){

                            if($children_lvl2->href==$current_path){
                                $element->active=true;
                                $children_lvl1->active=true;
                                $children_lvl2->active=true;
                            }

                            if(isset($children_lvl2->children)){
                                foreach($children_lvl2->children as $key_lvl3 => $children_lvl3){

                                    if($children_lvl3->href==$current_path){
                                        $element->active=true;
                                        $children_lvl1->active=true;
                                        $children_lvl2->active=true;
                                        $children_lvl3->active=true;
                                    }

                                    if(isset($children_lvl3->children)){
                                        foreach($children_lvl3->children as $key_lvl4 => $children_lvl4){

                                            if($children_lvl4->href==$current_path){
                                                $element->active=true;
                                                $children_lvl1->active=true;
                                                $children_lvl2->active=true;
                                                $children_lvl3->active=true;
                                                $children_lvl4->active=true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /////////////////////

            //lvl 1
            //echo $element->text."<br>";
            if(!isset($element->children)){

                $target = '';
                if($element->text=="Shop"){
                    $target = 'target="_blank"';
                }

                $navigation_html = $navigation_html.'<li class="nav-item"><a class="'.(isset($element->active) && $element->active ? "active " : "").'nav-link" href="'.url($element->href).'" '.$target.'>'.$element->text.'</a></li>';
            }

            if(isset($element->children)){

                $navigation_html = $navigation_html.'<li class="nav-item dropdown"><a class="'.(isset($element->active) && $element->active ? "active " : "").'nav-link" href="'.url($element->href).'" data-target="'.url($element->href).'" >'.$element->text.'</a>';

				if(isset($element->children)){
					$navigation_html = $navigation_html.'<ul class="dropdown-menu">';

					foreach ($element->children as $key_lvl1 => $children_lvl1){

						if(!isset($children_lvl1->children)) {
							$navigation_html = $navigation_html . '<li><a class="'.(isset($children_lvl1->active) && $children_lvl1->active ? "active " : "").'dropdown-item" href="' . url($children_lvl1->href) . '">' . $children_lvl1->text . '</a>';
							
							// Events Sub-menu
							if ($children_lvl1->href == "news/events")  {
							   $events = new NewsController();								
	                           $nav_events = $events->getNews(2); 
								
							   if (sizeof($nav_events) > 0)  {	
								   $navigation_html = $navigation_html.'<ul class="dropdown-menu nav-events">';

								   foreach($nav_events as $nav_event)  {	
									  $navigation_html = $navigation_html.'<li><a class="dropdown-item" href="'.url('news/events/' . $nav_event->slug).'">' . $nav_event->title . '</a></li>';
								   }

								   $navigation_html = $navigation_html.'</ul>';	
							   }
							}  
							// END - Events Sub-menu
							
							$navigation_html = $navigation_html . '</li>';
							
						}												
						
						//lvl2
						//echo '-'.$children_lvl1->text."<br>";
						if(isset($children_lvl1->children)){

							$navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl1->active) && $children_lvl1->active ? "active " : "").'dropdown-item" href="'.url($children_lvl1->href).'" data-target="'.url($children_lvl1->href).'">'.$children_lvl1->text.'</a>';
							$navigation_html = $navigation_html.'<ul class="dropdown-menu">';							
							
							foreach($children_lvl1->children as $key_lvl2 => $children_lvl2){

								if(!isset($children_lvl2->children)){
									$navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl2->active) && $children_lvl2->active ? "active " : "").'dropdown-item" href="'.url($children_lvl2->href).'">'.$children_lvl2->text.'</a></li>';
								}

								//lvl3
								//echo '--'.$children_lvl2->text."<br>";
								if(isset($children_lvl2->children)){

									$navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl2->active) && $children_lvl2->active ? "active " : "").'dropdown-item" href="'.url($children_lvl2->href).'" data-target="'.url($children_lvl2->href).'">'.$children_lvl2->text.'</a>';
									$navigation_html = $navigation_html.'<ul class="dropdown-menu">';

									foreach($children_lvl2->children as $key_lvl3 => $children_lvl3){

										if(!isset($children_lvl3->children)){
											$navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl3->active) && $children_lvl3->active ? "active " : "").'dropdown-item" href="'.url($children_lvl3->href).'">'.$children_lvl3->text.'</a></li>';
										}

										//lvl4
										//echo '---'.$children_lvl3->text."<br>";
										if(isset($children_lvl3->children)){


											$navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl3->active) && $children_lvl3->active ? "active " : "").'dropdown-item" href="'.url($children_lvl3->href).'" data-target="'.url($children_lvl3->href).'">'.$children_lvl3->text.'</a>';
											$navigation_html = $navigation_html.'<ul class="dropdown-menu">';

											foreach($children_lvl3->children as $key_lvl4 => $children_lvl4){

												//lvl5
												//echo '-----'.$children_lvl4->text."<br>";
												$navigation_html = $navigation_html.'<li><a class="'.(isset($children_lvl4->active) && $children_lvl4->active ? "active " : "").'nav-link" href="'.url($children_lvl4->href).'">'.$children_lvl4->text.'</a></li>';

											}

											$navigation_html = $navigation_html.'</ul></li>';
										}
									}

								$navigation_html = $navigation_html.'</ul></li>';
								}
							}

						$navigation_html = $navigation_html.'</ul></li>';
						}

					}
					$navigation_html = $navigation_html.'</ul>';
				}
				$navigation_html = $navigation_html.'</li>';
            }
        }

        return $navigation_html;
    }


    public function buildSideNavigation($category_href = null, $menu_type = "main")
    {

        if($category_href!=null) {
            $href = $category_href;
        }else{
            $href = request()->path();
        }

        $side_nav = (new NavigationHelper())->navigationItems('side-nav', null, null, null, $href, $menu_type);
        if(is_array($side_nav)){
            if(!$side_nav[0]){
                return null;
            }
        }else if($side_nav==null){
            return null;
        }

        $navigation_html = '';

        foreach($side_nav as $key => $element){

            //lvl 1
            //echo $element->text."<br>";
            if(!isset($element->children)){

                $style = '';
                if($element->href==$href){
                    $style = 'active ';
                }

                $navigation_html = $navigation_html.'<li class="'.$style.'list-group-item"><a class="navsidebar" href="'.url($element->href).'">'.$element->text.'</a></li>';
            }

            if(isset($element->children)){

                $style = '';
                if($element->href==$href){
                    $style = 'active ';
                }

                $navigation_html = $navigation_html.'<li class="'.$style.'list-group-item"><a class="navsidebar" href="'.url($element->href).'">'.$element->text.'</a>';
                $navigation_html = $navigation_html.'<ol class="list-group list-unstyled">';

                foreach ($element->children as $key_lvl1 => $children_lvl1){

                    if(!isset($children_lvl1->children)) {

                        $style = '';
                        if($children_lvl1->href==$href){
                            $style = 'active ';
                        }

                        $navigation_html = $navigation_html . '<li class="'.$style.'list-group-item"><a class="navsidebar" href="' . url($children_lvl1->href) . '">' . $children_lvl1->text . '</a></li>';
                    }
                    //lvl2
                    //echo '-'.$children_lvl1->text."<br>";
                    if(isset($children_lvl1->children)){

                        $style = '';
                        if($children_lvl1->href==$href){
                            $style = 'active ';
                        }

                        $navigation_html = $navigation_html.'<li class="'.$style.'list-group-item"><a class="navsidebar" href="'.url($children_lvl1->href).'">'.$children_lvl1->text.'</a>';
                        $navigation_html = $navigation_html.'<ol class="list-group list-unstyled">';


                        foreach($children_lvl1->children as $key_lvl2 => $children_lvl2){

                            if(!isset($children_lvl2->children)){

                                $style = '';
                                if($children_lvl2->href==$href){
                                    $style = 'active ';
                                }

                                $navigation_html = $navigation_html.'<li class="'.$style.'list-group-item"><a class="navsidebar" href="'.url($children_lvl2->href).'">'.$children_lvl2->text.'</a></li>';
                            }

                            //lvl3
                            //echo '--'.$children_lvl2->text."<br>";
                            if(isset($children_lvl2->children)){

                                $style = '';
                                if($children_lvl2->href==$href){
                                    $style = 'active ';
                                }

                                $navigation_html = $navigation_html.'<li class="'.$style.'list-group-item"><a class="navsidebar" href="'.url($children_lvl2->href).'">'.$children_lvl2->text.'</a>';
                                $navigation_html = $navigation_html.'<ol class="list-group list-unstyled">';

                                foreach($children_lvl2->children as $key_lvl3 => $children_lvl3){

                                    if(!isset($children_lvl3->children)){

                                        $style = '';
                                        if($children_lvl3->href==$href){
                                            $style = 'active ';
                                        }

                                        $navigation_html = $navigation_html.'<li class="'.$style.'list-group-item"><a class="dropdown-item" href="'.url($children_lvl3->href).'">'.$children_lvl3->text.'</a></li>';
                                    }

                                    //lvl4
                                    //echo '---'.$children_lvl3->text."<br>";
                                    if(isset($children_lvl3->children)){

                                        $style = '';
                                        if($children_lvl3->href==$href){
                                            $style = 'active ';
                                        }

                                        $navigation_html = $navigation_html.'<li class="'.$style.'list-group-item"><a class="dropdown-item" href="'.url($children_lvl3->href).'">'.$children_lvl3->text.'</a>';
                                        $navigation_html = $navigation_html.'<ol class="list-group list-unstyled">';

                                        foreach($children_lvl3->children as $key_lvl4 => $children_lvl4){

                                            $style = '';
                                            if($children_lvl4->href==$href){
                                                $style = 'active ';
                                            }

                                            //lvl5
                                            //echo '-----'.$children_lvl4->text."<br>";
                                            $navigation_html = $navigation_html.'<li class="'.$style.'list-group-item"><a href="'.url($children_lvl4->href).'">'.$children_lvl4->text.'</a></li>';

                                        }

                                        $navigation_html = $navigation_html.'</ol></li>';
                                    }
                                }

                                $navigation_html = $navigation_html.'</ol></li>';
                            }
                        }

                        $navigation_html = $navigation_html.'</ol></li>';
                    }

                }

                $navigation_html = $navigation_html.'</ol></li>';
            }
        }

        return $navigation_html;
    }
}
?>