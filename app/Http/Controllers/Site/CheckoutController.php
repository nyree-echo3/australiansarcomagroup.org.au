<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\CheckoutRequest;
use Konekt\Address\Models\CountryProxy;
use Vanilo\Cart\Facades\Cart;
use Vanilo\Checkout\Facades\Checkout;
use Vanilo\Order\Contracts\OrderFactory;

use Eway\Rapid;
use Illuminate\Support\Facades\Mail;

use App\Setting;
use App\Order;
use App\State;

use App\Mail\OrderMessageAdmin;
use App\Mail\OrderMessageUser;

class CheckoutController extends Controller
{
    public function show()
    {
        $checkout = false;

        if (Cart::isNotEmpty()) {
            $checkout = Checkout::getFacadeRoot();
            if ($old = old()) {
                $checkout->update($old);
            }

            $checkout->setCart(Cart::model());
        }
		
        return view('site/checkout/show', [
            'checkout'  => $checkout,
            'countries' => CountryProxy::all(),
			'states' => State::all()
        ]);
    }
	
    public function submit(CheckoutRequest $request, OrderFactory $orderFactory)
    {
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
        $checkout = Checkout::getFacadeRoot();
        $checkout->update($request->all());
        $checkout->setCart(Cart::model());

        $order = $orderFactory->createFromCheckout($checkout);	
		//dd($order);
		//dump($order->items);
       
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;				
		
		 // Process Payment
		 // ***************
		 $apiKey = $_ENV['EWAY_API_KEY'];
         $apiPassword = $_ENV['EWAY_API_PASSWORD'];
         $apiEndpoint = $_ENV['EWAY_API_MODE'];
		
         $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);
		
		 // Order Items
		 $items = array();
		 $arrSize = sizeof($items);
		 $orderTotal = 0;
		
		 foreach ($order->items as $item)  {
			 $items[$arrSize]['SKU'] = $item['product_id'];
			 $items[$arrSize]['Description'] = $item['name'];
			 $items[$arrSize]['Quantity'] = $item['quantity'];
			 $items[$arrSize]['UnitCost'] = $item['price'] * 100;			 
			 
			 $arrSize++;
			 
			 $orderTotal += $item['quantity'] * ($item['price'] * 100);
		 }
		 
		 // States
		 $customerState = State::where('id', '=', $order->billpayer->address->province_id)->first();
		 $shippingState = State::where('id', '=', $order->shippingAddress->province_id)->first();
		
		 // Payment Transaction
		 $transaction = [
			'Customer' => [
				'Reference' => $order->billpayer->id,				
				'FirstName' => $order->billpayer->firstname,
				'LastName' => $order->billpayer->lastname,								
				'Street1' => $order->billpayer->address->address,				
				'City' => $order->billpayer->address->city,
				'State' => $customerState->state,
				'PostalCode' => $order->billpayer->address->postalcode,
				'Country' => $order->billpayer->address->country_id,
			    'Email' => $order->billpayer->email,	
				//'Phone' => '09 889 0986',
				//'Mobile' => '09 889 6542',							
			],
			'ShippingAddress' => [
				'ShippingMethod' => \Eway\Rapid\Enum\ShippingMethod::NEXT_DAY,
				'FirstName' => $order->shippingAddress->name,
				//'LastName' => $order->billpayer->lastname,
				'Street1' => $order->shippingAddress->address,			
				'City' => $order->shippingAddress->city,
				'State' => $shippingState->state,
				'Country' => $order->shippingAddress->country_id,
				'PostalCode' => $order->shippingAddress->postalcode,
				//'Phone' => '09 889 0986',
			],
			'Items' => $items,
			'Payment' => [
				'TotalAmount' => $orderTotal,
				'InvoiceNumber' => $order->getNumber(),
				'InvoiceDescription' => $company_name . " Purchase",
				//'InvoiceReference' => '513456',
				'CurrencyCode' => 'AUD',
			],
			'RedirectUrl' => url('/') . "/checkout/complete/" . $order->id,
			'CancelUrl' =>  url('/') . "/checkout/cancel/" . $order->id,			
			'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
			'Capture' => true,
			//'LogoUrl' => url('/') . "/images/site/logo.png",  // MUST START WITH HTTPS
			'HeaderText' => $company_name,
			'Language' => 'EN',
			'CustomerReadOnly' => true
		];
        //dd ($transaction);
		
		$response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::RESPONSIVE_SHARED, $transaction);
		//dd($response);
		
		if (!$response->getErrors()) {
			// Redirect to the Responsive Shared Page
			header('Location: '.$response->SharedPaymentUrl);
			die();
		} else {
			$errorMessage = "<span class='alert-error'>";
			//foreach ($response->getErrors() as $error) {
			//	$errorMessage .= "<i class='fas fa-exclamation'></i> Error: ".\Eway\Rapid::getMessage($error)."<br>";
			//}
			$errorMessage .= "<i class='fas fa-exclamation'></i> eWAY payment error.  Please <a href='" . url('/') . "'>contact</a> website owner.";
			$errorMessage .= "</span>";
			
			flash($errorMessage);
			return redirect()->route('checkout.show');
		}
		
		
	}
	
	public function complete($orderFactory) {
		// Process Payment Return
		// **********************
		$apiKey = $_ENV['EWAY_API_KEY'];
        $apiPassword = $_ENV['EWAY_API_PASSWORD'];
        $apiEndpoint = $_ENV['EWAY_API_MODE'];		
		
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);    
		
		$ewayAccessCode = $_GET['AccessCode'];  
		
		$response = $client->queryTransaction($ewayAccessCode);
		//dd($response);

		// Update Order
		$order = Order::where('id','=',$orderFactory->id)->first();              
	    $order->paid_status = "completed";
		$order->paid_transaction_number = $response->Transactions[0]->TransactionID;
		$order->paid_transaction_result = ($response->Transactions[0]->ResponseCode == "00" ? "Successful" : "Error");
        $order->save();
		
		if ($response->Transactions[0]->ResponseCode == "00")  {
           flash("Thanks for your order.  Your order number is: " . $orderFactory->getNumber() . ".<br>We will send you an email confirmation of your order.");
		} else  {
		   flash("<span class='alert-error'><i class='fas fa-exclamation'></i> Your payment was not successful.  Your unpaid order number is: " . $orderFactory->getNumber() . ".<br>We will send you an email confirmation of your unpaid order.</span>");	
		}
		
		// Send Emails
		// ***********
		// Get Website Owner Email
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Get Order Details		
		$order = Order::where('id', '=', $orderFactory->id)->with("billpayers")->with("shiptoaddresses")->with("items")->first();		
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new OrderMessageAdmin($order));				
		
		// Email User	
        Mail::to($order->billpayers->email)->send(new OrderMessageUser($order));        		
		
		// Return to Shop and Clear Shopping Cart
		// **************************************
        Cart::destroy();
        return redirect()->route('products.list');	
	}
	
	public function cancel($orderFactory) {
		$order = Order::where('id','=',$orderFactory->id)->first();              
	    $order->paid_status = "cancelled";
        $order->save();

        if (Cart::isNotEmpty()) {
            $checkout = Checkout::getFacadeRoot();
            if ($old = old()) {
                $checkout->update($old);
            }

            $checkout->setCart(Cart::model());
        }
        flash("<span class='alert-error'><i class='fas fa-exclamation'></i> You cancelled your payment.  Your unpaid order number is: " . $orderFactory->getNumber() . ".</span>");
		Cart::destroy();

		// Send Emails
		// ***********
		// Get Website Owner Email
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Get Order Details		
		$order = Order::where('id', '=', $orderFactory->id)->with("billpayers")->with("shiptoaddresses")->with("items")->first();		
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new OrderMessageAdmin($order));				
				
		
        return redirect()->route('products.list');		        
	}

}
