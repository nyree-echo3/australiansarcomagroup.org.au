<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Mail\TestMessage;
use Illuminate\Http\Request;
use App\Setting;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index(){ 
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;

		if (env('APP_MODE') == "dev")  {
		   return view('site/holding', array(         
			'company_name' => $company_name,			
           ));	
		   
		} else  {
    	   return view('site/index');	
		}

    }
	
	public function indexDev(){ 		
		return view('site/index');			
    }
	
	public function styleGuide(){

    	return view('site/style-guide');

    }

    public function emailTest()
    {
        Mail::to('bilgin@echo3.com.au')->send(new TestMessage());
    }

}
