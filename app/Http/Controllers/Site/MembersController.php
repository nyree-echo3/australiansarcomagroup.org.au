<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Mail\MembersRenewMessageAdmin;
use App\Mail\MembersRenewMessageUser;
use App\MemberPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

use App\Setting;
use App\Member;
use App\MemberType;
use App\Module;
use App\MemberQualification;
use App\MemberAward;

use App\Mail\MembersMessageAdmin;
use App\Mail\MembersMessageUser;
use Srmklive\PayPal\Services\ExpressCheckout;


class MembersController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:member');
    }
	
	 public function index()
    {
		$module = Module::where('slug', '=', "members")->first();
		
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		
		// Company Name
        $home_members_intro_text = Setting::where('key', '=', 'home-members-intro-text')->first();
		$home_members_intro_text = $home_members_intro_text->value;

        return view('site/members/home', array(
			'module' => $module,
            'company_name' => $company_name,           			
			'home_members_intro_text' => $home_members_intro_text,   
        ));
    }	
	
	public function changeDetails()
    {
		$module = Module::where('slug', '=', "members")->first();
		
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		        		
		$member = Auth::guard('member')->user();		
		$qualifications = MemberQualification::where('member_id', '=', $member->id)->get();
		$awards = MemberAward::where('member_id', '=', $member->id)->get();
		
        return view('site/members/change-details', array(
			'module' => $module,
			'company_name' => $company_name,        
            'member' => $member, 
			'qualifications' => $qualifications,
			'awards' => $awards,
        ));
    }
	
	public function saveDetails (Request $request)
    {				
		// Logged in Member
		$member = Auth::guard('member')->user();
		
		// Field Validation
	    $rules = array(                                  			
            'title' => 'required',
			'firstName' => 'required',
            'lastName' => 'required',
			'dob' => 'required',
			'address1' => 'required',
			'suburb' => 'required',
            'state' => 'required',
            'postcode' => 'required',
			'country' => 'required',
            'phoneMobile' => 'required',
            'email' => 'required|string|email|max:255|unique_update:members,'.$member->id,
            'occupation' => 'required',
			'companyName' => 'required',
            'employmentDateCommencement' => 'required',
			'employmentAddress1' => 'required',
			'employmentSuburb' => 'required',
            'employmentState' => 'required',
            'employmentPostcode' => 'required',
			'employmentCountry' => 'required',
			'employmentEmail' => 'required|string|email|max:255',	
        );

        $messages = [           						
            'title.required' => 'Please select title',
			'firstName.required' => 'Please enter first name',
            'lastName.required' => 'Please enter last name',
			'dob.required' => 'Please enter date of birth',
			'address1.required' => 'Please enter address line 1',
            'suburb.required' => 'Please enter suburb',
            'state.required' => 'Please enter state',
            'postcode.required' => 'Please enter postcode',
			'country.required' => 'Please enter country',
            'phoneMobile.required' => 'Please enter mobile number',
            'email.required' => 'Please enter email',
            'email.unique_store' => 'We already have a member account using the provided email address',
            'occupation.required' => 'Please enter position',
			'companyName.required' => 'Please enter employer',
			'employmentDateCommencement.required' => 'Please enter employment date of commencement',
			'employmentAddress1.required' => 'Please enter employment address line 1',
			'employmentSuburb.required' => 'Please enter suburb',
            'employmentState.required' => 'Please enter state',
            'employmentPostcode.required' => 'Please enter postcode',
			'employmentCountry.required' => 'Please enter country',
			'employmentEmail.required' => 'Please enter email',		
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
			flash("<span class='alert-error'><i class='fas fa-exclamation'></i> There was a problem saving your details.  Please try again.</span>");	
            return redirect('members-portal/change-details')->withErrors($validator)->withInput();
        }
					                		
        $member->title = $request->title;
		$member->firstName = $request->firstName;
        $member->lastName = $request->lastName;
		$member->formerName = $request->formerName;
		$member->dob = date('Y-m-d', strtotime($request->dob));
        $member->address1 = $request->address1;
        $member->address2 = $request->address2;
        $member->suburb = $request->suburb;
        $member->state = $request->state;
        $member->postcode = $request->postcode;
		$member->country = $request->country;
		$member->phoneLandline = $request->phoneLandline;
        $member->phoneMobile = $request->phoneMobile;
        $member->email = $request->email;
		$member->occupation = $request->occupation;
		$member->companyName = $request->companyName;
		$member->employmentDateCommencement = date('Y-m-d', strtotime($request->employmentDateCommencement));
		$member->employmentAddress1 = $request->employmentAddress1;
        $member->employmentAddress2 = $request->employmentAddress2;
        $member->employmentSuburb = $request->employmentSuburb;
        $member->employmentState = $request->employmentState;
        $member->employmentPostcode = $request->employmentPostcode;
		$member->employmentCountry = $request->employmentCountry;
		$member->employmentPhoneNumber = $request->employmentPhoneNumber;
		$member->employmentEmail = $request->employmentEmail;
		$member->otherExperience = $request->otherExperience;
		$member->cvFile = $request->cv_current;
        $member->save();	
		
		// Save Qualifications
        // *******************
		if ($request->hidQualifications > 0)  {
		   for ($i=1; $i <= $request->hidQualifications; $i++)  {
			  if (isset($request['qualificationId' . $i])) {
			     $memberQualification = MemberQualification::where('id','=', $request['qualificationId' . $i])->first();
			  } else  {
			     $memberQualification = new MemberQualification();	
			     $memberQualification->member_id = $member->id;	
			  }
			   		      
			  $memberQualification->qualification = $request['qualification' . $i]; 
			  $memberQualification->institution = $request['institution' . $i]; 
			  $memberQualification->graduationYear = $request['graduationYear' . $i]; 
			  $memberQualification->save(); 
		   }
		}
		
		// Save Award
        // **********
		if ($request->hidAwards > 0)  {
		   for ($i=1; $i <= $request->hidAwards; $i++)  {			   
		      if (isset($request['awardId' . $i])) {
			     $memberAward = MemberAward::where('id','=', $request['awardId' . $i])->first();
			  } else  {
			     $memberAward = new MemberAward();	
			     $memberAward->member_id = $member->id;		
			  }
			   
			  $memberAward->award = $request['awards' . $i]; 			  
			  $memberAward->save(); 
		   }
		}
		
		// Save File
		// *******************
		$cv_file = $request->file('cv_file');
		if (isset($cv_file)) {
			$filename = $cv_file->getClientOriginalName();
			Storage::putFileAs('members/' . $member->id, $cv_file, $filename);

			$member->cvFile = $filename;
			$member->save();
		}
		
		
		flash("<span class='alert-success'><i class='fas fa-check'></i> Your details were saved successfully.</span>");	
  	    return \Redirect::to('members-portal/change-details');
	}
	
	public function changePassword()
    {
		$module = Module::where('slug', '=', "members")->first();
		
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;		        				
		
        return view('site/members/change-password', array(
			'module' => $module,
			'company_name' => $company_name,                    
        ));
    }
	
	public function savePassword (Request $request)
    {				
		// Logged in Member
		$member = Auth::guard('member')->user();
		
		if (!(Hash::check($request->get('passwordCurrent'), $member->password))) {           
			flash("<span class='alert-error'><i class='fas fa-exclamation'></i> Your current password does not matches with the password you provided. Please try again.</span>");	
            return redirect()->back()->withInput();
        }
		
		if(strcmp($request->get('passwordCurrent'), $request->get('password')) == 0){
            flash("<span class='alert-error'><i class='fas fa-exclamation'></i> The new password cannot be same as your current password. Please choose a different password.</span>");	
            return redirect()->back()->withInput();
        }
				
        $member->password = Hash::make($request->get('password'));
        $member->save();								 
		
		flash("<span class='alert-success'><i class='fas fa-check'></i> Your password was saved successfully.</span>");	
  	    return \Redirect::to('members-portal/change-password');
	
	}

    public function renew()
    {

		$module = Module::where('slug', '=', "members")->first();
		
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        // Direct Deposit text
        $payment_direct_deposit_members = Setting::where('key', '=', 'payment-direct-deposit-members')->first();

        // Logged in Member
        $member = Auth::guard('member')->user();

        return view('site/members/renew', array(
			'module' => $module,
            'member' => $member,
            'company_name' => $company_name,
            'payment_direct_deposit_members' => $payment_direct_deposit_members->value,
            'has_flash_message' => session()->has('flash_notification')
        ));
	}

    public function renewProcess(Request $request)
    {

        // Google Recaptcha Validation
        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself!',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself!'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('members-portal/renew')->withErrors($validator)->withInput();
        }

        // Logged in Member
        $member = Auth::guard('member')->user();

        if($member->type->price == 0){

            // New Expire Date
            $expire_date = Carbon::parse($member->dateExpire);
            if($expire_date->isPast()){
                $new_expire_date = Carbon::now()->addYear()->format('Y-m-d');
            }else{
                $new_expire_date = $expire_date->addYear()->format('Y-m-d');
            }

            $member->dateExpire = $new_expire_date;
            $member->payment_status = "completed";
            $member->status = 'active';
            $member->save();

            return $this->redirectRenewPage('success', 'The operation was successful! Your membership has been renewed and your expiry date updated.');
        }

        $pending_payment = $member->payments()->where('payment_status','=','pending')->exists();

        if ($pending_payment){
            return $this->redirectRenewPage('error', 'You have a pending payment. Please try again later.');
        }

        // Process Payment
        // ***************

        if ($request->payment_method == 'bank-deposit') {

            $member_payment = new MemberPayment();
            $member_payment->member_id = $member->id;
            $member_payment->amount = $member->type->price;
            $member_payment->payment_type = 'renew';
            $member_payment->payment_method = 'bank-deposit';
            $member_payment->payment_status = 'pending';
            $member_payment->save();

            $this->sendRenewEmail($member, $member_payment);

            return $this->redirectRenewPage('success', 'The operation was successful! Your membership has been renewed and your expiry date will be updated after your payment has been approved.');
        }

        if ($request->payment_method == 'paypal') {

            $provider = new ExpressCheckout;

            $data = $this->generatePaymentData($member);
            $response = $provider->setExpressCheckout($data);

            if ($response['ACK'] != 'Success' && $response['ACK'] != 'SuccessWithWarning') {
                return $this->redirectRenewPage('error', 'There was an error on payment. Please try again!');
            }

            $member_payment = new MemberPayment();
            $member_payment->member_id = $member->id;
            $member_payment->amount = $member->type->price;
            $member_payment->payment_type = 'renew';
            $member_payment->payment_method = 'paypal';
            $member_payment->payment_status = 'pending';
            $member_payment->paypal_token = $response['TOKEN'];
            $member_payment->save();

            return redirect($response['paypal_link']);
        }
	}

    public function paypalSuccess(Request $request)
    {

        $member_payment = MemberPayment::where('paypal_token','=',$request->token)->first();
        $member = Member::where('id','=',$member_payment->member_id)->first();

        $provider = new ExpressCheckout;
        $data = $this->generatePaymentData($member);

        $payment_response = $provider->doExpressCheckoutPayment($data, $member_payment->paypal_token, $request->PayerID);

        if ($payment_response['ACK'] != 'Success') {
            return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again!');
        }

        $checkout_details = $provider->getExpressCheckoutDetails($member_payment->paypal_token);

        if ($checkout_details['CHECKOUTSTATUS'] != 'PaymentActionCompleted') {
            return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again!');
        }

        $member_payment->payment_status = "completed";
        $member_payment->payment_transaction_number = $checkout_details['TRANSACTIONID'];
        $member_payment->paypal_payer_id = $request->PayerID;
        $member_payment->is_processed = "true";
        $member_payment->save();

        // New Expire Date
        $expire_date = Carbon::parse($member->dateExpire);
        if($expire_date->isPast()){
            $new_expire_date = Carbon::now()->addYear()->format('Y-m-d');
        }else{
            $new_expire_date = $expire_date->addYear()->format('Y-m-d');
        }

        $member->dateExpire = $new_expire_date;
        $member->payment_status = "completed";
        $member->status = 'active';
        $member->save();

        $this->sendRenewEmail($member, $member_payment);

        return $this->redirectRenewPage('success', 'The payment was successful! Your account has been renewed!');
    }

    public function paypalCancel(Request $request)
    {

        $payment = MemberPayment::where('paypal_token', '=', $request->token)->first();

        if ($payment) {
            $payment->delete();
            return $this->redirectRenewPage('error', 'Your payment has been cancelled!');
        }

        return $this->redirectRenewPage('error', 'Operation was not successful!');
    }

    private function generatePaymentData($member)
    {

        $renew_count = $member->payments->where('payment_method','=','paypal')
            ->where('payment_type','=','renew')
            ->count();

        $data = [];
        $data['items'] = [[
            'name' => 'ANZSA Membership Renew - '.$member->type->name.' - '.($renew_count+1),
            'price' => $member->type->price,
            'qty' => 1
        ]];

        $data['invoice_id'] = 'member-renew-'.$member->id.'-'.time();
        $data['invoice_description'] = "Membership Renew #" . $member->id . " Invoice (".($renew_count+1).")";
        $data['return_url'] = url('/members-portal/paypal-success');
        $data['cancel_url'] = url('/members-portal/paypal-cancel');
        $data['total'] = $member->type->price;

        return $data;
    }

    private function redirectRenewPage($type, $message)
    {

        if ($type == 'success') {
            flash('<i class="fas fa-exclamation"></i> '.$message)->success();
        }

        if ($type == 'error') {
            flash('<i class="fas fa-exclamation"></i> '.$message)->error();
        }

        return redirect('members-portal/renew');
    }

    public function payments()
    {
		$module = Module::where('slug', '=', "members")->first();
		
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        // Logged in Member
        $payments = Auth::guard('member')->user()->payments;

        return view('site/members/payments', array(
			'module' => $module,
            'company_name' => $company_name,
            'payments' => $payments,
        ));

    }

    public function sendRenewEmail($member, $member_payment)
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new MembersRenewMessageAdmin($member, $member_payment));

        // Email User
        Mail::to($member->email)->send(new MembersRenewMessageUser($member, $member_payment));
    }
}
