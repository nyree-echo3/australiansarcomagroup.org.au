<?php

namespace App\Http\Controllers\Site;

use App\MemberPayment;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Storage;

use App\Setting;
use App\Member;
use App\MemberType;
use App\MemberQualification;
use App\MemberAward;
use App\Mail\MembersMessageAdmin;
use App\Mail\MembersMessageUser;
use App\Mail\MembersActivateMessageAdmin;
use App\Mail\MembersActivateMessageUser;
use Srmklive\PayPal\Services\ExpressCheckout;
use Auth;

class RegisterController extends Controller
{
    protected $redirectTo = '/members-portal';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        // Direct Deposit text
        $payment_direct_deposit_members = Setting::where('key', '=', 'payment-direct-deposit-members')->first();

        $member_types = MemberType::where('status', '=', 'active')->orderBy('position', 'desc')->get();

        return view('site/members/register', array(
            'company_name' => $company_name,
            'member_types' => $member_types,
            'payment_direct_deposit_members' => $payment_direct_deposit_members->value,
        ));
    }

    public function store(Request $request)
    {

        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        // Google Recaptcha Validation
        //$rules = array(
        //    'g-recaptcha-response' => 'required|recaptcha'
        //);

        //$messages = [
        //    'g-recaptcha-response.required' => 'Please verify yourself',
        //    'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        //];

        //$validator = Validator::make($request->all(), $rules, $messages);
        //if ($validator->fails()) {
        //    return redirect('apply')->withErrors($validator)->withInput();
        //}

        // Field Validation
        $rules = array(
            'membershipType' => 'required',
            'title' => 'required',
			'firstName' => 'required',
            'lastName' => 'required',
			'dob' => 'required',
			'address1' => 'required',
			'suburb' => 'required',
            'state' => 'required',
            'postcode' => 'required',
			'country' => 'required',
            'phoneMobile' => 'required',
            'email' => 'required|string|email|max:255|unique_store:members',
            'occupation' => 'required',
			'companyName' => 'required',
            'employmentDateCommencement' => 'required',
			'employmentAddress1' => 'required',
			'employmentSuburb' => 'required',
            'employmentState' => 'required',
            'employmentPostcode' => 'required',
			'employmentCountry' => 'required',
			'employmentEmail' => 'required|string|email|max:255',			
            'confirmation' => 'required',
        );

        $messages = [
            'membershipType.required' => 'Please select your membership',
            'title.required' => 'Please select title',
			'firstName.required' => 'Please enter first name',
            'lastName.required' => 'Please enter last name',
			'dob.required' => 'Please enter date of birth',
			'address1.required' => 'Please enter address line 1',
            'suburb.required' => 'Please enter suburb',
            'state.required' => 'Please enter state',
            'postcode.required' => 'Please enter postcode',
			'country.required' => 'Please enter country',
            'phoneMobile.required' => 'Please enter mobile number',
            'email.required' => 'Please enter email',
            'email.unique_store' => 'We already have a member account using the provided email address',
            'occupation.required' => 'Please enter position',
			'companyName.required' => 'Please enter employer',
			'employmentDateCommencement.required' => 'Please enter employment date of commencement',
			'employmentAddress1.required' => 'Please enter employment address line 1',
			'employmentSuburb.required' => 'Please enter suburb',
            'employmentState.required' => 'Please enter state',
            'employmentPostcode.required' => 'Please enter postcode',
			'employmentCountry.required' => 'Please enter country',
			'employmentEmail.required' => 'Please enter email',
            'confirmation.required' => 'Please agree to terms & conditions',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('apply')->withErrors($validator)->withInput();
        }				
		
        $member = new Member();
        $member->type_id = $request->membershipType;
        $member->title = $request->title;
		$member->firstName = $request->firstName;
        $member->lastName = $request->lastName;
		$member->formerName = $request->formerName;
		$member->dob = date('Y-m-d', strtotime(str_replace("/", "-", $request->dob)));
        $member->address1 = $request->address1;
        $member->address2 = $request->address2;
        $member->suburb = $request->suburb;
        $member->state = $request->state;
        $member->postcode = $request->postcode;
		$member->country = $request->country;
		$member->phoneLandline = $request->phoneLandline;
        $member->phoneMobile = $request->phoneMobile;
        $member->email = $request->email;
		$member->occupation = $request->occupation;
		$member->companyName = $request->companyName;
		$member->employmentDateCommencement = date('Y-m-d', strtotime(str_replace("/", "-", $request->employmentDateCommencement)));
		$member->employmentAddress1 = $request->employmentAddress1;
        $member->employmentAddress2 = $request->employmentAddress2;
        $member->employmentSuburb = $request->employmentSuburb;
        $member->employmentState = $request->employmentState;
        $member->employmentPostcode = $request->employmentPostcode;
		$member->employmentCountry = $request->employmentCountry;
		$member->employmentPhoneNumber = $request->employmentPhoneNumber;
		$member->employmentEmail = $request->employmentEmail;
		$member->otherExperience = $request->otherExperience;
		//$member->cvFile = $filename;
		
        $member->dateJoin = date('Y-m-d');
        $member->dateExpire = date('Y-m-d', strtotime('+1 years'));
		$member->password = Hash::make(date('Ymdhis'));
		$member->payment_status = "pending";
        $member->status = 'passive';        

        $member->save();
		
		// Save File
		// *******************
		$cv_file = $request->file('cv_file');
		if (isset($cv_file)) {
			$filename = $cv_file->getClientOriginalName();
			Storage::putFileAs('members/' . $member->id, $cv_file, $filename);

			$member->cvFile = $filename;
			$member->save();
		}
		
		// Save Qualifications
        // *******************
		if ($request->hidQualifications > 0)  {
		   for ($i=1; $i <= $request->hidQualifications; $i++)  {			   
		      $memberQualification = new MemberQualification();
			  $memberQualification->member_id = $member->id;
			  $memberQualification->qualification = $request['qualification' . $i]; 
			  $memberQualification->institution = $request['institution' . $i]; 
			  $memberQualification->graduationYear = $request['graduationYear' . $i]; 
			  $memberQualification->save(); 
		   }
		}
		
		// Save Award
        // **********
		if ($request->hidAwards > 0)  {
		   for ($i=1; $i <= $request->hidAwards; $i++)  {			   
		      $memberAward = new MemberAward();
			  $memberAward->member_id = $member->id;
			  $memberAward->award = $request['awards' . $i]; 			  
			  $memberAward->save(); 
		   }
		}
		     
		$this->sendWelcomeEmail($member);
		
        return $this->redirectInfoPage('success', '<p>Thank you, your ANZSA Membership Application has been received and will be considered by our Board of Directors as soon as possible.</p><p>We will be in touch with you to inform you of your membership status.</p>');        
    }
	
	public function accountSetup($member_id = "")
    {	
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        // Direct Deposit text
        $payment_direct_deposit_members = Setting::where('key', '=', 'payment-direct-deposit-members')->first();

        $member = Member::where('id','=',$member_id)->first();

        $member_types = MemberType::where('status', '=', 'active')->orderBy('position', 'desc')->get();

        return view('site/members/account-setup', array(
            'company_name' => $company_name,
			'member' => $member,
			'member_id' => $member_id,
            'member_types' => $member_types,
            'payment_direct_deposit_members' => $payment_direct_deposit_members->value,
        ));
    }
	
	public function accountSetupStore(Request $request)
    {		
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        // Google Recaptcha Validation
        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('register/' . $request->member_id)->withErrors($validator)->withInput();
        }

        $member = Member::where('id','=',$request->member_id)->first();

        // Field Validation
        $rules = array(
            'password' => 'required',
        );

        $messages = [
            'password.required' => 'Please enter password',
            'payment_method.required' => 'Please select payment method',
        ];

        if($member->type->price>0){
            $rules['payment_method'] = 'required';
            $messages['payment_method.required'] = 'Please select payment method';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('register/' . $request->member_id)->withErrors($validator)->withInput();
        }

		$member->password = Hash::make($request->password);

		if($member->type->price==0){
            $member->payment_status = "completed";
            $member->status = 'active';
        }else{
            $member->payment_method = $request->payment_method;
        }

        $member->save();

        if($member->type->price==0){
            return $this->redirectInfoPage('success', 'Thank you for setting up your membership account! <a href="' . url('/login') . '">You can now log into your account here</a>.');
        }

		// Process Payment
        // ***************
        if ($request->payment_method == 'bank-deposit') {

            $member_payment = new MemberPayment();
            $member_payment->member_id = $member->id;
            $member_payment->amount = $member->type->price;
            $member_payment->payment_type = 'initial';
            $member_payment->payment_method = 'bank-deposit';
            $member_payment->payment_status = 'pending';
            $member_payment->save();

            $this->sendActivateEmailAdminOnly($member, $member_payment);

            return $this->redirectInfoPage('success', 'Thank you for setting up your membership account! Your account will be activated once your payment is received.');
        }

        if ($request->payment_method == 'paypal') {

            $provider = new ExpressCheckout;

            $data = $this->generatePaymentData($member);
            $response = $provider->setExpressCheckout($data);

            if ($response['ACK'] == 'Failure') {

                //$member->delete(); #4271
                return $this->redirectInfoPage('error', 'There was an error on payment. Please try again!');
            }

            $member_payment = new MemberPayment();
            $member_payment->member_id = $member->id;
            $member_payment->amount = $member->type->price;
            $member_payment->payment_type = 'initial';
            $member_payment->payment_method = 'paypal';
            $member_payment->payment_status = 'pending';
            $member_payment->paypal_token = $response['TOKEN'];
            $member_payment->save();

            return redirect($response['paypal_link']);
        }
    }

    public function paypalSuccess(Request $request)
    {

        $member_payment = MemberPayment::where('paypal_token','=',$request->token)->first();
        $member = Member::where('id','=',$member_payment->member_id)->first();

        $provider = new ExpressCheckout;
        $data = $this->generatePaymentData($member);

        $payment_response = $provider->doExpressCheckoutPayment($data, $member_payment->paypal_token, $request->PayerID);

        if ($payment_response['ACK'] != 'Success') {
            $member_payment->delete();
            $member->delete();
            return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again!');
        }

        $checkout_details = $provider->getExpressCheckoutDetails($member_payment->paypal_token);

        if ($checkout_details['CHECKOUTSTATUS'] != 'PaymentActionCompleted') {
            $member_payment->delete();
            $member->delete();
            return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again!');
        }

        $member_payment->payment_status = "completed";
        $member_payment->payment_transaction_number = $checkout_details['TRANSACTIONID'];
        $member_payment->paypal_payer_id = $request->PayerID;
        $member_payment->is_processed = "true";
        $member_payment->save();

        $member->payment_status = "completed";
        $member->status = 'active';
        $member->save();

        $this->sendActivateEmail($member, $member_payment);

        return $this->redirectInfoPage('success', 'Thank you for setting up your membership account and your payment.  <a href="' . url('/login') . '">You can now log into your account here</a>.');
    }

    private function activate($member_id)
    {
        $member = Member::where('id', '=', $member_id)->first();
        $member->status = "active";
        $member->save();
    }

    private function generatePaymentData($member)
    {

        $data = [];
        $data['items'] = [[
            'name' => 'ANZSA Membership - '.$member->type->name,
            'price' => $member->type->price,
            'qty' => 1
        ]];

        $data['invoice_id'] = 'member-'.$member->id;
        $data['invoice_description'] = "Membership #" . $member->id . " Invoice";
        $data['return_url'] = url('/register/paypal-success');
        $data['cancel_url'] = url('/register/paypal-cancel');
        $data['total'] = $member->type->price;

        return $data;
    }

    private function redirectInfoPage($type, $message)
    {
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        if ($type == 'success') {
            flash($message);
        }

        if ($type == 'error') {
            flash("<span class='alert-error'><i class='fas fa-exclamation'></i> " . $message . "</span>");
        }

        return view('site/members/info', array('company_name' => $company_name));
    }

    private function sendWelcomeEmail($member)
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new MembersMessageAdmin($member));

        // Email User
        Mail::to($member->email)->send(new MembersMessageUser($member));

    }
	
	private function sendActivateEmail($member, $member_payment)
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new MembersActivateMessageAdmin($member, $member_payment));

        // Email User
        Mail::to($member->email)->send(new MembersActivateMessageUser($member, $member_payment));

    }
	
	private function sendActivateEmailAdminOnly($member, $member_payment)
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new MembersActivateMessageAdmin($member, $member_payment));       
    }
	
}
