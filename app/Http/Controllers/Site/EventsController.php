<?php

namespace App\Http\Controllers\Site;

use App\Abstracts;
use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use App\Mail\Asm2021NormalRegistrationMessageUser;
use App\Mail\Asm2021VirtualRegistrationMessageUser;
use App\Mail\SubmitAbstractMessageAdmin;
use App\Mail\SubmitAbstractMessageUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

use App\Member;
use App\Events;
use App\EventsCategory;
use App\EventsTicket;
use App\EventsBooking;
use App\EventsBookingPayment;
use App\Setting;
use App\Module;
use App\Page;

use App\Mail\EventRegistrationMessageAdmin;
use App\Mail\EventRegistrationMessageUser;

use Srmklive\PayPal\Services\ExpressCheckout;

class EventsController extends Controller
{
    public function list($category_slug = "", $item_slug = ""){
        $module = Module::where('slug', '=', "events")->first();
		
		if ($category_slug == "")  {
		   // Get Latest Events
		   $category_name = "Events";	
		   $category_description = "";
		   $category_members_only = "passive";
			
		   $items = $this->getEvents(1);

		} elseif ($category_slug != "" && $item_slug == "") {
		  // Get Category Events	
		  $category = $this->getCategory($category_slug);
		  $category_name = $category->name;	
		  $category_description = $category->description;	
		  $category_members_only = $category->members_only;	
			
		  $items = $this->getEvents($category->id);
		}

        if($category_members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($category_members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }

        $side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

		return view('site/events/list', array(   
			'module' => $module,
			'side_nav' => $side_nav,
			'side_nav_mode' => $side_nav_mode,
			'category_name' => $category_name,
			'category_description' => $category_description,
			'category_members_only' => $category_members_only,
			'items' => $items,			
        ));

    }
	
    public function item($category_slug, $item_slug, $mode = ""){
		// Logged in Member
		$member = Auth::guard('member')->user();
		if ($member == null)  {
		   $isMemberLoggedIn = false;
		} else  {
		   $isMemberLoggedIn = true;
		}
		
		$module = Module::where('slug', '=', "events")->first();
    	$side_nav = $this->getCategories();				  			
		$events_item = $this->getEventsItem($item_slug, $mode);			  
		
		$asm_pages = "";
		if ($events_item->id == 3)  {		  
			$pages = new PagesController();
	        $asm_pages = $pages->getPages(11); 
		}

        $asm_2021_pages = "";
        if ($events_item->id == 6)  {
            $pages = new PagesController();
            $asm_2021_pages = $pages->getPages(12);
        }
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }
		
		return view('site/events/item', array(     
			'module' => $module,
			'isMemberLoggedIn' => $isMemberLoggedIn,			
			'side_nav' => $side_nav,
			'side_nav_mode' => $side_nav_mode,
			'events_item' => $events_item,
			'mode' => $mode,
			'category_slug' => $category_slug,
			'asm_pages' => $asm_pages,
			'asm_2021_pages' => $asm_2021_pages
        ));
    }
	
	public function register($event_slug){
		// Logged in Member
		$member = Auth::guard('member')->user();
		if ($member == null)  {
		   $isMemberLoggedIn = false;
		} else  {
		   $isMemberLoggedIn = true;
		}
		
		$module = Module::where('slug', '=', "events")->first();
    	$side_nav = $this->getCategories();				  	
		
		$event_item = $this->getEventsItem($event_slug, "");			  
		$event_tickets = $this->getEventTickets($event_item->id, $isMemberLoggedIn);
		
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		
		// Direct Deposit text
        $payment_direct_deposit_events = Setting::where('key', '=', 'payment-direct-deposit-events')->first();
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }				

        if($event_item->members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($event_item->members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }

        if($event_slug=='annual-scientific-meeting-2021'){

            return view('site/events/asm-2021-register', array(
                'module' => $module,
                'company_name' => $company_name,
                'payment_direct_deposit_events' => $payment_direct_deposit_events->value,
                'member' => $member,
                'side_nav' => $side_nav,
                'side_nav_mode' => $side_nav_mode,
                'event_item' => $event_item,
                'event_tickets' => $event_tickets,
            ));
        }
		
		return view('site/events/register', array(     
			'module' => $module,
			'company_name' => $company_name, 
			'payment_direct_deposit_events' => $payment_direct_deposit_events->value,
			'member' => $member, 
			'side_nav' => $side_nav,
			'side_nav_mode' => $side_nav_mode,
			'event_item' => $event_item,			
			'event_tickets' => $event_tickets,			
        ));
    }
	
	public function registerStore(Request $request)
    {		
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        // Google Recaptcha Validation
        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('event-register/' . $request->event_slug)->withErrors($validator)->withInput();
        }

        $member = Member::where('id','=',$request->member_id)->first();
		$event = Events::where('id','=',$request->event_id)->first();
		$ticket = EventsTicket::where('id','=',$request->event_ticket)->first();

        // Field Validation
        $rules = array(
            'title' => 'required',
			'firstName' => 'required',
			'lastName' => 'required',
			'email' => 'required',
			'designation' => 'required',
			'state' => 'required',
			'country' => 'required',
			//'phoneMobile' => 'required',
        );

        $messages = [
            'title.required' => 'Please select title',
            'firstName.required' => 'Please enter first name',
			'lastName.required' => 'Please enter last name',
			'email.required' => 'Please enter email',
			'designation.required' => 'Please select role',
			'state.required' => 'Please enter state',
			'country.required' => 'Please enter country',
			//'phoneMobile.required' => 'Please enter mobile number',
        ];

        if ($ticket->price > 0) {
            $rules['payment_method'] = 'required';
            $messages['payment_method.required'] = 'Please select payment method';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('event-register/' . $request->event_slug)->withErrors($validator)->withInput();
        }						
		
		// Create Event Booking
        // ********************
		$event_booking = new EventsBooking();
		$event_booking->event_id = $request->event_id;
		$event_booking->ticket_id = $request->event_ticket;
		$event_booking->member_id = $request->member_id;
		$event_booking->title = $request->title;
		$event_booking->first_name = $request->firstName;
		$event_booking->last_name = $request->lastName;
		$event_booking->email_address = $request->email;
		$event_booking->phone_mobile = $request->phoneMobile;
		$event_booking->designation = $request->designation;
		$event_booking->company_name = $request->companyName;
		$event_booking->state = $request->state;
		$event_booking->country = $request->country;
		$event_booking->token = md5(rand(1, 10) . microtime());		
		$event_booking->save();
		
		
		if ($ticket->price == 0){
			$this->sendRegistrationEmail($event, $ticket, $event_booking);
			
			if ($event_booking->event_id == 3)  {
			   $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the ASM sessions as well as a link to view the abstracts. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
			} elseif ($event_booking->event_id == 5)  {
			   $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the AGM session. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
			} else  {
			   $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';	
			}
			
            return $this->redirectInfoPage('success', $message_txt, $request->event_id);
        }

		// Process Payment
        // ***************
        if ($request->payment_method == 'bank-deposit') {

            $event_payment = new EventsBookingPayment();
            $event_payment->booking_id = $event_booking->id;
            $event_payment->amount = $ticket->price;            
            $event_payment->payment_method = 'bank-deposit';
            $event_payment->payment_status = 'pending';
            $event_payment->save();

            $this->sendRegistrationEmail($event, $ticket, $event_booking, $event_payment);

			if ($event_booking->event_id == 3)  {
			   $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the ASM sessions as well as a link to view the abstracts. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
			} elseif ($event_booking->event_id == 5)  {
			   $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the AGM session. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
			} else  {
			   $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';	
			}
			
            return $this->redirectInfoPage('success', $message_txt, $request->event_id);
        }

        if ($request->payment_method == 'paypal') {

            $provider = new ExpressCheckout;

            $data = $this->generatePaymentData($event, $ticket, $event_booking);
            $response = $provider->setExpressCheckout($data);

            if ($response['ACK'] == 'Failure') {            
                return $this->redirectInfoPage('error', 'There was an error on payment. Please try again!',$request->event_id);
            }

            $event_payment = new EventsBookingPayment();
            $event_payment->booking_id = $event_booking->id;
            $event_payment->amount = $ticket->price;           
            $event_payment->payment_method = 'paypal';
            $event_payment->payment_status = 'pending';
            $event_payment->paypal_token = $response['TOKEN'];
            $event_payment->save();

            return redirect($response['paypal_link']);
        }
    }

    public function registerStoreAsm2021(Request $request)
    {
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        // Google Recaptcha Validation
        $rules = array(
           // 'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('event-register/' . $request->event_slug)->withErrors($validator)->withInput();
        }

        $member = Member::where('id','=',$request->member_id)->first();
        $event = Events::where('id','=',$request->event_id)->first();
        $ticket = EventsTicket::where('id','=',$request->event_ticket)->first();

        // Field Validation
        $rules = array(
            'title' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'address1' => 'required',
            'suburb' => 'required',
            'postcode' => 'required',
            'email' => 'required',
            'state' => 'required',
            'country' => 'required',
            //'phoneMobile' => 'required',
        );

        $messages = [
            'title.required' => 'Please select title',
            'firstName.required' => 'Please enter first name',
            'lastName.required' => 'Please enter last name',
            'address1.required' => 'Please enter address',
            'suburb.required' => 'Please enter suburb',
            'postcode.required' => 'Please enter postcode',
            'email.required' => 'Please enter email',
            'designation.required' => 'Please select role',
            'state.required' => 'Please enter state',
            'country.required' => 'Please enter country',
            'phoneMobile.required' => 'Please enter mobile number',
        ];

        if ($ticket->price > 0) {
            $rules['payment_method'] = 'required';
            $messages['payment_method.required'] = 'Please select payment method';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('event-register/' . $request->event_slug)->withErrors($validator)->withInput();
        }

        // Create Event Booking
        // ********************
        $event_booking = new EventsBooking();
        $event_booking->event_id = $request->event_id;
        $event_booking->ticket_id = $request->event_ticket;
        $event_booking->member_id = $request->member_id;
        $event_booking->title = $request->title;
        $event_booking->first_name = $request->firstName;
        $event_booking->last_name = $request->lastName;

        $event_booking->address1 = $request->address1;
        $event_booking->address2 = $request->address2;
        $event_booking->suburb = $request->suburb;
        $event_booking->postcode = $request->postcode;
        $event_booking->social_activity = $request->social;
        $event_booking->partner = $request->partner;
        $event_booking->partner_name = $request->partner_name;
        $event_booking->partner_diet = $request->partner_diet;
        $event_booking->breastfeeding = $request->breastfeeding;
        $event_booking->delegate_list = $request->delegate_list;
        $event_booking->delegate_ack = $request->delegate_ack;
        $event_booking->cancellation = $request->cancellation;
        $event_booking->dietary = $request->dietary;

        $event_booking->email_address = $request->email;
        $event_booking->phone_mobile = $request->phoneMobile;
        $event_booking->designation = $request->designation;
        $event_booking->company_name = $request->companyName;
        $event_booking->state = $request->state;
        $event_booking->country = $request->country;
        $event_booking->token = md5(rand(1, 10) . microtime());
        $event_booking->save();


        if ($ticket->price == 0){
            $this->sendRegistrationEmail($event, $ticket, $event_booking);

            if ($event_booking->event_id == 3)  {
                $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the ASM sessions as well as a link to view the abstracts. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
            } elseif ($event_booking->event_id == 5)  {
                $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the AGM session. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
            } else  {
                $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
            }

            return $this->redirectInfoPage('success', $message_txt, $request->event_id);
        }

        // Process Payment
        // ***************
        if ($request->payment_method == 'bank-deposit') {

            $event_payment = new EventsBookingPayment();
            $event_payment->booking_id = $event_booking->id;
            $event_payment->amount = $ticket->price;
            $event_payment->payment_method = 'bank-deposit';
            $event_payment->payment_status = 'pending';
            $event_payment->save();

            $this->sendRegistrationEmail($event, $ticket, $event_booking, $event_payment);

            if ($event_booking->event_id == 3)  {
                $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the ASM sessions as well as a link to view the abstracts. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
            } elseif ($event_booking->event_id == 5)  {
                $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the AGM session. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
            } else  {
                $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.</p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
            }

            return $this->redirectInfoPage('success', $message_txt, $request->event_id);
        }

        if ($request->payment_method == 'paypal') {

            $provider = new ExpressCheckout;

            $data = $this->generatePaymentDataAsm2021($event, $ticket, $request->social, $request->partner, $event_booking);
            $response = $provider->setExpressCheckout($data);

            if ($response['ACK'] == 'Failure') {
                return $this->redirectInfoPage('error', 'There was an error on payment. Please try again!',$request->event_id);
            }

            $event_payment = new EventsBookingPayment();
            $event_payment->booking_id = $event_booking->id;
            $event_payment->amount = $ticket->price;
            $event_payment->payment_method = 'paypal';
            $event_payment->payment_status = 'pending';
            $event_payment->paypal_token = $response['TOKEN'];
            $event_payment->save();

            return redirect($response['paypal_link']);
        }
    }

    private function generatePaymentDataAsm2021($event, $ticket, $social, $partner, $booking)
    {

        $invoice_description = "Event Booking #" . $booking->id . " Invoice";
        $price = $ticket->price;

        if($social=="on"){
            $price = $price+50;
            $invoice_description = $invoice_description." - Dinner(+$50)";
        }

        if($partner=="on"){
            $price = $price+100;
            $invoice_description = $invoice_description." - with Partner(+$100)";
        }

        $data = [];
        $data['items'] = [[
            'name' => 'ANZSA Event - '.$event->title,
            'price' => $price,
            'qty' => 1
        ]];

        $data['invoice_id'] = 'booking-'.$booking->id;
        $data['invoice_description'] = $invoice_description;
        $data['return_url'] = url('/event-register/paypal-success');
        $data['cancel_url'] = url('/event-register/paypal-cancel');
        $data['total'] = $price;

        return $data;
    }
	
    private function generatePaymentData($event, $ticket, $booking)
    {

        $data = [];
        $data['items'] = [[
            'name' => 'ANZSA Event - '.$event->title,
            'price' => $ticket->price,
            'qty' => 1
        ]];

        $data['invoice_id'] = 'booking-'.$booking->id;
        $data['invoice_description'] = "Event Booking #" . $booking->id . " Invoice";
        $data['return_url'] = url('/event-register/paypal-success');
        $data['cancel_url'] = url('/event-register/paypal-cancel');
        $data['total'] = $ticket->price;

        return $data;
    }
	
    public function paypalSuccess(Request $request)
    {

        $event_payment = EventsBookingPayment::where('paypal_token','=',$request->token)->first();
		
		$booking = EventsBooking::where('id','=',$event_payment->booking_id)->first();
		$event = Events::where('id','=',$booking->event_id)->first();
		$ticket = EventsTicket::where('id','=',$booking->ticket_id)->first();		
        $member = Member::where('id','=',$booking->member_id)->first();

        $provider = new ExpressCheckout;
        $data = $this->generatePaymentData($event, $ticket, $booking);

        $payment_response = $provider->doExpressCheckoutPayment($data, $event_payment->paypal_token, $request->PayerID);

        if ($payment_response['ACK'] != 'Success') {
            $event_payment->delete();
            $booking->delete();
            return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again!', $event->id);
        }

        $checkout_details = $provider->getExpressCheckoutDetails($event_payment->paypal_token);

        if ($checkout_details['CHECKOUTSTATUS'] != 'PaymentActionCompleted') {
            $event_payment->delete();
            $booking->delete();
            return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again!', $event->id);
        }

        $event_payment->payment_status = "completed";
        $event_payment->payment_transaction_number = $checkout_details['TRANSACTIONID'];
        $event_payment->paypal_payer_id = $request->PayerID;
        $event_payment->is_processed = "true";
        $event_payment->save();

        $this->sendRegistrationEmail($event, $ticket, $booking, $event_payment);

		if ($booking->event_id == 3)  {
		   $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the ASM sessions as well as a link to view the abstracts. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
		} elseif ($booking->event_id == 5)  {
		   $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $booking->id . '.  A confirmation email has been sent to you.</p><p><b>It is important that you keep the confirmation email as it contains the Zoom details for the AGM session. </b></p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';
		} else  {
		   $message_txt = '<p>Thank you for your registration!  Your registration ID is ' . $booking->id . '.  A confirmation email has been sent to you.</p><p>Please check your spam/junk inbox if you did not receive it. Otherwise, please email us (<a href="contact@sarcoma.org.au">contact@sarcoma.org.au</a>) for assistance.</p><p>Thank you</p>';	
		}
		
        return $this->redirectInfoPage('success', $message_txt, $event->id);
    }	
	
	public function paypalCancel(Request $request)
    {

        $event_payment = EventsBookingPayment::where('paypal_token','=',$request->token)->first();
		
		$booking = EventsBooking::where('id','=',$event_payment->booking_id)->first();
		$event = Events::where('id','=',$booking->event_id)->first();
		$ticket = EventsTicket::where('id','=',$booking->ticket_id)->first();		
        $member = Member::where('id','=',$booking->member_id)->first();

        $provider = new ExpressCheckout;
        $data = $this->generatePaymentData($event, $ticket, $booking);

        $payment_response = $provider->doExpressCheckoutPayment($data, $event_payment->paypal_token, $request->PayerID);
        
		$event_payment->delete();
		$booking->delete();
		return $this->redirectInfoPage('error', 'You cancelled your payment. Please try again!', $event->id);
    }	
	
	private function redirectInfoPage($type, $message, $event_id)
    {
		$module = Module::where('slug', '=', "events")->first();
		
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

		$event_item = Events::where('id','=',$event_id)->first();
		
        if ($type == 'success') {
            flash($message);
        }

        if ($type == 'error') {
            flash("<span class='alert-error'><i class='fas fa-exclamation'></i> " . $message . "</span>");
        }

        return view('site/events/info', array(
			'module' => $module,
			'company_name' => $company_name,
			'event_item' => $event_item
		));
    }
	
	public function getCategories(){
		$categories = EventsCategory::whereHas("events")->where('status', '=', 'active')->get();
		foreach ($categories as $category){
            $category->url = $category->url;
        }
		return($categories);
	}	
	
	public function getEvents($category_id = "", $limit = 12){
		$today = date("Y/m/d");		
		
		if ($category_id == "")  {
			$events = Events::has("categorypublic")
			            ->where('status', '=', 'active')
						->where('start_date', '>=', $today)											        
						->orderBy('start_date', 'desc')				        
						->paginate($limit);	
		} else {
		   $events = Events::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)
						->where('start_date', '>=', $today)						
						->orderBy('start_date', 'desc')
						->paginate($limit);		
		}
		
		return($events);
	}		
	  
	public function getEventsItem($item_slug, $mode){		
		if ($mode == "preview") {
		   $events = Events::where(['slug' => $item_slug])->first();							
		} else {
		   $events = Events::where(['status' => 'active', 'slug' => $item_slug])->first();						
		}
		return($events);
	}
	
	public function getEventItem($id){				
		$events = Events::where(['id' => $id])->first();									
		return($events);
	}
	
	public function getEventTickets($event_id, $members_only){
		if ($members_only)  {
		   $tickets = EventsTicket::where(['event_id' => $event_id, 'is_deleted' => "false", 'members_only' => "active"])->orderBy('name','desc')->get();
		} else  {
		   $tickets = EventsTicket::where(['event_id' => $event_id, 'is_deleted' => "false", 'members_only' => "passive"])->orderBy('name','desc')->get();
		}
		
		return($tickets);
	}
	
	public function getCategory($category_slug){

		$categories = EventsCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}		
	
	private function sendRegistrationEmail($event, $ticket, $event_booking, $event_payment = NULL)
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new EventRegistrationMessageAdmin($event, $ticket, $event_booking, $event_payment));

        if($event->slug=='annual-scientific-meeting-2021'){

            if($ticket->type=='virtual'){
                Mail::to($event_booking->email_address)->send(new Asm2021VirtualRegistrationMessageUser($event, $ticket, $event_booking, $event_payment));
            }

            if($ticket->type=='normal'){
                Mail::to($event_booking->email_address)->send(new Asm2021NormalRegistrationMessageUser($event, $ticket, $event_booking, $event_payment));
            }

        }else{
            // Email User
            Mail::to($event_booking->email_address)->send(new EventRegistrationMessageUser($event, $ticket, $event_booking, $event_payment));
        }

    }
	
	public function abstracts($mode = ""){		
		$category_slug = "asm-2020";
		$item_slug = "annual-scientific-meeting-2020";
		
		// Logged in Member
		$member = Auth::guard('member')->user();
		if ($member == null)  {
		   $isMemberLoggedIn = false;
		} else  {
		   $isMemberLoggedIn = true;
		}
				
		$module = Module::where('slug', '=', "events")->first();
    	$side_nav = $this->getCategories();				  			
		$events_item = $this->getEventsItem($item_slug, $mode);			  
		
		$asm_pages = "";
		if ($events_item->id == 3)  {		  
			$pages = new PagesController();
	        $asm_pages = $pages->getPages(11); 
		}
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }				
		
		return view('site/events/abstracts', array(     		
			'module' => $module,		
			'isMemberLoggedIn' => $isMemberLoggedIn,
			'side_nav' => $side_nav,
			'side_nav_mode' => $side_nav_mode,
			'events_item' => $events_item,
			'mode' => $mode,
			'category_slug' => $category_slug,
			'asm_pages' => $asm_pages
        ));
    }
	
	public function abstractsRegister($token, $mode = ""){	
		$category_slug = "asm-2020";
		$item_slug = "annual-scientific-meeting-2020";
		
		// Check Abstract Token
		$booking = EventsBooking::where('token','=',$token)->first();
		if (isset($booking))  {
		   $view = "abstracts-register";	
		} else  {
		   $view = "abstracts";	
		}
			
		// Logged in Member
		$member = Auth::guard('member')->user();
		if ($member == null)  {
		   $isMemberLoggedIn = false;
		} else  {
		   $isMemberLoggedIn = true;
		}
		
		$module = Module::where('slug', '=', "events")->first();
    	$side_nav = $this->getCategories();				  			
		$events_item = $this->getEventsItem($item_slug, $mode);			  
		
		$asm_pages = "";
		if ($events_item->id == 3)  {		  
			$pages = new PagesController();
	        $asm_pages = $pages->getPages(11); 
		}
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }
				
		return view('site/events/' . $view, array(     
			'module' => $module,		
			'isMemberLoggedIn' => $isMemberLoggedIn,
			'side_nav' => $side_nav,
			'side_nav_mode' => $side_nav_mode,
			'events_item' => $events_item,
			'mode' => $mode,
			'category_slug' => $category_slug,
			'asm_pages' => $asm_pages
        ));
    }

    public function abstracts2021($mode = ""){
        $category_slug = "asm-2021";
        $item_slug = "annual-scientific-meeting-2021";

        // Logged in Member
        $member = Auth::guard('member')->user();
        if ($member == null)  {
            $isMemberLoggedIn = false;
        } else  {
            $isMemberLoggedIn = true;
        }

        $module = Module::where('slug', '=', "events")->first();
        $side_nav = $this->getCategories();
        $events_item = $this->getEventsItem($item_slug, $mode);

        $asm_2021_pages = "";
        if ($events_item->id == 6)  {
            $pages = new PagesController();
            $asm_2021_pages = $pages->getPages(12);
        }

        $side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }

        return view('site/events/abstracts2021', array(
            'module' => $module,
            'isMemberLoggedIn' => $isMemberLoggedIn,
            'side_nav' => $side_nav,
            'side_nav_mode' => $side_nav_mode,
            'events_item' => $events_item,
            'mode' => $mode,
            'category_slug' => $category_slug,
            'asm_2021_pages' => $asm_2021_pages
        ));
    }

    public function abstractsRegister2021($token, $mode = ""){
        $category_slug = "asm-2021";
        $item_slug = "annual-scientific-meeting-2021";

        // Check Abstract Token
        $booking = EventsBooking::where('token','=',$token)->first();
        if (isset($booking))  {
            $view = "abstracts-register";
        } else  {
            $view = "abstracts";
        }

        // Logged in Member
        $member = Auth::guard('member')->user();
        if ($member == null)  {
            $isMemberLoggedIn = false;
        } else  {
            $isMemberLoggedIn = true;
        }

        $module = Module::where('slug', '=', "events")->first();
        $side_nav = $this->getCategories();
        $events_item = $this->getEventsItem($item_slug, $mode);

        $asm_pages = "";
        if ($events_item->id == 6)  {
            $pages = new PagesController();
            $asm_pages = $pages->getPages(12);
        }

        $side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }

        return view('site/events/' . $view, array(
            'module' => $module,
            'isMemberLoggedIn' => $isMemberLoggedIn,
            'side_nav' => $side_nav,
            'side_nav_mode' => $side_nav_mode,
            'events_item' => $events_item,
            'mode' => $mode,
            'category_slug' => $category_slug,
            'asm_pages' => $asm_pages
        ));
    }

    public function submitAbstracts2021($mode = "")
    {

        $category_slug = "asm-2021";
        $item_slug = "annual-scientific-meeting-2021";

        // Logged in Member
        $member = Auth::guard('member')->user();
        if ($member == null)  {
            $isMemberLoggedIn = false;
        } else  {
            $isMemberLoggedIn = true;
        }

        $module = Module::where('slug', '=', "events")->first();
        $side_nav = $this->getCategories();
        $events_item = $this->getEventsItem($item_slug, $mode);

        $asm_2021_pages = "";
        if ($events_item->id == 6)  {
            $pages = new PagesController();
            $asm_2021_pages = $pages->getPages(12);
        }

        $side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

        return view('site/events/submit-abstract2021', array(
            'module' => $module,
            'isMemberLoggedIn' => $isMemberLoggedIn,
            'side_nav' => $side_nav,
            'side_nav_mode' => $side_nav_mode,
            'events_item' => $events_item,
            'mode' => $mode,
            'category_slug' => $category_slug,
            'asm_2021_pages' => $asm_2021_pages
        ));
    }

    public function storeAbstracts2021(Request $request)
    {

        $rules = array(
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'hospital' => 'required',
            'abstract_title' => 'required',
            'authors' => 'required',
            'institutions' => 'required',
            'copy' => 'required'
        );

        $messages = [
            'title.required' => 'Please select title',
            'first_name.required' => 'Please enter your first name',
            'last_name.required' => 'Please enter your last name',
            'email.required' => 'Please enter your email',
            'mobile.required' => 'Please enter your mobile',
            'hospital.required' => 'Please enter your hospital/institution',
            'abstract_title.required' => 'Please enter the title',
            'authors.required' => 'Please enter authors',
            'institutions.required' => 'Please enter institutions',
            'copy.required' => 'Please enter copy'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('/asm-2021/submit-abstract')->withErrors($validator)->withInput();
        }

        $abstract = new Abstracts();
        $abstract->title = $request->title;
        $abstract->first_name = $request->first_name;
        $abstract->last_name = $request->last_name;
        $abstract->email = $request->email;
        $abstract->mobile = $request->mobile;
        $abstract->hospital = $request->hospital;
        $abstract->is_poster = $request->is_poster;
        $abstract->is_podium = $request->is_podium;
        $abstract->abstract_title = $request->abstract_title;
        $abstract->authors = $request->authors;
        $abstract->institutions = $request->institutions;
        $abstract->copy = $request->copy;
        $abstract->save();

        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new SubmitAbstractMessageAdmin($abstract));

        // Email User
        Mail::to($abstract->email)->send(new SubmitAbstractMessageUser($abstract));

        Session::flash('message', 'Abstract has submitted successfully!');
        return redirect('/asm-2021/submit-abstract');
    }
}
