<?php

namespace App\Http\Controllers\Site;

use App\Donation;
use App\Http\Controllers\Controller;
use App\Mail\DonationCancelMessageAdmin;
use App\Mail\DonationCancelMessageUser;
use App\Mail\DonationMessageAdmin;
use App\Mail\DonationMessageUser;
use App\Setting;
use Illuminate\Support\Facades\Mail;
use App\PaypalReceivedNotifications;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use Carbon\Carbon;

class PayPalController extends Controller
{

    public function postNotify(Request $request)
    {
        $provider = new ExpressCheckout();

        $request->merge(['cmd' => '_notify-validate']);
        $post = $request->all();

        $response = (string)$provider->verifyIPN($post);

        if ($response === 'VERIFIED') {

            $notification = new PaypalReceivedNotifications();
            $notification->request = json_encode($request->all());
            $notification->save();

            $donation_id = $request->rp_invoice_id;

            if ($request->txn_type == 'recurring_payment_profile_created') {

                if ($request->initial_payment_status == 'Completed') {

                    $donation = Donation::where('id', '=', $donation_id)->first();
                    $donation->payment_status = "completed";
                    $donation->payment_transaction_number = $request->initial_payment_txn_id;
                    $donation->payment_transaction_result = 'Successful';
                    $donation->save();

                    $this->sendEmails($donation);
                }
            }

            if ($request->txn_type == 'recurring_payment_profile_cancel') {

                $donation = Donation::where('id', '=', $donation_id)->first();
                $donation->payment_status = 'regular_payment_cancelled';
                $donation->save();

                $this->sendCancelEmails($donation);
            }
        }
    }

    public function sendEmails($donation)
    {

        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        $fields = json_decode($donation->data);

        foreach ($fields as $field) {
            if ($field->field == 'email') {
                $user_email = $field->value;
            }
        }

        $next_billing_date = null;
        $provider = new ExpressCheckout;
        $profile_details = $provider->getRecurringPaymentsProfileDetails($donation->paypal_profile_id);

        if(isset($profile_details['ACK']) && $profile_details['ACK']=='Success'){
            $next_billing_date = Carbon::createFromTimeString($profile_details['NEXTBILLINGDATE'])->format('d-m-Y H:i:s');
        }

        $cancel_link_hash = md5($donation->id.env('CANCEL_LINK_SECRET'));

        //Email Website Owner
        Mail::to($contactEmail)->send(new DonationMessageAdmin($donation,$next_billing_date));

        // Email User
        Mail::to($user_email)->send(new DonationMessageUser($donation,$next_billing_date,$cancel_link_hash));

    }

    public function sendCancelEmails($donation)
    {

        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        $fields = json_decode($donation->data);

        foreach ($fields as $field) {
            if ($field->field == 'email') {
                $user_email = $field->value;
            }
        }

        //Email Website Owner
        Mail::to($contactEmail)->send(new DonationCancelMessageAdmin($donation));

        // Email User
        Mail::to($user_email)->send(new DonationCancelMessageUser($donation));

    }
}
