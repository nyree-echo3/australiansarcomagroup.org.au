<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Property;
use App\PropertyCategory;

class PropertiesController extends Controller
{
    public function list($category_slug = "", $item_slug = ""){
    	$side_nav = $this->getCategories();	
		
		if (sizeof($side_nav) > 0)  {
			if ($category_slug == "")  {
			   // Get Latest Properties
			   $category_name = "Latest Properties";

			   $items = $this->getProperties();
			} elseif ($category_slug != "" && $item_slug == "") {
			  // Get Category Properties
			  $category = $this->getCategory($category_slug);
			  $category_name = $category->name;	

			  $items = $this->getProperties($category->id);
			} 		
		}

		return view('site/properties/list', array(
			'side_nav' => $side_nav,
			'category_name' => (sizeof($side_nav) > 0 ? $category_name : null),
			'items' => (sizeof($side_nav) > 0 ? $items : null),			
        ));

    }
	
    public function item($category_slug, $item_slug){
		$property = $this->getPropertyItem($item_slug);
			
		return view('site/properties/item', array(
			'property' => $property,
        ));
    }		
	public function getCategories(){
		$categories = PropertyCategory::where('status', '=', 'active')->get();
        foreach ($categories as $category){
            $category->url = $category->url;
        }
		return($categories);
	}	
	
	public function getProperties($category_id = "", $limit = 6){
		if ($category_id == "")  {
			$properties = Property::where('status', '=', 'active')
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {
            $properties = Property::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($properties);
	}	
	  
	public function getPropertyItem($item_slug){
		$property = Property::where(['status' => 'active', 'slug' => $item_slug])
			        ->where('status', '=', 'active')
			        ->first();						
		return($property);
	}
	
	public function getCategory($category_slug){
		$categories = PropertyCategory::where('slug', '=', $category_slug)->first();
		return($categories);
	}		
}
