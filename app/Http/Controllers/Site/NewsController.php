<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\News;
use App\NewsCategory;
use App\PageCategory;
use App\Setting;
use App\Module;

class NewsController extends Controller
{
    public function list($category_slug = "", $item_slug = ""){
        $module = Module::where('slug', '=', "news")->first();
		 
		if ($category_slug == "")  {
		   // Get Latest News
		   $category_name = "Latest News";	
		   $category_description = "";
		   $category_members_only = "passive";
			
		   $items = $this->getNews(1);

		} elseif ($category_slug != "" && $item_slug == "") {
		  // Get Category News	
		  $category = $this->getCategory($category_slug);
		  $category_name = $category->name;	
		  $category_description = $category->description;	
		  $category_members_only = $category->members_only;	
			
		  $items = $this->getNews($category->id);
		}

        if($category_members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($category_members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }

        $side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

		if ($category_slug != "sarcoma-stories") {
			return view('site/news/list', array(   
				'module' => $module,
				'side_nav' => $side_nav,
				'side_nav_mode' => $side_nav_mode,
				'category_name' => $category_name,
				'category_description' => $category_description,
				'category_members_only' => $category_members_only,
				'items' => $items,			
			));
		} else  {
			return view('site/news/list-wide', array(   
				'module' => $module,
				'side_nav' => $side_nav,
				'side_nav_mode' => $side_nav_mode,
				'category_name' => $category_name,
				'category_description' => $category_description,
				'category_members_only' => $category_members_only,
				'items' => $items,			
			));
		}

    }
	
    public function item($category_slug, $item_slug, $mode = ""){
		$module = Module::where('slug', '=', "news")->first();
    	$side_nav = $this->getCategories();				  			
		$news_item = $this->getNewsItem($item_slug, $mode);			  
			
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

        if($news_item->category->members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($news_item->category->members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }	
		
		if ($news_item->category_id != 5) {
			return view('site/news/item', array(     
				'module' => $module,
				'side_nav' => $side_nav,
				'side_nav_mode' => $side_nav_mode,
				'news_item' => $news_item,
				'mode' => $mode,
				'category_slug' => $category_slug
			));
		} else  {
			$side_nav = (new NavigationBuilder())->buildSideNavigation("pages/about-sarcoma");
	        $category = PageCategory::where('slug', '=', "about-sarcoma")->orderBy('position', 'asc')->get();		
			
			return view('site/news/item-wide', array(     
				'module' => $module,
				'side_nav' => $side_nav,
				'side_nav_mode' => $side_nav_mode,
				'news_item' => $news_item,
				'mode' => $mode,
				'category_slug' => $category_slug,
				'category' => $category
			));
		}
    }	
	
	public function archive(Request $request, $age = ""){        		
    	//$side_nav = $this->getCategories();	
				
		// Get Archived News		
		$items = $this->getNewsArchive($age);			
		
		return view('site/news/archive', array(            								
			'age' => $age,	
			'items' => $items,			
        ));

    }
	
	public function getCategories(){
		$categories = NewsCategory::whereHas("news")->where('status', '=', 'active')->get();
		foreach ($categories as $category){
            $category->url = $category->url;
        }
		return($categories);
	}	
	
	public function getNews($category_id = "", $limit = 12){
		$today = date("Y/m/d");		
		
		if ($category_id == "")  {
			$news = News::has("categorypublic")
			            ->where('status', '=', 'active')
						->where('start_date', '<=', $today)
						->where('archive_date', '>', $today)					        
						->orderBy('start_date', 'desc')				        
						->paginate($limit);	
		} else {
		   $news = News::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)
						->where('start_date', '<=', $today)
						->where('archive_date', '>', $today)
						->orderBy('start_date', 'desc')
						->paginate($limit);		
		}
		
		return($news);
	}
	
	public function getNewsArchive($age = 6){
		$today = date("Y/m/d");		
		
		switch ($age)  {
			case 0:
		        $start_period = $today;
		        $end_period = date("Y-m-d", strtotime($today . ' -6 months'));
				break;
			
			case 6:
		        $start_period = date("Y-m-d", strtotime($today  . ' -6 months'));
		        $end_period = date("Y-m-d", strtotime($start_period . ' -6 months'));
				break;
				
			case 12:
		        $start_period = date("Y-m-d", strtotime($today  . ' -12 months'));
		        $end_period = date("Y-m-d", strtotime($start_period . ' -12 months'));
				break;
			
			case 24:
		        $start_period = date("Y-m-d", strtotime($today  . ' -24 months'));
		        $end_period = date("Y-m-d", strtotime($start_period . ' -12 months'));
				break;
			
			case 36:
		        $start_period = date("Y-m-d", strtotime($today  . ' -36 months'));
		        $end_period = date("Y-m-d", strtotime($start_period . ' -100 years'));
				break;
				
		}				
		
		$news = News::has("categorypublic")
			        ->where('status', '=', 'active')						
					->where('archive_date', '<=', $start_period)
			        ->where('archive_date', '>', $end_period)
			        ->where('status', '=', 'active')
					->orderBy('start_date', 'desc')
					->paginate(2);
		;
		
		return($news);
	}
	  
	public function getNewsItem($item_slug, $mode){		
		if ($mode == "preview") {
		   $news = News::where(['slug' => $item_slug])->first();							
		} else {
		   $news = News::where(['status' => 'active', 'slug' => $item_slug])->first();						
		}
		return($news);
	}
	
	public function getCategory($category_slug){

		$categories = NewsCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}
	
	public function newsletter(){
		
		// Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();
		
		// Social Media
        $social_facebook = Setting::where('key', '=', 'social-facebook')->first();
	    $social_twitter = Setting::where('key', '=', 'social-twitter')->first();

    	return view('site/news/newsletter', array(         
            'contact_details' => $contact_details->value,
			'social_facebook' => $social_facebook->value,
			'social_twitter' => $social_twitter->value
        ));

    }
}
