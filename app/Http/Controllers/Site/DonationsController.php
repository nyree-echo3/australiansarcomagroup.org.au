<?php

namespace App\Http\Controllers\Site;

use App\Donation;
use App\DonationCategory;
use App\Helpers\NavigationBuilder;

use App\Http\Controllers\Controller;
use App\Mail\DonationMessageAdmin;
use App\Mail\DonationMessageUser;
use App\Setting;
use App\Module;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Srmklive\PayPal\Services\ExpressCheckout;


class DonationsController extends Controller
{
    public function index()
    {
		$module = Module::where('slug', '=', "donations")->first();
		
		$side_navV2 = (new NavigationBuilder())->buildSideNavigation();

        $side_nav_mode = 'manual';
//        if($side_navV2==null){
//            //$side_navV2 =  'support-us';
//            $side_nav_mode = 'auto';
//        }
		
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();

        // Direct Deposit text
        $payment_direct_deposit = Setting::where('key', '=', 'payment-direct-deposit')->first();

        // Categories
        $categories = $this->getCategories();

        // Donation Form
        $fields = Setting::where('key', '=', 'donation-form-fields')->first();
        $form = $fields->value;

        // Amount
        $form = rtrim($form, ']');
        $form = $form . ',{"type":"text","subtype":"text","required":true,"label":" Donation type","className":"form-control","name":"type","readonly":"readonly","placeholder":""}, {"type":"text","subtype":"text","required":true,"label":" Donation amount","className":"form-control","name":"donation","readonly":"readonly"}]';

        $donation = 0;
        if (old('donation')) {
            $fields = json_decode($form);
            foreach ($fields as $field) {
                $field->value = old($field->name);

                if ($field->name == "donation") {
                    $donation = old($field->name);
                }
            }

            $form = json_encode($fields);
        }

        return view('site/donations/donation', array(
			'module' => $module,
			'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,			
            'company_name' => $company_name->value,
            'payment_direct_deposit' => $payment_direct_deposit->value,
            'categories' => $categories,
            'form' => $form,
            'donation' => $donation,
        ));
    }

    public function getCategories()
    {
        $categories = DonationCategory::where('status', '=', 'active')->orderBy('amount', 'asc')->get();
        return ($categories);
    }

    public function saveDonation(Request $request)
    {
        // Save Donation
        // ***************
        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('donation')->withErrors($validator)->withInput();
        }

        if ($request->radPaymentMethod == 'paypal') {

            $donation = $this->saveDonationRecord($request);

            $provider = new ExpressCheckout;

            $data = $this->generatePaymentData($donation);

            if ($request->type == 'once') {
                $response = $provider->setExpressCheckout($data);
            }

            if ($request->type == 'regular') {
                $response = $provider->setExpressCheckout($data, true);
            }

            if ($response['ACK'] == 'Failure') {
                return $this->redirectInfoPage('error', 'There was an error. Please try again!');
            }

            $donation->paypal_token = $response['TOKEN'];
            $donation->save();

            return redirect($response['paypal_link']);
        }

        if ($request->radPaymentMethod == 'bank-deposit') {
            $donation = $this->saveDonationRecord($request);

            $this->sendEmails($donation);

            $message = "<p>Thank you for your donation!  We will send you an email confirmation.<p>Through your generosity, ANZSA dedicates 100% of your donations towards crucial sarcoma research and clinical trials to save and improve lives.</p>";
            return $this->redirectInfoPage('success', $message);
        }
    }

    private function saveDonationRecord($request)
    {

        $fields = Setting::where('key', '=', 'donation-form-fields')->first();
        $form = $fields->value;

        // Amount fix here
        $form = rtrim($form, ']');
        $form = $form . ',{"type":"text","subtype":"text","required":true,"label":" Donation type","className":"form-control","name":"type","readonly":"readonly","placeholder":""}, {"type":"text","subtype":"text","required":true,"label":" Donation amount","className":"form-control","name":"donation","readonly":"readonly"}]';

        $fields = json_decode($form);

        $data = array();

        foreach ($fields as $field) {
            if ($field->type != 'header' && $field->type != 'paragraph') {

                if ($field->type == 'date') {
                    $date_array = explode('-', $request->{$field->name});
                    $request->{$field->name} = $date_array[2] . '-' . $date_array[1] . '-' . $date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }


        $donation = new Donation();
        $donation->data = json_encode($data);
        $donation->amount = substr($request->donation, 1);
        $donation->payment_type = $request->type;
        $donation->payment_method = $request->radPaymentMethod;

        if ($request->radPaymentMethod == 'bank-deposit') {
            $donation->payment_status = 'completed';
            $donation->payment_transaction_result = 'Successful';
        } else {
            $donation->payment_status = 'pending';
        }

        $donation->save();

        return $donation;
    }

    private function generatePaymentData($donation)
    {

        $data = [];
        $data['items'] = [[
            'name' => 'ANZSA Donation',
            'price' => $donation->amount,
            'qty' => 1
        ]];

        $data['invoice_id'] = $donation->id;
        $data['invoice_description'] = "Donation #" . $donation->id . " Invoice";
        $data['return_url'] = url('/donations/paypal-success');
        $data['cancel_url'] = url('/donations/paypal-cancel');
        $data['total'] = $donation->amount;

        if ($donation->payment_type == 'regular') {
            $data['subscription_desc'] = "Monthly Subscription #" . $donation->id;
        }

        return $data;
    }

    private function redirectInfoPage($type, $message)
    {
        $module = Module::where('slug', '=', "donations")->first();		
		
		$side_navV2 = (new NavigationBuilder())->buildSideNavigation("support-us");
		$side_nav_mode = 'manual';
		
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        if ($type == 'success') {
            flash($message);
        }

        if ($type == 'error') {
            flash("<span class='alert-error'><i class='fas fa-exclamation'></i> " . $message . "</span>");
        }

        return view('site/donations/info', array(
			'module' => $module,
			'company_name' => $company_name,
			'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,		
		));
    }

    public function sendEmails($donation)
    {

        if ($donation->payment_method == 'paypal' && $donation->payment_type == 'regular') {
            return true;
        }

        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        $fields = json_decode($donation->data);

        foreach ($fields as $field) {
            if ($field->field == 'email') {
                $user_email = $field->value;
            }
        }

        //Email Website Owner
        Mail::to($contactEmail)->send(new DonationMessageAdmin($donation));

        // Email User
        Mail::to($user_email)->send(new DonationMessageUser($donation));

    }

    public function paypalSuccess(Request $request)
    {

        $provider = new ExpressCheckout;

        // Update Donation
        $donation = Donation::where('paypal_token', '=', $request->token)->first();
        $data = $this->generatePaymentData($donation);

        if ($donation->payment_type == 'once') {

            $payment_response = $provider->doExpressCheckoutPayment($data, $donation->paypal_token, $request->PayerID);

            if ($payment_response['ACK'] != 'Success') {
                return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again to <a href="' . url('/donations') . '">Make a Donation</a>!');
            }

            $checkout_details = $provider->getExpressCheckoutDetails($donation->paypal_token);

            if ($checkout_details['CHECKOUTSTATUS'] != 'PaymentActionCompleted') {
                return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again to <a href="' . url('/donations') . '">Make a Donation</a>!');
            }

            $donation->payment_transaction_number = $checkout_details['TRANSACTIONID'];
            $donation->payment_status = "completed";
            $donation->payment_transaction_result = 'Successful';
        }

        if ($donation->payment_type == 'regular') {

            $checkout_details = $provider->getExpressCheckoutDetails($donation->paypal_token);

            if ($checkout_details['BILLINGAGREEMENTACCEPTEDSTATUS'] == '1') {

                $description = "Monthly Subscription #" . $donation->id;

                $startdate = Carbon::now()->addMonth()->toAtomString();
                $data = [
                    'PROFILESTARTDATE' => $startdate,
                    'DESC' => $description,
                    'BILLINGPERIOD' => 'Month', // Can be 'Day', 'Week', 'SemiMonth', 'Month', 'Year'
                    'BILLINGFREQUENCY' => 1, //
                    'AMT' => $donation->amount, // Billing amount for each billing cycle
                    'CURRENCYCODE' => 'AUD', // Currency code
                    'INITAMT' => $donation->amount,
                    'PROFILEREFERENCE' => $donation->id
                ];

                $create_profile = $provider->createRecurringPaymentsProfile($data, $donation->paypal_token);

                if ($create_profile['ACK'] != 'Success') {
                    return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again to <a href="' . url('/donations') . '">Make a Donation</a>!');
                }

                $donation->paypal_profile_id = $create_profile['PROFILEID'];
            }
        }

        $donation->paypal_payer_id = $request->PayerID;
        $donation->save();

        $this->sendEmails($donation);

        // Return
        // ******
        $message = "<p>Thank you for your donation!  We will send you an email confirmation.<p>Through your generosity, ANZSA dedicates 100% of your donations towards crucial sarcoma research and clinical trials to save and improve lives.</p>";
        return $this->redirectInfoPage('success', $message);
    }

    public function paypalCancel(Request $request)
    {

        $donation = Donation::where('paypal_token', '=', $request->token)->first();

        if ($donation) {
            $donation->payment_status = "cancelled";
            $donation->save();
            return $this->redirectInfoPage('error', 'Your payment has been cancelled!');
        }

        return $this->redirectInfoPage('error', 'Operation was not successful!');
    }

    public function cancelRecurringPayment(Request $request)
    {

        $donation = Donation::where('id','=',$request->id)->first();

        $provider = new ExpressCheckout;
        $response = $provider->cancelRecurringPaymentsProfile($donation->paypal_profile_id);

        if($response['ACK']=='Success'){

            $donation->payment_status = 'regular_payment_cancelled';
            $donation->save();

            return $this->redirectInfoPage('success', 'Your regular donation has been cancelled!');
        }


        return $this->redirectInfoPage('error', 'Operation was not successful! Please  to <a href="' . url('/contact') . '">contact us</a>.');
    }

    public function xmas()
    {
        $module = Module::where('slug', '=', "donations")->first();

        $side_navV2 = (new NavigationBuilder())->buildSideNavigation();

        $side_nav_mode = 'manual';
//        if($side_navV2==null){
//            //$side_navV2 =  'support-us';
//            $side_nav_mode = 'auto';
//        }


        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        // Direct Deposit text
        $payment_direct_deposit = Setting::where('key', '=', 'payment-direct-deposit')->first();

        // Categories
        $categories = $this->getCategories();

        // Donation Form
        $fields = Setting::where('key', '=', 'donation-form-fields')->first();
        $form = $fields->value;

        // Amount
        $form = rtrim($form, ']');
        $form = $form . ',{"type":"text","subtype":"text","required":true,"label":" Donation type","className":"form-control","name":"type","readonly":"readonly","placeholder":""}, {"type":"text","subtype":"text","required":true,"label":" Donation amount","className":"form-control","name":"donation","readonly":"readonly"}]';

        $donation = 0;
        if (old('donation')) {
            $fields = json_decode($form);
            foreach ($fields as $field) {
                $field->value = old($field->name);

                if ($field->name == "donation") {
                    $donation = old($field->name);
                }
            }

            $form = json_encode($fields);
        }

        return view('site/donations/xmas-donation', array(
            'module' => $module,
            'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,
            'company_name' => $company_name->value,
            'payment_direct_deposit' => $payment_direct_deposit->value,
            'categories' => $categories,
            'form' => $form,
            'donation' => $donation,
        ));
    }

}
