<?php

namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;

use App\Setting;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'members-portal';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:member')->except('logout');
    }

    public function showLoginForm()
    {
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		
        return view('site/members/login', array(
            'company_name' => $company_name,           					
        ));
    }


    public function login(Request $request)		
    {			
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('member')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            $user = Auth::guard('member')->getLastAttempted();

            if($user->status!='active'){

                auth('member')->logout();
                flash('Your account is not active. <a href="'.url('contact').'">Please contact us!</a>')->error();
                return \Redirect::to('login');
            }

            if($user->isExpired()){
                return \Redirect::to('members-portal/renew');
            }

			if ($request->hidRedirect != "") {
			   return \Redirect::to($request->hidRedirect);	
			} else  {
               return \Redirect::to('members-portal');
			}
        }
		
        return back()->withInput($request->only('email', 'remember'));		
    }

    public function logout(Request $request)
    {
		auth('member')->logout();       
		
        //$request->session()->invalidate();
		
        return redirect('login');
    }		
}
