<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Module;
use App\Specialist;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\SpecialUrl;
use App\Setting;

class SpecialistsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'specialists')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request) 
    {				
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $specialists = Specialist::where('title', 'LIKE', '%' .  $request->search . '%')->orderBy('updated_at', 'desc')->paginate($paginate_count);
        } else {
            $specialists = Specialist::sortable()->orderBy('updated_at', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('specialists-filter');
       
        return view('admin/specialists/specialists', array(
            'specialists' => $specialists,            
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {        
        return view('admin/specialists/add', array(          
        ));
    }

    public function edit($specialist_id)
    {
        $specialist = Specialist::where('id', '=', $specialist_id)->first();

        return view('admin/specialists/edit', array(
            'specialist' => $specialist,            
        ));
    }

    public function store(Request $request)
    {
        $rules = array(                      
			'title' => 'required',            
            'address1' => 'required',
            'suburb' => 'required',            
			'country' => 'required', 		
			'postcode' => 'required', 			
        );

        $messages = [          
			'title.required' => 'Please enter title',			
			'address1.required' => 'Please enter address',
            'suburb.required' => 'Please enter suburb',		
			'country.required' => 'Please enter country',					
			'postcode.required' => 'Please enter postcode',							
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/specialists/add')->withErrors($validator)->withInput();
        }

        $specialist = new Specialist();        		
        $specialist->title = $request->title;
		$specialist->address1 = $request->address1;
		$specialist->address2 = $request->address2;
        $specialist->suburb = $request->suburb;
        $specialist->country = $request->country;
        $specialist->state = $request->state_other;
        $specialist->postcode = $request->postcode;
		$specialist->contact_name = $request->contact_name;
		$specialist->contact_email = $request->contact_email;
		$specialist->contact_phone = $request->contact_phone;
		$specialist->contact_fax = $request->contact_fax;
		$specialist->medical_oncologist = nl2br($request->medical_oncologist);
		$specialist->paediatric_medical_oncologist = nl2br($request->paediatric_medical_oncologist);
		$specialist->adolescent_medical_oncologist = nl2br($request->adolescent_medical_oncologist);
		$specialist->radiation_oncologist = nl2br($request->radiation_oncologist);
		$specialist->surgical_oncologist = nl2br($request->surgical_oncologist);
		$specialist->orthopaedic_surgeon = nl2br($request->orthopaedic_surgeon);
		$specialist->pathologist = nl2br($request->pathologist);
		$specialist->radiologist = nl2br($request->radiologist);
		$specialist->sarcoma_nurse = nl2br($request->sarcoma_nurse);
		$specialist->other = nl2br($request->other);
		
        if($request->live=='on'){
           $specialist->status = 'active'; 
        }

        $specialist->save();
		
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/specialists/' . $specialist->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/specialists')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		}
    }
	
    public function update(Request $request)
    {
        $rules = array(            
            'title' => 'required',            
            'address1' => 'required',
            'suburb' => 'required',            
			'country' => 'required', 			
			'postcode' => 'required', 				
        );

        $messages = [
            'title.required' => 'Please enter title',			
			'address1.required' => 'Please enter address',
            'suburb.required' => 'Please enter suburb',		
			'country.required' => 'Please enter country',					
			'postcode.required' => 'Please enter postcode',					
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/specialists/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }
	
        $specialist = Specialist::where('id','=',$request->id)->first();
        $specialist->title = $request->title;
		$specialist->address1 = $request->address1;
		$specialist->address2 = $request->address2;
        $specialist->suburb = $request->suburb;
        $specialist->country = $request->country;
        $specialist->state = $request->state_other;
        $specialist->postcode = $request->postcode;
		$specialist->contact_name = $request->contact_name;
		$specialist->contact_email = $request->contact_email;
		$specialist->contact_phone = $request->contact_phone;
		$specialist->contact_fax = $request->contact_fax;
		$specialist->medical_oncologist = nl2br($request->medical_oncologist);
		$specialist->paediatric_medical_oncologist = nl2br($request->paediatric_medical_oncologist);
		$specialist->adolescent_medical_oncologist = nl2br($request->adolescent_medical_oncologist);
		$specialist->radiation_oncologist = nl2br($request->radiation_oncologist);
		$specialist->surgical_oncologist = nl2br($request->surgical_oncologist);
		$specialist->orthopaedic_surgeon = nl2br($request->orthopaedic_surgeon);
		$specialist->pathologist = nl2br($request->pathologist);
		$specialist->radiologist = nl2br($request->radiologist);
		$specialist->sarcoma_nurse = nl2br($request->sarcoma_nurse);
		$specialist->other = nl2br($request->other);
		
		if($request->live=='on'){
           $specialist->status = 'active'; 
        } else {
			$specialist->status = 'passive'; 
		}
		
        $specialist->save();
		
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/specialists/' . $specialist->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/specialists')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		}
		
        
    }

    public function delete($specialist_id)
    {
        $specialist = Specialist::where('id','=',$specialist_id)->first();
        $specialist->is_deleted = true;
        $specialist->save();

        SpecialUrl::where('item_id', '=', $specialist->id)->where('module', '=', 'specialists')->where('type', '=', 'item')->delete();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeSpecialistStatus(Request $request, $specialist_id)
    {
        $specialist = Specialist::where('id', '=', $specialist_id)->first();
        if ($request->status == "true") {
            $specialist->status = 'active';
        } else if ($request->status == "false") {
            $specialist->status = 'passive';
        }
        $v->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $specialists = Specialist::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/specialists/sort', array(
            'specialists' => $specialists
        ));
    }
    
    public function emptyFilter()
    {
        session()->forget('specialists-filter');
        return redirect()->to('dreamcms/specialists');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

//        if ($request->type && $request->type != "all") {
//            $filter_control = true;
//        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('specialists-filter', [               
                'search' => $request->search
            ]);
        }

        if (session()->has('specialists-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }	
	
	public function content(){

		$content = Setting::where('key','=','specialists-content')->first();
			
        return view('admin/specialists/content', array(
            'content' => $content,   			
        ));

    }
	
	public function updateContent(Request $request){

		$content = Setting::where('key','=','specialists-content')->first();
		$content->value = $request->content;
		$content->save();
       
		return \Redirect::to('dreamcms/specialists/content')->with('message', Array('text' => 'Values has been updated.', 'status' => 'success'));
    }


}