<?php
 
namespace App\Http\Controllers\Admin;

use App\Abstracts;
use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Module;
use App\Events;
use App\EventsCategory;
use App\EventsTicket;
use App\EventsBooking;
use App\EventsBookingPayment;
use App\Member;
use App\Helpers\General;
use App\SpecialUrl;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

use App\Exports\EventsRegistrationExport;
use Maatwebsite\Excel\Facades\Excel;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'events')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $events = Events::Filter()->sortable()->orderBy('updated_at', 'desc')->paginate($paginate_count);
        } else {
            $events = Events::with('category')->sortable()->orderBy('updated_at', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('events-filter');
        $categories = EventsCategory::orderBy('created_at', 'desc')->get();
        return view('admin/events/events', array(
            'events' => $events,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('events-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('events-filter')) {
            $filter_control = true;
        }

        if (!$filter_control) {
            $request->session()->put('events-filter', [
                'category' => null,
                'search' => null
            ]);
        }

        return $filter_control;
    }
	
    public function registrationIsFiltered($request)
    {
        $filter_control = false;

        if ($request->event && $request->event != "all") {
            $filter_control = true;
        }
		
		if ($request->payment_type && $request->payment_type != "all") {
            $filter_control = true;
        }

        if ($request->payment_method && $request->payment_method != "all") {
            $filter_control = true;
        }

        if ($request->payment_status && $request->payment_status != "all") {
            $filter_control = true;
        }

        if($request->daterange){
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('registration-filter', [
                'event_id' => $request->event,
				'payment_type' => $request->payment_type,
                'payment_method' => $request->payment_method,
                'payment_status' => $request->payment_status,
                'daterange' => $request->daterange,
                'search' => $request->search
            ]);
        }

        if (session()->has('registration-filter')) {
            $filter_control = true;
        }

        if (!$filter_control) {
            $request->session()->put('registration-filter', [
                'event_id' => null,
                'payment_type' => null,
                'payment_method' => null,
                'payment_status' => null,
                'daterange' => null,
                'search' => null
            ]);
        }

        return $filter_control;
    }	

    public function add()
    {
        $categories = EventsCategory::orderBy('created_at', 'desc')->get();
        return view('admin/events/add', array(
            'categories' => $categories
        ));
    }

    public function edit($events_id)
    {
        $events = Events::where('id', '=', $events_id)->first();
		$event_tickets = EventsTicket::where('event_id', '=', $events_id)->get();

        $events->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $events->id)->where('module', '=', 'events')->where('type', '=', 'item')->first();
        if ($special_url) {
            $events->special_url = $special_url->url;
        }

        $categories = EventsCategory::orderBy('created_at', 'desc')->get();
        return view('admin/events/edit', array(
            'events' => $events,
			'event_tickets' => $event_tickets,
            'categories' => $categories
        ));
    }

    public function preview($events_id)
    {
        $events = Events::with("category")->where('id', '=', $events_id)->first();

        $general = new General();
        $view = $general->eventsPreview($events->category->slug, $events->slug);

        return ($view);
    }

    public function store(Request $request)
    {
        $rules = array(
            'title' => 'required',
            'slug' => 'required|unique_store:events',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'short_description' => 'required',
            'start_date' => 'required',
			'end_date' => 'required',
            //'end_date' => 'required|date_checker:' . $request->start_date,
			'start_time' => 'required',
			'end_time' => 'required',
            'body' => 'required',
            //'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store',
			'special_url' => 'nullable|special_url_store',
        );

        $messages = [
            'title.required' => 'Please enter title',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_store' => 'The SEO Name is already taken',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'short_description.required' => 'Please enter short desciption',
            'start_date.required' => 'Please enter start date',
            'end_date.required' => 'Please enter end date',
            //'end_date.date_checker' => 'End date should be greater than the start date',
			'start_time.required' => 'Please enter start time',
			'end_time.required' => 'Please enter end time',
            'body.required' => 'Please enter body',
            //'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/events/add')->withErrors($validator)->withInput();
        }

        $events = new Events();
        $events->category_id = $request->category_id;
        $events->title = $request->title;
        $events->slug = $request->slug;
        $events->meta_title = $request->meta_title;
        $events->meta_keywords = $request->meta_keywords;
        $events->meta_description = $request->meta_description;
        $events->short_description = $request->short_description;
        $events->start_date = date('Y-m-d', strtotime(str_replace("/", "-", $request->start_date)));
        $events->end_date = date('Y-m-d', strtotime(str_replace("/", "-", $request->end_date)));
		$events->start_time = date('G:i', strtotime($request->start_time));
		$events->end_time = date('G:i', strtotime($request->end_time));
        $events->body = $request->body;
        $events->thumbnail = $request->thumbnail;
        if ($request->members_only == 'on') {
            $events->members_only = 'active';
        } else  {
			$events->members_only = 'passive';
		}
        if ($request->live == 'on') {
            $events->status = 'active';
        }

        $events->save();
        $this->updateTickets($events->id, $request);
		
        ///////////special URL//
        if ($request->special_url != "") {
            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $events->id;
            $new_special_url->module = 'events';
            $new_special_url->type = 'item';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();

        }
        ////////////////////////

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/events/' . $events->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/events/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        }


    }

    public function update(Request $request)
    {
        $rules = array(
            'title' => 'required',
            'slug' => 'required|unique_update:events,' . $request->id,
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'short_description' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
			'start_time' => 'required',
			'end_time' => 'required',
            'body' => 'required',
            //'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:events,item,' . $request->id,
			'special_url' => 'nullable|special_url_update:events,item,' . $request->id,
        );

        $messages = [
            'title.required' => 'Please enter title',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_update' => 'The SEO Name is already taken',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'short_description.required' => 'Please enter short desciption',
            'start_date.required' => 'Please enter start date',
            'end_date.required' => 'Please enter end date',
            //'end_date.date_checker' => 'End date should be greater than the start date',
			'start_time.required' => 'Please enter start time',
			'end_time.required' => 'Please enter end time',
            'body.required' => 'Please enter body',
            //'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/events/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }
       
        $events = Events::where('id', '=', $request->id)->first();
        $events->category_id = $request->category_id;
        $events->title = $request->title;
        $events->slug = $request->slug;
        $events->meta_title = $request->meta_title;
        $events->meta_keywords = $request->meta_keywords;
        $events->meta_description = $request->meta_description;
        $events->short_description = $request->short_description;
        $events->start_date = date('Y-m-d', strtotime(str_replace("/", "-", $request->start_date)));
        $events->end_date = date('Y-m-d', strtotime(str_replace("/", "-", $request->end_date)));
		$events->start_time = date('G:i', strtotime($request->start_time));
		$events->end_time = date('G:i', strtotime($request->end_time));
        $events->body = $request->body;
        $events->thumbnail = $request->thumbnail;
		if ($request->members_only == 'on') {
            $events->members_only = 'active';
        } else  {
			$events->members_only = 'passive';
		}
        if ($request->live == 'on') {
            $events->status = 'active';
        } else {
            $events->status = 'passive';
        }
        $events->save();
		
		$this->updateTickets($events->id, $request);

        ///////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $events->id)->where('module', '=', 'events')->where('type', '=', 'item')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();
            } else {
                $special_url->delete();
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $events->id;
                $new_special_url->module = 'events';
                $new_special_url->type = 'item';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();
            }
        }
        ////////////////////////

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/events/' . $events->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/events/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        }
    }

	public function updateTickets($event_id, Request $request)
    {   	
		if ($request->event_tickets > 0)  {
			for ($i = 1; $i <= $request->event_tickets; $i++)  {			
				if ($request->input('ticket_id' . $i) != "") {
				   $tickets = EventsTicket::where('id', '=', $request->input('ticket_id' . $i))->first();
				} else  {
				   $tickets = new EventsTicket();	
				}
				
				$tickets->event_id = $event_id;
				$tickets->name = $request->input('ticket_name' . $i);
				$tickets->price = ($request->input('ticket_price' . $i) != "" ? $request->input('ticket_price' . $i) : 0);				
				if ($request->input('ticket_members_only' . $i) == 'on') {
				   $tickets->members_only = 'active';
				} else {
				   $tickets->members_only = 'passive';
				}

                if ($request->input('ticket_virtual' . $i) == 'on') {
                    $tickets->type = 'virtual';
                } else {
                    $tickets->type = 'normal';
                }

                $tickets->is_deleted = $request->input('ticket_is_deleted' . $i);
				$tickets->save();				
			}
		}
    }

	
    public function delete($events_id)
    {
        $events = Events::where('id', '=', $events_id)->first();
        $events->is_deleted = true;
        $events->save();

        SpecialUrl::where('item_id', '=', $events->id)->where('module', '=', 'events')->where('type', '=', 'item')->delete();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeStatus(Request $request, $events_id)
    {
        $events = Events::where('id', '=', $events_id)->first();
        if ($request->status == "true") {
            $events->status = 'active';
        } else if ($request->status == "false") {
            $events->status = 'passive';
        }
        $events->save();

        return Response::json(['status' => 'success']);
    }

    public function categories()
    {
        $categories = EventsCategory::orderBy('position', 'desc')->get();
        return view('admin/events/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/events/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:events_categories',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store',
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/events/add-category')->withErrors($validator)->withInput();
        }

        $category = new EventsCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;		
        if ($request->live == 'on') {
            $category->status = 'active';
        }		
        $category->save();

        ///////////special URL//
        if ($request->special_url != "") {
            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $category->id;
            $new_special_url->module = 'events';
            $new_special_url->type = 'category';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();

        }
        ////////////////////////

        (new NavigationHelper())->navigationAddItem($request->name, $category->url, 'category-'.$category->id, 'events', 'category', $category->slug);

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/events/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/events/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        }

    }

    public function editCategory($category_id)
    {
        $category = EventsCategory::where('id', '=', $category_id)->first();

        $category->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'events')->where('type', '=', 'category')->first();
        if ($special_url) {
            $category->special_url = $special_url->url;
        }

        return view('admin/events/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:events_categories,' . $request->id,
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:events,category,' . $request->id,
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/events/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = EventsCategory::findOrFail($request->id);

        (new NavigationHelper())->navigationItems('update', 'category', 'events', $category->slug, null, $request->name, $request->slug);

        $category->name = $request->name;
        $category->slug = $request->slug;		
		
        if ($request->live == 'on') {
            $category->status = 'active';
            (new NavigationHelper())->navigationAddItem($category->name, $category->url, 'category-'.$category->id, 'events', 'category', $category->slug);

        } else {
            $category->status = 'passive';
            (new NavigationHelper())->navigationItems('delete-item', 'category', 'events', $category->slug);
        }
        $category->save();

        (new NavigationHelper())->navigationItems('change-href', 'category', 'events', $category->slug, $category->url);

        ///////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'events')->where('type', '=', 'category')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'events', $category->slug, $request->special_url);

            } else {
                $special_url->delete();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'events', $category->slug, 'events/'.$category->slug);
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $category->id;
                $new_special_url->module = 'events';
                $new_special_url->type = 'category';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'events', $category->slug, $request->special_url);
            }
        }
        ////////////////////////

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/events/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/events/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
        }
    }

    public function deleteCategory($category_id)
    {
        $category = EventsCategory::where('id', '=', $category_id)->first();

        if (count($category->events)) {
            return \Redirect::to('dreamcms/events/categories')->with('message', Array('text' => 'Category has events. Please delete events first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'events')->where('type', '=', 'category')->delete();

        (new NavigationHelper())->navigationItems('delete-item', 'category', 'events', $category->slug);

        return \Redirect::to('dreamcms/events/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $category_id)
    {
        $category = EventsCategory::where('id', '=', $category_id)->first();
        if ($request->status == "true") {
            $category->status = 'active';

            (new NavigationHelper())->navigationAddItem($category->name, $category->url, 'category-'.$category->id, 'events', 'category', $category->slug);

        } else if ($request->status == "false") {
            $category->status = 'passive';

            (new NavigationHelper())->navigationItems('delete-item', 'category', 'events', $category->slug);
        }
        $category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = EventsCategory::where('status', '=', 'active')->orderBy('position', 'desc')->get();

        return view('admin/events/sort-category', array(
            'categories' => $categories
        ));
    }

    public function emptyFilter()
    {
        session()->forget('events-filter');
        return redirect()->to('dreamcms/events');
    }
	
	public function emptyRegistrationsFilter()
    {
        session()->forget('registration-filter');
        return redirect()->to('dreamcms/events/registrations');
    }
	
	public function registrations(Request $request)
    {	
        $is_filtered = $this->registrationIsFiltered($request);
        $paginate_count = session()->get('pagination-count');

					
        if ($is_filtered) {
		   $bookings = EventsBooking::Filter()->with('event')->with('ticket')->with('payment')->orderBy('id', 'desc')->paginate($paginate_count);			
        } else {
		   $bookings = EventsBooking::with('event')->with('ticket')->with('payment')->orderBy('id', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('registration-filter');
        $events = Events::orderBy('created_at', 'desc')->get();
		
        return view('admin/events/registrations', array(
            'bookings' => $bookings,
            'events' => $events,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }	
	
	public function viewRegistration($booking_id)
    {
        $registration = EventsBooking::with('event')->with('ticket')->with('payment')->where('id','=',$booking_id)->first();

        return view('admin/events/view-registration', array(
            'registration' => $registration
        ));
    }
	
	public function updateRegistration(Request $request)
    {				
        $payment = EventsBookingPayment::where('id','=',$request->id)->first();
        $payment->payment_status = $request->payment_status;
        $payment->payment_transaction_number = $request->payment_transaction_number;
        $payment->save();

        return \Redirect::to('dreamcms/events/registrations')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
    }
	
	public function export() 
    {
       return Excel::download(new EventsRegistrationExport, 'events-registration' . date('YmdGis') . '.xlsx');
    }	
	
	public function addRegistration()
    {
        $events = Events::with('category')->where('status', '=', 'active')->orderBy('created_at', 'desc')->get();						
		$tickets = EventsTicket::where('event_id', '=', $events[0]->id)->where('is_deleted', '=', 'false')->get();
		$members = Member::where('status', '=', 'active')->where('is_deleted', '=', 'false')->get();
        
        return view('admin/events/add-registration', array(
            'events' => $events,
			'tickets' => $tickets,
			'members' => $members,
        ));
    }
	
	public function getTickets(Request $request, $id)
    {
        $tickets = EventsTicket::where('event_id', '=', $id)->get();
		
        return Response::json([
            'status' => 'success',
            'tickets' => $tickets
        ]);
    }
	
	public function getMember(Request $request, $id)
    {
        $member = Member::where('id', '=', $id)->first();
		
        return Response::json([
            'status' => 'success',
            'member' => $member
        ]);
    }
	
	public function storeRegistration(Request $request)
    {
        $rules = array(
			'event_id' => 'required',
			'ticket_id' => 'required',
            'title' => 'required',  
			'first_name' => 'required',  
			'last_name' => 'required',  
			'designation' => 'required',  
			'company_name' => 'required',  
			'state' => 'required',  
			'country' => 'required',  
			'email_address' => 'required',  
        );

        $messages = [
            'event_id.required' => 'Please select an event',
			'ticket_id.required' => 'Please select a ticket',
			'title.required' => 'Please enter title',
			'first_name.required' => 'Please enter first name',
			'last_name.required' => 'Please enter last name',
			'designation.required' => 'Please select role',
			'company_name.required' => 'Please enter company/institution',
			'state.required' => 'Please select a state',
			'country.required' => 'Please select a country',
			'email_address.required' => 'Please enter email address',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/events/registration/add')->withErrors($validator)->withInput();
        }

        $registration = new EventsBooking();
        $registration->event_id = $request->event_id;
		$registration->ticket_id = $request->ticket_id;
		$registration->member_id = $request->member_id;
        $registration->title = $request->title; 
		$registration->first_name = $request->first_name; 
		$registration->last_name = $request->last_name; 
		$registration->designation = $request->designation; 
		$registration->company_name = $request->company_name; 
		$registration->state = $request->state; 
		$registration->country = $request->country; 
		$registration->token = md5(rand(1, 10) . microtime());
		$registration->email_address = $request->email_address; 
		$registration->phone_mobile = $request->phone_mobile;
		$registration->is_manual = true;
        $registration->save();        

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/events/' . $registration->id . '/edit-registration')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/events/registrations')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        }
    }
	
	public function editRegistration($booking_id)
    {
		$registration = EventsBooking::where('id', '=', $booking_id)->first();
        $events = Events::with('category')->where('status', '=', 'active')->orderBy('created_at', 'desc')->get();						
		$tickets = EventsTicket::where('event_id', '=', $registration->event_id)->where('is_deleted', '=', 'false')->get();
		$members = Member::where('status', '=', 'active')->where('is_deleted', '=', 'false')->get();
        
        return view('admin/events/edit-registration', array(
            'registration' => $registration,
			'events' => $events,
			'tickets' => $tickets,
			'members' => $members,
        ));
    }
	
	public function modifyRegistration(Request $request)
    {
        $rules = array(
			'event_id' => 'required',
			'ticket_id' => 'required',
            'title' => 'required',  
			'first_name' => 'required',  
			'last_name' => 'required',  
			'designation' => 'required',  
			'company_name' => 'required',  
			'state' => 'required',  
			'country' => 'required',  
			'email_address' => 'required',  
        );

        $messages = [
            'event_id.required' => 'Please select an event',
			'ticket_id.required' => 'Please select a ticket',
			'title.required' => 'Please enter title',
			'first_name.required' => 'Please enter first name',
			'last_name.required' => 'Please enter last name',
			'designation.required' => 'Please select role',
			'company_name.required' => 'Please enter company/institution',
			'state.required' => 'Please select a state',
			'country.required' => 'Please select a country',
			'email_address.required' => 'Please enter email address',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/events/' . $request->id . '/edit-registration')->withErrors($validator)->withInput();
        }

        $registration = EventsBooking::where('id', '=', $request->id)->first();
        $registration->event_id = $request->event_id;
		$registration->ticket_id = $request->ticket_id;
		$registration->member_id = $request->member_id;
        $registration->title = $request->title; 
		$registration->first_name = $request->first_name; 
		$registration->last_name = $request->last_name; 
		$registration->designation = $request->designation; 
		$registration->company_name = $request->company_name; 
		$registration->state = $request->state; 
		$registration->country = $request->country; 	
		$registration->email_address = $request->email_address; 
		$registration->phone_mobile = $request->phone_mobile;		
        $registration->save();        

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/events/' . $registration->id . '/edit-registration')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/events/registrations')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        }


    }
	
	public function deleteRegistration($registration_id)
    {
        $registration = EventsBooking::where('id', '=', $registration_id)->first();
        $registration->is_deleted = true;
        $registration->save();
       
        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function abstracts()
    {
        $abstracts = Abstracts::orderBy('created_at','desc')->get();

        return view('admin/events/abstracts', array(
            'abstracts' => $abstracts
        ));
    }

    public function showAbstract($id)
    {
        $abstract = Abstracts::where('id','=',$id)->first();

        return view('admin/events/show-abstract', array(
            'abstract' => $abstract
        ));
    }
}