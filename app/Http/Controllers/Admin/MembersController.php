<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\MemberPayment;
use App\Module;
use App\Member;
use App\MemberType;
use App\MemberQualification;
use App\MemberAward;
use App\Setting;
use App\Mail\MembersApprovalMessageUser;
use App\Mail\MembersRejectedMessageUser;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Util\MimeType;


use App\Exports\MembersExport;
use Maatwebsite\Excel\Facades\Excel;


class MembersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'members')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $members = Member::Filter()->sortable()->orderBy('id', 'desc')->paginate($paginate_count);
			//$members = Member::Filter()->orderBy('id', 'desc')->paginate($paginate_count);
        } else {
            $members = Member::with('type')->sortable()->orderBy('id', 'desc')->paginate($paginate_count);
			//$members = Member::with('type')->orderBy('id', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('members-filter');
        $types = MemberType::orderBy('created_at', 'desc')->get();
        return view('admin/members/members', array(
            'members' => $members,
            'types' => $types,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $types = MemberType::orderBy('created_at', 'desc')->get();
        return view('admin/members/add', array(
            'types' => $types
        ));
    }

    public function edit($member_id)
    {
        $member = Member::where('id', '=', $member_id)->first();
		$qualifications = MemberQualification::where('member_id', '=', $member_id)->get();
		$awards = MemberAward::where('member_id', '=', $member_id)->get();
		
        $types = MemberType::orderBy('created_at', 'desc')->get();
        return view('admin/members/edit', array(
            'member' => $member,
			'qualifications' => $qualifications,
			'awards' => $awards,
            'types' => $types
        ));
    }

    public function store(Request $request)
    {
        $rules = array(            
            'membershipType' => 'required',
            'title' => 'required',
			'firstName' => 'required',
            'lastName' => 'required',
			'dob' => 'required',
			'address1' => 'required',
			'suburb' => 'required',
            'state' => 'required',
            'postcode' => 'required',
			'country' => 'required',
            'phoneMobile' => 'required',
            'email' => 'required|string|email|max:255|unique_store:members',
            'occupation' => 'required',
			'companyName' => 'required',
            'employmentDateCommencement' => 'required',
			'employmentAddress1' => 'required',
			'employmentSuburb' => 'required',
            'employmentState' => 'required',
            'employmentPostcode' => 'required',
			'employmentCountry' => 'required',
			'employmentEmail' => 'required|string|email|max:255',
			
            'password' => 'required',
            'confirmation' => 'required',
        );

        $messages = [
            'membershipType.required' => 'Please select your membership',
            'title.required' => 'Please select title',
			'firstName.required' => 'Please enter first name',
            'lastName.required' => 'Please enter last name',
			'dob.required' => 'Please enter date of birth',
			'address1.required' => 'Please enter address line 1',
            'suburb.required' => 'Please enter suburb',
            'state.required' => 'Please enter state',
            'postcode.required' => 'Please enter postcode',
			'country.required' => 'Please enter country',
            'phoneMobile.required' => 'Please enter mobile number',
            'email.required' => 'Please enter email',
            'email.unique_store' => 'We already have a member account using the provided email address',
            'occupation.required' => 'Please enter position',
			'companyName.required' => 'Please enter employer',
			'employmentDateCommencement.required' => 'Please enter employment date of commencement',
			'employmentAddress1.required' => 'Please enter employment address line 1',
			'employmentSuburb.required' => 'Please enter suburb',
            'employmentState.required' => 'Please enter state',
            'employmentPostcode.required' => 'Please enter postcode',
			'employmentCountry.required' => 'Please enter country',
			'employmentEmail.required' => 'Please enter email',
			
            'password.required' => 'Please enter password',
            'confirmation.required' => 'Please agree to terms & conditions',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/members/add')->withErrors($validator)->withInput();
        }

        $member = new Member();
        $member->type_id = $request->membershipType;
        $member->title = $request->title;
		$member->firstName = $request->firstName;
        $member->lastName = $request->lastName;
		$member->formerName = $request->formerName;
		$member->dob = date('Y-m-d', strtotime(str_replace("/", "-", $request->dob)));
        $member->address1 = $request->address1;
        $member->address2 = $request->address2;
        $member->suburb = $request->suburb;
        $member->state = $request->state;
        $member->postcode = $request->postcode;
		$member->country = $request->country;
		$member->phoneLandline = $request->phoneLandline;
        $member->phoneMobile = $request->phoneMobile;
        $member->email = $request->email;
		$member->occupation = $request->occupation;
		$member->companyName = $request->companyName;
		$member->employmentDateCommencement = date('Y-m-d', strtotime(str_replace("/", "-", $request->employmentDateCommencement)));
		$member->employmentAddress1 = $request->employmentAddress1;
        $member->employmentAddress2 = $request->employmentAddress2;
        $member->employmentSuburb = $request->employmentSuburb;
        $member->employmentState = $request->employmentState;
        $member->employmentPostcode = $request->employmentPostcode;
		$member->employmentCountry = $request->employmentCountry;
		$member->employmentPhoneNumber = $request->employmentPhoneNumber;
		$member->employmentEmail = $request->employmentEmail;
		$member->otherExperience = $request->otherExperience;
		$member->comments = $request->comments;
		
        $member->dateJoin = date('Y-m-d');
        $member->dateExpire = date('Y-m-d', strtotime('+1 years'));
        $member->password = Hash::make($request->password);
        $member->payment_method = $request->payment_method;        
        $member->status = 'passive'; 

        $member->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/members/' . $member->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/members')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		}
    }

    public function update(Request $request)
    {	
        $rules = array(
            'type_id' => 'required',
            'title' => 'required',
			'firstName' => 'required',
            'lastName' => 'required',
			'dob' => 'required',
			'address1' => 'required',
			'suburb' => 'required',
            'state' => 'required',
            'postcode' => 'required',
			'country' => 'required',
            'phoneMobile' => 'required',
            'email' => 'required|string|email|max:255|unique_update:members,'.$request->id,
            'occupation' => 'required',
			'companyName' => 'required',
            'employmentDateCommencement' => 'required',
			'employmentAddress1' => 'required',
			'employmentSuburb' => 'required',
            'employmentState' => 'required',
            'employmentPostcode' => 'required',
			'employmentCountry' => 'required',
			'employmentEmail' => 'required|string|email|max:255',			            
        );

        $messages = [
            'type_id.required' => 'Please select your membership',
            'title.required' => 'Please select title',
			'firstName.required' => 'Please enter first name',
            'lastName.required' => 'Please enter last name',
			'dob.required' => 'Please enter date of birth',
			'address1.required' => 'Please enter address line 1',
            'suburb.required' => 'Please enter suburb',
            'state.required' => 'Please enter state',
            'postcode.required' => 'Please enter postcode',
			'country.required' => 'Please enter country',
            'phoneMobile.required' => 'Please enter mobile number',
            'email.required' => 'Please enter email',
            'email.unique_store' => 'We already have a member account using the provided email address',
            'occupation.required' => 'Please enter position',
			'companyName.required' => 'Please enter employer',
			'employmentDateCommencement.required' => 'Please enter employment date of commencement',
			'employmentAddress1.required' => 'Please enter employment address line 1',
			'employmentSuburb.required' => 'Please enter suburb',
            'employmentState.required' => 'Please enter state',
            'employmentPostcode.required' => 'Please enter postcode',
			'employmentCountry.required' => 'Please enter country',
			'employmentEmail.required' => 'Please enter email',			
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {			
            return redirect('dreamcms/members/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }
		
		// Save CV File	
		$filename = $request->cv_current;
		
		$cv_file = $request->file('cv_file');
		if (isset($cv_file)) {
			$filename = $cv_file->getClientOriginalName();
			Storage::putFileAs('members/' . $request->id, $cv_file, $filename);			
		}		

        $member = Member::where('id','=',$request->id)->first();
		$previousApprovalStatus = $member->status;
		
        $member->type_id = $request->type_id;
        $member->title = $request->title;
		$member->firstName = $request->firstName;
        $member->lastName = $request->lastName;
		$member->formerName = $request->formerName;
		$member->dob = date('Y-m-d', strtotime(str_replace("/", "-", $request->dob)));
        $member->address1 = $request->address1;
        $member->address2 = $request->address2;
        $member->suburb = $request->suburb;
        $member->state = $request->state;
        $member->postcode = $request->postcode;
		$member->country = $request->country;
		$member->phoneLandline = $request->phoneLandline;
        $member->phoneMobile = $request->phoneMobile;
        $member->email = $request->email;
		$member->occupation = $request->occupation;
		$member->companyName = $request->companyName;
		$member->employmentDateCommencement = date('Y-m-d', strtotime(str_replace("/", "-", $request->employmentDateCommencement)));
		$member->employmentAddress1 = $request->employmentAddress1;
        $member->employmentAddress2 = $request->employmentAddress2;
        $member->employmentSuburb = $request->employmentSuburb;
        $member->employmentState = $request->employmentState;
        $member->employmentPostcode = $request->employmentPostcode;
		$member->employmentCountry = $request->employmentCountry;
		$member->employmentPhoneNumber = $request->employmentPhoneNumber;
		$member->employmentEmail = $request->employmentEmail;
		$member->otherExperience = $request->otherExperience;
		$member->comments = $request->comments;
		$member->status = $request->status;        
		if ($request->password != "")  {
		   $member->password = Hash::make($request->password);
		}  
		//if ($filename != "")  {
		   $member->cvFile = $filename;
		//}
        $member->save();
		
		// Update Qualifications		
		for ($i = 1; $i <= $request->hidQualifications; $i++)  {	
			if (isset($request['qualificationId' . $i])) {
			   $qualification = MemberQualification::where('id','=', $request['qualificationId' . $i])->first();
			} else  {
			   $qualification = new MemberQualification();	
			   $qualification->member_id = $member->id;	
			}
				
            $qualification->qualification = $request['qualification' . $i];
			$qualification->institution = $request['institution' . $i];
			$qualification->graduationYear = $request['graduationYear' . $i];
			$qualification->save();
		}
		
		// Update Awards	
		for ($i = 1; $i <= $request->hidAwards; $i++)  {	
			if (isset($request['awardId' . $i])) {
			   $award = MemberAward::where('id','=', $request['awardId' . $i])->first();
			} else  {
			   $award = new MemberAward();	
			   $award->member_id = $member->id;		
			}
			
			$award->award = $request['award' . $i];			
			$award->save();
		}				
		
		// Send emails on change of approval status
		if ($previousApprovalStatus != "active" && $member->status == "active") {
		   $this->sendApprovalEmail($member);
		}
		if ($previousApprovalStatus != "rejected" && $member->status == "rejected") {
		   $this->sendRejectedEmail($member);
		}
		
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/members/' . $member->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/members')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		}
		
        
    }

    public function show($member_id)
    {
        $member = Member::where('id', '=', $member_id)->first();
		$qualifications = MemberQualification::where('member_id', '=', $member_id)->get();
		$awards = MemberAward::where('member_id', '=', $member_id)->get();

        return view('admin/members/show', array(
            'member' => $member,
			'qualifications' => $qualifications,
			'awards' => $awards,
        ));
    }

    public function delete($member_id)
    {
        $member = Member::where('id','=',$member_id)->first();
        $member->is_deleted = true;
        $member->save();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeMemberStatus(Request $request, $member_id)
    {
        $member = Member::where('id', '=', $member_id)->first();
        if ($request->status == "true") {
            $member->status = 'active';
        } else if ($request->status == "false") {
            $member->status = 'passive';
        }
        $member->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $members = Member::where('status','=','active')->orderBy('id', 'desc')->get();

        return view('admin/members/sort', array(
            'members' => $members
        ));
    }

    public function editPayment($payment_id)
    {
        $payment = MemberPayment::where('id','=',$payment_id)->first();

        return view('admin/members/edit-payment', array(
            'payment' => $payment
        ));
    }

    public function updatePayment(Request $request)
    {
        $payment = MemberPayment::where('id','=',$request->id)->first();
        $payment->payment_status = $request->payment_status;
        $payment->payment_transaction_number = $request->payment_transaction_number;
        $payment->save();

        return \Redirect::to('dreamcms/members/' . $payment->member_id . '/show')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
    }

    public function types()
    {		       		
        $types = MemberType::orderBy('position', 'desc')->get();
        return view('admin/members/types', compact('types'));
    }

    public function addType()
    {
        return view('admin/members/add-type');
    }

    public function storeType(Request $request)
    {
        $rules = array(
            'name' => 'required',
			'price' => 'required'
        );

        $messages = [
            'name.required' => 'Please enter name.',
			'price.required' => 'Please enter price.'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/members/add-type')->withErrors($validator)->withInput();
        }

        $type = new MemberType();
        $type->name = $request->name;
		$type->price = str_replace(',', '', $request->price);
		if($request->live=='on'){
           $type->status = 'active'; 
        }
        $type->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/members/' . $type->id . '/edit-type')->with('message', Array('text' => 'Type has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/members/types')->with('message', Array('text' => 'Type has been added', 'status' => 'success'));
		}		        

    }

    public function editType($type_id)
    {
        $type = MemberType::where('id', '=', $type_id)->first();
        return view('admin/members/edit-type', array(
            'type' => $type,
        ));
    }

    public function updateType(Request $request)
    {
        $rules = array(
            'name' => 'required',
			'price' => 'required'
        );

        $messages = [
            'name.required' => 'Please enter name.',
			'price.required' => 'Please enter price.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/members/' . $request->id . '/edit-type')->withErrors($validator)->withInput();
        }

        $price = str_replace('.', '', $request->price);
        $price = str_replace(',', '.', $price);

        $type = MemberType::findOrFail($request->id);
        $type->name = $request->name;
		$type->price = str_replace(',', '', $request->price);
		if($request->live=='on'){
           $type->status = 'active'; 
		} else {
			$type->status = 'passive';
        }
        $type->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/members/' . $type->id . '/edit-type')->with('message', Array('text' => 'Type has been update', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/members/types')->with('message', Array('text' => 'Type has been update', 'status' => 'success'));
		}	
		        
    }

    public function deleteType($type_id)
    {
        $type = MemberType::where('id','=',$type_id)->first();

        if(count($type->members)){
            return \Redirect::to('dreamcms/members/types')->with('message', Array('text' => 'Member Type has members. Please delete members first.', 'status' => 'error'));
        }

        $type->is_deleted = true;
        $type->save();

        return \Redirect::to('dreamcms/members/types')->with('message', Array('text' => 'Member Type has been deleted.', 'status' => 'success'));
    }

    public function emptyFilter()
    {
        session()->forget('members-filter');
        return redirect()->to('dreamcms/members');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->type && $request->type != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('members-filter', [
                'type' => $request->type,
                'search' => $request->search
            ]);
        }

        if (session()->has('members-filter')) {
            $filter_control = true;
        }

        if (!$filter_control) {
            $request->session()->put('members-filter', [
                'type' => null,
                'search' => null
            ]);
        }

        return $filter_control;
    }
	
	public function changeTypeStatus(Request $request, $type_id)
    {
        $type = MemberType::where('id', '=', $type_id)->first();
        if ($request->status == "true") {
            $type->status = 'active';
        } else if ($request->status == "false") {
            $type->status = 'passive';
        }
        $type->save();

        return Response::json(['status' => 'success']);
    }

    public function sortType()
    {
        $types = MemberType::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/members/sort-type', array(
            'types' => $types
        ));
    }
	
	private function sendApprovalEmail($member)
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;       

        // Email User
        Mail::to($member->email)->send(new MembersApprovalMessageUser($member));

    }

    private function sendRejectedEmail($member)
    {
//        // Contact Email
//        $setting = Setting::where('key', '=', 'contact-email')->first();
//        $contactEmail = $setting->value;       
//
//        // Email User
//        Mail::to($member->email)->send(new MembersRejectedMessageUser($member));

    }
	
	public function download($member_id)
    {
        $member = Member::where('id','=',$member_id)->first();
        $file = Storage::get('members/'.$member_id.'/'.$member->cvFile);
        $type = MimeType::detectByContent($file);

        $headers = [
            'Content-Type' => $type
        ];

        return response($file,200,$headers);
    }
	
	public function export() 
    {
       return Excel::download(new MembersExport, 'members' . date('YmdGis') . '.xlsx');
    }		

}