<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Module;
use App\TeamMember;
use App\TeamCategory;
use App\Helpers\General;
use App\SpecialUrl;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'team')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $team_members = TeamMember::Filter()->sortable()->orderBy('updated_at', 'desc')->paginate($paginate_count);
        } else {
            $team_members = TeamMember::with('category')->sortable()->orderBy('updated_at', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('team-filter');
        $categories = TeamCategory::orderBy('created_at', 'desc')->get();
        return view('admin/team/team-members', array(
            'team_members' => $team_members,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = TeamCategory::orderBy('created_at', 'desc')->get();
        return view('admin/team/add', array(
            'categories' => $categories
        ));
    }

    public function edit($team_member_id)
    {
        $team_member = TeamMember::where('id', '=', $team_member_id)->first();

        $team_member->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $team_member->id)->where('module', '=', 'team')->where('type', '=', 'item')->first();
        if ($special_url) {
            $team_member->special_url = $special_url->url;
        }

        $categories = TeamCategory::orderBy('created_at', 'desc')->get();
        return view('admin/team/edit', array(
            'team_member' => $team_member,
            'categories' => $categories
        ));
    }

	public function preview($team_member_id)
    {
        $team_member = TeamMember::with("category")->where('id', '=', $team_member_id)->first();
		
		$general = new General();
		$view = $general->faqPreview($team_member->category->slug, $team_member->slug);
		
        return ($view);
    }
	
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:team_members',
            //'short_description' => 'required',
            'body' => 'required',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store'
        );

        $messages = [
            'name.required' => 'Please enter title',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_store' => 'The SEO Name is already taken',
            //'short_description.required' => 'Please enter short desciption',
            'body.required' => 'Please enter body',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/team/add')->withErrors($validator)->withInput();
        }

        $team_member = new TeamMember();
        $team_member->category_id = $request->category_id;
        $team_member->name = $request->name;
        $team_member->slug = $request->slug;
        $team_member->short_description = $request->short_description;
        $team_member->body = $request->body;
        $team_member->job_title = $request->job_title;
        $team_member->phone = $request->phone;
        $team_member->mobile = $request->mobile;
        $team_member->email = $request->email;
        $team_member->role = $request->role;
        $team_member->photo = $request->photo;

        if($request->live=='on'){
            $team_member->status = 'active';
        }

        $team_member->save();

        ///////////special URL//
        if ($request->special_url != "") {

            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $team_member->id;
            $new_special_url->module = 'team';
            $new_special_url->type = 'item';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();
        }
        ////////////////////////

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/team/' . $team_member->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/team/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        }
    }

    public function update(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:team_members,' . $request->id,
            //'short_description' => 'required',
            'body' => 'required',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:team,item,' . $request->id,
        );

        $messages = [
            'name.required' => 'Please enter title',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_store' => 'The SEO Name is already taken',
            //'short_description.required' => 'Please enter short desciption',
            'body.required' => 'Please enter body',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/team/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $team_member = TeamMember::where('id','=',$request->id)->first();
        $team_member->category_id = $request->category_id;
        $team_member->name = $request->name;
        $team_member->slug = $request->slug;
        $team_member->short_description = $request->short_description;
        $team_member->body = $request->body;
        $team_member->job_title = $request->job_title;
        $team_member->phone = $request->phone;
        $team_member->mobile = $request->mobile;
        $team_member->email = $request->email;
        $team_member->role = $request->role;
        $team_member->photo = $request->photo;
        if($request->live=='on'){
            $team_member->status = 'active';
        } else {
            $team_member->status = 'passive';
        }
        $team_member->save();

        //////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $team_member->id)->where('module', '=', 'team')->where('type', '=', 'item')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();
            } else {
                $special_url->delete();
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $team_member->id;
                $new_special_url->module = 'team';
                $new_special_url->type = 'item';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();
            }
        }
        ////////////////////////

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/team/' . $team_member->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/team/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        }
    }

    public function delete($team_member_id)
    {
        $team_member = TeamMember::where('id','=',$team_member_id)->first();
        $team_member->is_deleted = true;
        $team_member->save();

        SpecialUrl::where('item_id', '=', $team_member->id)->where('module', '=', 'team')->where('type', '=', 'item')->delete();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeMemberStatus(Request $request, $team_member_id)
    {
        $team_member = TeamMember::where('id', '=', $team_member_id)->first();
        if ($request->status == "true") {
            $team_member->status = 'active';
        } else if ($request->status == "false") {
            $team_member->status = 'passive';
        }
        $team_member->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $team_members = TeamMember::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/team/sort', array(
            'team_members' => $team_members
        ));
    }

    public function categories()
    {
        $categories = TeamCategory::orderBy('position', 'desc')->get();
        return view('admin/team/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/team/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:team_categories',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store'
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/team/add-category')->withErrors($validator)->withInput();
        }

        $category = new TeamCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;
		$category->description = $request->description;
        if($request->live=='on'){
            $category->status = 'active';
        }
        $category->save();

        ///////////special URL//
        if ($request->special_url != "") {
            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $category->id;
            $new_special_url->module = 'team';
            $new_special_url->type = 'category';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();

        }
        ////////////////////////

        //(new NavigationHelper())->navigationAddItem($request->name, $category->url, 'category-'.$category->id, 'team', 'category', $category->slug);

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/team/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/team/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}

    }

    public function editCategory($category_id)
    {
        $category = TeamCategory::where('id', '=', $category_id)->first();

        $category->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'team')->where('type', '=', 'category')->first();
        if ($special_url) {
            $category->special_url = $special_url->url;
        }

        return view('admin/team/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:team_categories,'.$request->id,
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:team,category,' . $request->id,
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/team/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = TeamCategory::findOrFail($request->id);

        (new NavigationHelper())->navigationItems('update', 'category', 'team', $category->slug, null, $request->name, $request->slug);

        $category->name = $request->name;
        $category->slug = $request->slug;
		$category->description = $request->description;
        if($request->live=='on'){
           $category->status = 'active';

            //(new NavigationHelper())->navigationAddItem($category->name, $category->url, 'category-'.$category->id, 'team', 'category', $category->slug);
		} else {
			$category->status = 'passive';

            (new NavigationHelper())->navigationItems('delete-item', 'category', 'team', $category->slug);
        }
        $category->save();

        (new NavigationHelper())->navigationItems('change-href', 'category', 'team', $category->slug, $category->url);

        ///////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'team')->where('type', '=', 'category')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'team', $category->slug, $request->special_url);
            } else {
                $special_url->delete();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'team', $category->slug, 'team/'.$category->slug);
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $category->id;
                $new_special_url->module = 'team';
                $new_special_url->type = 'category';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'team', $category->slug, $request->special_url);
            }
        }
        ////////////////////////

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/team/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/team/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}
    }

    public function deleteCategory($category_id)
    {
        $category = TeamCategory::where('id','=',$category_id)->first();

        if(count($category->members)){
            return \Redirect::to('dreamcms/team/categories')->with('message', Array('text' => 'Category has members. Please delete members first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'team')->where('type', '=', 'category')->delete();

        (new NavigationHelper())->navigationItems('delete-item', 'category', 'team', $category->slug);

        return \Redirect::to('dreamcms/team/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $team_category_id)
    {
        $team_category = TeamCategory::where('id', '=', $team_category_id)->first();
        if ($request->status == "true") {
            $team_category->status = 'active';

            //(new NavigationHelper())->navigationAddItem($team_category->name, $team_category->url, 'category-'.$team_category->id, 'team', 'category', $team_category->slug);

        } else if ($request->status == "false") {
            $team_category->status = 'passive';
            (new NavigationHelper())->navigationItems('delete-item', 'category', 'team', $team_category->slug);
        }
        $team_category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/team/sort-category', array(
            'categories' => $categories
        ));
    }

    public function emptyFilter()
    {
        session()->forget('team-filter');
        return redirect()->to('dreamcms/team');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('team-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('team-filter')) {
            $filter_control = true;
        }

        if (!$filter_control) {
            $request->session()->put('team-filter', [
                'category' => null,
                'search' => null
            ]);
        }

        return $filter_control;
    }

}