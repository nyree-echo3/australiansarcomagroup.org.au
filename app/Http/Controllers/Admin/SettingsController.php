<?php

namespace App\Http\Controllers\Admin;

use App\DocumentCategory;
use App\FaqCategory;
use App\GalleryCategory;
use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Navigation;
use App\NewsCategory;
use App\Page;
use App\ProductCategory;
use App\ProjectCategory;
use App\PropertyCategory;
use App\TeamCategory;
use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Setting;
use App\Module;
use App\PageCategory;
use App\Helpers\General;
use App\ImagesHomeSlider;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index(){

		$company_name = Setting::where('key','=','company-name')->first();
        $email = Setting::where('key','=','email')->first();
		$phone_number = Setting::where('key','=','phone-number')->first();		
		$fax_number = Setting::where('key','=','fax-number')->first();
		$address = Setting::where('key','=','address')->first();

		$live_date = Setting::where('key','=','live-date')->first();		
        $google_analytics = Setting::where('key','=','google-analytics')->first();
		
		$payment_direct_deposit = Setting::where('key','=','payment-direct-deposit')->first();
		$payment_direct_deposit_members = Setting::where('key','=','payment-direct-deposit-members')->first();
		$payment_direct_deposit_events = Setting::where('key','=','payment-direct-deposit-events')->first();

        return view('admin/settings/general', array(
            'company_name' => $company_name,   
			'phone_number' => $phone_number,
            'email' => $email,
            'fax_number' => $fax_number,
			'address' => $address,
			'live_date' => $live_date,
			'google_analytics' => $google_analytics,   
			'payment_direct_deposit' => $payment_direct_deposit,
			'payment_direct_deposit_members' => $payment_direct_deposit_members,
			'payment_direct_deposit_events' => $payment_direct_deposit_events,
        ));

    }

    public function update(Request $request){

		$company_name = Setting::where('key','=','company-name')->first();
		$company_name->value = $request->company_name;
		$company_name->save();

        $email = Setting::where('key','=','email')->first();
        $email->value = $request->email;
        $email->save();

        $phone_number = Setting::where('key','=','phone-number')->first();
		$phone_number->value = $request->phone_number;
		$phone_number->save();

        $fax_number = Setting::where('key','=','fax-number')->first();
        $fax_number->value = $request->fax_number;
        $fax_number->save();

        $address = Setting::where('key','=','address')->first();
        $address->value = $request->address;
        $address->save();
		
		$email = Setting::where('key','=','email')->first();
		$email->value = $request->email;
		$email->save();		
		
		$live_date = Setting::where('key','=','live-date')->first();
		$live_date->value = $request->live_date;
		$live_date->save();		
		
		//$google_analytics = Setting::where('key','=','google-analytics')->first();
		//$google_analytics->value = $request->google_analytics;
		//$google_analytics->save();		
		
		$payment_direct_deposit = Setting::where('key','=','payment-direct-deposit')->first();
		$payment_direct_deposit->value = $request->payment_direct_deposit;
		$payment_direct_deposit->save();

        $payment_direct_deposit_members = Setting::where('key','=','payment-direct-deposit-members')->first();
        $payment_direct_deposit_members->value = $request->payment_direct_deposit_members;
        $payment_direct_deposit_members->save();
		
		$payment_direct_deposit_events = Setting::where('key','=','payment-direct-deposit-events')->first();
        $payment_direct_deposit_events->value = $request->payment_direct_deposit_events;
        $payment_direct_deposit_events->save();

		return \Redirect::to('dreamcms/settings')->with('message', Array('text' => 'Values has been updated.', 'status' => 'success'));
    }

    public function homePage(){

        $meta_title = Setting::where('key','=','meta-title')->first();
        $meta_keywords = Setting::where('key','=','meta-keywords')->first();
        $meta_description = Setting::where('key','=','meta-description')->first();
        $home_intro_text = Setting::where('key','=','home-intro-text')->first();
		$home_supportus_text = Setting::where('key','=','home-supportus-text')->first();

        return view('admin/settings/home-page', array(
            'meta_title' => $meta_title,
            'meta_keywords' => $meta_keywords,
            'meta_description' => $meta_description,
            'home_intro_text' => $home_intro_text,
			'home_supportus_text' => $home_supportus_text
        ));

    }

    public function homePageUpdate(Request $request){

        $meta_title = Setting::where('key','=','meta-title')->first();
        $meta_title->value = $request->meta_title;
        $meta_title->save();

        $meta_keywords = Setting::where('key','=','meta-keywords')->first();
        $meta_keywords->value = $request->meta_keywords;
        $meta_keywords->save();

        $meta_description = Setting::where('key','=','meta-description')->first();
        $meta_description->value = $request->meta_description;
        $meta_description->save();

        $home_intro_text = Setting::where('key','=','home-intro-text')->first();
        $home_intro_text->value = $request->home_intro_text;
        $home_intro_text->save();
		
		$home_supportus_text = Setting::where('key','=','home-supportus-text')->first();
        $home_supportus_text->value = $request->home_supportus_text;
        $home_supportus_text->save();

        return \Redirect::to('dreamcms/settings/home-page')->with('message', Array('text' => 'Values has been updated.', 'status' => 'success'));
    }

    public function headerImages()
    {
        $modules = Module::where('status','=','active')->orderBy('name', 'desc')->get();
        return view('admin/settings/header-images', array(
            'modules' => $modules
        ));
    }

    public function saveHeaderImage(Request $request)
    {
        $module = Module::where('id','=',$request->id)->first();
        $module->header_image = $request->header_image;
        $module->save();

        return Response::json(['status' => 'success']);
    }

    public function removeHeaderImage(Request $request)
    {
        $module = Module::where('id','=',$request->id)->first();
        $module->header_image = null;
        $module->save();

        return Response::json(['status' => 'success']);
    }
	
	public function contacts()
    {
		$email = Setting::where('key','=','contact-email')->first(); 
        $details = Setting::where('key', '=', 'contact-details')->first();
		$map = Setting::where('key','=','contact-map')->first();
		
        return view('admin/settings/contacts', array(
			'email' => $email,   
            'details' => $details,
			'map' => $map
        ));
    }
	
	public function contactsUpdate(Request $request)
    {
        $email = Setting::where('key','=','contact-email')->first();
		$email->value = $request->email;
		$email->save();		
		
        $details = Setting::where('key', '=', 'contact-details')->first();
        $details->value = $request->details;
        $details->save();
		
		$map = Setting::where('key', '=', 'contact-map')->first();
        $map->value = $request->map;
        $map->save();

        return \Redirect::to('dreamcms/settings/contacts')->with('message', Array('text' => 'Details has been updated', 'status' => 'success'));
    }

	public function navigation(Request $request)
    {
        // Initialise Navigation (if required)
        $this->initialiseNavigation();

        $general = new General();
        $modules = $general->getNavigation();

        return view('admin/settings/navigation', array(
            'navigation' => $modules
        ));
    }

    public function navigationv2()
    {

        $modules = Module::where('status','=','active')->where('slug','!=','pages')->where('slug','!=','orders')->orderBy('position', 'desc')->get();
        $categories = array();

        foreach($modules as $module){
            $module->show = (new NavigationHelper())->navigationItems('existence', 'module', $module->slug, $module->slug);
        }

        //Documents Module
        $document_module = Module::where('slug','=','documents')->first();
        if($document_module->status=='active'){

            $document_categories = DocumentCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($document_categories as $document_category){
                $document_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $document_module->slug, $document_category->slug);
                $document_category->module_slug = $document_module->slug;
            }
            $categories[$document_module->display_name] = $document_categories;
        }

        //FAQs Module
        $faqs_module = Module::where('slug','=','faqs')->first();
        if($faqs_module->status=='active'){
            $faqs_categories = FaqCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($faqs_categories as $faqs_category){
                $faqs_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $faqs_module->slug, $faqs_category->slug);
                $faqs_category->module_slug = $faqs_module->slug;
            }
            $categories[$faqs_module->display_name] = $faqs_categories;
        }

        //Gallery Module
        $gallery_module = Module::where('slug','=','gallery')->first();
        if($gallery_module->status=='active'){

            $gallery_categories = GalleryCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($gallery_categories as $gallery_category){
                $gallery_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $gallery_module->slug, $gallery_category->slug);
                $gallery_category->module_slug = $gallery_module->slug;
            }
            $categories[$gallery_module->display_name] = $gallery_categories;
        }

        //News Module
        $news_module = Module::where('slug','=','news')->first();
        if($news_module->status=='active'){

            $news_categories = NewsCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($news_categories as $news_category){
                $news_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $news_module->slug, $news_category->slug);
                $news_category->module_slug = $news_module->slug;
            }
            $categories[$news_module->display_name] = $news_categories;
        }

        //Pages Module
        $pages_module = Module::where('slug','=','pages')->first();
        if($pages_module->status=='active'){

            $pages_categories = PageCategory::where('status','=','active')->where('members_only','=','passive')->orderBy('created_at', 'desc')->get();
            foreach($pages_categories as $pages_category){
                $pages_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $pages_module->slug, $pages_category->slug);
                $pages_category->module_slug = $pages_module->slug;
            }
            $categories[$pages_module->display_name] = $pages_categories;
        }

        //Products Module
        $products_module = Module::where('slug','=','products')->first();
        if($products_module->status=='active'){

            $products_categories = ProductCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($products_categories as $products_category){
                $products_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $products_module->slug, $products_category->slug);
                $products_category->module_slug = $products_module->slug;
            }
            $categories[$products_module->display_name] = $products_categories;
        }

        //Projects Module
        $projects_module = Module::where('slug','=','projects')->first();
        if($projects_module->status=='active'){

            $projects_categories = ProjectCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($projects_categories as $projects_category){
                $projects_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $projects_module->slug, $projects_category->slug);
                $projects_category->module_slug = $projects_module->slug;
            }
            $categories[$projects_module->display_name] = $projects_categories;
        }

        //Properties Module
        $properties_module = Module::where('slug','=','properties')->first();
        if($properties_module->status=='active'){

            $property_categories = PropertyCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($property_categories as $property_category){
                $property_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $properties_module->slug, $property_category->slug);
                $property_category->module_slug = $properties_module->slug;
            }
            $categories[$properties_module->display_name] = $property_categories;
        }

        //Team Module
        $team_module = Module::where('slug','=','team')->first();
        if($team_module->status=='active'){

            $team_categories = TeamCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($team_categories as $team_category){
                $team_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $team_module->slug, $team_category->slug);
                $team_category->module_slug = $team_module->slug;
            }
            $categories[$team_module->display_name] = $team_categories;
        }

        $pages = Page::where('status','=','active')->orderBy('category_id', 'desc')->get();

        foreach($pages as $page){
            $page->show = (new NavigationHelper())->navigationItems('existence', 'page', 'pages', $page->slug);
        }
		
		foreach($pages as $page){			
			if ($page->category->members_only == "active") {
				$existence_control[0] = true;
                $page->show = $existence_control;			
			}
        }

        return view('admin/settings/navigationv2', array(
            'modules' => $modules,
            'categories' => $categories,
            'pages' => $pages
        ));
    }

    public function getNavigation(Request $request)
    {
        $navigation = Navigation::where('id', '=', 1)->first();
        return Response::json(['navigation' => json_decode($navigation->navigation)]);
    }

    public function saveNavigation(Request $request)
    {
        $navigation = Navigation::where('id', '=', 1)->first();
        $navigation->navigation = $request->navigation;
        $navigation->save();

        (new NavigationHelper())->navigationUpdatePagesPosition();

        return Response::json(['status' => 'success']);
    }

	private function initialiseNavigation()  {
		$totalModules = sizeOf(Module::get()->toArray());				
		$totalModulesWithNoPosition = sizeOf(Module::where('position', '=', '0')->get()->toArray());								
		
		if ($totalModules == $totalModulesWithNoPosition)  {
		   // Initialisation is required for modules only.				   
		   $modules = Module::get();
		   $position = $this->getNextPosition();
		   foreach ($modules as $module)  {
			   if ($module->name != "Pages" && $module->name != "Settings" && $module->name != "Member")  {
				   $module->position = $position;
				   $module->save();

				   $position++;			   
			   }
		   }			  			
		}		
	}
	
	private function getNextPosition()  {	
		$moduleMaxPosition = Module::orderBy('position', 'desc')->value('position');
		
		$pageCategoryMaxPosition = PageCategory::orderBy('position', 'desc')->value('position');
		$pageCategoryMaxPosition = (!isset($pageCategoryMaxPosition) ? 0 : $pageCategoryMaxPosition);
		
		$maxPosition = ($moduleMaxPosition > $pageCategoryMaxPosition ? $moduleMaxPosition : $pageCategoryMaxPosition);
		
		return ($maxPosition+1);		
	}
	
	public function changeTopMenuStatus(Request $request, $module_id)
    {
        $module = Module::where('id', '=', $module_id)->first();
        if ($request->tm_status == "true") {
            $module->top_menu = 'active';
        } else if ($request->tm_status == "false") {
            $module->top_menu = 'passive';
        }
        $module->save();

        return Response::json(['status' => 'success']);
    }
	
	public function navigationSort(Request $request)
    {
        $item_count = 1;
		
        foreach($request->item as $item_id){
			$item_details = explode("::", $item_id);
			
			if ($item_details[0] == "M") {
               $item = Module::where('id','=',$item_details[1])->first();            
			} else  {
			   $item = PageCategory::where('id','=',$item_details[1])->first();            
			}
			
			$item->position = $item_count;
            $item->save();	
			
            $item_count++;
        }
        return Response::json(['status' => 'success']);
    }
	
    public function homeSliders(Request $request)
    {
		$paginate_count = session()->get('pagination-count');
        $images = ImagesHomeSlider::orderBy('position', 'desc')->paginate($paginate_count);
        
        return view('admin/settings/home-sliders', array(
            'images' => $images,           
        ));
    }
	
	public function homeSlidersAdd()
    {        
        return view('admin/settings/add-home-slider', array(            
        ));
    }
	
	public function homeSlidersStore(Request $request)
    {
        $rules = array(                       
            'location' => 'required',
        );

        $messages = [              
            'location.required' => 'Please select an image',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/settings/add-home-slider')->withErrors($validator)->withInput();
        }

        $image = new ImagesHomeSlider();        
        $image->title = $request->title;
		$image->description = $request->description;
		$image->url = $request->url;
        $image->location = $request->location;        
        if($request->live=='on'){
           $image->status = 'active'; 
        }

        $image->position = ImagesHomeSlider::max('position')+1;
        $image->save();
      
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/settings/' . $image->id . '/edit-home-slider')->with('message', Array('text' => 'Image has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/settings/home-sliders')->with('message', Array('text' => 'Image has been added', 'status' => 'success'));
		}		        


    }

    public function homeSlidersEdit($image_id)
    {
        $image = ImagesHomeSlider::where('id', '=', $image_id)->first();        
        return view('admin/settings/edit-home-slider', array(
            'image' => $image,           
        ));
    }
	
	public function homeSlidersUpdate(Request $request)
    {
        $rules = array(            
            'location' => 'required'
        );

        $messages = [            
            'location.required' => 'Please select an image'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/settings/' . $request->id . '/edit-home-slider')->withErrors($validator)->withInput();
        }

        $image = ImagesHomeSlider::where('id','=',$request->id)->first();       
        $image->title = $request->title;
		$image->description = $request->description;
		$image->url = $request->url;
		$image->location = $request->location;        
		if($request->live=='on'){
           $image->status = 'active'; 
		} else {
			$image->status = 'passive';
        }
        $image->save();
       
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/settings/' . $image->id . '/edit-home-slider')->with('message', Array('text' => 'Image has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/settings/home-sliders')->with('message', Array('text' => 'Image has been updated', 'status' => 'success'));
		}
    }

    public function homeSlidersDelete($image_id)
    {
        ImagesHomeSlider::where('id', '=', $image_id)->delete();
        return \Redirect::to('dreamcms/settings/home-sliders')->with('message', Array('text' => 'Image has been deleted', 'status' => 'success'));
    }
	
	public function homeSlidersChangeStatus(Request $request, $image_id)
    {
        $image = ImagesHomeSlider::where('id', '=', $image_id)->first();
        if ($request->status == "true") {
            $image->status = 'active';
        } else if ($request->status == "false") {
            $image->status = 'passive';
        }
        $image->save();

        return Response::json(['status' => 'success']);
    }

	public function homeSlidersImageSort(Request $request)
    {
        $image_count = count($request->item);

        foreach($request->item as $image_id){
            $image = ImagesHomeSlider::where('id','=',$image_id)->first();
            $image->position = $image_count;
            $image->save();
            $image_count--;
        }
        return Response::json(['status' => 'success']);
    }
	
	public function socialMedia()
    {
        $social_facebook = Setting::where('key', '=', 'social-facebook')->first();
		$social_twitter = Setting::where('key', '=', 'social-twitter')->first();
		$social_linkedin = Setting::where('key', '=', 'social-linkedin')->first();
		$social_googleplus = Setting::where('key', '=', 'social-googleplus')->first();
		$social_instagram = Setting::where('key', '=', 'social-instagram')->first();
		$social_pinterest = Setting::where('key', '=', 'social-pinterest')->first();
		$social_youtube = Setting::where('key', '=', 'social-youtube')->first();
		
        return view('admin/settings/social-media', array(
            'social_facebook' => $social_facebook,
			'social_twitter' => $social_twitter,
			'social_linkedin' => $social_linkedin,
			'social_googleplus' => $social_googleplus,
			'social_instagram' => $social_instagram,
			'social_pinterest' => $social_pinterest,
			'social_youtube' => $social_youtube,
        ));
    }
	
	public function socialMediaUpdate(Request $request)
    {

        $social_facebook = Setting::where('key','=','social-facebook')->first();
		$social_facebook->value = $request->social_facebook;
		$social_facebook->save();	
		
		$social_twitter = Setting::where('key','=','social-twitter')->first();
		$social_twitter->value = $request->social_twitter;
		$social_twitter->save();	
		
		$social_linkedin = Setting::where('key','=','social-linkedin')->first();
		$social_linkedin->value = $request->social_linkedin;
		$social_linkedin->save();	
		
		$social_googleplus = Setting::where('key','=','social-googleplus')->first();
		$social_googleplus->value = $request->social_googleplus;
		$social_googleplus->save();
		
		$social_instagram = Setting::where('key','=','social-instagram')->first();
		$social_instagram->value = $request->social_instagram;
		$social_instagram->save();
		
		$social_pinterest = Setting::where('key','=','social-pinterest')->first();
		$social_pinterest->value = $request->social_pinterest;
		$social_pinterest->save();
		
		$social_youtube = Setting::where('key','=','social-youtube')->first();
		$social_youtube->value = $request->social_youtube;
		$social_youtube->save();

        return \Redirect::to('dreamcms/settings/social-media')->with('message', Array('text' => 'Details has been updated', 'status' => 'success'));
    }
	
	public function homePageMembers(){        
        $home_members_intro_text = Setting::where('key','=','home-members-intro-text')->first();		

        return view('admin/settings/home-members-page', array(
            'home_members_intro_text' => $home_members_intro_text,			
        ));

    }

    public function homePageMembersUpdate(Request $request){

        $home_members_intro_text = Setting::where('key','=','home-members-intro-text')->first();
        $home_members_intro_text->value = $request->home_members_intro_text;
        $home_members_intro_text->save();
				
        return \Redirect::to('dreamcms/settings/home-page-members')->with('message', Array('text' => 'Values has been updated.', 'status' => 'success'));
    }
	
	// Members Navigation
	public function navigationMembers()
    {

        $modules = Module::where('status','=','active')->where('slug','!=','pages')->where('slug','!=','orders')->orderBy('position', 'desc')->get();
        $categories = array();

        foreach($modules as $module){
            $module->show = (new NavigationHelper())->navigationItems('existence', 'module', $module->slug, $module->slug, "members");
        }

        //Documents Module
        $document_module = Module::where('slug','=','documents')->first();
        if($document_module->status=='active'){

            $document_categories = DocumentCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($document_categories as $document_category){
                $document_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $document_module->slug, $document_category->slug, "members");
                $document_category->module_slug = $document_module->slug;
            }
            $categories[$document_module->display_name] = $document_categories;
        }

        //FAQs Module
        $faqs_module = Module::where('slug','=','faqs')->first();
        if($faqs_module->status=='active'){
            $faqs_categories = FaqCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($faqs_categories as $faqs_category){
                $faqs_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $faqs_module->slug, $faqs_category->slug, "members");
                $faqs_category->module_slug = $faqs_module->slug;
            }
            $categories[$faqs_module->display_name] = $faqs_categories;
        }

        //Gallery Module
        $gallery_module = Module::where('slug','=','gallery')->first();
        if($gallery_module->status=='active'){

            $gallery_categories = GalleryCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($gallery_categories as $gallery_category){
                $gallery_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $gallery_module->slug, $gallery_category->slug, "members");
                $gallery_category->module_slug = $gallery_module->slug;
            }
            $categories[$gallery_module->display_name] = $gallery_categories;
        }

        //News Module
        $news_module = Module::where('slug','=','news')->first();
        if($news_module->status=='active'){

            $news_categories = NewsCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($news_categories as $news_category){
                $news_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $news_module->slug, $news_category->slug, "members");
                $news_category->module_slug = $news_module->slug;
            }
            $categories[$news_module->display_name] = $news_categories;
        }

        //Pages Module
        $pages_module = Module::where('slug','=','pages')->first();
        if($pages_module->status=='active'){

            $pages_categories = PageCategory::where('status','=','active')->where('members_only','=','active')->orderBy('created_at', 'desc')->get();
            foreach($pages_categories as $pages_category){
                $pages_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $pages_module->slug, $pages_category->slug, "members");
                $pages_category->module_slug = $pages_module->slug;
            }
            $categories[$pages_module->display_name] = $pages_categories;
        }

        //Products Module
        $products_module = Module::where('slug','=','products')->first();
        if($products_module->status=='active'){

            $products_categories = ProductCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($products_categories as $products_category){
                $products_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $products_module->slug, $products_category->slug, "members");
                $products_category->module_slug = $products_module->slug;
            }
            $categories[$products_module->display_name] = $products_categories;
        }

        //Projects Module
        $projects_module = Module::where('slug','=','projects')->first();
        if($projects_module->status=='active'){

            $projects_categories = ProjectCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($projects_categories as $projects_category){
                $projects_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $projects_module->slug, $projects_category->slug, "members");
                $projects_category->module_slug = $projects_module->slug;
            }
            $categories[$projects_module->display_name] = $projects_categories;
        }

        //Properties Module
        $properties_module = Module::where('slug','=','properties')->first();
        if($properties_module->status=='active'){

            $property_categories = PropertyCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($property_categories as $property_category){
                $property_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $properties_module->slug, $property_category->slug, "members");
                $property_category->module_slug = $properties_module->slug;
            }
            $categories[$properties_module->display_name] = $property_categories;
        }

        //Team Module
        $team_module = Module::where('slug','=','team')->first();
        if($team_module->status=='active'){

            $team_categories = TeamCategory::where('status','=','active')->orderBy('created_at', 'desc')->get();
            foreach($team_categories as $team_category){
                $team_category->show = (new NavigationHelper())->navigationItems('existence', 'category', $team_module->slug, $team_category->slug, "members");
                $team_category->module_slug = $team_module->slug;
            }
            $categories[$team_module->display_name] = $team_categories;
        }

        $pages = Page::with('category')->where('status','=','active')->orderBy('category_id', 'desc')->get();

        foreach($pages as $page){			
            $page->show = (new NavigationHelper())->navigationItems('existence', 'page', 'pages', $page->slug);			
        }
		
		foreach($pages as $page){			
			if ($page->category->members_only != "active") {
				$existence_control[0] = true;
                $page->show = $existence_control;			
			}
        }
		
        return view('admin/settings/navigation-members', array(
            'modules' => $modules,
            'categories' => $categories,
            'pages' => $pages
        ));
    }

    public function getNavigationMembers(Request $request)
    {
        $navigation = Navigation::where('id', '=', 2)->first();
        return Response::json(['navigation' => json_decode($navigation->navigation)]);
    }

    public function saveNavigationMembers(Request $request)
    {
        $navigation = Navigation::where('id', '=', 2)->first();
        $navigation->navigation = $request->navigation;
        $navigation->save();

        (new NavigationHelper())->navigationUpdatePagesPosition();

        return Response::json(['status' => 'success']);
    }	

}