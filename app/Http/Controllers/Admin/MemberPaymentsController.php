<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\MembersInitialApproveMessageAdmin;
use App\Mail\MembersInitialApproveMessageUser;
use App\Mail\MembersRenewApproveMessageAdmin;
use App\Mail\MembersRenewApproveMessageUser;
use App\MemberPayment;
use App\Module;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Exports\MembersPaymentsExport;
use Maatwebsite\Excel\Facades\Excel;

class MemberPaymentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'members')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $payments = MemberPayment::Filter()->sortable()->with('member')->orderBy('id', 'desc')->paginate($paginate_count);
        } else {
            $payments = MemberPayment::sortable()->with('member')->orderBy('id', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('member-payment-filter');

        return view('admin/members/payments', array(
            'payments' => $payments,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function editPayment($payment_id)
    {
        $payment = MemberPayment::where('id','=',$payment_id)->first();

        return view('admin/members/edit-payment', array(
            'payment' => $payment
        ));
    }

    public function updatePayment(Request $request)
    {		
        $payment = MemberPayment::where('id','=',$request->id)->first();
        $payment->payment_status = $request->payment_status;
        $payment->payment_transaction_number = $request->payment_transaction_number;

        if($request->payment_status=='completed'){

            if($payment->is_processed=='false'){

                if($payment->payment_method == 'bank-deposit' && $payment->payment_type == 'renew'){

                    // New Expire Date
                    $expire_date = Carbon::parse($payment->member->dateExpire);
                    if($expire_date->isPast()){
                        $new_expire_date = Carbon::now()->addYear()->format('Y-m-d');
                    }else{
                        $new_expire_date = $expire_date->addYear()->format('Y-m-d');
                    }

                    $payment->member->dateExpire = $new_expire_date;

                    $this->sendBankDepositRenewalApproveEmail($payment->member, $payment);
                }

                if($payment->payment_method == 'bank-deposit' && $payment->payment_type == 'initial'){

                    //Initial Payment Approved
                    $this->sendBankDepositInitialApproveEmail($payment->member);
                }
				
                $payment->member->status = 'active';
                $payment->member->save();

                $payment->is_processed = 'true';
            }
        }

		$payment->created_at = date('Y-m-d', strtotime(str_replace("/", "-", $request->created_at)));
        $payment->save();

        return \Redirect::to('dreamcms/members/payments')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
    }

    public function emptyFilter()
    {
        session()->forget('member-payment-filter');
        return redirect()->to('dreamcms/members/payments');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->payment_type && $request->payment_type != "all") {
            $filter_control = true;
        }

        if ($request->payment_method && $request->payment_method != "all") {
            $filter_control = true;
        }

        if ($request->payment_status && $request->payment_status != "all") {
            $filter_control = true;
        }

        if($request->daterange){
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('member-payment-filter', [
                'payment_type' => $request->payment_type,
                'payment_method' => $request->payment_method,
                'payment_status' => $request->payment_status,
                'daterange' => $request->daterange
            ]);
        }

        if (session()->has('member-payment-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

    private function sendBankDepositInitialApproveEmail($member)
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new MembersInitialApproveMessageAdmin($member));

        // Email User
        Mail::to($member->email)->send(new MembersInitialApproveMessageUser($member));

    }

    private function sendBankDepositRenewalApproveEmail($member, $payment)
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new MembersRenewApproveMessageAdmin($member, $payment));

        // Email User
        Mail::to($member->email)->send(new MembersRenewApproveMessageUser($member, $payment));
    }
	
	public function export() 
    {
       return Excel::download(new MembersPaymentsExport, 'members-payment' . date('YmdGis') . '.xlsx');
    }
	
	public function delete($payment_id)
    {
        $payment = MemberPayment::where('id','=',$payment_id)->first();
        $payment->is_deleted = true;
        $payment->save();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }


}