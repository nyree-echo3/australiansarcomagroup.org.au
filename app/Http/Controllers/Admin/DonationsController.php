<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Module;
use App\Donation;
use App\DonationCategory;
use App\Helpers\General;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

use App\Exports\DonationsExport;
use Maatwebsite\Excel\Facades\Excel;

class DonationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'donations')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $donations = Donation::Filter()->orderBy('created_at', 'desc')->paginate($paginate_count);
        } else {
            $donations = Donation::where('payment_status','=','completed')
                ->orWhere('payment_status','=','regular_payment_cancelled')
                ->orderBy('created_at', 'desc')
                ->paginate($paginate_count);
        }

        $session = session()->get('donations-filter');

        return view('admin/donations/donations', array(
            'donations' => $donations,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));

    }
	
	public function read($donation_id)
    {
        $previous = Donation::where('id', '<', $donation_id)->max('id');
        $next = Donation::where('id', '>', $donation_id)->min('id');

        $donation = Donation::where('id', '=', $donation_id)->first();        
        $donation->save();

        return view('admin/donations/read', array(
            'donation' => $donation,
            'previous' => $previous,
            'next' => $next
        ));
    }
    
    public function categories()
    {
        $categories = DonationCategory::orderBy('id', 'desc')->get();
        return view('admin/donations/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/donations/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            //'title' => 'required',
            'amount' => 'required',            
        );

        $messages = [
            //'title.required' => 'Please enter category title.',
			'amount.required' => 'Please enter category amount.',           
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/donations/add-category')->withErrors($validator)->withInput();
        }

        $category = new DonationCategory();
        $category->title = $request->title;
        $category->amount = $request->amount;
		if ($request->type == 'on') {
            $category->type = 'regular';
        }
        if ($request->live == 'on') {
            $category->status = 'active';
        }
        $category->save();               

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/donations/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/donations/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        }

    }

    public function editCategory($category_id)
    {
        $category = DonationCategory::where('id', '=', $category_id)->first();       

        return view('admin/donations/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            //'title' => 'required',
            'amount' => 'required',            
        );

        $messages = [
            //'title.required' => 'Please enter category title.',
            'amount.required' => 'Please enter category amount.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/donations/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = DonationCategory::findOrFail($request->id);
       
		$category->title = $request->title;
        $category->amount = $request->amount; 
		if ($request->type == 'on') {
            $category->type = 'regular';
        } else  {
			$category->type = 'once';
		}
        $category->save();

		
        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/donations/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/donations/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
        }
    }

    public function deleteCategory($category_id)
    {
        $category = DonationCategory::where('id', '=', $category_id)->first();

        if (count($category->news)) {
            return \Redirect::to('dreamcms/donations/categories')->with('message', Array('text' => 'Category has news. Please delete news first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();
        

        return \Redirect::to('dreamcms/donations/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }
	
	public function formBuilder()
    {
        $form = Setting::where('key', '=', 'donation-form-fields')->first();

        return view('admin/donations/form-builder', array(
            'form' => $form->value
        ));
    }

    public function saveForm(Request $request)
    {
        $name_field_control = false;
        $email_field_control = false;

        $fields = json_decode($request->form);
        foreach ($fields as $field) {
            if ($field->type == 'text' && $field->name == 'name') {
                $name_field_control = true;
            }

            if ($field->type == 'text' && $field->subtype == 'email' && $field->name == 'email') {
                $email_field_control = true;
            }
        }

        if(!$name_field_control){
            return Response::json([
                'status' => 'fail',
                'message' => 'Please create a Text Field with the name parameter is "name"'
            ]);
        }

        if(!$email_field_control){
            return Response::json([
                'status' => 'fail',
                'message' => 'Please create a Text Field with the name parameter is "email" and the type is "email"'
            ]);
        }

        $form = Setting::where('key', '=', 'donation-form-fields')->first();
        $form->value = strip_tags($request->form);
        $form->save();

        return Response::json(['status' => 'success']);
    }

    public function cancelRegularPayment($donation_id)
    {
        $donation = Donation::where('id','=',$donation_id)->first();

        $provider = new ExpressCheckout;
        $response = $provider->cancelRecurringPaymentsProfile($donation->paypal_profile_id);

        if($response['ACK']=='Success'){

            $donation->payment_status = 'regular_payment_cancelled';
            $donation->save();

            return \Redirect::to('dreamcms/donations/'.$donation_id.'/read')->with('message', Array('text' => 'Regular payment has been cancelled', 'status' => 'success'));
        }


        return \Redirect::to('dreamcms/donations/'.$donation_id.'/read')->with('message', Array('text' => 'Error! Please try again.', 'status' => 'error'));
    }

    public function getDetails(Request $request)
    {
        $donation = Donation::where('id','=',$request->id)->first();

        if(!$donation){
            return Response::json(['status' => 'fail']);
        }

        $provider = new ExpressCheckout;
        $profile_details = $provider->getRecurringPaymentsProfileDetails($donation->paypal_profile_id);

        $table = '<div class="table-responsive">'.
            '<h5><strong>Payment Details</strong></h5>'.
            ' <table class="table">'.
            '<tr>'.
            '<th style="width:20%">Status :</th>'.
            '<td>'.$profile_details['STATUS'].'</td>'.
            '</tr>'.
            '<tr>'.
            '<th style="width:20%">Description :</th>'.
            '<td>'.$profile_details['DESC'].'</td>'.
            '</tr>'.
            '<tr>'.
            '<th style="width:20%">Amount :</th>'.
            '<td>'.$profile_details['AMT']." ".$profile_details['CURRENCYCODE'].'</td>'.
            '</tr>'.
            '<tr>'.
            '<th style="width:20%">Profile Start Date :</th>'.
            '<td>'.Carbon::createFromTimeString($profile_details['PROFILESTARTDATE'])->format('d-m-Y H:i:s').'</td>'.
            '</tr>'.
            '<tr>'.
            '<th style="width:20%">Last Payment Date :</th>'.
            '<td>'.Carbon::createFromTimeString($profile_details['LASTPAYMENTDATE'])->format('d-m-Y H:i:s').'</td>'.
            '</tr>';

            if($profile_details['STATUS']=='Active'){
                $table = $table.'<tr>'.
                '<th style="width:20%">Next Billing Date :</th>'.
                '<td>'.Carbon::createFromTimeString($profile_details['NEXTBILLINGDATE'])->format('d-m-Y H:i:s').'</td>'.
                '</tr>';
            }

            $table = $table.'<tr>'.
            '<th style="width:20%">Cycles Completed :</th>'.
            '<td>'.$profile_details['NUMCYCLESCOMPLETED'].'</td>'.
            '</tr>'.
            '<tr>'.
            '<th style="width:20%">Failed Payment Count :</th>'.
            '<td>'.$profile_details['FAILEDPAYMENTCOUNT'].'</td>'.
            '</tr>'.
            '</table>'.
            '</div>';

        return Response::json([
            'status' => 'success',
            'table' => $table
        ]);
    }

    public function emptyFilter()
    {
        session()->forget('donations-filter');
        return redirect()->to('dreamcms/donations');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('donations-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('donations-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }
	
	public function export() 
    {
       return Excel::download(new DonationsExport, 'donations' . date('YmdGis') . '.xlsx');
    }

}