<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Link;
use App\LinkCategory;
use App\Module;
use App\Helpers\General;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class LinksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'links')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $links = Link::Filter()->sortable()->orderBy('updated_at', 'desc')->paginate($paginate_count);
        } else {
            $links = Link::with('category')->sortable()->orderBy('updated_at', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('links-filter');
        $categories = LinkCategory::orderBy('created_at', 'desc')->get();
        return view('admin/links/links', array(
            'links' => $links,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('links-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('links-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

    public function add()
    {
        $categories = LinkCategory::orderBy('created_at', 'desc')->get();      
        return view('admin/links/add', array(
            'categories' => $categories,
        ));
    }

    public function edit($link_id)
    {
        $link = Link::where('id', '=', $link_id)->first();       

        $categories = LinkCategory::orderBy('created_at', 'desc')->get();
       
        return view('admin/links/edit', array(
            'link' => $link,
            'categories' => $categories,            
        ));
    }  

    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',         
			'category_id' => 'required',         
            'image' => 'required',                       
			'url' => 'required',  
        );

        $messages = [
            'name.required' => 'Please enter name',           
			'category_id.required' => 'Please select category',        
            'image.required' => 'Please select image',  
			'url.required' => 'Please enter url',
        ];
       
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/links/add')->withErrors($validator)->withInput();
        }

        $link = new Link();

        $link->category_id = $request->category_id;
        $link->name = $request->name;        
        $link->description = $request->description;
		$link->url = $request->url;
        $link->image = $request->image;        
		
        if ($request->live == 'on') {
            $link->status = 'active';
        }

        $link->save();
       
        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/links/' . $link->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/links/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        }
    }

    public function update(Request $request)
    {
        $rules = array(
            'name' => 'required',   
			'category_id' => 'required',                   
            'image' => 'required',
            'url' => 'required',            
        );

        $messages = [
            'name.required' => 'Please enter name',
            'category_id.required' => 'Please select category',        
            'image.required' => 'Please select image',   
			'url.required' => 'Please enter url',
        ];        

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/links/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $link = Link::where('id', '=', $request->id)->first();
       
        $link->category_id = $request->category_id;
        $link->name = $request->name;        
        $link->description = $request->description;
		$link->url = $request->url;
        $link->image = $request->image;        
		
        if ($request->live == 'on') {
            $link->status = 'active';
        } else {
            $link->status = 'passive';
        }
        $link->save();        

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/links/' . $link->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/links/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        }
    }

    public function delete($link_id)
    {
        $link = Link::where('id', '=', $link_id)->first();
        $link->is_deleted = true;
        $link->save();
       
        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $link_category_id)
    {
        $link_category = LinkCategory::where('id', '=', $link_category_id)->first();
        if ($request->status == "true") {
            $link_category->status = 'active';          
        } else if ($request->status == "false") {
            $link_category->status = 'passive';           
        }
        $link_category->save();

        return Response::json(['status' => 'success']);
    }

    public function changeLinkStatus(Request $request, $link_id)
    {
        $link = Link::where('id', '=', $link_id)->first();
        if ($request->status == "true") {
            $link->status = 'active';
            $link->save();           

        } else if ($request->status == "false") {          
            $link->status = 'passive';
            $link->save();
        }


        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $links = Link::where('status', '=', 'active')->orderBy('position', 'desc')->get();

        return view('admin/links/sort', array(
            'links' => $links
        ));
    }

    public function categories()
    {
        $categories = LinkCategory::orderBy('created_at', 'desc')->get();
        return view('admin/links/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/links/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',            

        );

        $messages = [
            'name.required' => 'Please enter category name.',            
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/links/add-category')->withErrors($validator)->withInput();
        }

        $category = new LinkCategory();
        $category->name = $request->name;       
        $category->position = $this->getNextPosition();
        $category->save();
     
        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/links/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/links/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        }

    }

    private function getNextPosition()
    {
        $moduleMaxPosition = Module::orderBy('position', 'desc')->value('position');

        $linkCategoryMaxPosition = linkCategory::orderBy('position', 'desc')->value('position');
        $linkCategoryMaxPosition = (!isset($linkCategoryMaxPosition) ? 0 : $linkCategoryMaxPosition);

        $maxPosition = ($moduleMaxPosition > $linkCategoryMaxPosition ? $moduleMaxPosition : $linkCategoryMaxPosition);

        return ($maxPosition + 1);
    }

    public function editCategory($category_id)
    {
        $category = LinkCategory::where('id', '=', $category_id)->first();
        
        return view('admin/links/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',           
        );

        $messages = [
            'name.required' => 'Please enter category name.',            
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/links/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = LinkCategory::findOrFail($request->id);
        
        $category->name = $request->name;        
        $category->save();
       
        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/links/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/links/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
        }
    }

    public function deleteCategory($category_id)
    {
        $category = LinkCategory::where('id', '=', $category_id)->first();

        if (count($category->links)) {
            return \Redirect::to('dreamcms/links/categories')->with('message', Array('text' => 'Category has links. Please delete links first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();
       
        return \Redirect::to('dreamcms/links/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function emptyFilter()
    {
        session()->forget('links-filter');
        return redirect()->to('dreamcms/links');
    }

}