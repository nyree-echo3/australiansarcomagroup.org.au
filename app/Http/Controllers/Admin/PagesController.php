<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Page;
use App\PageCategory;
use App\Module;
use App\Helpers\General;

use App\SpecialUrl;
use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'pages')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $pages = Page::Filter()->sortable()->orderBy('updated_at', 'desc')->paginate($paginate_count);
        } else {
            $pages = Page::with('category')->sortable()->orderBy('updated_at', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('pages-filter');
        $categories = PageCategory::orderBy('created_at', 'desc')->get();
        return view('admin/pages/pages', array(
            'pages' => $pages,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('pages-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('pages-filter')) {
            $filter_control = true;
        }

        if (!$filter_control) {
            $request->session()->put('pages-filter', [
                'category' => null,
                'search' => null
            ]);
        }

        return $filter_control;
    }

    public function add()
    {
        $categories = PageCategory::orderBy('created_at', 'desc')->get();
        $parent_pages = Page::where('page_type', '=', 'parent')->orderBy('created_at', 'desc')->get();
        return view('admin/pages/add', array(
            'categories' => $categories,
            'parent_pages' => $parent_pages
        ));
    }

    public function edit($page_id)
    {
        $page = Page::where('id', '=', $page_id)->first();

        $page->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $page->id)->where('module', '=', 'pages')->where('type', '=', 'item')->first();
        if ($special_url) {
            $page->special_url = $special_url->url;
        }

        $categories = PageCategory::orderBy('created_at', 'desc')->get();
        $parent_pages = Page::where('page_type', '=', 'parent')->orderBy('created_at', 'desc')->get();
        return view('admin/pages/edit', array(
            'page' => $page,
            'categories' => $categories,
            'parent_pages' => $parent_pages
        ));
    }

    public function preview($page_id)
    {
        $page = Page::with("category")->where('id', '=', $page_id)->first();

        $general = new General();
        $view = $general->pagePreview($page->category->slug, $page->slug);

        return ($view);
    }

    public function store(Request $request)
    {
        $rules = array(
            'title' => 'required',
            'slug' => 'required|unique_store:pages',
            'page_type' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'body' => 'required',
            //'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store',
			'special_url' => 'nullable|special_url_store',
        );

        $messages = [
            'title.required' => 'Please enter title',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_store' => 'The SEO Name is already taken',
            'page_type.required' => 'Please select page type',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'body.required' => 'Please enter body',
            //'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        if ($request->page_type == 'parent') {
            $rules['category_id'] = 'required';
            $messages['category_id.required'] = 'Please select category';
        }

        if ($request->page_type == 'child') {
            $rules['parent_page_id'] = 'required';
            $messages['parent_page_id.required'] = 'Please select parent page';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/pages/add')->withErrors($validator)->withInput();
        }

        $page = new Page();

        if ($request->page_type == 'parent') {
            $page->category_id = $request->category_id;
        }

        if ($request->page_type == 'child') {
            $page->parent_page_id = $request->parent_page_id;
            $page->category_id = Page::where('id', '=', $request->parent_page_id)->value('category_id');
        }

        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->page_type = $request->page_type;
        $page->meta_title = $request->meta_title;
        $page->meta_keywords = $request->meta_keywords;
        $page->meta_description = $request->meta_description;
        $page->body = $request->body;
        $page->thumbnail = $request->thumbnail;
		$page->header = $request->header;
		
        if ($request->live == 'on') {
            $page->status = 'active';
        }

        $page->save();

        ///////////special URL//
        if ($request->special_url != "") {
            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $page->id;
            $new_special_url->module = 'pages';
            $new_special_url->type = 'item';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();

        }
        ////////////////////////

        //(new NavigationHelper())->navigationUpdateCategoryPages($page->category_id);

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/pages/' . $page->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/pages/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        }
    }

    public function update(Request $request)
    {
        $rules = array(
            'title' => 'required',
            'slug' => 'required|unique_update:pages,' . $request->id,
            'page_type' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'body' => 'required',
            //'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:pages,item,' . $request->id,
			'special_url' => 'nullable|special_url_update:pages,item,' . $request->id,
        );

        $messages = [
            'title.required' => 'Please enter title',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique' => 'The SEO Name is already taken',
            'page_type.required' => 'Please select page type',
            'meta_title.required' => 'Please enter meta title',
            'meta_keywords.required' => 'Please enter meta keywords',
            'meta_description.required' => 'Please enter meta desciption',
            'body.required' => 'Please enter body',
            //'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        if ($request->page_type == 'parent') {
            $rules['category_id'] = 'required';
            $messages['category_id.required'] = 'Please select category';
        }

        if ($request->page_type == 'child') {
            $rules['parent_page_id'] = 'required';
            $messages['parent_page_id.required'] = 'Please select parent page';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/pages/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $page = Page::where('id', '=', $request->id)->with('childPages')->first();

        //(new NavigationHelper())->navigationItems('update', 'page', 'pages', $page->slug, null, $request->title, $request->slug);

        if (count($page->childPages) != 0 && $request->page_type == 'child') {
            return \Redirect::to('dreamcms/pages/' . $request->id . '/edit')->with('message', Array('text' => 'Due to this page has child pages, cannot be updated as child page.', 'status' => 'error'));
        }

        if ($request->page_type == 'parent') {

            $old_category_id = $page->category_id;

            $page->category_id = $request->category_id;
            $page->parent_page_id = NULL;
        }

        if ($request->page_type == 'child') {
            $page->parent_page_id = $request->parent_page_id;
            $page->category_id = Page::where('id', '=', $request->parent_page_id)->value('category_id');
        }

        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->page_type = $request->page_type;
        $page->meta_title = $request->meta_title;
        $page->meta_keywords = $request->meta_keywords;
        $page->meta_description = $request->meta_description;
        $page->body = $request->body;
		$page->thumbnail = $request->thumbnail;
		$page->header = $request->header;
		
        if ($request->live == 'on') {
            $page->status = 'active';
        } else {
            $page->status = 'passive';
        }
        $page->save();

        if ($request->page_type == 'parent') {

            if($old_category_id!=$request->category_id){
               // (new NavigationHelper())->navigationUpdateCategoryPages($old_category_id);
                //(new NavigationHelper())->navigationUpdateCategoryPages($request->category_id);
            }

        }

        (new NavigationHelper())->navigationItems('change-href', 'page', 'pages', $page->slug, $page->url);

        if (count($page->childPages) != 0) {
            $page->childPages()->update(['category_id' => $request->category_id]);
        }

        ///////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $page->id)->where('module', '=', 'pages')->where('type', '=', 'item')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'page', 'pages', $page->slug, $request->special_url);
            } else {
                $special_url->delete();

                (new NavigationHelper())->navigationItems('change-href', 'page', 'pages', $page->slug, 'pages/'.$page->category->slug.'/'.$page->slug);
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $page->id;
                $new_special_url->module = 'pages';
                $new_special_url->type = 'item';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'page', 'pages', $page->slug, $request->special_url);
            }
        }
        ////////////////////////

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/pages/' . $page->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/pages/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        }
    }

    public function delete($page_id)
    {
        $page = Page::where('id', '=', $page_id)->first();
        $page->is_deleted = true;
        $page->save();

        (new NavigationHelper())->navigationItems('delete-item', 'page', 'pages', $page->slug);

        SpecialUrl::where('item_id', '=', $page->id)->where('module', '=', 'pages')->where('type', '=', 'item')->delete();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $page_category_id)
    {
        $page_category = PageCategory::where('id', '=', $page_category_id)->first();
        if ($request->status == "true") {
            $page_category->status = 'active';

            //(new NavigationHelper())->navigationAddPagesCategory($page_category->name, $page_category->url, 'category-'.$page_category->id, $page_category->slug);

        } else if ($request->status == "false") {
            $page_category->status = 'passive';

            (new NavigationHelper())->navigationItems('delete-item', 'category', 'pages', $page_category->slug);
        }
        $page_category->save();

        return Response::json(['status' => 'success']);
    }

    public function changePageStatus(Request $request, $page_id)
    {
        $page = Page::where('id', '=', $page_id)->first();
        if ($request->status == "true") {
            $page->status = 'active';
            $page->save();

            //(new NavigationHelper())->navigationUpdateCategoryPages($page->category_id);

        } else if ($request->status == "false") {

            (new NavigationHelper())->navigationItems('delete-item', 'page', 'pages', $page->slug);

            $page->status = 'passive';
            $page->save();
        }


        return Response::json(['status' => 'success']);
    }

    public function sort(Request $request)
    {
        $categories = PageCategory::orderBy('created_at', 'asc')->get();
        $category_id = $categories->first()->id;

        if(isset($request->category)){
            $category_id = $request->category;
        }

        $pages = Page::where('status', '=', 'active')
            ->where('category_id','=',$category_id)
            ->where('parent_page_id','=',null)
            ->sorted()
            ->get();

        return view('admin/pages/sort', array(
            'pages' => $pages,
            'categories' => $categories,
            'selected_category_id' => $category_id
        ));
    }

    function childSort($parent_page_id){

        $parent_page = Page::where('id','=',$parent_page_id)->first();
        $child_pages = Page::where('parent_page_id','=',$parent_page_id)->sorted()->get();

        return view('admin/pages/child-sort', array(
            'parent_page' => $parent_page,
            'pages' => $child_pages
        ));
    }

    public function categories()
    {
        $categories = PageCategory::orderBy('created_at', 'desc')->get();
        return view('admin/pages/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/pages/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:page_categories',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store',

        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_store' => 'The SEO Name is already taken',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/pages/add-category')->withErrors($validator)->withInput();
        }

        $category = new PageCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->header = $request->header;
        $category->position = $this->getNextPosition();
		if ($request->members_only == 'on') {
            $category->members_only = 'active';
        }
        $category->save();

        ///////////special URL//
        if ($request->special_url != "") {
            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $category->id;
            $new_special_url->module = 'pages';
            $new_special_url->type = 'category';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();

        }
        ////////////////////////
        if ($request->members_only != 'on') {
           //(new NavigationHelper())->navigationAddItem($request->name, $category->url, 'category-'.$category->id, 'pages', 'category', $category->slug);
		} else  {
		  // (new NavigationHelper())->navigationAddItem($request->name, $category->url, 'category-'.$category->id, 'pages', 'category', $category->slug, "members");
		}		
		
        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/pages/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/pages/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
        }

    }

    private function getNextPosition()
    {
        $moduleMaxPosition = Module::orderBy('position', 'desc')->value('position');

        $pageCategoryMaxPosition = PageCategory::orderBy('position', 'desc')->value('position');
        $pageCategoryMaxPosition = (!isset($pageCategoryMaxPosition) ? 0 : $pageCategoryMaxPosition);

        $maxPosition = ($moduleMaxPosition > $pageCategoryMaxPosition ? $moduleMaxPosition : $pageCategoryMaxPosition);

        return ($maxPosition + 1);
    }

    public function editCategory($category_id)
    {
        $category = PageCategory::where('id', '=', $category_id)->first();

        $category->special_url = "";
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'pages')->where('type', '=', 'category')->first();
        if ($special_url) {
            $category->special_url = $special_url->url;
        }

        return view('admin/pages/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:page_categories,' . $request->id,
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:pages,category,' . $request->id,
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_update' => 'The SEO Name is already taken',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/pages/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = PageCategory::findOrFail($request->id);
		
		$menu_type = ($request->members_only != 'on' ? "main" : "members");	
		$menu_type_prev = ($category->members_only != 'active' ? "main" : "members");	
		
		if ($menu_type != $menu_type_prev)  {			
		   (new NavigationHelper())->navigationItems('delete-item', 'category', 'pages', $category->slug, NULL, NULL, NULL, NULL, $menu_type_prev);
		   //(new NavigationHelper())->navigationAddItem($request->name, $category->url, 'category-'.$category->id, 'pages', 'category', $category->slug, $menu_type);
		}
								
        //(new NavigationHelper())->navigationItems('update', 'category', 'pages', $category->slug, null, $request->name, $request->slug, $menu_type);

        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->header = $request->header;
		if ($request->members_only == 'on') {
            $category->members_only = 'active';
        } else  {
			$category->members_only = 'passive';
		}
        $category->save();

        (new NavigationHelper())->navigationItems('change-href', 'category', 'pages', $category->slug, $category->url, $menu_type);

        ///////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'pages')->where('type', '=', 'category')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'pages', $category->slug, $request->special_url, $menu_type);
            } else {
                $special_url->delete();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'pages', $category->slug, 'pages/'.$category->slug, $menu_type);
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $category->id;
                $new_special_url->module = 'pages';
                $new_special_url->type = 'category';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'pages', $category->slug, $request->special_url, $menu_type);
            }
        }
        ////////////////////////

        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/pages/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/pages/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
        }
    }

    public function navigationPageSort()
    {
        $page_categories =  PageCategory::orderBy('created_at', 'desc')->get();        
		
        foreach ($page_categories as $page_category){
			$menu_type = ($page_category->members_only != 'on' ? "main" : "members");
            //(new NavigationHelper())->navigationUpdateCategoryPages($page_category->id, $menu_type);
        }

        return Response::json(['status' => 'success']);
    }

    public function deleteCategory($category_id)
    {
        $category = PageCategory::where('id', '=', $category_id)->first();

        if (count($category->pages)) {
            return \Redirect::to('dreamcms/pages/categories')->with('message', Array('text' => 'Category has pages. Please delete pages first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'pages')->where('type', '=', 'category')->delete();

		$menu_type = ($category->members_only != 'active' ? "main" : "members");		
        (new NavigationHelper())->navigationItems('delete-item', 'category', 'pages', $category->slug, NULL, NULL, NULL, NULL, $menu_type);

        return \Redirect::to('dreamcms/pages/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function emptyFilter()
    {
        session()->forget('pages-filter');
        return redirect()->to('dreamcms/pages');
    }

}