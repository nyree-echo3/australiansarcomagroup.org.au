<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            'billpayer.firstname'  => 'required|min:2|max:255',
            'billpayer.lastname'  => 'required|min:2|max:255',
			'billpayer.email'  => 'required|string|email|max:255',
						
			'billpayer.address.address'  => 'required|min:2|max:255',
			'billpayer.address.city'  => 'required|min:2|max:255',
			'billpayer.address.province_id'  => 'required',
            'billpayer.address.postalcode'  => 'required|min:4|max:4'
        ];
    }

    /**
     * @inheritDoc
     */
    public function authorize()
    {
        return true;
    }
}
