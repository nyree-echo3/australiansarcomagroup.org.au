<?php

namespace App\Http\Middleware;
use App\Module;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckModule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $slug)
    {

        if($slug=='modules'){

            if(Auth::user()->role=='admin'){
                return $next($request);
            }else{
                return \Redirect::to('dreamcms/dashboard');
            }
        }

        $module = Module::where('slug', '=', $slug)->first();

        if($module->status == 'active'){
            return $next($request);
        }

        return \Redirect::to('dreamcms/dashboard');
    }
}
