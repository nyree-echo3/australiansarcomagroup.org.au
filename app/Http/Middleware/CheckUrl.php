<?php

namespace App\Http\Middleware;

use App\DocumentCategory;
use App\FaqCategory;
use App\GalleryCategory;
use App\News;
use App\NewsCategory;
use App\Page;
use App\PageCategory;
use App\ProductCategory;
use App\Products;
use App\Project;
use App\ProjectCategory;
use App\Property;
use App\PropertyCategory;
use App\SpecialUrl;
use App\TeamCategory;
use App\TeamMember;
use App\Events;
use App\EventsCategory;
use Closure;
use Illuminate\Http\Request;

class CheckUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->segment(1)!='dreamcms'){

            $special_url = SpecialUrl::where('url', '=',  $request->path())->first();

            if($special_url){

                ///documents module
                if($special_url->module == 'documents'){

                    $documents_controller = new \App\Http\Controllers\Site\DocumentsController();

                    if($special_url->type=='category'){

                        $document_category = DocumentCategory::where('id','=', $special_url->item_id)->first();
                        return response($documents_controller->index($document_category->slug));
                    }
                }
                ////////

                ///FAQ module
                if($special_url->module == 'faq'){

                    $faq_controller = new \App\Http\Controllers\Site\FaqsController();

                    if($special_url->type=='category'){

                        $faq_category = FaqCategory::where('id','=', $special_url->item_id)->first();
                        return response($faq_controller->index($faq_category->slug));
                    }
                }
                ////////

                ///Gallery module
                if($special_url->module == 'gallery'){

                    $gallery_controller = new \App\Http\Controllers\Site\GalleryController();

                    if($special_url->type=='category'){

                        $gallery_category = GalleryCategory::where('id','=', $special_url->item_id)->first();
                        return response($gallery_controller->detail($gallery_category->slug));
                    }
                }
                ////////

                ///news module
                if($special_url->module == 'news'){

                    $news_controller = new \App\Http\Controllers\Site\NewsController();

                    if($special_url->type=='item'){

                        $news = News::where('id','=', $special_url->item_id)->first();
                        return response($news_controller->item('', $news->slug));
                    }

                    if($special_url->type=='category'){

                        $news_category = NewsCategory::where('id','=', $special_url->item_id)->first();
                        return response($news_controller->list($news_category->slug));
                    }
                }
                ////////

                ///pages module
                if($special_url->module == 'pages'){

                    $pages_controller = new \App\Http\Controllers\Site\PagesController();

                    if($special_url->type=='item'){

                        $page = Page::where('id','=', $special_url->item_id)->first();
                        return response($pages_controller->index($page->category->slug, $page->slug));
                    }

                    if($special_url->type=='category'){

                        $page_category = PageCategory::where('id','=', $special_url->item_id)->first();
                        return response($pages_controller->index($page_category->slug));
                    }
                }
                ////////


                ///products module
                if($special_url->module == 'products'){

                    $products_controller = new \App\Http\Controllers\Site\ProductsController();

                    if($special_url->type=='item'){

                        $product = Products::where('id','=', $special_url->item_id)->first();
                        return response($products_controller->item($product->category->slug, $product->slug));
                    }

                    if($special_url->type=='category'){

                        $product_category = ProductCategory::where('id','=', $special_url->item_id)->first();
                        return response($products_controller->list($product_category->slug));
                    }
                }
                ////////

                ///projects module
                if($special_url->module == 'projects'){

                    $projects_controller = new \App\Http\Controllers\Site\ProjectsController();

                    if($special_url->type=='item'){

                        $project = Project::where('id','=', $special_url->item_id)->first();
                        return response($projects_controller->item($project->category->slug, $project->slug));
                    }

                    if($special_url->type=='category'){

                        $project_category = ProjectCategory::where('id','=', $special_url->item_id)->first();
                        return response($projects_controller->list($project_category->slug));
                    }
                }
                ////////

                ///properties module
                if($special_url->module == 'properties'){

                    $properties_controller = new \App\Http\Controllers\Site\PropertiesController();

                    if($special_url->type=='item'){

                        $property = Property::where('id','=', $special_url->item_id)->first();
                        return response($properties_controller->item($property->category->slug, $property->slug));
                    }

                    if($special_url->type=='category'){

                        $property_category = PropertyCategory::where('id','=', $special_url->item_id)->first();
                        return response($properties_controller->list($property_category->slug));
                    }
                }
                ////////

                ///team module
                if($special_url->module == 'team'){

                    $team_controller = new \App\Http\Controllers\Site\TeamController();

                    if($special_url->type=='item'){

                        $team_member = TeamMember::where('id','=', $special_url->item_id)->first();
                        return response($team_controller->member($team_member->category->slug, $team_member->slug));
                    }

                    if($special_url->type=='category'){

                        $team_category = TeamCategory::where('id','=', $special_url->item_id)->first();

                        $parameter = new Request();
                        $parameter->category = $team_category->slug;

                        return response($team_controller->index($parameter));
                    }
                }
                ////////
				
				///events module
                if($special_url->module == 'events'){

                    $events_controller = new \App\Http\Controllers\Site\EventsController();

					if($special_url->type=='item'){

                        $events = Events::where('id','=', $special_url->item_id)->first();
                        return response($events_controller->item('', $events->slug));
                    }
					
                    if($special_url->type=='category'){

                        $event_category = EventCategory::where('id','=', $special_url->item_id)->first();
                        return response($events_controller->index($event_category->slug));
                    }
                }
                ////////


            }
            return $next($request);
        }
        return $next($request);
    }
}
