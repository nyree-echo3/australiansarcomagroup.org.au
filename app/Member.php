<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Notifications\MemberResetPasswordNotification;

class Member extends Authenticatable //Model
{

    use Sortable;
    use Notifiable;

    public $sortable = ['firstName', 'lastName', 'email', 'suburb', 'type_id', 'status'];
    protected $table = 'members';
    protected $guard = 'member';

    protected $fillable = [
        'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted', '=', 'false');
    }

    public function type()
    {
        return $this->belongsTo(MemberType::class, 'type_id');
    }

    public function typesort()
    {
        return $this->hasOne(MemberType::class, 'id', 'type_id');
    }

    public function payments()
    {
        return $this->hasMany(MemberPayment::class, 'member_id')->orderBy('created_at', 'desc');
    }

    public function InitialPayment()
    {
        return $this->hasOne(MemberPayment::class, 'member_id')->where('payment_type','=','initial');
    }

    public function getFullNameAttribute()
    {
        return $this->attributes['firstName'].' '.$this->attributes['lastName'];
    }

    public function hasPendingRenewalPayment()
    {
        return MemberPayment::where('member_id','=',$this->attributes['id'])
            ->where('payment_type', '=', 'renew')
            ->where('payment_status','=','pending')
            ->exists();
    }

    public function isExpired()
    {
        $expire_date = Carbon::parse($this->attributes['dateExpire']);
        if($expire_date->isPast()){
            return true;
        }

        return false;
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('members-filter');
        $select = $query;

        if ($filter['type'] && $filter['type'] != "all") {
            $select = $query->where('type_id', $filter['type']);
        }

        if ($filter['search']) {
            $select = $query
                ->where('firstName', 'like', '%' . $filter['search'] . '%')
                ->orWhere('lastName', 'like', '%' . $filter['search'] . '%');
        }

        return $select;
    }

    //Send password reset notification
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MemberResetPasswordNotification($token));
    }
	
	public function qualifications()
    {
        return $this->hasMany(MemberQualification::class, 'member_id');
    }
	
	public function awards()
    {
        return $this->hasMany(MemberAward::class, 'member_id');
    }
}
