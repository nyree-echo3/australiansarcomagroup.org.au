<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkCategory extends Model
{
    protected $table = 'link_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function links()
    {
        return $this->hasMany(Link::class, 'category_id')->where('status', '=', 'active')->orderBy('position', 'desc');
    }
		
}
