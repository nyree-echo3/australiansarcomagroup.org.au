<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class EventsBooking extends Model
{
    use Sortable;

    public $sortable = ['event_id', 'member_id', 'created_at'];
    protected $table = 'events_bookings';

	public function newQuery()
    {
        return parent::newQuery()->where('is_deleted', '=', 'false');
    }
	
    public function event()
    {
        return $this->belongsTo(Events::class, 'event_id');
    }
	
	public function ticket()
    {
        return $this->belongsTo(EventsTicket::class, 'ticket_id');
    }
	
	public function payment()
    {
        
		return $this->hasOne(EventsBookingPayment::class, 'booking_id');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('registration-filter');
        $select = $query;

        if($filter['event_id'] && $filter['event_id']!="all"){
            $select =  $query->with('payment')->where('event_id', $filter['event_id']);
        }  
		
		if($filter['payment_method'] && $filter['payment_method']!="all"){
			$filter_payment_method = $filter['payment_method'];
			           
			$select =  $query->whereHas('payment', 
							function($q) use($filter_payment_method) {				
                            	$q->where('payment_method', '=', $filter_payment_method);
							});
        }

        if($filter['payment_status'] && $filter['payment_status']!="all"){
			$filter_payment_status = $filter['payment_status'];
			           
			$select =  $query->whereHas('payment', 
							function($q) use($filter_payment_status) {				
                            	$q->where('payment_status', '=', $filter_payment_status);
							});			            
        }

        if($filter['daterange']){
            $range_arr = explode(" - ",$filter['daterange']);
            $start = Carbon::createFromFormat('d/m/Y',$range_arr[0])->format('Y-m-d');
            $finish = Carbon::createFromFormat('d/m/Y',$range_arr[1])->format('Y-m-d');

            $select =  $query->with('payment')->whereBetween('created_at',[$start.' 00:00:00',$finish.' 23:59:59']);
        }
		
		if ($filter['search']) {
            $select = $query
                ->where('first_name', 'like', '%' . $filter['search'] . '%')
                ->orWhere('last_name', 'like', '%' . $filter['search'] . '%');;
        }
        
        return $select;
    }

}
