<?php 
   // Set Meta Tags
   $meta_title_inner = "Register | " . $company_name; 
   $meta_keywords_inner = "Register " . $company_name; 
   $meta_description_inner = "Register " . $company_name;  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">                                 
        <div class="col-lg-8 blog-main blog-wide">

          <div class="blog-post">   
               <h1 class="blog-post-title">Membership</h1>    
               @include('flash::message')                                                      
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection            
