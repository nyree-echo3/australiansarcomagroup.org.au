<?php
// Set Meta Tags
$meta_title_inner = "Apply for Membership | " . $company_name;
$meta_keywords_inner = "Apply for Membership " . $company_name;
$meta_description_inner = "Apply for Membership " . $company_name;
?>

@extends('site.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/register-steps.css') }}">
@endsection
@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-lg-12 blog-main blog-wide">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Apply for Membership</h1>
                        
                        <p><a href='{{ url('')}}/login'>Already a member yet? Please log in here.</a></p>
                             
                        @if (isset($errors) && sizeof($errors) > 0)
                            <div class='alert-info'>
                               <span class='alert-error'><i class="fas fa-exclamation"></i> Some issues have occurred with your details.  Please review each section to amend the issues.</span>
                            </div>
                        @endif
                        
						<form id="frmRegister" enctype="multipart/form-data" method="POST" action="{{ url('') }}/apply/store">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">           

							<h5>Membership Class</h5>
							<section>
							    <h2>Membership Class Applied For *</h2>
							    
								<div class="form-group row">                                
                                <div class="col-md-12">
                                    <select class="form-control" name="membershipType" required>
                                        <option value="">Choose your membership class </option>
                                        @foreach ($member_types as $member_type)
                                            <option value="{{ $member_type->id }}" {{ (old('membershipType') == $member_type->id ? " selected" : "") }}>{{ $member_type->name }}
                                                - ${{ $member_type->price }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('state'))
                                        <div class="fv-plugins-message-container">
                                            <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('state') }}</div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="pages-membership">
								<div class="mj_tab" id="tab1559097030638">
								<div class="mj_tab_items">
								<div class="mj_accordion_item active" data-tab="#tab1">Ordinary Member</div>

								<div class="mj_accordion_item" data-tab="#tab2">Associate Member</div>

								<div class="mj_accordion_item" data-tab="#tab3">Corresponding Member</div>
								</div>

								<div class="mj_tab_contents">
								<div class="mj_accordion_content active" id="tab1">
								<p><strong>Eligibility</strong></p>

								<ul>
									<li>Qualified clinicians and researchers in sarcoma and related tumours</li>
									<li>Consumers and patient advocates</li>
									<li>Resident of Australia or New Zealand</li>
								</ul>

								<p><strong>Fee:</strong> AUD$150 | AUD$10 (consumer and patient advocates only)</p>
								</div>

								<div class="mj_accordion_content" id="tab2">
								<p><strong>Eligibility</strong></p>

								<ul>
									<li>Trainee clinicians, researchers, fellows</li>
									<li>Resident of Australia or New Zealand</li>
								</ul>

								<p><strong>Fee:</strong> AUD$100</p>
								</div>

								<div class="mj_accordion_content" id="tab3">
								<p><strong>Eligibility</strong></p>

								<ul>
									<li>Qualified clinicians and researchers with an interest in sarcoma and related tumours</li>
									<li>Consumers and patient advocates</li>
									<li>Non-resident of Australia or New Zealand</li>
								</ul>

								<p><strong>Fee:</strong> AUD$50 | AUD$10 (consumer and patient advocates only)</p>
								</div>
								</div>
								</div>
								</div>

							</section>


							<h5>Personal Information</h5>
							<section>
							    <h2>Personal Information</h2>
							    
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Title *</label>
									<div class="col-md-9">
										<select class="form-control" name="title" id="title" required>
											<option value="">Your title</option>
											<option value="Mr" {{ (old('title') == "Mr") ? ' selected="selected"' : '' }}>Mr</option>
											<option value="Ms" {{ (old('title') == "Ms") ? ' selected="selected"' : '' }}>Ms</option>
											<option value="Mrs" {{ (old('title') == "Mrs") ? ' selected="selected"' : '' }}>Mrs</option>
											<option value="Dr" {{ (old('title') == "Dr") ? ' selected="selected"' : '' }}>Dr</option>
											<option value="Assc. Professor" {{ (old('title') == "Assc. Professor") ? ' selected="selected"' : '' }}>Assc. Professor</option>
											<option value="Professor" {{ (old('title') == "Professor") ? ' selected="selected"' : '' }}>Professor</option>
										</select>
										@if ($errors->has('title'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('title') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">First Name *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="firstName" placeholder="Your first name" value="{{ old('firstName') }}" required />
										@if ($errors->has('firstName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('firstName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Last Name *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="lastName" placeholder="Your last name" value="{{ old('lastName') }}" required />
										@if ($errors->has('lastName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('lastName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Former Names<br>(if applicable)</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="formerName" placeholder="Your former names" value="{{ old('formerName') }}" />
										@if ($errors->has('formerName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('formerName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Date of Birth *</label>
									<div class="col-md-9">
										<input name="dob" type="text" class="form-control pull-right datepicker" placeholder="DD/MM/YYYY"  value="{{ old('dob') }}" required>
										@if ($errors->has('dob'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('dob') }}</div>
											</div>
										@endif
									</div>
								</div>
							</section>


							<h5>Personal Contact Details</h5>
							<section>
							    <h2>Personal Contact Details</h2>
							    
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Address *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="address1" placeholder="Your address (line 1)" value="{{ old('address1') }}" required />
										@if ($errors->has('address1'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('addresss1') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label"></label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="address2" placeholder="Your address (line 2)" value="{{ old('address2') }}" />
										@if ($errors->has('address2'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('address2') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Suburb *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="suburb" placeholder="Your suburb" value="{{ old('suburb') }}" required />
										@if ($errors->has('suburb'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('suburb') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">State *</label>
									<div class="col-md-4">
										<select class="form-control" name="state" required>
											<option value="">Your state</option>
											<option value="ACT" {{ (old('state') == "ACT") ? ' selected="selected"' : '' }}>
												ACT
											</option>
											<option value="NSW" {{ (old('state') == "NSW") ? ' selected="selected"' : '' }}>
												NSW
											</option>
											<option value="NT" {{ (old('state') == "NT") ? ' selected="selected"' : '' }}>
												NT
											</option>
											<option value="QLD" {{ (old('state') == "QLD") ? ' selected="selected"' : '' }}>
												QLD
											</option>
											<option value="SA" {{ (old('state') == "SA") ? ' selected="selected"' : '' }}>
												SA
											</option>
											<option value="TAS" {{ (old('state') == "TAS") ? ' selected="selected"' : '' }}>
												TAS
											</option>
											<option value="VIC" {{ (old('state') == "VIC") ? ' selected="selected"' : '' }}>
												VIC
											</option>
											<option value="WA" {{ (old('state') == "WA") ? ' selected="selected"' : '' }}>
												WA
											</option>
											<option value="NA" {{ (old('state') == "NA") ? ' selected="selected"' : '' }}>
												Not Applicable
											</option>
										</select>

										@if ($errors->has('state'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('state') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Postcode *</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="postcode" placeholder="Your postcode" value="{{ old('postcode') }}" required />
										@if ($errors->has('postcode'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('postcode') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Country *</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="country" placeholder="Your country" value="{{ old('country') }}" required />
										@if ($errors->has('country'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('country') }}</div>
											</div>
										@endif
									</div>
								</div>


								<div class="form-group row">
									<label class="col-md-3 col-form-label">Phone Number</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="phoneLandline" placeholder="Your phone number (10 digits with no spaces)" value="{{ old('phoneLandline') }}"/>
										@if ($errors->has('phoneLandline'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('phoneLandline') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Mobile *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="phoneMobile" placeholder="Your mobile number (10 digits with no spaces)" value="{{ old('phoneMobile') }}" required />
										@if ($errors->has('phoneMobile'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('phoneMobile') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Email Address *</label>
									<div class="col-md-9">
										<input type="email" class="form-control" name="email" placeholder="Your email address" value="{{ old('email') }}" required />
										@if ($errors->has('email'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('email') }}</div>
											</div>
										@endif
									</div>
								</div>
							</section>							

							<h5>Qualifications Details</h5>
							<section>
								<h2>Qualifications Details</h2>
                                <h3>Please specify institutions and year of graduation</h3>
                                
                                <div id="divQualifications">
                                    @php                                     
                                       if (old('hidQualifications') != "")  {
									      $totalQualifications = old('hidQualifications');
									   } else  {
									      $totalQualifications = 1;
									   }
									@endphp
									
									@for ($i = 1; $i <= $totalQualifications; $i++)
										<div class="form-group row">  									    
											<label class="col-md-4 col-form-label">Qualification #{{ $i }}</label>

											<div class="col-md-8">
												<input type="text" class="form-control members-qualification" name="qualification{{ $i }}" placeholder="Qualification" value="{{ old('qualification' . $i) }}"/>
												<input type="text" class="form-control members-institution" name="institution{{ $i }}" placeholder="Institution" value="{{ old('institution' . $i) }}"/>
												<input type="text" class="form-control members-graduationYear" name="graduationYear{{ $i }}" placeholder="Graduation year" value="{{ old('graduationYear' . $i) }}"/>

												@if ($errors->has('qualification' . $i))
													<div class="fv-plugins-message-container">
														<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('qualification' . $i) }}</div>
													</div>
												@endif
												@if ($errors->has('institution' . $i))
													<div class="fv-plugins-message-container">
														<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('institution' . $i) }}</div>
													</div>
												@endif
												@if ($errors->has('graduationYear' . $i))
													<div class="fv-plugins-message-container">
														<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('graduationYear' . $i) }}</div>
													</div>
												@endif
											</div>
										</div> 
									@endfor                  
								</div>  

								<div class="form-group row">  
									<label class="col-md-4 col-form-label"></label>     
									<div class="col-md-8">
										<a href="#" id="btnAddQualification">+ Add another qualification</a> 
										<input type="hidden" id="hidQualifications" name="hidQualifications" value="{{ $totalQualifications }}"/>     
									</div>                                                                
								</div>

								<div id="divAwards">
								    @php
                                       if (old('hidAwards') != "")  {
									      $totalAwards = old('hidAwards');
									   } else  {
									      $totalAwards = 1;
									   }
									@endphp
									
									@for ($i = 1; $i <= $totalAwards; $i++)
										<div class="form-group row">  
											<label class="col-md-4 col-form-label">Honours & Awards #{{ $i }}</label>

											<div class="col-md-8">
												<input type="text" class="form-control members-awards" name="awards{{ $i }}" placeholder="Honours and Awards" value="{{ old('awards' . $i) }}"/>										

												@if ($errors->has('awards'))
													<div class="fv-plugins-message-container">
														<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('qualification' . $i) }}</div>
													</div>
												@endif										
											</div>
										</div> 
									@endfor                       
								</div>  

								<div class="form-group row">  
									<label class="col-md-4 col-form-label"></label>     
									<div class="col-md-8">
										<a href="#" id="btnAddAwards">+ Add another honours & awards</a> 
										<input type="hidden" id="hidAwards" name="hidAwards" value="{{ $totalAwards }}"/>     
									</div>                                                                
								</div>
							</section>


							<h5>Employment Details</h5>
							<section>
								<h2>Employment Details</h2>
                                <h3>Please detail your current relevant employment (if any)</h3>
                                
                                <div class="form-group row">
									<label class="col-md-3 col-form-label">Position *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="occupation" placeholder="Your position" value="{{ old('occupation') }}"  required/>
										@if ($errors->has('occupation'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('occupation') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Employer *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="companyName" placeholder="Your employer"  value="{{ old('companyName') }}" required />
										@if ($errors->has('companyName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('companyName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Date of Commencement *</label>
									<div class="col-md-9">
										<input type="text" class="form-control pull-right datepicker" placeholder="DD/MM/YYYY" name="employmentDateCommencement" value="{{ old('employmentDateCommencement') }}" required />
										@if ($errors->has('employmentDateCommencement'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentDateCommencement') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Address *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="employmentAddress1" placeholder="Your address (line 1)" value="{{ old('employmentAddress1') }}" required />
										@if ($errors->has('employmentAddress1'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentAddresss1') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label"></label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="employmentAddress2" placeholder="Your address (line 2)" value="{{ old('address2') }}"/>
										@if ($errors->has('employmentAddress2'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentAddress2') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Suburb *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="employmentSuburb" placeholder="Your suburb" value="{{ old('employmentSuburb') }}" required />
										@if ($errors->has('employmentSuburb'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentSuburb') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">State *</label>
									<div class="col-md-4">
										<select class="form-control" name="employmentState" required>
											<option value="">Your state</option>
											<option value="ACT" {{ (old('employmentState') == "ACT") ? ' selected="selected"' : '' }}>
												ACT
											</option>
											<option value="NSW" {{ (old('employmentState') == "NSW") ? ' selected="selected"' : '' }}>
												NSW
											</option>
											<option value="NT" {{ (old('employmentState') == "NT") ? ' selected="selected"' : '' }}>
												NT
											</option>
											<option value="QLD" {{ (old('employmentState') == "QLD") ? ' selected="selected"' : '' }}>
												QLD
											</option>
											<option value="SA" {{ (old('employmentState') == "SA") ? ' selected="selected"' : '' }}>
												SA
											</option>
											<option value="TAS" {{ (old('employmentState') == "TAS") ? ' selected="selected"' : '' }}>
												TAS
											</option>
											<option value="VIC" {{ (old('employmentState') == "VIC") ? ' selected="selected"' : '' }}>
												VIC
											</option>
											<option value="WA" {{ (old('employmentState') == "WA") ? ' selected="selected"' : '' }}>
												WA
											</option>
											<option value="NA" {{ (old('employmentState') == "NA") ? ' selected="selected"' : '' }}>
												Not Applicable
											</option>
										</select>

										@if ($errors->has('employmentState'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentState') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Postcode *</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="employmentPostcode" placeholder="Your postcode" value="{{ old('employmentPostcode') }}" required />
										@if ($errors->has('employmentPostcode'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentPostcode') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Country *</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="employmentCountry" placeholder="Your country" value="{{ old('employmentCountry') }}" required />
										@if ($errors->has('employmentCountry'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentCountry') }}</div>
											</div>
										@endif
									</div>
								</div>


								<div class="form-group row">
									<label class="col-md-3 col-form-label">Phone Number</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="employmentPhoneNumber"
											   placeholder="Your phone number (10 digits with no spaces)"
											   value="{{ old('employmentPhoneNumber') }}"/>
										@if ($errors->has('employmentPhoneNumber'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentPhoneNumber') }}</div>
											</div>
										@endif
									</div>
								</div>                                                        

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Email Address *</label>
									<div class="col-md-9">
										<input type="email" class="form-control" name="employmentEmail" placeholder="Your email address" value="{{ old('employmentEmail') }}" required />
										@if ($errors->has('employmentEmail'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentEmail') }}</div>
											</div>
										@endif
									</div>
								</div>                       
							</section>


							<h5>Other Relevant Experience</h5>
							<section>
								<h2>Other Relevant Experience</h2>
						    	<h3>Please outline any other experience or contributions to the sarcoma community that may be relevant</h3>
							    								
								<div class="form-group row">                                
									<div class="col-md-12">
										<textarea class="form-control" type="textarea" rows="5" name="otherExperience" placeholder="Other experiences or contributions">{{ old('otherExperience') }}</textarea>
										@if ($errors->has('otherExperience'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('otherExperience') }}</div>
											</div>
										@endif
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Confirmation *</label>
									<div class="col-md-9">
										<input type="checkbox" class="form-control" name="confirmation" required />
										<label class="form-control-label" for="confirmation">I agree and acknowledge the
											<a href="{{ url('') }}/pages/other/membership-terms--conditions" target="_blank">terms & conditions</a> outlined in this agreement. Your submission of this form
											confirms that you have read and agree with our <a href="{{ url('') }}/pages/other/membership-terms--conditions" target="_blank">terms & conditions</a>.</label>

										@if ($errors->has('confirmation'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('confirmation') }}</div>
											</div>
										@endif
									</div>
								</div>          
							</section>

							<h5>Account Details</h5>
							<section>
							    <h2>Account Details</h2>
							    
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Password *</label>
									<div class="col-md-9">
										<input type="password" class="form-control" name="password" placeholder="Your password" required />

										<div id="progressBarDiv">
											Strength
											<div class="progress mt-2" id="progressBar">
												<div class="progress-bar bg-secondary progress-bar-animate"
													 style="width: 100%"></div>
											</div>
										</div>

										@if ($errors->has('password'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('password') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Confirm Password *</label>
									<div class="col-md-9">
										<input type="password" class="form-control" name="passwordConfirm" placeholder="Confirm your password" required />
									</div>
								</div>								

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Payment *</label>
									<div class="col-md-9 payment-options">
										<input type="radio" name="payment_method" value="paypal" checked>
										<img src="{{ url('') }}/images/site/Paypal.png" title="PayPal" alt="PayPal"><br />

										<input type="radio" name="payment_method" value="bank-deposit">
										<strong>Bank Deposit</strong><br />

										<label class="form-control-label"> {!!  $payment_direct_deposit_members !!} </label>
									</div>
								</div>

								<!--<div class="form-group row has-error">
									<label class="col-md-3 col-form-label"></label>
									<div class="col-md-4 g-recaptcha-container">
										<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
										@if ($errors->has('g-recaptcha-response'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
											</div>
										@endif
									</div>
								</div>								-->
							</section>

						</form>
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->

@endsection
@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('components/jquery.steps/build/jquery.steps.js') }}"></script>    
    <script src="{{ asset('components/theme/plugins/iCheck/icheck.min.js') }}"></script>       
    <script src="{{ asset('components/jquery-validation/dist/jquery.validate.js') }}"></script>  
    <script src="{{ asset('js/site/register-form.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
		$( document ).ready(function() {
			$('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
			
		    $( "#btnAddQualification" ).click(function(e) {		
			  qualificationNum = parseInt($( "#hidQualifications" ).val()) + 1;
			  $( "#hidQualifications" ).val(qualificationNum);
				
			  html  = '';
			  html += '<div class="form-group row">';  
			  html += '<label class="col-md-4 col-form-label">Qualification #' + qualificationNum + '</label>'; 
			  html += '<div class="col-md-8">'; 
			  html += '<input type="text" class="form-control members-qualification" name="qualification' + qualificationNum + '" placeholder="Qualification" value=""/>'; 
			  html += '<input type="text" class="form-control members-institution" name="institution' + qualificationNum + '" placeholder="Institution" value=""/>'; 
			  html += '<input type="text" class="form-control members-graduationYear" name="graduationYear' + qualificationNum + '" placeholder="Graduation year" value=""/>';       						
			  html += '</div>'; 
			  html += '</div>';   
				
			  $( "#divQualifications" ).append(html);	
				
			  e.preventDefault();
										
			});
			
			$( "#btnAddAwards" ).click(function(e) {		
			  awardsNum = parseInt($( "#hidAwards" ).val()) + 1;
			  $( "#hidAwards" ).val(awardsNum);
				
			  html  = '';
			  html += '<div class="form-group row">';  
			  html += '<label class="col-md-4 col-form-label">Honours & Awards #' + awardsNum + '</label>'; 
			  html += '<div class="col-md-8">'; 
			  html += '<input type="text" class="form-control members-awards" name="awards' + awardsNum + '" placeholder="Honours and Awards" value=""/>'; 			  					
			  html += '</div>'; 
			  html += '</div>';   
				
			  $( "#divAwards" ).append(html);	
				
			  e.preventDefault();
										
			});
			
		});
		
		
		
        document.addEventListener('DOMContentLoaded', function (e) {
            const strongPassword = function () {
                return {
                    validate: function (input) {
                        // input.value is the field value
                        // input.options are the validator options

                        const value = input.value;
                        if (value === '') {
                            return {
                                valid: true,
                            };
                        }

                        const result = zxcvbn(value);
                        const score = result.score;
                        const message = result.feedback.warning || 'The password is weak';

                        // By default, the password is treat as invalid if the score is smaller than 3
                        // We allow user to change this number via options.minimalScore
                        const minimalScore = input.options && input.options.minimalScore
                            ? input.options.minimalScore
                            : 3;

                        if (score < minimalScore) {
                            return {
                                valid: false,
                                // Yeah, this will be set as error message
                                message: message,
                                meta: {
                                    // This meta data will be used later
                                    score: score,
                                },
                            }
                        }
                    },
                };
            };

            const form = document.getElementById('frmRegister');
            FormValidation.formValidation(form, {
                    fields: {
                       
                        phoneNumber: {
                            validators: {
                                notEmpty: {
                                    message: 'The phone number is required'
                                },
                                digits: {
                                    message: 'The phone number is not valid.  It must only contain 10 digits with no spaces'
                                },
                                stringLength: {
                                    max: 10,
                                    min: 10,
                                    message: 'The phone number is not valid.  It must only contain 10 digits with no spaces'
                                }
                            }
                        },
                        firstName: {
                            validators: {
                                notEmpty: {
                                    message: 'The first name is required'
                                }
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'The email is required'
                                },
                                emailAddress: {
                                    message: 'The email is not valid'
                                }
                            }
                        },
                        address1: {
                            validators: {
                                notEmpty: {
                                    message: 'The address is required'
                                }
                            }
                        },
                        suburb: {
                            validators: {
                                notEmpty: {
                                    message: 'The suburb is required'
                                }
                            }
                        },
                        state: {
                            validators: {
                                notEmpty: {
                                    message: 'The state is required'
                                }
                            }
                        },
                        postcode: {
                            validators: {
                                notEmpty: {
                                    message: 'The postcode is required'
                                },
                                digits: {
                                    message: 'The postcode is not valid.  It must only contain 4 digits'
                                },
                                stringLength: {
                                    max: 4,
                                    min: 4,
                                    message: 'The phone number is not valid.  It must only contain 4 digits'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required'
                                },
                                checkPassword: {
                                    message: 'The password is too weak',
                                    minimalScore: 2,
                                }
                            }
                        },
                        passwordConfirm: {
                            validators: {
                                notEmpty: {
                                    message: 'The password confirmation is required'
                                },
                                identical: {
                                    compare: function () {
                                        return form.querySelector('[name="password"]').value;
                                    },
                                    message: 'The password and its confirm are not the same'
                                }
                            }
                        },

                        confirmation: {
                            validators: {
                                notEmpty: {
                                    message: 'The confirmation is required'
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fa fa-check',
                            invalid: 'fa fa-times',
                            validating: 'fa fa-refresh',
                        }),
                    },
                }
            )
                .registerValidator('checkPassword', strongPassword)

                .on('core.validator.validating', function (e) {
                    if (e.field === 'password' && e.validator === 'checkPassword') {
                        document.getElementById('progressBarDiv').style.opacity = '1';
                    }
                })
                .on('core.validator.validated', function (e) {
                    if (e.field === 'password' && e.validator === 'checkPassword') {
                        const progressBar = document.getElementById('progressBar');

                        if (e.result.meta) {
                            // Get the score which is a number between 0 and 4
                            const score = e.result.meta.score;

                            // Update the width of progress bar
                            const width = (score == 0) ? '1%' : score * 25 + '%';
                            progressBar.style.opacity = 1;
                            progressBar.style.width = width;
                        } else {
                            progressBar.style.opacity = 0;
                            progressBar.style.width = '0%';
                        }
                    }
                });
        });
		
		$.validator.addMethod("anyDate",
			function(value, element) {
				return value.match(/^(0?[1-9]|[12][0-9]|3[0-1])[/., -](0?[1-9]|1[0-2])[/., -](19|20)?\d{2}$/);
			},
			"Please enter a date in the format!"
		);				

		$("#frmRegister").validate({
		   rules : {
			  dob : { anyDate : true },
			  postcode: {digits: true, minlength: 4, maxlength: 4},
              phoneLandline: {digits: true, minlength: 10, maxlength: 10},
			  phoneMobile: {digits: true, minlength: 10, maxlength: 10},
			  employmentDateCommencement : { anyDate : true },
			  employmentPostcode: {digits: true, minlength: 4, maxlength: 4},
			  employmentPhoneNumber: {digits: true, minlength: 10, maxlength: 10},
			  //passwordConfirm: {equalTo : "#password"} 
		   }
		});
		
    </script>
@endsection
