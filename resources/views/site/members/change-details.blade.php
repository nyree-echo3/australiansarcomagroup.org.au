<?php
// Set Meta Tags
$meta_title_inner = "Change My Details | " . $company_name;
$meta_keywords_inner = "Change My Details " . $company_name;
$meta_description_inner = "Change My Details " . $company_name;
?>

@extends('site.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload-ui.css') }}">
	<link rel="stylesheet" href="{{ asset('components/intl-tel-input/build/css/intlTelInput.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/update-steps.css') }}">
@endsection
@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-members-portal')        
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post">            
            <h1 class="blog-post-title">Change My Details</h1>                                    
            @include('flash::message')   
                                              
						<form id="frmRegister" enctype="multipart/form-data" method="POST" action="{{ url('') }}/members-portal/save-details">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">           
							
							<h5>Personal Information</h5>
							<section>
							    <h2>Personal Information</h2>
							    
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Title *</label>
									<div class="col-md-9">
										<select class="form-control" name="title" id="title" required>
											<option value="">Your title</option>
											<option value="Mr" {{ (old('title',$member->title) == "Mr") ? ' selected="selected"' : '' }}>Mr</option>
											<option value="Ms" {{ (old('title',$member->title) == "Ms") ? ' selected="selected"' : '' }}>Ms</option>
											<option value="Mrs" {{ (old('title',$member->title) == "Mrs") ? ' selected="selected"' : '' }}>Mrs</option>
											<option value="Dr" {{ (old('title',$member->title) == "Dr") ? ' selected="selected"' : '' }}>Dr</option>
											<option value="Assc. Professor" {{ (old('title',$member->title) == "Assc. Professor") ? ' selected="selected"' : '' }}>Assc. Professor</option>
											<option value="Professor" {{ (old('title',$member->title) == "Professor") ? ' selected="selected"' : '' }}>Professor</option>
										</select>
										@if ($errors->has('title'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('title') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">First Name *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="firstName" placeholder="Your first name" value="{{ old('firstName',$member->firstName) }}" required />
										@if ($errors->has('firstName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('firstName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Last Name *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="lastName" placeholder="Your last name" value="{{ old('lastName',$member->lastName) }}" required />
										@if ($errors->has('lastName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('lastName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Former Names<br>(if applicable)</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="formerName" placeholder="Your former names" value="{{ old('formerName',$member->formerName) }}" />
										@if ($errors->has('formerName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('formerName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Date of Birth *</label>
									<div class="col-md-9">
										<input name="dob" type="text" class="form-control pull-right datepicker" placeholder="DD/MM/YYYY"  value="{{ old('dob',date("d/m/Y", strtotime($member->dob))) }}" required>
										@if ($errors->has('dob'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('dob') }}</div>
											</div>
										@endif
									</div>
								</div>
							</section>


							<h5>Personal Contact Details</h5>
							<section>
							    <h2>Personal Contact Details</h2>
							    
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Address *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="address1" placeholder="Your address (line 1)" value="{{ old('address1',$member->address1) }}" required />
										@if ($errors->has('address1'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('addresss1') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label"></label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="address2" placeholder="Your address (line 2)" value="{{ old('address2',$member->address2) }}" />
										@if ($errors->has('address2'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('address2') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Suburb *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="suburb" placeholder="Your suburb" value="{{ old('suburb',$member->suburb) }}" required />
										@if ($errors->has('suburb'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('suburb') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">State *</label>
									<div class="col-md-4">
										<select class="form-control" name="state" required>
											<option value="">Your state</option>
											<option value="ACT" {{ (old('state',$member->state) == "ACT") ? ' selected="selected"' : '' }}>
												ACT
											</option>
											<option value="NSW" {{ (old('state',$member->state) == "NSW") ? ' selected="selected"' : '' }}>
												NSW
											</option>
											<option value="NT" {{ (old('state',$member->state) == "NT") ? ' selected="selected"' : '' }}>
												NT
											</option>
											<option value="QLD" {{ (old('state',$member->state) == "QLD") ? ' selected="selected"' : '' }}>
												QLD
											</option>
											<option value="SA" {{ (old('state',$member->state) == "SA") ? ' selected="selected"' : '' }}>
												SA
											</option>
											<option value="TAS" {{ (old('state',$member->state) == "TAS") ? ' selected="selected"' : '' }}>
												TAS
											</option>
											<option value="VIC" {{ (old('state',$member->state) == "VIC") ? ' selected="selected"' : '' }}>
												VIC
											</option>
											<option value="WA" {{ (old('state',$member->state) == "WA") ? ' selected="selected"' : '' }}>
												WA
											</option>
											<option value="NA" {{ (old('state',$member->state) == "NA") ? ' selected="selected"' : '' }}>
												OTHER
											</option>
										</select>

										@if ($errors->has('state'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('state') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Postcode *</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="postcode" placeholder="Your postcode" value="{{ old('postcode',$member->postcode) }}" required />
										@if ($errors->has('postcode'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('postcode') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Country *</label>
									<div class="col-md-4">

										<select id="country" class="form-control" name="country" placeholder="Your country">
										</select>
										@if ($errors->has('country'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('country') }}</div>
											</div>
										@endif
									</div>
								</div>


								<div class="form-group row">
									<label class="col-md-3 col-form-label">Phone Number</label>
									<div class="col-md-9">
										<input type="text" class="form-control" id="phoneLandline" name="phoneLandline" value="{{ old('phoneLandline',$member->phoneLandline) }}"/>
										@if ($errors->has('phoneLandline'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('phoneLandline') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Mobile *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" id="phoneMobile" name="phoneMobile" value="{{ old('phoneMobile',$member->phoneMobile) }}" required />
										@if ($errors->has('phoneMobile'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('phoneMobile') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Email Address *</label>
									<div class="col-md-9">
										<input type="email" class="form-control" name="email" placeholder="Your email address" value="{{ old('email',$member->email) }}" required />
										@if ($errors->has('email'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('email') }}</div>
											</div>
										@endif
									</div>
								</div>
							</section>							

							<h5>Qualifications Details</h5>
							<section>
                                <h2>Qualifications Details (please specify institutions and year of graduation)</h2>
                                
                                <div id="divQualifications">
                                    @php 
                                       $counter = 0;
                                    @endphp
                                    
                                    @foreach($qualifications as $qualification)
                                       @php                                         
                                          $counter++;
                                       @endphp
                                       
										<div class="form-group row">  
											<label class="col-md-4 col-form-label">Qualification #{{ $counter }}</label>

											<div class="col-md-8">
												<input type="text" class="form-control members-qualification" name="qualification{{ $counter }}" placeholder="Qualification" value="{{ $qualification->qualification }}"/>
												<input type="text" class="form-control members-institution" name="institution{{ $counter }}" placeholder="Institution" value="{{ $qualification->institution }}"/>
												<input type="text" class="form-control members-graduationYear" name="graduationYear{{ $counter }}" placeholder="Graduation year" value="{{ $qualification->graduationYear }}"/>
												<input type="hidden" name="qualificationId{{ $counter }}" value="{{ $qualification->id }}">		
											</div>
										</div> 
									@endforeach                     
								</div>  

								<div class="form-group row">  
									<label class="col-md-4 col-form-label"></label>     
									<div class="col-md-8">
										<a href="#" id="btnAddQualification">+ Add another qualification</a> 
										<input type="hidden" id="hidQualifications" name="hidQualifications" value="{{ sizeof($qualifications) }}"/>     
									</div>                                                                
								</div>

								<div id="divAwards">
								    @php 
                                       $counter = 0;
                                    @endphp
                                    
                                    @foreach($awards as $award)
                                       @php                                         
                                          $counter++;
                                       @endphp
                                       
										<div class="form-group row">  
											<label class="col-md-4 col-form-label">Honours & Awards #{{ $counter }}</label>

											<div class="col-md-8">
												<input type="text" class="form-control members-awards" name="awards{{ $counter }}" placeholder="Honours and Awards" value="{{ $award->award }}"/>	
												<input type="hidden" name="awardId{{ $counter }}" value="{{ $award->id }}">				
											</div>
										</div>  
									@endforeach                      
								</div>  

								<div class="form-group row">  
									<label class="col-md-4 col-form-label"></label>     
									<div class="col-md-8">
										<a href="#" id="btnAddAwards">+ Add another honours & awards</a> 
										<input type="hidden" id="hidAwards" name="hidAwards" value="{{ sizeof($awards) }}"/>     
									</div>                                                                
								</div>
							</section>


							<h5>Employment Details</h5>
							<section>
                                <h2>Employment Details (please detail your current relevant employment (if any)</h2>
                                
                                <div class="form-group row">
									<label class="col-md-3 col-form-label">Discipline *</label>
									<div class="col-md-9">										
										<select class="form-control" name="occupation" required>
											<option value="">Choose your discipline </option>
											<option value="Academic" {{ (old('occupation',$member->occupation) == 'Academic' ? " selected" : "") }}>Academic</option>
											<option value="Administration" {{ (old('occupation',$member->occupation) == 'Administration' ? " selected" : "") }}>Administration</option>
											<option value="Biostatistician" {{ (old('occupation',$member->occupation) == 'Biostatistician' ? " selected" : "") }}>Biostatistician</option>
											<option value="Consumer" {{ (old('occupation',$member->occupation) == 'Consumer' ? " selected" : "") }}>Consumer</option>
											<option value="Dietetics" {{ (old('occupation',$member->occupation) == 'Dietetics' ? " selected" : "") }}>Dietetics</option>
											<option value="Exercise Physiology" {{ (old('occupation') == 'Exercise Physiology' ? " selected" : "") }}>Exercise Physiology</option>
											<option value="Fellow/Resident/Registrar" {{ (old('occupation',$member->occupation) == 'Fellow/Resident/Registrar' ? " selected" : "") }}>Fellow/Resident/Registrar</option>
											<option value="General Practitioner" {{ (old('occupation',$member->occupation) == 'General Practitioner' ? " selected" : "") }}>General Practitioner</option>
											<option value="Genetics" {{ (old('occupation',$member->occupation) == 'Genetics' ? " selected" : "") }}>Genetics</option>
											<option value="Haematologist" {{ (old('occupation',$member->occupation) == 'Haematologist' ? " selected" : "") }}>Haematologist</option>
											<option value="Medical Oncology (Adult)" {{ (old('occupation',$member->occupation) == 'Medical Oncology (Adult)' ? " selected" : "") }}>Medical Oncology (Adult)</option>
											<option value="Medical Oncology (Paediatric)" {{ (old('occupation',$member->occupation) == 'Medical Oncology (Paediatric)' ? " selected" : "") }}>Medical Oncology (Paediatric)</option>
											<option value="Nuclear Medicine" {{ (old('occupation',$member->occupation) == 'Nuclear Medicine' ? " selected" : "") }}>Nuclear Medicine</option>
											<option value="Nursing" {{ (old('occupation',$member->occupation) == 'Nursing' ? " selected" : "") }}>Nursing</option>
											<option value="Occupational Therapy" {{ (old('occupation') == 'Occupational Therapy' ? " selected" : "") }}>Occupational Therapy</option>
											<option value="Orthopaedic Surgery" {{ (old('occupation',$member->occupation) == 'Orthopaedic Surgery' ? " selected" : "") }}>Orthopaedic Surgery</option>
											<option value="Palliative Care" {{ (old('occupation',$member->occupation) == 'Palliative Care' ? " selected" : "") }}>Palliative Care</option>
											<option value="Pathology" {{ (old('occupation',$member->occupation) == 'Pathology' ? " selected" : "") }}>Pathology</option>
											<option value="Pharmacist" {{ (old('occupation',$member->occupation) == 'Pharmacist' ? " selected" : "") }}>Pharmacist</option>
											<option value="Physiotherapist" {{ (old('occupation',$member->occupation) == 'Physiotherapist' ? " selected" : "") }}>Physiotherapist</option>
											<option value="Plastic and Reconstructive Surgery" {{ (old('occupation',$member->occupation) == 'Plastic and Reconstructive Surgery' ? " selected" : "") }}>Plastic and Reconstructive Surgery</option>
											<option value="Psychiatry" {{ (old('occupation',$member->occupation) == 'Psychiatry' ? " selected" : "") }}>Psychiatry</option>
											<option value="Psychology" {{ (old('occupation',$member->occupation) == 'Psychology' ? " selected" : "") }}>Psychology</option>
											<option value="Radiation Oncology" {{ (old('occupation',$member->occupation) == 'Radiation Oncology' ? " selected" : "") }}>Radiation Oncology</option>
											<option value="Radiation Therapy" {{ (old('occupation',$member->occupation) == 'Radiation Therapy' ? " selected" : "") }}>Radiation Therapy</option>
											<option value="Radiology" {{ (old('occupation',$member->occupation) == 'Radiology' ? " selected" : "") }}>Radiology</option>
											<option value="Scientist" {{ (old('occupation',$member->occupation) == 'Scientist' ? " selected" : "") }}>Scientist</option>
											<option value="Research" {{ (old('occupation',$member->occupation)== 'Research' ? " selected" : "") }}>Research</option>
											<option value="Social Work" {{ (old('occupation',$member->occupation) == 'Social Work' ? " selected" : "") }}>Social Work</option>
											<option value="Speech Pathology" {{ (old('occupation') == 'Speech Pathology' ? " selected" : "") }}>Speech Pathology</option>
											<option value="Student" {{ (old('occupation',$member->occupation) == 'Student' ? " selected" : "") }}>Student</option>
											<option value="Surgical Oncology (General Surgeon)" {{ (old('occupation',$member->occupation) == 'Surgical Oncology (General Surgeon)' ? " selected" : "") }}>Surgical Oncology (General Surgeon)</option>
										</select>

										@if ($errors->has('occupation'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('occupation') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Employer *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="companyName" placeholder="Your employer"  value="{{ old('companyName',$member->companyName) }}" required />
										@if ($errors->has('companyName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('companyName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Date of Commencement *</label>
									<div class="col-md-9">
										<input type="text" class="form-control pull-right datepicker" placeholder="DD/MM/YYYY" name="employmentDateCommencement" value="{{ old('employmentDateCommencement',date("d/m/Y", strtotime($member->employmentDateCommencement))) }}" required />
										@if ($errors->has('employmentDateCommencement'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentDateCommencement') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Address *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="employmentAddress1" placeholder="Your address (line 1)" value="{{ old('employmentAddress1',$member->employmentAddress1) }}" required />
										@if ($errors->has('employmentAddress1'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentAddresss1') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label"></label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="employmentAddress2" placeholder="Your address (line 2)" value="{{ old('employmentAddress2',$member->employmentAddress2) }}"/>
										@if ($errors->has('employmentAddress2'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentAddress2') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Suburb *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="employmentSuburb" placeholder="Your suburb" value="{{ old('employmentSuburb',$member->employmentSuburb) }}" required />
										@if ($errors->has('employmentSuburb'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentSuburb') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">State *</label>
									<div class="col-md-4">
										<select class="form-control" name="employmentState" required>
											<option value="">Your state</option>
											<option value="ACT" {{ (old('employmentState',$member->employmentState) == "ACT") ? ' selected="selected"' : '' }}>
												ACT
											</option>
											<option value="NSW" {{ (old('employmentState',$member->employmentState) == "NSW") ? ' selected="selected"' : '' }}>
												NSW
											</option>
											<option value="NT" {{ (old('employmentState',$member->employmentState) == "NT") ? ' selected="selected"' : '' }}>
												NT
											</option>
											<option value="QLD" {{ (old('employmentState',$member->employmentState) == "QLD") ? ' selected="selected"' : '' }}>
												QLD
											</option>
											<option value="SA" {{ (old('employmentState',$member->employmentState) == "SA") ? ' selected="selected"' : '' }}>
												SA
											</option>
											<option value="TAS" {{ (old('employmentState',$member->employmentState) == "TAS") ? ' selected="selected"' : '' }}>
												TAS
											</option>
											<option value="VIC" {{ (old('employmentState',$member->employmentState) == "VIC") ? ' selected="selected"' : '' }}>
												VIC
											</option>
											<option value="WA" {{ (old('employmentState',$member->employmentState) == "WA") ? ' selected="selected"' : '' }}>
												WA
											</option>
											<option value="OTHER" {{ (old('employmentState',$member->employmentState) == "OTHER") ? ' selected="selected"' : '' }}>
												OTHER
											</option>
										</select>

										@if ($errors->has('employmentState'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentState') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Postcode *</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="employmentPostcode" placeholder="Your postcode" value="{{ old('employmentPostcode',$member->employmentPostcode) }}" required />
										@if ($errors->has('employmentPostcode'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentPostcode') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Country *</label>
									<div class="col-md-4">
										<select id="employmentCountry" class="form-control" name="employmentCountry">
										</select>
										@if ($errors->has('employmentCountry'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentCountry') }}</div>
											</div>
										@endif
									</div>
								</div>


								<div class="form-group row">
									<label class="col-md-3 col-form-label">Phone Number</label>
									<div class="col-md-9">
										<input type="text" class="form-control" id="employmentPhoneNumber" name="employmentPhoneNumber" value="{{ old('employmentPhoneNumber',$member->employmentPhoneNumber) }}"/>
										@if ($errors->has('employmentPhoneNumber'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentPhoneNumber') }}</div>
											</div>
										@endif
									</div>
								</div>                                                        

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Email Address *</label>
									<div class="col-md-9">
										<input type="email" class="form-control" name="employmentEmail" placeholder="Your email address" value="{{ old('employmentEmail',$member->employmentEmail) }}" required />
										@if ($errors->has('employmentEmail'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('employmentEmail') }}</div>
											</div>
										@endif
									</div>
								</div>                       
							</section>


							<h5>Other Relevant Experience</h5>
							<section>
							    <h2>Other Relevant Experience (please outline any other experience or contributions to the sarcomacommunity that may be relevant)</h2>
							    								
								<div class="form-group row">                                
									<div class="col-md-12">
										<textarea class="form-control" type="textarea" rows="5" name="otherExperience" placeholder="Other experiences or contributions">{{ old('otherExperience',$member->otherExperience) }}</textarea>
										@if ($errors->has('otherExperience'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('otherExperience') }}</div>
											</div>
										@endif
									</div>
								</div> 
								
								<div class="form-group row">
									<label class="col-lg-1 col-form-label">CV</label>
									<div class="col-lg-11">
									    <input type="hidden" name="cv_current" id="cv_current" value="{{ old('cv_current', $member->cvFile) }}">									    									    
										
										<div class="div-cvfile-upload">
                                           <div>+ Upload New CV</div>
                                           <div><input type="file" name="cv_file" id="cv_file"></div>
										</div>
										
										<a href="#" id="btnDeleteCV">- Delete CV</a>
										
										<div class="div-cvfile">
											@if ($member->cvFile != "")											  
											   Current CV - {{ $member->cvFile }}
											@endif
										</div> 																				
									</div>
									
									@if ($errors->has('cv_file'))
										<div class="fv-plugins-message-container">
											<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('cv_file') }}</div>
										</div>
									@endif
								</div>  								           
							</section>							

						</form>
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->

@endsection
@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('components/jquery.steps/build/jquery.steps.js') }}"></script>    
    <script src="{{ asset('components/theme/plugins/iCheck/icheck.min.js') }}"></script>       
    <script src="{{ asset('components/jquery-validation/dist/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/site/update-form.js') }}"></script>
	<script src="{{ asset('/components/jquery-mask-plugin/dist/jquery.mask.js') }}"></script>
	<script src="{{ asset('/components/intl-tel-input/build/js/intlTelInput-jquery.js') }}"></script>
	<script src="{{ asset('/components/intl-tel-input/build/js/utils.js') }}"></script>
	<script src="{{ asset('/components/libphonenumber/dist/libphonenumber.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
		$( document ).ready(function() {
			$('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
			
		    $( "#btnAddQualification" ).click(function(e) {		
			  qualificationNum = parseInt($( "#hidQualifications" ).val()) + 1;
			  $( "#hidQualifications" ).val(qualificationNum);
				
			  html  = '';
			  html += '<div class="form-group row">';  
			  html += '<label class="col-md-4 col-form-label">Qualification #' + qualificationNum + '</label>'; 
			  html += '<div class="col-md-8">'; 
			  html += '<input type="text" class="form-control members-qualification" name="qualification' + qualificationNum + '" placeholder="Qualification" value=""/>'; 
			  html += '<input type="text" class="form-control members-institution" name="institution' + qualificationNum + '" placeholder="Institution" value=""/>'; 
			  html += '<input type="text" class="form-control members-graduationYear" name="graduationYear' + qualificationNum + '" placeholder="Graduation year" value=""/>';       						
			  html += '</div>'; 
			  html += '</div>';   
				
			  $( "#divQualifications" ).append(html);	
				
			  e.preventDefault();
										
			});
			
			$( "#btnAddAwards" ).click(function(e) {		
			  awardsNum = parseInt($( "#hidAwards" ).val()) + 1;
			  $( "#hidAwards" ).val(awardsNum);
				
			  html  = '';
			  html += '<div class="form-group row">';  
			  html += '<label class="col-md-4 col-form-label">Honours & Awards #' + awardsNum + '</label>'; 
			  html += '<div class="col-md-8">'; 
			  html += '<input type="text" class="form-control members-awards" name="awards' + awardsNum + '" placeholder="Honours and Awards" value=""/>'; 			  					
			  html += '</div>'; 
			  html += '</div>';   
				
			  $( "#divAwards" ).append(html);	
				
			  e.preventDefault();
										
			});
			
			$( "#btnDeleteCV" ).click(function(e) {				
				$( ".div-cvfile" ).remove();
				$( "#cv_current" ).val('');
				$( "#cv_file" ).val('');
				
				e.preventDefault();
										
			});

            var countryData = window.intlTelInputGlobals.getCountryData();

            for (var i = 0; i < countryData.length; i++) {
                var country = countryData[i];
                $('#country').append($('<option>', {
                    value: country.name,
                    text: country.name,
                    'data-iso':country.iso2
                }));

                $('#employmentCountry').append($('<option>', {
                    value: country.name,
                    text: country.name,
                    'data-iso':country.iso2
                }));
            }

            $("#country").val("{{ old('country',$member->country) }}");
            $("#employmentCountry").val("{{ old('employmentCountry',$member->employmentCountry) }}");

            var phone_land_line = $("#phoneLandline");
            var phone_mobile = $("#phoneMobile");
            var employment_phone = $("#employmentPhoneNumber");

            phone_land_line.intlTelInput({
                allowDropdown:false,
                initialCountry: $("#country option:selected").attr('data-iso'),
                separateDialCode: true,
                placeholderNumberType: "FIXED_LINE",
                autoPlaceholder: 'aggressive',
                utilsScript:"{!!  asset('/components/intl-tel-input/build/js/utils.js')  !!}"
            });

            phone_mobile.intlTelInput({
                allowDropdown:false,
                initialCountry: $("#country option:selected").attr('data-iso'),
                separateDialCode: true,
                placeholderNumberType: "MOBILE",
                autoPlaceholder: 'aggressive',
                utilsScript:"{!!  asset('/components/intl-tel-input/build/js/utils.js')  !!}"
            });

            employment_phone.intlTelInput({
                allowDropdown:false,
                initialCountry: $("#employmentCountry option:selected").attr('data-iso'),
                separateDialCode: true,
                placeholderNumberType: "FIXED_LINE",
                autoPlaceholder: 'aggressive',
                utilsScript:"{!!  asset('/components/intl-tel-input/build/js/utils.js')  !!}"
            });

            $("#employmentCountry").on("change", function() {

                employment_phone.unmask();

                employment_phone.val('');
                employment_phone.intlTelInput("setCountry", $("#employmentCountry option:selected").attr('data-iso'));
                employment_phone_mask = employment_phone.attr('placeholder').replace(/[0-9]/g, 0);
                employment_phone.mask(employment_phone_mask);
            });

            $("#country").on("change", function() {

                phone_land_line.unmask();
                phone_mobile.unmask();

                phone_land_line.val('');
                phone_land_line.intlTelInput("setCountry", $("#country option:selected").attr('data-iso'));
                phone_land_line_mask = phone_land_line.attr('placeholder').replace(/[0-9]/g, 0);
                phone_land_line.mask(phone_land_line_mask);

                phone_mobile.val('');
                phone_mobile.intlTelInput("setCountry", $("#country option:selected").attr('data-iso'));
                phone_mobile_mask = phone_mobile.attr('placeholder').replace(/[0-9]/g, 0);
                phone_mobile.mask(phone_mobile_mask);
            });

            phone_land_line_mask = phone_land_line.attr('placeholder').replace(/[0-9]/g, 0);
            phone_land_line.mask(phone_land_line_mask);

            phone_mobile_mask = phone_mobile.attr('placeholder').replace(/[0-9]/g, 0);
            phone_mobile.mask(phone_mobile_mask);

            employment_phone_mask = employment_phone.attr('placeholder').replace(/[0-9]/g, 0);
            employment_phone.mask(employment_phone_mask);
			
		});

        $.validator.addMethod("phoneLandlineCheck",
            function(value, element) {
                if (value != "")  {
                    selected_country = $("#country option:selected").attr('data-iso');
                    number = $("#phoneLandline").intlTelInput('getNumber');
                    return phoneUtils.isValidNumberForRegion(number, selected_country);
                } else  {
                    return (true);
                }
            },
            "Please enter a valid number!"
        );

        $.validator.addMethod("phoneMobileCheck",
            function(value, element) {

                selected_country = $("#country option:selected").attr('data-iso');
                number = $("#phoneMobile").intlTelInput('getNumber');
                return phoneUtils.isValidNumberForRegion(number, selected_country);
            },
            "Please enter a valid number!"
        );

        $.validator.addMethod("employmentPhoneNumberCheck",
            function(value, element) {
                if (value != "")  {
                    selected_country = $("#employmentCountry option:selected").attr('data-iso');
                    number = $("#employmentPhoneNumber").intlTelInput('getNumber');
                    return phoneUtils.isValidNumberForRegion(number, selected_country);
                } else  {
                    return (true);
                }
            },
            "Please enter a valid number!"
        );
		
		$.validator.addMethod("anyDate",
			function(value, element) {
				return value.match(/^(0?[1-9]|[12][0-9]|3[0-1])[/., -](0?[1-9]|1[0-2])[/., -](19|20)?\d{2}$/);
			},
			"Please enter a date in the format!"
		);

		$("#frmRegister").validate({
		   rules : {
			  dob : { anyDate : true },
			  postcode: {digits: true, minlength: 4, maxlength: 4},
		      phoneLandline: { phoneLandlineCheck:true },
		      phoneMobile: { phoneMobileCheck:true },
			  employmentDateCommencement : { anyDate : true },
			  employmentPostcode: {digits: true, minlength: 4, maxlength: 4},
			  employmentPhoneNumber: { employmentPhoneNumberCheck:true },
			 // passwordConfirm: {equalTo : "#password"} 
		   }
		});
		
    </script>
@endsection
