<?php 
   // Set Meta Tags
   $meta_title_inner = "Reset Password | " . $company_name; 
   $meta_keywords_inner = "Reset Password " . $company_name; 
   $meta_description_inner = "Reset Password " . $company_name; 
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

<style>
input[type="text"]::-ms-input-placeholder, input[type="email"]::-ms-input-placeholder { color: #000!important; }
</style>

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">                     
        <div class="col-lg-8 blog-main blog-wide">

          <div class="blog-post">            
            <h1 class="blog-post-title">Reset Password</h1>    
            <p>Please fill out your details to reset your password.</p>                                                       
            
			 @if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			 @endif
                        
				<form id="frmRegister" method="POST" action="{{ url('') }}/password/reset">
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">				    				    
				    <input type="hidden" name="token" value="{{ $token }}">
				    
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Email *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="email" placeholder="Your email" value="{{ $email or old('email') }}" />
							@if ($errors->has('email'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('email') }}</div>
                                </div>
                            @endif
						</div>           
					</div>	
									
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Password *</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="password" placeholder="Your password" />
							
							<div id="progressBarDiv">
								Strength
								<div class="progress mt-2" id="progressBar">
									<div class="progress-bar bg-secondary progress-bar-animate" style="width: 100%"></div>
								</div>
							</div>
							
							@if ($errors->has('password'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('password') }}</div>
                                </div>
                            @endif												
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Confirm Password *</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="password_confirmation" placeholder="Confirm your password" />
						</div>           
					</div>												
										
					<div class="form-group row">
						<div class="col-md-9 offset-md-3">
							<button type="submit" class="btn-checkout" name="reset" value="Reset Password">Reset Password</button>
						</div>
					</div>
				</form>
     
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->        
@endsection

                        
@section('scripts')                                                                        
<script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
<script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js"></script>
@endsection

@section('inline-scripts')

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e) { 			
		const strongPassword = function() {
			return {
				validate: function(input) {
					// input.value is the field value
					// input.options are the validator options

					const value = input.value;
					if (value === '') {
						return {
							valid: true,
						};
					}

					const result = zxcvbn(value);
					const score = result.score;
					const message = result.feedback.warning || 'The password is weak';

					// By default, the password is treat as invalid if the score is smaller than 3
					// We allow user to change this number via options.minimalScore
					const minimalScore = input.options && input.options.minimalScore
										? input.options.minimalScore
										: 3;

					if (score < minimalScore) {
						return {
							valid: false,
							// Yeah, this will be set as error message
							message: message,
							meta: {
								// This meta data will be used later
								score: score,
							},
						}
					}
				},
			};
		};
			        
		const form = document.getElementById('frmRegister');
		FormValidation.formValidation(form, {
			fields: {				
				email: {
					validators: {
						notEmpty: {
							message: 'The email is required'
						},
						emailAddress: {
							message: 'The email is not valid'
						}
					}
				},        				
				password: {
					validators: {
						notEmpty: {
							message: 'The password is required'
						},
						checkPassword: {
                                message: 'The password is too weak',
                                minimalScore: 2,
                            }
					}
				},        
				password_confirmation: {
					validators: {
						notEmpty: {
							message: 'The password confirmation is required'
						},
						identical: {
                            compare: function() {
                                return form.querySelector('[name="password"]').value;
                            },
                            message: 'The password and its confirm are not the same'
                        }
					}
				},       				
			},
			plugins: {
				trigger: new FormValidation.plugins.Trigger(),
				bootstrap: new FormValidation.plugins.Bootstrap(),
				submitButton: new FormValidation.plugins.SubmitButton(),
				defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				icon: new FormValidation.plugins.Icon({
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh',
				}),
                },
            }
        )
        .registerValidator('checkPassword', strongPassword)
		
        .on('core.validator.validating', function(e) {
            if (e.field === 'password' && e.validator === 'checkPassword') {				
                document.getElementById('progressBarDiv').style.opacity = '1';				
            }
        })
        .on('core.validator.validated', function(e) {
            if (e.field === 'password' && e.validator === 'checkPassword') {
                const progressBar = document.getElementById('progressBar');

                if (e.result.meta) {
                    // Get the score which is a number between 0 and 4
                    const score = e.result.meta.score;
                    
                    // Update the width of progress bar
                    const width = (score == 0) ? '1%' : score * 25 + '%';
                    progressBar.style.opacity = 1;
                    progressBar.style.width = width;
                } else {
                    progressBar.style.opacity = 0;
                    progressBar.style.width = '0%';
                }
            }
        });		
});
</script>

@endsection