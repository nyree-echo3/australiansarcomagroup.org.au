<?php 
   // Set Meta Tags
   $meta_title_inner = "Home | " . $company_name; 
   $meta_keywords_inner = "Home " . $company_name; 
   $meta_description_inner = "Home " . $company_name; 
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-members-portal')        
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post">            
            <h1 class="blog-post-title">Welcome</h1>                                    
           
           	@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			 @endif			 				
            				 				
             {!! $home_members_intro_text !!}
             
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->        
@endsection
