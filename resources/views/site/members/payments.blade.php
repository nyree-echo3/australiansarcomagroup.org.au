<?php
// Set Meta Tags
$meta_title_inner = "Payments | " . $company_name;
$meta_keywords_inner = "Payments " . $company_name;
$meta_description_inner = "Payments " . $company_name;
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @include('site/partials/sidebar-members-portal')

                <div class="col-sm-8 blog-main">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Payments</h1>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Type</th>
                                <th scope="col">Method</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Status</th>
                                <th scope="col">Date / Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                            <tr>
                                <td>{{ ucfirst($payment->payment_type) }}</td>
                                <td>
                                    @if($payment->payment_method=='paypal')
                                        PayPal
                                    @endif
                                    @if($payment->payment_method=='bank-deposit')
                                        Bank Deposit
                                    @endif
                                </td>
                                <td>${{ $payment->amount }}</td>
                                <td>
                                    @if($payment->payment_status=='completed')
                                        <span class="label label-success">Completed</span>
                                    @endif

                                    @if($payment->payment_status=='pending')
                                        <span class="label label-warning">Pending</span>
                                    @endif
                                </td>
                                <td>{{ \Carbon\Carbon::parse($payment->created_at)->format('d-m-Y H:i') }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection

@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('inline-scripts')



@endsection