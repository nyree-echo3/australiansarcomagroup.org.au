<?php
// Set Meta Tags
$meta_title_inner = "Login | " . $company_name;
$meta_keywords_inner = "Login " . $company_name;
$meta_description_inner = "Login " . $company_name;
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

    <style>
    input[type="text"]::-ms-input-placeholder, input[type="email"]::-ms-input-placeholder { color: #000!important; }
    </style>
   
    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-lg-8 blog-main blog-wide">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Login</h1>

                        @include('flash::message')

                        <p><a href='{{ url('')}}/apply'>Not a member yet? Please apply here.</a></p>

                        <form id="frmLogin" method="POST" action="{{ url('') }}/login">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="hidRedirect" value="{{ (isset($_GET['redirect']) ? $_GET['redirect'] : "") }}">

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Email *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="email" placeholder="Your email"
                                           value="{{ old('email') }}"/>
                                    @if ($errors->has('email'))
                                        <div class="fv-plugins-message-container">
                                            <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('email') }}</div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Password *</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" name="password" placeholder="Your password"/>

                                    @if ($errors->has('password'))
                                        <div class="fv-plugins-message-container">
                                            <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('password') }}</div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-9 offset-md-3">
                                    <button type="submit" class="btn-checkout" name="login" value="Log In">Log In
                                    </button>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-9 offset-md-3">
                                    <a href='{{ url('')}}/password/forgot'>Forgotten your password?</a>
                                </div>
                            </div>
                        </form>

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection


@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
@endsection

@section('inline-scripts')

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function (e) {

            const form = document.getElementById('frmLogin');
            FormValidation.formValidation(form, {
                    fields: {
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'The email is required'
                                },
                                emailAddress: {
                                    message: 'The email is not valid'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required'
                                },
                                checkPassword: {
                                    message: 'The password is too weak',
                                    minimalScore: 2,
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fa fa-check',
                            invalid: 'fa fa-times',
                            validating: 'fa fa-refresh',
                        }),
                    },
                }
            )
        });
    </script>

@endsection