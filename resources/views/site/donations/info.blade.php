<?php 
   // Set Meta Tags
   $meta_title_inner = "Make a Donation" . $company_name; 
   $meta_keywords_inner = "donation, " . $company_name; 
   $meta_description_inner = "Make a Donation | " . $company_name;  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">                                   
        @include('site/partials/sidebar-donation')       
                
        <div class="col-sm-9 blog-main">

          <div class="blog-post">   
               <h1>Make a Donation</h1>
               <blockquote><p>Thanks to your generosity, we can achieve more!</p></blockquote>  
               @include('flash::message')           
          </div>
          
          <div class="page-note">
				<div class="donation-charity">
					<div class="donation-charity-img">
						<img src="{{ url('') }}/images/site/logo-acnc.png" title="Registered Charity"
							 alt="Registered Charity" style='float: right; margin-right: 10px; width: 50%;'>
					</div>

					<div class="donation-charity-txt">
						<p>The Australia and New Zealand Sarcoma Association is registered as a charity with
							the Australian Charities and Not-for-profits Commission with the ABN 73 132 759
							525.</p>
						<p>* Your donation amount is subjected to small transaction fees paid to our payment
							gateway provider or fundraising platform. ANZSA does not take any of your
							donations for its operational or promotional cost.</p>
					</div>
				</div>
			</div>
  
            
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>

@include('site/partials/index-links')

@endsection            
