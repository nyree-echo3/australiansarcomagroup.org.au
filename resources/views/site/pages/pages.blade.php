<?php
// Set Meta Tags
$meta_title_inner = $page->meta_title;
$meta_keywords_inner = $page->meta_keywords;
$meta_description_inner = $page->meta_description;
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @if ($page->category->members_only == "active")
                    @include('site/partials/sidebar-members-portal')
                @elseif ($page->category->id == 11)
                    @include('site/partials/sidebar-events-asm2020')
                    @include('site/partials/sidebar-events-book')
                @elseif ($page->category->id == 12)
                    @include('site/partials/sidebar-events-asm2021')
                    @include('site/partials/sidebar-events-book')
                @else
                    @include('site/partials/sidebar-pages')
                @endif

                <div class="col-sm-9 blog-main">

                    <div class="blog-post">
                        @if (isset($page))
                            <h1 class="blog-post-title">{{$page->title}}</h1>
                            {!! $page->body !!}
                        @endif
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->

    @if (($page->category->slug == "support-us" && $page->slug == "fundraise-for-us") || ($page->category->slug == "support-us" && $page->slug == "partner-with-us"))
        @include('site/partials/index-links')
    @endif

@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>
@endsection

@section('inline-scripts-pages')
    <script>
        $(document).ready(function () {
            // Grants 1
            $("#grants-open1").click(function () {
                $("#grants-popup1").show();
            });

            $("#grants-close1").click(function (e) {
                $("#grants-popup1").hide();
                e.preventDefault();
                return false;
            });

            // Grants 2
            $("#grants-open2").click(function () {
                $("#grants-popup2").show();
            });

            $("#grants-close2").click(function (e) {
                $("#grants-popup2").hide();
                e.preventDefault();
                return false;
            });

            // Grants 3
            $("#grants-open3").click(function () {
                $("#grants-popup3").show();
            });

            $("#grants-close3").click(function (e) {
                $("#grants-popup3").hide();
                e.preventDefault();
                return false;
            });

            // Grants 4
            $("#grants-open4").click(function () {
                $("#grants-popup4").show();
            });

            $("#grants-close4").click(function (e) {
                $("#grants-popup4").hide();
                e.preventDefault();
                return false;
            });

            // Grants 5
            $("#grants-open5").click(function () {
                $("#grants-popup5").show();
            });

            $("#grants-close5").click(function (e) {
                $("#grants-popup5").hide();
                e.preventDefault();
                return false;
            });

            // Grants 6
            $("#grants-open6").click(function () {
                $("#grants-popup6").show();
            });

            $("#grants-close6").click(function (e) {
                $("#grants-popup6").hide();
                e.preventDefault();
                return false;
            });
        });
    </script>
@endsection