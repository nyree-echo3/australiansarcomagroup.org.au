<?php
// Set Meta Tags
$meta_title_inner = $events_item->meta_title;
$meta_keywords_inner = $events_item->meta_keywords;
$meta_description_inner = $events_item->meta_description;
?>
@extends('site/layouts/app')

@section('content')



    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @php
                    $class = "";
                    if ($events_item->id == 3 || $events_item->id == 6) {
                       $class = "paymentHide";
                    }
                @endphp

                @if ($events_item->id == 3)
                    @include('site/partials/sidebar-events-asm2020')
                @elseif($events_item->id == 6)
                    @include('site/partials/sidebar-events-asm2021')
                @else
                    @include('site/partials/sidebar-events')
                @endif
                @include('site/partials/sidebar-events-book')

                <div class="col-sm-9 blog-main">

                    <div class="blog-post">
                        <h1 class="blog-post-title">{{ $events_item->title }}</h1>

                        <div class='events-item'>
                            <div class="events-item-date {{ $class }}">
                                @if ($events_item->start_date != $events_item->end_date)
                                    {{date("D j M Y", strtotime($events_item->start_date))}} {{date("g:ia", strtotime($events_item->start_time))}}
                                    - {{date("D j M Y", strtotime($events_item->end_date))}} {{date("g:ia", strtotime($events_item->end_time))}}
                                @else
                                    {{date("D j M Y", strtotime($events_item->start_date))}}
                                    - {{date("g:ia", strtotime($events_item->start_time))}} <!-- to {{date("g:ia", strtotime($events_item->end_time))}}-->
                                @endif
                            </div>

                            <input type="button" class="btnRegister" value="Register Now" />

                            <div class='events-item-txt'>
                                {!! $events_item->body !!}
                            </div>
                        </div>
                        @if($events_item->id != 3 || $events_item->id != 6)
                        <input type="button" class="btnBooking {{ $class }}" value="Register Now"/>
                        @endif

                    <!--<div class='btn-back'>
           	    <a class='btn-home-events' href='{{ url('') }}/events/{{ $events_item->category->slug }}'><i class="fas fa-chevron-left"></i> back</a>	
			 </div>-->
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection
