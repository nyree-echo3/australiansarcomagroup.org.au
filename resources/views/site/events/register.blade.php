<?php
// Set Meta Tags
$meta_title_inner = "Event Register | " . $company_name;
$meta_keywords_inner = "Event Register " . $company_name;
$meta_description_inner = "Event Register " . $company_name;
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/intl-tel-input/build/css/intlTelInput.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

    <style>
    input[type="text"]::-ms-input-placeholder, input[type="email"]::-ms-input-placeholder { color: #000!important; }
    </style>
    
    @if ($event_item->id == 3)
		<style>
			.inside-page {height:auto!important;}
			.inside-page .carousel-item {height:auto!important;}
			.inside-page .carousel-item > img {height:auto!important; position: inherit!important; width:100%!important;}

			@media only screen and  (max-width: 480px) {	
			   .inside-page .carousel-item > img {margin-top: 0px!important;}
			   .inside-page .carousel-item { height: 130px!important;}
			}

			@media only screen and  (max-width: 991px) {	
			   .inside-page .carousel-item > img {margin-top: 78px;}
			}	

			@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation:portrait) {
				/* For ipad portrait layouts only */
			   .inside-page .carousel-item > img {margin-top: 0px!important;}
			   .inside-page .carousel-item { height: 180px!important;}
			}
		</style>
	@endif
     
    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-lg-8 blog-main blog-wide">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Event Register</h1>

                        @include('flash::message')
                        
                        <div class='events-item'>
							<h2>{{ $event_item->title }}</h2>

							<div class="events-item-date">
						        @if ($event_item->id == 3)
							        Thursday, 8 October 2020 (4pm-6pm AEDT)<br>
                                    Friday, 9 October 2020 (2pm-4pm AEDT)
                                @elseif ($event_item->id == 5)							        
                                    Friday, 9 October 2020 (4:30pm AEDT)    
							    @else
									@if ($event_item->start_date != $event_item->end_date)
										{{date("D j M Y", strtotime($event_item->start_date))}} {{date("g:ia", strtotime($event_item->start_time))}} - {{date("D j M Y", strtotime($event_item->end_date))}} {{date("g:ia", strtotime($event_item->end_time))}}
									@else
										{{date("D j M Y", strtotime($event_item->start_date))}} - {{date("g:ia", strtotime($event_item->start_time))}} <!-- to {{date("g:ia", strtotime($event_item->end_time))}}-->
									@endif
								@endif
							</div>  
							
							@if ($event_item->id == 3)
							    <p>Thank you for your interest in joining us at our virtual ASM 2020. Please complete the registration process below.</p>                 
							@else 
							    <p>Thank you for your interest. Please complete the registration process below.</p>    
							@endif
						</div>    

                        <form id="frmRegister" method="POST" action="{{ url('') }}/event-register/store">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">                            

                            <div class="form-wrapper">
                                <h2>Your {{ (isset($member) ? "Membership" : "") }} Details</h2>
                                
                                @php
                                   if (isset($member)) {
                                      $member_id = $member->id;
                                      $member_title = $member->title;
                                      $member_firstName = $member->firstName;
                                      $member_lastName = $member->lastName;
                                      $member_email = $member->email;
                                      $member_phoneMobile = $member->phoneMobile;
                                      $member_designation = "";
                                      $member_companyName = $member->companyName;
                                      $member_state = $member->state;
                                      $member_country = $member->country;
                                   } else  {
                                      $member_id = "";
                                      $member_title = "";
                                      $member_firstName = "";
                                      $member_lastName = "";
                                      $member_email = "";
                                      $member_phoneMobile = "";
                                      $member_designation = "";
                                      $member_companyName = ""; 
                                      $member_state = "";     
                                      $member_country = "Australia";                                  
                                   }                                                                      
                                @endphp
                                
                                <input type="hidden" name="member_id" value="{{ $member_id }}">
                                <input type="hidden" name="event_id" value="{{ $event_item->id }}">
                                <input type="hidden" name="event_slug" value="{{ $event_item->slug }}">
                                
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Title*</label>
									<div class="col-md-9">
									    @if ($member_title != "")
									        <input type="text" class="form-control" name="title" placeholder="Your title" value="{{ old('title', $member_title) }}" readonly />
									    @else
											<select class="form-control" name="title" id="title" required>
												<option value="">Your title</option>
												<option value="Mr" {{ (old('title', $member_title) == "Mr") ? ' selected="selected"' : '' }}>Mr</option>
												<option value="Ms" {{ (old('title', $member_title) == "Ms") ? ' selected="selected"' : '' }}>Ms</option>
												<option value="Mrs" {{ (old('title', $member_title) == "Mrs") ? ' selected="selected"' : '' }}>Mrs</option>
												<option value="Dr" {{ (old('title', $member_title) == "Dr") ? ' selected="selected"' : '' }}>Dr</option>
												<option value="Assc. Professor" {{ (old('title', $member_title) == "Assc. Professor") ? ' selected="selected"' : '' }}>Assc. Professor</option>
												<option value="Professor" {{ (old('title', $member_title) == "Professor") ? ' selected="selected"' : '' }}>Professor</option>
											</select>
										@endif
										
										@if ($errors->has('title'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('title') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">First Name*</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="firstName" placeholder="Your first name" required value="{{ old('firstName', $member_firstName) }}" {{ ($member_firstName != "" ? "readonly" : "") }} />
										@if ($errors->has('firstName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('firstName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Last Name*</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="lastName" placeholder="Your last name" required value="{{ old('lastName', $member_lastName) }}" {{ ($member_lastName != "" ? "readonly" : "") }}/>
										@if ($errors->has('lastName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('lastName') }}</div>
											</div>
										@endif
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Role*</label>
									<div class="col-md-9">
										<select class="form-control" name="designation" id="designation" required>
												<option value="">Your role</option>
												<option value="Medical oncologist" {{ (old('designation', $member_designation) == "Medical oncologist") ? ' selected="selected"' : '' }}>Medical oncologist</option>
												<option value="Surgical oncologist" {{ (old('designation', $member_designation) == "Surgical oncologist") ? ' selected="selected"' : '' }}>Surgical oncologist</option>
												<option value="Radiation oncologist" {{ (old('designation', $member_designation) == "Radiation oncologist") ? ' selected="selected"' : '' }}>Radiation oncologist</option>
												<option value="Paediatric oncologist" {{ (old('designation', $member_designation) == "Paediatric oncologist") ? ' selected="selected"' : '' }}>Paediatric oncologist</option>
												<option value="AYA oncologist" {{ (old('designation', $member_designation) == "AYA oncologist") ? ' selected="selected"' : '' }}>AYA oncologist</option>
												<option value="Orthopaedic surgeon" {{ (old('designation', $member_designation) == "Orthopaedic surgeon") ? ' selected="selected"' : '' }}>Orthopaedic surgeon</option>
												<option value="Plastic surgeon" {{ (old('designation', $member_designation) == "Plastic surgeon") ? ' selected="selected"' : '' }}>Plastic surgeon</option>
												<option value="Vascular surgeon" {{ (old('designation', $member_designation) == "Vascular surgeon") ? ' selected="selected"' : '' }}>Vascular surgeon</option>
												<option value="Pathologist" {{ (old('designation', $member_designation) == "Pathologist") ? ' selected="selected"' : '' }}>Pathologist</option>
												<option value="Allied health" {{ (old('designation', $member_designation) == "Allied health") ? ' selected="selected"' : '' }}>Allied health</option>
												<option value="Nurse" {{ (old('designation', $member_designation) == "Nurse") ? ' selected="selected"' : '' }}>Nurse</option>
												<option value="Research scientist" {{ (old('designation', $member_designation) == "Research scientist") ? ' selected="selected"' : '' }}>Research scientist</option>
												<option value="Student" {{ (old('designation', $member_designation) == "Student") ? ' selected="selected"' : '' }}>Student</option>
												<option value="Consumer" {{ (old('designation', $member_designation) == "Consumer") ? ' selected="selected"' : '' }}>Consumer</option>
												<option value="Other" {{ (old('designation', $member_designation) == "Other") ? ' selected="selected"' : '' }}>Other</option>
											</select>
										@if ($errors->has('designation'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('designation') }}</div>
											</div>
										@endif
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Company/Institution*</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="companyName" placeholder="Your company or institution" required value="{{ old('companyName', $member_companyName) }}" {{ ($member_companyName != "" ? "readonly" : "") }}/>
										@if ($errors->has('companyName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('companyName') }}</div>
											</div>
										@endif
									</div>
								</div>
								
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Location*</label>
									<div class="col-lg-9">
										<select class="form-control" name="state" required {{ ($member_state != "" ? "readonly" : "") }}>
											<option value="">Your state</option>
											<option value="ACT" {{ (old('state', $member_state) == "ACT") ? ' selected="selected"' : '' }}>
												ACT
											</option>
											<option value="NSW" {{ (old('state', $member_state) == "NSW") ? ' selected="selected"' : '' }}>
												NSW
											</option>
											<option value="NT" {{ (old('state', $member_state) == "NT") ? ' selected="selected"' : '' }}>
												NT
											</option>
											<option value="QLD" {{ (old('state', $member_state) == "QLD") ? ' selected="selected"' : '' }}>
												QLD
											</option>
											<option value="SA" {{ (old('state', $member_state) == "SA") ? ' selected="selected"' : '' }}>
												SA
											</option>
											<option value="TAS" {{ (old('state', $member_state) == "TAS") ? ' selected="selected"' : '' }}>
												TAS
											</option>
											<option value="VIC" {{ (old('state', $member_state) == "VIC") ? ' selected="selected"' : '' }}>
												VIC
											</option>
											<option value="WA" {{ (old('state', $member_state) == "WA") ? ' selected="selected"' : '' }}>
												WA
											</option>
											<option value="NA" {{ (old('state', $member_state) == "NA") ? ' selected="selected"' : '' }}>
												Not Applicable
											</option>
										</select>

										@if ($errors->has('state'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('state') }}</div>
											</div>
										@endif
										
										<select id="country" class="form-control" name="country" placeholder="Your country" required {{ ($member_country != "" ? "readonly" : "") }}>
										</select>
										
										@if ($errors->has('country'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('country') }}</div>
											</div>
										@endif
									</div>
									
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Email Address*</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="email" placeholder="Your email address" required value="{{ old('email', $member_email) }}" {{ ($member_email != "" ? "readonly" : "") }}/>
										@if ($errors->has('email'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('email') }}</div>
											</div>
										@endif
									</div>
								</div>	
								
								<div class="form-group row">
									<label class="col-lg-3 col-form-label">Mobile</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" id="phoneMobile" name="phoneMobile" value="{{ old('phoneMobile', $member_phoneMobile) }}" {{ ($member_phoneMobile != "" ? "readonly" : "") }} />
										@if ($errors->has('phoneMobile'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('phoneMobile') }}</div>
											</div>
										@endif
									</div>
								</div>																														
																
							</div>
                            
                            <div class="form-wrapper">
                                <h2>Your Ticket</h2>
                                
								<div class="form-group row">
									<div class="col-md-12">
									    <select class="form-control" id="event_ticket" name="event_ticket" placeholder="Your ticket">
									        @php
								               $counterPayment = 0;
									           $paymentHide = true;
									        @endphp
									        
									    	@foreach ($event_tickets as $event_ticket)		
								    	       @php
								    	       if ($counterPayment == 0 && $event_ticket->price > 0)  {
								    	          $paymentHide = false;
								    	       } 
								    	       $counterPayment++;
								    	       @endphp
								    	       						    	       
									    	   <option value="{{ $event_ticket->id }}">{{ $event_ticket->name }} ${{ number_format($event_ticket->price, 2) }}</option>									    	   
									    	@endforeach	
									    </select>										
									</div>																		
								</div> 																
							</div>
                          
                            @foreach ($event_tickets as $event_ticket)									   
							   <input type="hidden" id="hidTicketValue{{ $event_ticket->id }}" value="{{ $event_ticket->price }}">								    	   
							@endforeach	 
                           
                            <div class="form-wrapper form-wrapper-white {{ ($paymentHide ? "paymentHide" : "") }}" id="divPayment">                            								
								<h2>Payment Type</h2>								

								<div class="form-group row">									
									<div class="col-md-12 payment-options">
										<input type="radio" name="payment_method" value="paypal" checked>
										<img src="{{ url('') }}/images/site/Paypal.png" title="PayPal" alt="PayPal"><br />

										<!--
										<input type="radio" name="payment_method" value="bank-deposit">
										<strong>Bank Deposit</strong><br />

										<label class="form-control-label"> {!!  $payment_direct_deposit_events !!} </label>
										-->
									</div>
								 </div>
								 
								 @if ($event_item->id == 3)
									 <h2>Terms & Conditions</h2>
									 <p>By registering for the ANZSA virtual ASM, you agree to pay the Registration Fee (if applicable) during the registration process. </p>                 							 
								 @endif
							 </div>                                                       
                            
                             <div class="form-group row has-error">
								<label class="col-md-12 g-recaptcha-container">
									<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
									@if ($errors->has('g-recaptcha-response'))
										<div class="fv-plugins-message-container">
											<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
										</div>
									@endif
								</div>
							</div>                                                                                                            

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn-checkout" name="register" value="Register">Register</button>
                                </div>
                            </div>                            
                        </form>

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection


@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{ asset('/components/jquery-mask-plugin/dist/jquery.mask.js') }}"></script>
	<script src="{{ asset('/components/intl-tel-input/build/js/intlTelInput-jquery.js') }}"></script>
	<script src="{{ asset('/components/intl-tel-input/build/js/utils.js') }}"></script>
	<script src="{{ asset('/components/libphonenumber/dist/libphonenumber.js') }}"></script>
@endsection

@section('inline-scripts')

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function (e) {

            const form = document.getElementById('frmLogin');
            FormValidation.formValidation(form, {
                    fields: {
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'The email is required'
                                },
                                emailAddress: {
                                    message: 'The email is not valid'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required'
                                },
                                checkPassword: {
                                    message: 'The password is too weak',
                                    minimalScore: 2,
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fa fa-check',
                            invalid: 'fa fa-times',
                            validating: 'fa fa-refresh',
                        }),
                    },
                }
            )
        });
				
		$( "#event_ticket" ).change(function() {
		    ticket_id = $("#event_ticket").val();
			ticket_val = parseFloat($("#hidTicketValue" + ticket_id).val());
		  
			if (ticket_val > 0)  {
				 $("#divPayment").removeClass("paymentHide");				
			} else  {
				 $("#divPayment").addClass("paymentHide");
			}
			
		});
		
		$( document ).ready(function() {
			var countryData = window.intlTelInputGlobals.getCountryData();

            for (var i = 0; i < countryData.length; i++) {
                var country = countryData[i];
                $('#country').append($('<option>', {
                    value: country.name,
                    text: country.name,
                    'data-iso':country.iso2
                }));               
            }
			$("#country").val("{{ $member_country }}");
			
			var phone_mobile = $("#phoneMobile");
			
			phone_mobile.intlTelInput({
                allowDropdown:false,
                initialCountry: "au",
                separateDialCode: true,
                placeholderNumberType: "MOBILE",
                autoPlaceholder: 'aggressive',
                utilsScript:"{!!  asset('/components/intl-tel-input/build/js/utils.js')  !!}"
            });
			
			phone_mobile_mask = phone_mobile.attr('placeholder').replace(/[0-9]/g, 0);
            phone_mobile.mask(phone_mobile_mask);						
			
			$("#country").on("change", function() {               
                phone_mobile.unmask();
               
                phone_mobile.val('');
                phone_mobile.intlTelInput("setCountry", $("#country option:selected").attr('data-iso'));
                phone_mobile_mask = phone_mobile.attr('placeholder').replace(/[0-9]/g, 0);
                phone_mobile.mask(phone_mobile_mask);
            });

		});	
    </script>

@endsection