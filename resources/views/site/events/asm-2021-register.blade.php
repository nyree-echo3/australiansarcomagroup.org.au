<?php
// Set Meta Tags
$meta_title_inner = "Event Register | " . $company_name;
$meta_keywords_inner = "Event Register " . $company_name;
$meta_description_inner = "Event Register " . $company_name;
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/intl-tel-input/build/css/intlTelInput.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

    <style>
        input[type="text"]::-ms-input-placeholder, input[type="email"]::-ms-input-placeholder {
            color: #000 !important;
        }
    </style>

    @if ($event_item->id == 3)
        <style>
            .inside-page {
                height: auto !important;
            }

            .inside-page .carousel-item {
                height: auto !important;
            }

            .inside-page .carousel-item > img {
                height: auto !important;
                position: inherit !important;
                width: 100% !important;
            }

            @media only screen and  (max-width: 480px) {
                .inside-page .carousel-item > img {
                    margin-top: 0px !important;
                }

                .inside-page .carousel-item {
                    height: 130px !important;
                }
            }

            @media only screen and  (max-width: 991px) {
                .inside-page .carousel-item > img {
                    margin-top: 78px;
                }
            }

            @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {
                /* For ipad portrait layouts only */
                .inside-page .carousel-item > img {
                    margin-top: 0px !important;
                }

                .inside-page .carousel-item {
                    height: 180px !important;
                }
            }
        </style>
    @endif

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-lg-8 blog-main blog-wide">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Event Register</h1>

                        @include('flash::message')

                        <div class='events-item'>
                            <h2>{{ $event_item->title }}</h2>

                            <div class="events-item-date">
                                Friday, 5 November 2021 (12pm-4pm AEDT)<br>
                                Saturday, 6 November 2021 (9am-2.30pm AEDT)
                            </div>

                            <p>Thank you for your interest in joining us at ASM 2021. Please complete the registration
                                process below.</p>
                        </div>

                        <form id="frmRegister" method="POST" action="{{ url('') }}/event-register/store-asm2021">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-wrapper">
                                <h2>Your {{ (isset($member) ? "Membership" : "") }} Details</h2>

                                @php
                                    if (isset($member)) {
                                       $member_id = $member->id;
                                       $member_title = $member->title;
                                       $member_firstName = $member->firstName;
                                       $member_lastName = $member->lastName;
                                       $member_email = $member->email;
                                       $member_phoneMobile = $member->phoneMobile;
                                       $member_designation = "";
                                       $member_companyName = $member->companyName;
                                       $member_state = $member->state;
                                       $member_country = $member->country;
                                       $member_address1 = $member->address1;
                                       $member_address2 = $member->address2;
                                       $member_suburb = $member->suburb;
                                       $member_postcode = $member->postcode;
                                    } else  {
                                       $member_id = "";
                                       $member_title = "";
                                       $member_firstName = "";
                                       $member_lastName = "";
                                       $member_email = "";
                                       $member_phoneMobile = "";
                                       $member_designation = "";
                                       $member_companyName = "";
                                       $member_state = "";
                                       $member_country = "Australia";
                                       $member_address1 = "";
                                       $member_address2 = "";
                                       $member_suburb = "";
                                       $member_postcode = "";
                                    }
                                @endphp

                                <input type="hidden" name="member_id" value="{{ $member_id }}">
                                <input type="hidden" name="event_id" value="{{ $event_item->id }}">
                                <input type="hidden" name="event_slug" value="{{ $event_item->slug }}">

                                @if($errors)
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                @endif

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Title*</label>
                                    <div class="col-md-9">
                                        @if ($member_title != "")
                                            <input type="text" class="form-control" name="title" placeholder="Your title" value="{{ old('title', $member_title) }}"/>
                                        @else
                                            <select class="form-control" name="title" id="title" required>
                                                <option value="">Your title</option>
                                                <option value="Mr" {{ (old('title', $member_title) == "Mr") ? ' selected="selected"' : '' }}>
                                                    Mr
                                                </option>
                                                <option value="Ms" {{ (old('title', $member_title) == "Ms") ? ' selected="selected"' : '' }}>
                                                    Ms
                                                </option>
                                                <option value="Mrs" {{ (old('title', $member_title) == "Mrs") ? ' selected="selected"' : '' }}>
                                                    Mrs
                                                </option>
                                                <option value="Dr" {{ (old('title', $member_title) == "Dr") ? ' selected="selected"' : '' }}>
                                                    Dr
                                                </option>
                                                <option value="Assc. Professor" {{ (old('title', $member_title) == "Assc. Professor") ? ' selected="selected"' : '' }}>
                                                    Assc. Professor
                                                </option>
                                                <option value="Professor" {{ (old('title', $member_title) == "Professor") ? ' selected="selected"' : '' }}>
                                                    Professor
                                                </option>
                                            </select>
                                        @endif

                                        @if ($errors->has('title'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('title') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">First Name*</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="firstName"
                                               placeholder="Your first name" required
                                               value="{{ old('firstName', $member_firstName) }}" />
                                        @if ($errors->has('firstName'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('firstName') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Last Name*</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="lastName"
                                               placeholder="Your last name" required
                                               value="{{ old('lastName', $member_lastName) }}" />
                                        @if ($errors->has('lastName'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('lastName') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Role*</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="designation" id="designation" required>
                                            <option value="">Your role</option>
                                            <option value="Medical oncologist" {{ (old('designation', $member_designation) == "Medical oncologist") ? ' selected="selected"' : '' }}>Medical oncologist</option>
                                            <option value="Surgical oncologist" {{ (old('designation', $member_designation) == "Surgical oncologist") ? ' selected="selected"' : '' }}>Surgical oncologist</option>
                                            <option value="Radiation oncologist" {{ (old('designation', $member_designation) == "Radiation oncologist") ? ' selected="selected"' : '' }}>Radiation oncologist</option>
                                            <option value="Paediatric oncologist" {{ (old('designation', $member_designation) == "Paediatric oncologist") ? ' selected="selected"' : '' }}>Paediatric oncologist</option>
                                            <option value="AYA oncologist" {{ (old('designation', $member_designation) == "AYA oncologist") ? ' selected="selected"' : '' }}>AYA oncologist</option>
                                            <option value="Orthopaedic surgeon" {{ (old('designation', $member_designation) == "Orthopaedic surgeon") ? ' selected="selected"' : '' }}>Orthopaedic surgeon</option>
                                            <option value="Plastic surgeon" {{ (old('designation', $member_designation) == "Plastic surgeon") ? ' selected="selected"' : '' }}>Plastic surgeon</option>
                                            <option value="Vascular surgeon" {{ (old('designation', $member_designation) == "Vascular surgeon") ? ' selected="selected"' : '' }}>Vascular surgeon</option>
                                            <option value="Pathologist" {{ (old('designation', $member_designation) == "Pathologist") ? ' selected="selected"' : '' }}>Pathologist</option>
                                            <option value="Allied health" {{ (old('designation', $member_designation) == "Allied health") ? ' selected="selected"' : '' }}>Allied health</option>
                                            <option value="Nurse" {{ (old('designation', $member_designation) == "Nurse") ? ' selected="selected"' : '' }}>Nurse</option>
                                            <option value="Research scientist" {{ (old('designation', $member_designation) == "Research scientist") ? ' selected="selected"' : '' }}>Research scientist</option>
                                            <option value="Student" {{ (old('designation', $member_designation) == "Student") ? ' selected="selected"' : '' }}>Student</option>
                                            <option value="Consumer" {{ (old('designation', $member_designation) == "Consumer") ? ' selected="selected"' : '' }}>Consumer</option>
                                            <option value="Other" {{ (old('designation', $member_designation) == "Other") ? ' selected="selected"' : '' }}>Other</option>
                                        </select>
                                        @if ($errors->has('designation'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('designation') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Company/Institution*</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="companyName" placeholder="Your company or institution" required value="{{ old('companyName', $member_companyName) }}" />
                                        @if ($errors->has('companyName'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('companyName') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Address 1*</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="address1" placeholder="Address"
                                               required
                                               value="{{ old('address1', $member_address1) }}" />
                                        @if ($errors->has('address1'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('address1') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Address 2</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="address2" placeholder="Address"
                                               value="{{ old('address2', $member_address2) }}" />
                                        @if ($errors->has('address2'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('address2') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Suburb*</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="suburb" placeholder="Suburb"
                                               required
                                               value="{{ old('suburb', $member_suburb) }}" />
                                        @if ($errors->has('suburb'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('suburb') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Post Code*</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="postcode" placeholder="Post Code" required value="{{ old('postcode') }}" />
                                        @if ($errors->has('postcode'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('postcode') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Location*</label>
                                    <div class="col-lg-9">
                                        <select class="form-control" name="state" required>
                                            <option value="">Your state</option>
                                            <option value="ACT" {{ (old('state', $member_state) == "ACT") ? ' selected="selected"' : '' }}>
                                                ACT
                                            </option>
                                            <option value="NSW" {{ (old('state', $member_state) == "NSW") ? ' selected="selected"' : '' }}>
                                                NSW
                                            </option>
                                            <option value="NT" {{ (old('state', $member_state) == "NT") ? ' selected="selected"' : '' }}>
                                                NT
                                            </option>
                                            <option value="QLD" {{ (old('state', $member_state) == "QLD") ? ' selected="selected"' : '' }}>
                                                QLD
                                            </option>
                                            <option value="SA" {{ (old('state', $member_state) == "SA") ? ' selected="selected"' : '' }}>
                                                SA
                                            </option>
                                            <option value="TAS" {{ (old('state', $member_state) == "TAS") ? ' selected="selected"' : '' }}>
                                                TAS
                                            </option>
                                            <option value="VIC" {{ (old('state', $member_state) == "VIC") ? ' selected="selected"' : '' }}>
                                                VIC
                                            </option>
                                            <option value="WA" {{ (old('state', $member_state) == "WA") ? ' selected="selected"' : '' }}>
                                                WA
                                            </option>
                                            <option value="Auckland(NZ)" {{ (old('state', $member_state) == "Auckland(NZ)") ? ' selected="selected"' : '' }}>
                                                Auckland(NZ)
                                            </option>
                                            <option value="Christchurch(NZ)" {{ (old('state', $member_state) == "Christchurch(NZ)") ? ' selected="selected"' : '' }}>
                                                Christchurch(NZ)
                                            </option>
                                            <option value="NA" {{ (old('state', $member_state) == "NA") ? ' selected="selected"' : '' }}>
                                                Not Applicable
                                            </option>
                                        </select>

                                        @if ($errors->has('state'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('state') }}</div>
                                            </div>
                                        @endif

                                        <select id="country" class="form-control" name="country" placeholder="Your country" required>
                                        </select>

                                        @if ($errors->has('country'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('country') }}</div>
                                            </div>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Email Address*</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email"
                                               placeholder="Your email address" required
                                               value="{{ old('email', $member_email) }}" />
                                        @if ($errors->has('email'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('email') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label">Mobile *</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="phoneMobile" name="phoneMobile" required
                                               value="{{ old('phoneMobile', $member_phoneMobile) }}" />
                                        @if ($errors->has('phoneMobile'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('phoneMobile') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            <!--<div class="form-group row">
                                    <label class="col-md-3 col-form-label">Dietary Requirements</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="dietary" placeholder="Dietary" value="{{ old('dietary') }}" />
                                        @if ($errors->has('dietary'))
                                            <div class="fv-plugins-message-container">
                                                <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('dietary') }}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>-->

                            </div>

                            <div class="form-wrapper">
                                <h2>Your Ticket</h2>

                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <select class="form-control" id="event_ticket" name="event_ticket"
                                                placeholder="Your ticket">
                                            @php
                                                $counterPayment = 0;
                                                $paymentHide = true;
                                            @endphp

                                            @foreach ($event_tickets as $event_ticket)
                                                @php
                                                    if ($counterPayment == 0 && $event_ticket->price > 0)  {
                                                       $paymentHide = false;
                                                    }
                                                    $counterPayment++;
                                                @endphp

                                                <option value="{{ $event_ticket->id }}" data-price="{{ $event_ticket->price }}">{{ $event_ticket->name }}
                                                    ${{ number_format($event_ticket->price, 2) }}</option>
                                            @endforeach
                                        </select>
                                        @if(!isset($member))
                                        <a href="{{ url('login') }}">If you are already an ANZSA member, please click here to login before register</a>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            @foreach ($event_tickets as $event_ticket)
                                <input type="hidden" id="hidTicketValue{{ $event_ticket->id }}"
                                       value="{{ $event_ticket->price }}">
                            @endforeach

                            <!--<div class="form-wrapper">
                                <h2>Social Activities</h2>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="checkbox" value="on" name="social" id="social">
                                        <label for="social">
                                            FRIDAY CONFERENCE DINNER (AU$50.00)
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-wrapper">
                                <h2>Partner Registration</h2>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="checkbox" value="on" name="partner" id="partner">
                                        <label for="partner">
                                            PARTNER ATTENDANCE - FRIDAY CONFERENCE DINNER (AU$100.00)
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Partner Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="partner_name"
                                               placeholder="Partner Name" value="{{ old('partner_name') }}"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Partner Dietary Requirements</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="partner_diet" placeholder="Partner Dietary Requirements" value="{{ old('partner_diet') }}"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-wrapper">
                                <h2>Breastfeeding / Childcare services</h2>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="checkbox" value="on" name="breastfeeding" id="breastfeeding">
                                        <label for="breastfeeding">IF YOU SELECT THIS SERVICE FURTHER INFORMATION WILL BE SENT TO YOU SHORTLY FROM THE EVENT CO-ORDINATOR.</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-wrapper">
                                <h2>Delegate List</h2>
                                <p>By leaving this box unticked you are providing confirmation that your name and state
                                    of residence may be included on the delegate list and meeting App for the event
                                    which will be available to all other delegates attending the event, this may include
                                    non-ANZSA members and industry sponsors</p>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="checkbox" value="on" name="delegate_list" id="delegate_list">
                                        <label for="delegate_list">
                                            I DO NOT WISH TO HAVE MY NAME AND STATE INCLUDED IN THE DELEGATE LIST
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-wrapper">
                                <h2>Delegate Acknowledgment</h2>
                                <p>By completing and submitting this Registration Form, I acknowledge and agree
                                    that:</p>
                                <p>I have elected to attend this event organised by ANZSA on a voluntarily basis;</p>
                                <p>I understand that State and Territory governments have different restrictions in
                                    place for interstate and intrastate travel and quarantine requirements that are not
                                    within ANZSA's control;</p>
                                <p>I understand and have considered in my decision to both register and attend the event
                                    that such government imposed travel restrictions and quarantine requirements can be
                                    altered with limited notice to the public;</p>
                                <p>In the event the government of the State or Territory where this event organised by
                                    ANZSA is to be held imposes interstate or intrastate travel restrictions or
                                    quarantine requirements which were not in place either at the time of registration
                                    or at the commencement of the event, I will be solely responsible for any loss that
                                    I may incur as a result of making the decision to attend or stay at the event. Such
                                    loss may include travel expenses, accommodation expenses, complying with quarantine
                                    requirements in the relevant State or Territory and/or any other unanticipated
                                    expenses incurred; and</p>
                                <p>To the full extent permitted by law, I release, waive and discharge ANZSA from any
                                    loss that I may incur as a result of making the decision to attend or stay at the
                                    event.</p>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="checkbox" value="on" name="delegate_ack" required id="delegate_ack">
                                        <label for="delegate_ack">
                                            I AGREE *
                                        </label>
                                    </div>
                                </div>
                            </div>-->

                            <div class="form-wrapper">
                                <h2>Privacy policy</h2>
                                <p>ANZSA is committed to maintaining the privacy of its members, suppliers, conference
                                    delegates and other individuals who interact with ANZSA.
                                <p>
                                <p>ANZSA will be taking photos at this event. These images may be used in AOA/ANZSA
                                    internal publications and published on the AOA/ANZSA website and AOA/ANZSA social
                                    media pages. They will be stored securely until they are of no further use, after
                                    which they will be deleted.</p>
                                <p>Registration cancellations must be made in writing to ANZSA (contact@sarcoma.org.au). No refund will be given for cancellations received within 30 days of the conference.</p>
                            </div>

                            <div class="form-wrapper">
                                <h2>Cancellation</h2>
                                <p>Registration and accommodation cancellations must be made in writing to ANZSA (contact@sarcoma.org.au).
                                    Conference Registration and Accommodation cancellations received up to 30
                                    days prior to the conference receive a full refund. No refund will be given for
                                    cancellations received within 30 days of the conference.</p>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input type="checkbox" value="on" name="cancellation" required id="cancellation">
                                        <label for="cancellation">
                                            I AGREE *
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-wrapper">
                                <div class="row">
                                    <div class="col-md-3">
                                        <h2>Total</h2>
                                    </div>
                                    <div class="col-md-9">
                                        <div id="total">$0</div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-wrapper form-wrapper-white {{ ($paymentHide ? "paymentHide" : "") }}"
                                 id="divPayment">
                                <h2>Payment Type</h2>

                                <div class="form-group row">
                                    <div class="col-md-12 payment-options">
                                        <input type="radio" name="payment_method" value="paypal" checked>
                                        <img src="{{ url('') }}/images/site/Paypal.png" title="PayPal"
                                             alt="PayPal"><br/>

                                    <!--
										<input type="radio" name="payment_method" value="bank-deposit">
										<strong>Bank Deposit</strong><br />

										<label class="form-control-label"> {!!  $payment_direct_deposit_events !!} </label>
										-->
                                    </div>
                                </div>

                                @if ($event_item->id == 3)
                                    <h2>Terms & Conditions</h2>
                                    <p>By registering for the ANZSA virtual ASM, you agree to pay the Registration Fee
                                        (if applicable) during the registration process. </p>
                                @endif
                            </div>

                            <div class="form-group row has-error">
                                <label class="col-md-12 g-recaptcha-container">
                                    <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <div class="fv-plugins-message-container">
                                            <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
                                        </div>
                                @endif
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn-checkout" name="register" value="Register">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection


@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{ asset('/components/jquery-mask-plugin/dist/jquery.mask.js') }}"></script>
    <script src="{{ asset('/components/intl-tel-input/build/js/intlTelInput-jquery.js') }}"></script>
    <script src="{{ asset('/components/intl-tel-input/build/js/utils.js') }}"></script>
    <script src="{{ asset('/components/libphonenumber/dist/libphonenumber.js') }}"></script>
@endsection

@section('inline-scripts')

    <script type="text/javascript">
        $("#event_ticket").change(function () {
            ticket_id = $("#event_ticket").val();
            ticket_val = parseFloat($("#hidTicketValue" + ticket_id).val());

            if (ticket_val > 0) {
                $("#divPayment").removeClass("paymentHide");
            } else {
                $("#divPayment").addClass("paymentHide");
            }

            calculateTotal();

        });

        $(document).ready(function () {
            var countryData = window.intlTelInputGlobals.getCountryData();

            for (var i = 0; i < countryData.length; i++) {
                var country = countryData[i];
                $('#country').append($('<option>', {
                    value: country.name,
                    text: country.name,
                    'data-iso': country.iso2
                }));
            }
            $("#country").val("{{ $member_country }}");

            var phone_mobile = $("#phoneMobile");

            phone_mobile.intlTelInput({
                allowDropdown: false,
                initialCountry: "au",
                separateDialCode: true,
                placeholderNumberType: "MOBILE",
                autoPlaceholder: 'aggressive',
                utilsScript: "{!!  asset('/components/intl-tel-input/build/js/utils.js')  !!}"
            });

            phone_mobile_mask = phone_mobile.attr('placeholder').replace(/[0-9]/g, 0);
            phone_mobile.mask(phone_mobile_mask);

            $("#country").on("change", function () {
                phone_mobile.unmask();

                phone_mobile.val('');
                phone_mobile.intlTelInput("setCountry", $("#country option:selected").attr('data-iso'));
                phone_mobile_mask = phone_mobile.attr('placeholder').replace(/[0-9]/g, 0);
                phone_mobile.mask(phone_mobile_mask);
            });

            $('#social,#partner').change(function() {
                calculateTotal();
            });

            calculateTotal();

        });

        function calculateTotal(){

            total = parseFloat($('#event_ticket').find(':selected').data('price'));

            if ($('#social').is(':checked')) {
                total = total+50;
            }

            if ($('#partner').is(':checked')) {
                total = total+100;
            }

            console.log(total);

            $('#total').html('$'+total);
        }
    </script>

@endsection