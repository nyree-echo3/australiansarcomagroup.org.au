<?php 
   // Set Meta Tags
   $meta_title_inner = $events_item->meta_title; 
   $meta_keywords_inner = $events_item->meta_keywords; 
   $meta_description_inner = $events_item->meta_description;  
?>
@extends('site/layouts/app')

@section('content')

<style>
	.inside-page {height:auto!important;}
	.inside-page .carousel-item {height:auto!important;}
	.inside-page .carousel-item > img {height:auto!important; position: inherit!important; width:100%!important;}

	@media only screen and  (max-width: 480px) {	
	   .inside-page .carousel-item > img {margin-top: 0px!important;}
	   .inside-page .carousel-item { height: 130px!important;}
	}

	@media only screen and  (max-width: 991px) {	
	   .inside-page .carousel-item > img {margin-top: 78px;}
	}	

	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation:portrait) {
		/* For ipad portrait layouts only */
	   .inside-page .carousel-item > img {margin-top: 0px!important;}
	   .inside-page .carousel-item { height: 180px!important;}
	}
</style>
	
@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @if ($events_item->id == 3)
            @include('site/partials/sidebar-events-asm2020')    
        @else   
            @include('site/partials/sidebar-events')                   
        @endif
        @include('site/partials/sidebar-events-book')
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">                          
            <h1 class="blog-post-title">Abstracts</h1>
                       
            <div class='events-item'>                              				
               <div class='events-item-txt'>
			      <p>You need to be a registered participant to view the abstracts. Please <a href='#' class="btnBooking btnBooking-inline">register</a> today.</p>
                  <p>If you have registered, you can view the abstracts via a unique URL link that has been provided in the confirmation email sent to you. </p>
                  <p>If you have any issues, please do not hesitate to contact us (<a href='mailto:contact@sarcoma.org.au'>contact@sarcoma.org.au</a>).</p>
			   </div>          	             	            	            	   
          	 </div>			   	              	      					  								  			            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection
