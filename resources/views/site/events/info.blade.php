<?php 
   // Set Meta Tags
   $meta_title_inner = "Event Register | " . $company_name; 
   $meta_keywords_inner = "Event Register " . $company_name; 
   $meta_description_inner = "Event Register " . $company_name;  
?>

@extends('site/layouts/app')

@section('content')

@if ($event_item->id == 3)
	<style>
		.inside-page {height:auto!important;}
		.inside-page .carousel-item {height:auto!important;}
		.inside-page .carousel-item > img {height:auto!important; position: inherit!important; width:100%!important;}

		@media only screen and  (max-width: 480px) {	
		   .inside-page .carousel-item > img {margin-top: 0px!important;}
		   .inside-page .carousel-item { height: 130px!important;}
		}

		@media only screen and  (max-width: 991px) {	
		   .inside-page .carousel-item > img {margin-top: 78px;}
		}	

		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation:portrait) {
			/* For ipad portrait layouts only */
		   .inside-page .carousel-item > img {margin-top: 0px!important;}
		   .inside-page .carousel-item { height: 180px!important;}
		}
	</style>
@endif
	
@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">                                 
        <div class="col-lg-8 blog-main blog-wide">

          <div class="blog-post">   
               <h1 class="blog-post-title">Event Register</h1>    
               @include('flash::message')  
                             
               <!-- <div class='events-item'>
					<h2>{{ $event_item->title }}</h2>

					<div class="events-item-date">
						@if ($event_item->start_date != $event_item->end_date)
							{{date("D j M Y", strtotime($event_item->start_date))}} {{date("g:ia", strtotime($event_item->start_time))}} - {{date("D j M Y", strtotime($event_item->end_date))}} {{date("g:ia", strtotime($event_item->end_time))}}
						@else
							{{date("D j M Y", strtotime($event_item->start_date))}} - {{date("g:ia", strtotime($event_item->start_time))}} to {{date("g:ia", strtotime($event_item->end_time))}}
						@endif
					</div>		
					
				   <div class="panel-events-item-shortdesc">{!! $event_item->short_description !!}</div>
						
					<div class="btn-back"><a href="{{ url($event_item->url) }}">View Event <i class="fas fa-chevron-right"></i></a></div>
									                  
				</div> -->                                                                                                            
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection            
