<?php
// Set Meta Tags
$meta_title_inner = $events_item->meta_title;
$meta_keywords_inner = $events_item->meta_keywords;
$meta_description_inner = $events_item->meta_description;
?>
@extends('site/layouts/app')

@section('content')

    <style>
        .inside-page {
            height: auto !important;
        }

        .inside-page .carousel-item {
            height: auto !important;
        }

        .inside-page .carousel-item > img {
            height: auto !important;
            position: inherit !important;
            width: 100% !important;
        }

        @media only screen and  (max-width: 480px) {
            .inside-page .carousel-item > img {
                margin-top: 0px !important;
            }

            .inside-page .carousel-item {
                height: 130px !important;
            }
        }

        @media only screen and  (max-width: 991px) {
            .inside-page .carousel-item > img {
                margin-top: 78px;
            }
        }

        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {
            /* For ipad portrait layouts only */
            .inside-page .carousel-item > img {
                margin-top: 0px !important;
            }

            .inside-page .carousel-item {
                height: 180px !important;
            }
        }
    </style>

    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @if ($events_item->id == 6)
                    @include('site/partials/sidebar-events-asm2021')
                @else
                    @include('site/partials/sidebar-events')
                @endif
                @include('site/partials/sidebar-events-book')

                <div class="col-sm-9 blog-main">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Submit Abstract</h1>
                        @if(Session::has('message'))
                            <p class="alert alert-success">{{ Session::get('message') }}</p>
                        @endif
                        <div class='events-item'>
                            <div class='events-item-txt'>
                                <form id="frmRegister" method="POST" action="{{ url('') }}/asm-2021/submit-abstract/store">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-wrapper">
                                        <h2>Presenter Details</h2>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Title*</label>
                                            <div class="col-md-9">

                                                <select class="form-control" name="title" id="title" required>
                                                    <option value="">Your title</option>
                                                    <option value="Assc. Professor" {{ (old('title') == "Assc. Professor") ? ' selected="selected"' : '' }}>
                                                        Assc. Professor
                                                    </option>
                                                    <option value="Dr" {{ (old('title') == "Dr") ? ' selected="selected"' : '' }}>
                                                        Dr
                                                    </option>
                                                    <option value="Ms" {{ (old('title') == "Miss") ? ' selected="selected"' : '' }}>
                                                        Miss
                                                    </option>
                                                    <option value="Mr" {{ (old('title') == "Mr") ? ' selected="selected"' : '' }}>
                                                        Mr
                                                    </option>
                                                    <option value="Mrs" {{ (old('title') == "Mrs") ? ' selected="selected"' : '' }}>
                                                        Mrs
                                                    </option>
                                                    <option value="Ms" {{ (old('title') == "Ms") ? ' selected="selected"' : '' }}>
                                                        Ms
                                                    </option>
                                                    <option value="Professor" {{ (old('title') == "Professor") ? ' selected="selected"' : '' }}>
                                                        Professor
                                                    </option>
                                                    <option value="Sir" {{ (old('title') == "Sir") ? ' selected="selected"' : '' }}>
                                                        Sir
                                                    </option>
                                                </select>

                                                @if ($errors->has('title'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('title') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">First Name*</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="first_name" placeholder="Your first name" required value="{{ old('first_name') }}"/>
                                                @if ($errors->has('first_name'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('first_name') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Last Name*</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="last_name" placeholder="Your last name" required value="{{ old('last_name') }}"/>
                                                @if ($errors->has('last_name'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('last_name') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Email Address*</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" name="email" placeholder="Your email address" required value="{{ old('email') }}" />
                                                @if ($errors->has('email'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('email') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label">Mobile*</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Your mobile number" required value="{{ old('mobile') }}" />
                                                @if ($errors->has('mobile'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('mobile') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-wrapper">
                                        <h2>Hospital/Institution</h2>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Please enter details*</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="hospital" placeholder="Hospital/Institution" required value="{{ old('hospital') }}"/>
                                                @if ($errors->has('hospital'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('hospital') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-wrapper">
                                        <h2>Abstract/Poster Submission</h2>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Is this a poster submission?*</label>
                                            <div class="col-md-9">
                                                <select class="form-control" name="is_poster" id="is_poster" required>
                                                    <option value="No" {{ (old('is_poster') == "No") ? ' selected="selected"' : '' }} selected="selected">No</option>
                                                    <option value="Yes" {{ (old('is_poster') == "Yes") ? ' selected="selected"' : '' }}>Yes</option>
                                                </select>
                                                @if ($errors->has('is_poster'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('is_poster') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Is this a podium submission?*</label>
                                            <div class="col-md-9">
                                                <select class="form-control" name="is_podium" id="is_podium" required>
                                                    <option value="No" {{ (old('is_podium') == "No") ? ' selected="selected"' : '' }} selected="selected">No</option>
                                                    <option value="Yes" {{ (old('is_podium') == "Yes") ? ' selected="selected"' : '' }}>Yes</option>
                                                </select>
                                                @if ($errors->has('is_podium'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('is_podium') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Abstract/Poster Title*</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="abstract_title" placeholder="Maximum 50 words" rows="5" required>{{ old('abstract_title') }}</textarea>
                                                @if ($errors->has('abstract_title'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('abstract_title') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Author(s)/Presenter(s)*</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="authors" rows="5" placeholder="Surname and initials only – eg Smith J Y. Presenter’s name must be first and in CAPITALS & then Other Author's name/s" required>{{ old('authors') }}</textarea>
                                                @if ($errors->has('title'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('authors') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Institution(s)*</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="institutions" placeholder="1-4 centres where the work originated. Give only institution name and city - eg Westmead Hospital, Sydney" rows="5" required>{{ old('institutions') }}</textarea>
                                                @if ($errors->has('institutions'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('institutions') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label">Abstract copy*</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="copy" placeholder="Maximum 400 words, excluding headings but including references. Do not include graphs or diagrams. Use abbreviations only for common terms; for uncommon terms give abbreviation in brackets after first full use of the term. Must include: Introduction & aims, Hypothesis, Method, Results and Conclusions." rows="5" required>{{ old('copy') }}</textarea>
                                                @if ($errors->has('copy'))
                                                    <div class="fv-plugins-message-container">
                                                        <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('copy') }}</div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn-checkout" name="send" value="Submit">Submit</button>
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection
