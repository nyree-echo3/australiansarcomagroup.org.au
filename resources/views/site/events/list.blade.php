<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Events" ? $category_name : $category_name . " - Events"); 
   $meta_keywords_inner = "Events"; 
   $meta_description_inner = ($category_name == "Events" ? $category_name : $category_name . " - Events");  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">        
        @include('site/partials/sidebar-events')                
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">     
            <h1 class="blog-post-title">{{ $category_name }}</h1>
			
			@if ($category_description != "") 
               {!! $category_description !!}
            @endif         					          					
          
           <div class="blog-post row">           
                               	        
            @if(isset($items))  
                 @foreach($items as $item)                
                    <div class="col-lg-6 panel-events">
                       <a href="{{ url('') }}/events/{{ $item->category->slug }}/{{$item->slug}}">
						   <div class="panel-events-item">													                                    
						        @if ($item->thumbnail != "")
									 <div class="div-img">
									   <img src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{$item->title}}" />	
									 </div>
								@endif
								
								<div class="panel-events-item-title">{{$item->title}}</div>
								<div class="panel-events-item-date">
									@if ($item->start_date != $item->end_date)
									    {{date("D j M Y", strtotime($item->start_date))}} {{date("g:ia", strtotime($item->start_time))}} - {{date("D j M Y", strtotime($item->end_date))}} {{date("g:ia", strtotime($item->end_time))}}
									@else
									    {{date("D j M Y", strtotime($item->start_date))}} - {{date("g:ia", strtotime($item->start_time))}} to {{date("g:ia", strtotime($item->end_time))}}
									@endif
								</div>																								
								<hr>								
								<div class="panel-events-item-shortdesc">{!! $item->short_description !!}</div>

								<div class="events-list-more"><a class="btn-home-events" href='{{ url('') }}/events/{{ $item->category->slug }}/{{$item->slug}}'>More <i class="fas fa-chevron-right"></i></a></div>		
						  </div>                                                 
					  </a>
                    </div>                    
                               
				 @endforeach                                         
              
               @else
                 <p>Currently there is no events items to display.</p>    
               @endif
          
             </div><!-- /.blog-post -->
             
             <!-- Pagination -->
             <div id="pagination">{{ $items->links() }}</div>                          
             
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    
@endsection
