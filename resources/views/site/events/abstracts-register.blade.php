<?php 
   // Set Meta Tags
   $meta_title_inner = $events_item->meta_title; 
   $meta_keywords_inner = $events_item->meta_keywords; 
   $meta_description_inner = $events_item->meta_description;  
?>
@extends('site/layouts/app')

@section('content')

<style>
	.inside-page {height:auto!important;}
	.inside-page .carousel-item {height:auto!important;}
	.inside-page .carousel-item > img {height:auto!important; position: inherit!important; width:100%!important;}

	@media only screen and  (max-width: 480px) {	
	   .inside-page .carousel-item > img {margin-top: 0px!important;}
	   .inside-page .carousel-item { height: 130px!important;}
	}

	@media only screen and  (max-width: 991px) {	
	   .inside-page .carousel-item > img {margin-top: 78px;}
	}	

	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation:portrait) {
		/* For ipad portrait layouts only */
	   .inside-page .carousel-item > img {margin-top: 0px!important;}
	   .inside-page .carousel-item { height: 180px!important;}
	}
</style>

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @if ($events_item->id == 3)
            @include('site/partials/sidebar-events-asm2020')    
        @else   
            @include('site/partials/sidebar-events')                   
        @endif
        @include('site/partials/sidebar-events-book')
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">                          
            <h1 class="blog-post-title">Abstracts</h1>
                       
            <div class='events-item'>                              				
               <div class='events-item-txt'>
			      <p><strong>Lead author | Abstract Title</strong></p>

					<ul>
						<li><a href="/web/public/media/Adele%20Lee%20-%20ANZSA_poster_ALedit_230920(1).pdf" target="_blank">Adele Lee</a> |&nbsp;Pre-operative Neutrophil-Lymphocyte Ratio is an<br />
						Indicator of Poor Overall Survival for Primary Retroperitoneal Sarcoma</li>						
						<li><a href="/web/public/media/Catherine%20Mitchell%20-%20ANZSA_BRAF_rearranged_sarcoma.pdf" target="_blank">Catherine Mitchell</a> |&nbsp;Primary Pancreatic Spindle Cell Sarcoma with BRAF gene rearrangement.</li>
						<li><a href="/web/public/media/Christopher%20McEvoy%20-%20ANZSA%20Fusion%20detection%20poster.pdf" target="_blank">Christopher McEvoy</a> |&nbsp;Development of a Detection Method for Sarcoma Related Gene Fusions using Targeted RNA Sequencing</li>
						<li><a href="/web/public/media/Daniel%20Wong%20-%20ANZSA%202020%20poster.pdf" target="_blank">Daniel Wong</a> |&nbsp;Comprehensive Next Generation Sequencing of Sarcomas: Experience from a Western Australian Tertiary Hospital Laboratory and State Sarcoma Service.</li>
						<li><a href="/web/public/media/Edward%20Cheong%20-%20ANZSA2020_PROMS(Final).pdf" target="_blank">Edward Cheong</a> |&nbsp;Assessing the Quality of Patient Reported Outcome Measures in Extremity Bone and Soft Tissue Sarcoma: a COSMIN Systematic Review.</li>
						<li><a href="/web/public/media/Elizabeth%20Connolly%20-%20CIC%20poster.pdf" target="_blank">Elizabeth Connolly</a> |&nbsp;Management and Outcomes of CIC-Rearranged Sarcoma; Australia Multi-Centre Review.</li>
						<li><a href="/web/public/media/Elizabeth%20Connolly%20-%20EHE%20Poster.pdf" target="_blank">Elizabeth Connolly</a> |&nbsp;Epithelioid Hemangioendothelioma (EHE) - Single Institution Experience 2015-2020.</li>
						<li><a href="/web/public/media/Ilycia%20Wutami%20-%20ANZSA%20Poster.pdf" target="_blank">Ilycia Wutami</a> |&nbsp;What It Is Like and How to Recreate It: A&nbsp;Practical Guide for the Biofabrication of a Tissue Engineered 3D Primary Osteosarcoma Model.</li>
						<li><a href="/web/public/media/Joshua%20Serov%20-%20Poster%20Presentation%20ANZSA.pdf" target="_blank">Joshua Serov</a> |&nbsp;Diagnosis and Management of a Splenic Lesion Discovered on Surveillance Imaging after Treatment for Metastatic Osteosarcoma.</li>
						<li><a href="/web/public/media/Dr%20Katie%20Sadler%20%26%20Dr%20Joanna%20Connor%20-%20ANZSA%20poster%20presentation.pdf" target="_blank">Katie Sadler &amp; Dr Joanna Connor</a>&nbsp; |&nbsp;Promising Pembrolizumab? A Case Report on the Role of Immunotherapy in the Management of Metastatic Undifferentiated Pleomorphic Sarcoma.</li>
						<li><a href="/web/public/media/Keith%20Lee%20-%20ANZSA%20poster%20presentation.pdf" target="_blank">Keith Lee</a> |&nbsp;Auckland Tumour Center Myxofibrosarcoma Experience: Are We Treating it Right? An Analysis of Factors Related to Local Recurrence, 5-Year Survival Rate, 5-Year Metastatic Free Survival Rate.</li>
						<li><a href="/web/public/media/Krystel%20Tran%20-%20PRE_OP_RT_RPS.pdf" target="_blank">Kyrstel Tran</a> |&nbsp;Patterns and Predictors of Treatment Failure and Long-term outcomes after Neoadjuvant Radiation Therapy in the Management of Primary Retroperitoneal Sarcoma treated at an Australian Specialist Sarcoma Centre.</li>
						<li><a href="/web/public/media/Michael%20Harris%20-%20ANZSA%20poster%202020.pdf" target="_blank">Michael Harris</a> |&nbsp;The Proteasome Inhibitor Ixazomib Inhibits the Formation and Growth of Pulmonary and Abdominal Osteosarcoma Metastases in Mice.</li>
						<li><a href="/web/public/media/Owen%20Prall%20-%20ANZSA%20poster.pdf" target="_blank">Owen Prall </a>|&nbsp;A Small Round Cell Sarcoma Harbouring a Novel<br />
						MXI1-NUTM1 Fusion Protein with MYC like Activity.</li>
						<li><a href="/web/public/media/Rachael%20Terry%20-%20ANZSA%20Poster.pdf" target="_blank">Rachael Terry</a> |&nbsp;Exploring the Immune Landscape of Relapsed, Refractory, and Rare Paediatric Sarcomas.</li>					
						<li><a href="/web/public/media/Susie%20Bae%20-%20ANZSA%20ASM%202020.pdf" target="_blank">Susie Bae</a> |&nbsp;Managing patients with advanced soft tissue sarcoma: Evolving landscape from an Australian perspective.</li>
						<li><a href="/web/public/media/Thomas%20Chow%20-%20ANZSA%20Poster%202020%20FINAL.pdf" target="_blank">Thomas Chow</a> |&nbsp;Biofabrication of a Tissue Engineered 3D Osteosarcoma Model (3DOSM).</li>
						<li><a href="/web/public/media/William%20McNamara%20-%20ANZSA%20Poster.pdf" target="_blank">William McNamara</a> |&nbsp;10 Year Review of Over 350 Cases Using a Traditional Then Altered Antibiotic Protocol for Tumour Megaprosthesis.</li>
					</ul>

					<p>&nbsp;</p>

					<p>&nbsp;</p>

			   </div>          	             	            	            	   
          	 </div>			   	              	      					  								  			            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection
