<?php 
   // Set Meta Tags
   $meta_title_inner = "Our Sarcoma Research"; 
   $meta_keywords_inner = "Our Sarcoma Research"; 
   $meta_description_inner = "Our Sarcoma Research";  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
       		
        @if (strpos(Request::url(), '/members/projects') !== false) 
           @include('site/partials/sidebar-members-portal')
        @else
           @include('site/partials/sidebar-projects')
        @endif
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">           
            <h1 class="blog-post-title">Clinical Trials and Studies</h1>                             	            	        
           
            @if(isset($items))          
                  <section class="project-block cards-project">
                     <div class="container">	  
                      @php
					    $counter = 0;
					    $category = "";
					  @endphp                   								
					                                                     
					  @foreach($items as $item)    
					    @php
					      $counter++;
					    @endphp 
					    
                        @if ($category != $item->category->name)
                           {!! $item->category->description !!}
                           
                           @php
                              $category = $item->category->name;
                           @endphp
				        @endif
					                      						
					                      								                  										                  								
						<div class='project-list-item'>
							<div class="project-name">{{$item->name}}</div>
							
							<div class="project-title">{{$item->title}}</div>	
								
								<hr>
								
								<div class="project-row">
								   <div class="project-row-field">Summary</div>
								   
								   <div class="project-row-value">{{$item->summary}}</div>								   									
								</div>																
								
								<div class="project-row">
								   <div class="project-row-field">Recruitment Status</div>
								   
								   <div class="project-row-value">{{$item->recruitment_status}}</div>
								   
								   <div class="project-btn">
								      <a id="{{ $counter }}" class="btnMore" href='#'><i id="btnMoreDown" class="fas fa-chevron-down"></i><i id="btnMoreUp" class="fas fa-chevron-up"></i></a>
								   </div>								   									
								</div>
								
								
								
								<div id="divMore{{ $counter }}" class="project-row-more">
								    <div class="project-row">
									   <div class="project-row-field">Eligible Cohort</div>

									   <div class="project-row-value">{{$item->eligible_cohort}}</div>								   									
									</div>
								
									<div class="project-row">
									   <div class="project-row-field">PHASE</div>

									   <div class="project-row-value">{{$item->phase}}</div>								   									
									</div>

									<div class="project-row">
									   <div class="project-row-field">RECRUITMENT TARGET</div>

									   <div class="project-row-value">{{$item->recruitment_target}}</div>								   									
									</div>

									<div class="project-row">
									   <div class="project-row-field">EXPECTED COMPLETION</div>

									   <div class="project-row-value">{{$item->expected_completion}}</div>								   									
									</div>
									
									<div class="project-row">
									   <div class="project-row-field">COUNTRY PI</div>

									   <div class="project-row-value">{{$item->country_pi}}</div>								   									
									</div>

									<div class="project-row">
									   <div class="project-row-field">PARTICIPATING SITES</div>

									   <div class="project-row-value">{{$item->participating_sites}}</div>								   									
									</div>							

									<!--<div class="project-row">
									   <div class="project-row-field">COLLABORATORS</div>

									   <div class="project-row-value">{{$item->collaborators}}</div>								   									
									</div>-->

									<div class="project-row">
									   <div class="project-row-field">CO-SPONSORS</div>

									   <div class="project-row-value">{{$item->co_sponsors}}</div>								   									
									</div>

								    @if ($item->link != "")
									<div class="project-row">
									   <div class="project-row-field">LINK</div>

									   <div class="project-row-value"><a href="{{$item->link}}" target="_blank">{{$item->link}}</a></div>								   									
									</div>
									@endif

									@if ($item->publications != "")
 								    <div class="project-row">
									   <div class="project-row-field">PUBLICATIONS &<br>PRESENTATIONS</div>

									   <div class="project-row-value">{!! $item->publications !!}</div>								   									
									</div>
									@endif
						    
							    </div>
							</div>	<!-- /.project-list-item --> 						
																													                                                    
					   @endforeach  
                                                   
	                  </div><!-- /.container --> 
                   </section>  
                   
                   <!-- Pagination -->
                   <!--<div id="pagination">{{ $items->links() }}</div>-->
              
               @else
                 <p>Currently there is no projects to display.</p>    
               @endif
          
   
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection

@section('inline-scripts')
   <script type="text/javascript">
      $(document).ready(function () {
		  $(".btnMore").click(function (event) {
			 event.preventDefault(); 
             $("#divMore" + $(this).attr('id')).toggle();
			 $("#btnMoreDown").toggle();
			 $("#btnMoreUp").toggle(); 
          });
	  });	  
   </script>
@endsection 
