<?php 
   // Set Meta Tags
   $meta_title_inner = "Find a Sarcoma Specialist"; 
   $meta_keywords_inner = "Find a Sarcoma Specialist"; 
   $meta_description_inner = "Find a Sarcoma Specialist";  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">       		        
        @include('site/partials/sidebar-specialists')        
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">           
            <h1 class="blog-post-title">Find a Sarcoma Specialist</h1>                             	            	        
           
            {!! $content !!}
            
            <div class="specialist-filters">
                <label>Filter the list to find a Sarcoma specialist that suits your requirements</label>
                
                <div class="specialist-filters-item">
					<div class="specialist-filters-item-label">Country</div>
					
					<div class="specialist-filters-item-input">
						<input type="radio" name="radCountry" value="all" class="radCountry" checked>All
						<input type="radio" name="radCountry" value="australia" class="radCountry">Australia
						<input type="radio" name="radCountry" value="newzealand" class="radCountry">New Zealand         
					</div>
				</div>
           
                <div class="divStates">						
					<div class="specialist-filters-item">
						<div class="specialist-filters-item-label">State</div>

						<div class="specialist-filters-item-input">
							<input type="radio" id="state-all" name="radState" value="all" class="radState" checked>All

							<div class="divStateAustralia">	
							    @foreach ($au_states as $au_state)						
								   <input type="radio" name="radState" value="{{ str_replace(" ", "", strtolower($au_state)) }}" class="radState">{{ $au_state }}
								@endforeach																						
							</div>
							
							<div class="divStateNewZealand">	
							    @foreach ($nz_states as $nz_state)						
								   <input type="radio" name="radState" value="{{ str_replace(" ", "", strtolower($nz_state)) }}" class="radState">{{ $nz_state }}
								@endforeach																						
							</div>
						</div>   
					</div>
				</div>
           
            </div>
            
            @if(isset($items))          
                  <section class="project-block cards-project">
                     <div class="container">	  
                      @php
					    $counter = 0;					  
					  @endphp                   								
					                                                     
					  @foreach($items as $item)    
					    @php
					      $counter++;
					    @endphp 					                                  						
					                      								                  										                  								
						<div class='specialist-list-item filter-country-all filter-state-all filter-{{ str_replace(" ", "", strtolower($item->country))}}  filter-{{ str_replace(" ", "", strtolower($item->state)) }}'>
							<div class="specialist-name">{{$item->title}}</div>
							
							
							
							<div class="specialist-row">
							   <div class="project-row-field specialist-row-top-field">Address</div>

							   <div class="project-row-value specialist-row-top-value">
								   {!! $item->address1 !!}
							       {!! ($item->address2 != "" ? "<br>" . $item->address2 : "") !!}
							       <br>{{ $item->suburb }} {{ $item->state }} {{ $item->postcode }} {{ $item->country }}							   
							   </div>								  													
							</div>			
							
							<div class="specialist-row">
							   <div class="project-row-field specialist-row-top-field">Contact</div>

							   <div class="project-row-value specialist-row-top-value">
								   @if ( $item->contact_name != ""){{ $item->contact_name }}<br>@endif
								   @if ( $item->contact_phone != "")<strong>T</strong> <a href="tel:{{ str_replace(')', '', str_replace('(', '', str_replace('+', '', str_replace(' ', '', $item->contact_phone)))) }}">{{ $item->contact_phone }}</a><br> @endif
								   @if ( $item->contact_email != "")<strong>E</strong> <a href="mailto:{{ $item->contact_email }}">{{ $item->contact_email }}</a>	@endif								   
							   </div>								  													
							</div>																															
								
								<hr>
								
															   								   								   						   
								   <div class="specialist-btn">
							          <div class="specialist-title specialist-title-inner">Sarcoma Multidisciplinary Team</div> 
								      <a id="{{ $counter }}" class="btnMore" href='#'><i id="btnMoreDown" class="fas fa-chevron-down"></i><i id="btnMoreUp" class="fas fa-chevron-up"></i></a>
								   </div>														
								
								
								<div id="divMore{{ $counter }}" class="specialist-row-more">							        
							        @if($item->medical_oncologist != "")
										<div class="specialist-row">
										   <div class="specialist-row-field">Medical Oncologist</div>

										   <div class="specialist-row-value">{!! $item->medical_oncologist !!}</div>								   									
										</div>
									@endif
								
								    @if($item->paediatric_medical_oncologist != "")
										<div class="specialist-row">
										   <div class="specialist-row-field">Paediatric Medical Oncologist</div>

										   <div class="specialist-row-value">{!! $item->paediatric_medical_oncologist !!}</div>								   									
										</div>
									@endif

									@if($item->adolescent_medical_oncologist != "")
										<div class="specialist-row">
										   <div class="specialist-row-field">Adolescent Medical Oncologist</div>

										   <div class="specialist-row-value">{!! $item->adolescent_medical_oncologist !!}</div>								   									
										</div>
									@endif

									@if($item->radiation_oncologist != "")
										<div class="specialist-row">
										   <div class="specialist-row-field">Radiation Oncologist</div>

										   <div class="specialist-row-value">{!! $item->radiation_oncologist !!}</div>								   									
										</div>
									@endif
									
									@if($item->surgical_oncologist != "")
										<div class="specialist-row">
										   <div class="specialist-row-field">Surgical Oncologist</div>

										   <div class="specialist-row-value">{!! $item->surgical_oncologist !!}</div>								   									
										</div>
									@endif

									@if($item->orthopaedic_surgeon != "")
										<div class="specialist-row">
										   <div class="specialist-row-field">Orthopaedic Surgeon</div>

										   <div class="specialist-row-value">{!! $item->orthopaedic_surgeon !!}</div>								   									
										</div>
									@endif															

									@if($item->pathologist != "")
								    <div class="specialist-row">
									   <div class="specialist-row-field">Pathologist</div>

									   <div class="specialist-row-value">{!! $item->pathologist !!}</div>								   									
									</div>
							        @endif
								    
									@if($item->radiologist != "")
										<div class="specialist-row">
										   <div class="specialist-row-field">Radiologist</div>

										   <div class="specialist-row-value">{!! $item->radiologist !!}</div>								   									
										</div>
									@endif
									
									@if($item->sarcoma_nurse != "")
										<div class="v-row">
										   <div class="specialist-row-field">Sarcoma Nurse</div>

										   <div class="specialist-row-value">{!! $item->sarcoma_nurse !!}</div>								   									
										</div>
									@endif
									
									@if($item->other != "")
										<div class="specialist-row">
										   <div class="specialist-row-field">Other</div>

										   <div class="specialist-row-value">{!! $item->other !!}</div>								   									
										</div>
									@endif
									
						    
							    </div>
							</div>	<!-- /.project-list-item --> 						
																													                                                    
					   @endforeach  
                                                   
	                  </div><!-- /.container --> 
                  </section>                                 
               @else
                 <p>Currently there is no projects to display.</p>    
               @endif
          
   
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>

<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('inline-scripts')
   <script type="text/javascript">
      $(document).ready(function () {
		  $(".btnMore").click(function (event) {
			 event.preventDefault(); 
             $("#divMore" + $(this).attr('id')).toggle();
			 $("#btnMoreDown").toggle();
			 $("#btnMoreUp").toggle(); 
          });
		  
		  $(".radCountry").click(function () {	
			 selectedCountry = $("input[name='radCountry']:checked").val();
			 
			 if (selectedCountry == "all") { 
			    $(".filter-country-all").show();
				$(".divStates").hide();
			 } else  {
			    $(".filter-country-all").hide();
			    $(".filter-" + selectedCountry).show();
				 
				if (selectedCountry == "australia") {
				   $(".divStateAustralia").show();	
				   $(".divStateNewZealand").hide();		
				} else  {
				   $(".divStateNewZealand").show();	
				   $(".divStateAustralia").hide();		
				} 
				 
				$(".divStates").show(); 
			 }			
		  });
		  
		  $(".radState").click(function () {				 
			 selectedState = $("input[name='radState']:checked").val();
			 selectedCountry = $("input[name='radCountry']:checked").val();
			  
			 if (selectedState == "all") { 
			    $(".filter-country-all").hide();				 			
				$(".filter-" + selectedCountry).show();							
			 } else  {			  
				$(".filter-state-all").hide(); 
			    $(".filter-" + selectedState).show();
				 				
			 }			
		  });
	  });	  
   </script>
@endsection 
