@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @include('site/partials/sidebar-pages')

                <div class="col-sm-9 blog-main">

                    <div class="blog-post row">
                        <div class="col-12">                           
						</div>
                       
                        @if($items)
                            @php $currentCategory = "" @endphp
  
                            @foreach($items as $item)
                                @if ($currentCategory == "" || $currentCategory != $item->category->name)
                                   @if ($currentCategory != "")  
                                      <div class="col-11 team-hr">
                                         <hr>
						              </div>
                                   @endif
                                   
                                   <div class="col-12">                                      
                                      <h1 class="blog-post-title team-h2 {{ ($currentCategory != "" ? 'team-h2-gap' : '') }}">{{ $item->category->name }}</h1>
                                      
                                      @if ($item->category->description != "")
                                          {!! $item->category->description !!}
                                      @endif
						           </div>
                                   
                                   @php $currentCategory = $item->category->name @endphp
                                @endif
                                
                                
								<div class="col-lg-4 team-item">
								  <a href='#' data-toggle="modal" data-target="#modal{{ $item->id }}">				
									   <div class="{{ ($item->category->slug != "sarcoma-research-grant-program" ? "team-a" :"team-a") }}">
										 <div class="div-img {{ ($item->category->slug != "sarcoma-research-grant-program" ? "" :"div-img-grants") }}">
											<img src="{{ url('') }}/{{ $item->photo }}" alt="{{ $item->name }}"> 
										 </div>
										 <div class="team-txt {{ ($item->category->slug != "sarcoma-research-grant-program" ? "" :"team-txt-grants") }}">
											<div class="team-name-band-name">{{ $item->name }}</div>
											<div class="team-name-band-position">{{ $item->job_title }}</div>
											<div class="team-name-band-qualification">{{ $item->role }}</div>											
										 </div>   
									   </div>							
								   </a>     			
								</div> 
                               
                                                                                                                                                                       
                                <!-- The Modal -->
								<div class="modal" id="modal{{ $item->id }}">
								  <div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content">

									  <!-- Modal Header -->
									  <div class="modal-header">
									    <div class="modal-header-content">
										   <div class="team-name-band-name">{{ $item->name }}</div>
										   <div class="team-name-band-position">{{ $item->job_title }}</div>
										   <div class="team-name-band-qualification">{{ $item->role }}</div>	
										</div>
										<hr>
										<div class="modal-btn">
								           <a class="btnMore" href='#' data-dismiss="modal"><i id="btnMoreDown" class="fas fa-times"></i></a>
								        </div>											
									  </div>

									  <!-- Modal body -->
									  <div class="modal-body">
								        <div class="modal-body-body">								           
								           @if ($item->category->slug != "sarcoma-research-grant-program")
								              <div class="modal-body-txt"> {!! $item->body !!}</div>
									          <div class="modal-body-img"><img src="{{ url('') }}/{{ $item->photo }}" alt="{{ $item->name }}"></div> 										   
									       @else
									          <div class="modal-body-txt-grants"> {!! $item->body !!}</div>
									       @endif
										</div>
									  </div>
									  
									  <!-- Modal footer -->
									  <div class="modal-footer">										
									  </div>
									  
									</div>
								  </div>
								</div> 
                            @endforeach

                        @else
                            <p>Currently there is no team member to display.</p>
                        @endif

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
@endsection
