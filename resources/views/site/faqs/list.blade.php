@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-faqs')        
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post faqs">          
            @if (isset($items))        
				@foreach ($items as $item)					
					<h4><a class="accordion-toggle" data-toggle="collapse" href="#faqitem{{ $loop->iteration }}">{{$item->title}}</a></h4>
					
					<div id="item{{ $loop->iteration }}" class="panel-collapse collapse in"> {!! $item->description !!} </div>                                   					
				@endforeach 
            @endif			     
         
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
