<?php 
   // Set Meta Tags
   $meta_title_inner = $category_name;
   $meta_keywords_inner = $category_name;
   $meta_description_inner = $category_name;  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">        
        @include('site/partials/sidebar-news-wide')                
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">     
            <h1 class="blog-post-title">{{ $category_name }}</h1>
			
			@if ($category_description != "") 
               {!! $category_description !!}
            @endif         					          					
          
           <div class="blog-post row m-0">           
                               	        
            @if(isset($items))  
                 @foreach($items as $item)                                    
                       						
					<div class="col-sm-3 panel-news-item-wide panel-news-item-wide-1">						             								                                    
						@if ($item->thumbnail != "")
							<a href="{{ url('') }}/{{ $item->url }}">		             
							    <div class="div-img">
							     <img src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{$item->title}}" />	
							   </div>
							</a>
						@endif

					</div>

					<div class="col-sm-9 panel-news-item-wide panel-news-item-wide-2">
					   <a href="{{ url('') }}/{{ $item->url}}">
						 <div class="panel-news-item-title">{{$item->title}}</div>
						 <div class="panel-news-item-date">{{date("M Y", strtotime($item->start_date))}}</div>																								
						 <hr>								
						 <div class="panel-news-item-shortdesc">{!! $item->short_description !!}</div>

						 <div class="news-list-more"><a class="btn-home-news" href='{{ url('') }}/{{ $item->url }}'>More <i class="fas fa-chevron-right"></i></a></div>		
					   </a>
					</div>        
                               
				 @endforeach                                         
              
               @else
                 <p>Currently there is no news items to display.</p>    
               @endif
          
             </div><!-- /.blog-post -->
             
             <!-- Pagination -->
             <div id="pagination">{{ $items->links() }}</div>                          
             
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
        
        <div class="news-newsletter">
                @include('site/partials/index-newsletter')
             </div>
@endsection
