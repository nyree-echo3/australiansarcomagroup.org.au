@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-contact')
                     
        <div class="col-sm-9 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Newsletter Subscription</h1>    
               <p>Thank you for subscribing to the ANZSA newsletter and sarcoma news and events! </p>             
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection            
