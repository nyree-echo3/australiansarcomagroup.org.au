<?php 
   // Set Meta Tags
   $meta_title_inner = $news_item->meta_title; 
   $meta_keywords_inner = $news_item->meta_keywords; 
   $meta_description_inner = $news_item->meta_description;  
?>
@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @if ($news_item->category->members_only == "active")
           @include('site/partials/sidebar-members-portal')        
        @else
           @include('site/partials/sidebar-news')        
        @endif            
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">                          
            <h1 class="blog-post-title">{{ $news_item->title }}</h1>
            
            @if ($category_slug == "newsletters")
                <div class='newsletter-container'>
            @endif
            
            <div class='news-item'>
               <div class='news-item-img'>
          	      @if($news_item->thumbnail != "")
			      <img src='{{ url('') }}/{{ $news_item->thumbnail }}' alt='{{ $news_item->title }}'>
			      @endif
			   </div>
              
               <div class='news-item-txt'>
			      {!! $news_item->body !!}
			   </div>          	             	   
         	   
         	   @if ($category_slug == "newsletters")
         	   <div class='news-item-foot'> 
				   <strong>Newsletter of the Australia and New Zealand Sarcoma Association VCCC</strong></br>
				    Level 1, 305 Grattan Street, Melbourne Victoria 3000 Australia<br>
				    <strong>T</strong> <a href="tel:1800177657">1800 177 657</a> <strong>F</strong> 613 9656 5875 <strong>E</strong> <a href="mailto:contact@sarcoma.org.au">contact@sarcoma.org.au</a> <strong>W</strong> <a href="http://www.sarcoma.org.au">www.sarcoma.org.au</a>
			   </div>
         	  @endif
         	  
          	 </div>
         	 
         	 @if ($category_slug == "newsletters")
                </div>
             @endif
                    
          	<div class='btn-back'>
           	    <a class='btn-home-news' href='{{ url('') }}/{{ $news_item->category->url }}'><i class="fas fa-chevron-left"></i> back</a>	
			 </div>
          	   
           	 @include('site/partials/helper-sharing')	             					  								  			            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection
