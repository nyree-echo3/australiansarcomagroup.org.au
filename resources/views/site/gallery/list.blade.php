@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-gallery')        
        
        <div class="col-sm-8 blog-main">
          <h1>Galleries</h1>
                   
		  @if(isset($items))
		     <section class="gallery-block cards-gallery">
	            <div class="container">	        
	               <div class="row">
	               
						 @foreach ($items as $item)
                             <div class="col-md-6 col-lg-4">
								<div class="card border-0 transform-on-hover">
									<a class="lightbox" href="{{ url('').'/'.$item->url }}">
										<img src="{{ url('') }}{{$item->images[0]->location}}" alt="{{$item->images[0]->name}}" class="card-img-top">
									</a>
									<div class="card-body">
										<h6><a href="{{ url('').'/'.$item->url }}">{{$item->name}}</a></h6>
										<p class="text-muted card-text">{!! $item->description !!}</p>
									</div>
								</div>
							</div>							   

						 @endforeach
	 	        
		 	       </div>
	            </div>
             </section>  
		  @endif			                                                                                                                                                                                                                                                                                                                                                                  
                                                                                                                                                                                                
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
