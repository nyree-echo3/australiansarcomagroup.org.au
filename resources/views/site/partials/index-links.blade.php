<!-- Slideshow Cycle http://jquery.malsup.com/cycle2/demo/-->
<script src="{{ url('') }}/js/site/jquery-1.22.4.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/js/site/jquery.cycle2.js"></script>
<script src="{{ url('') }}/js/site/jquery.cycle2.carousel.min.js"></script>

<script>
$.fn.cycle.defaults.autoSelector = '.links-carousel';
</script>

<div class="home-links-panel">
		<div class="home-links">
	       <h2>Our Partners</h2>
			<p>ANZSA is grateful to all our partners (individuals, corporate companies philanthropic trusts and foundations) for their kind support towards the sarcoma community and ANZSA. With your continuous funding, we can improve outcomes for sarcoma patients and their families.</p>	    		   
		</div>


	<div class="links-carousel" 
	   data-cycle-fx=carousel
	   data-cycle-carousel-visible=5
		data-cycle-timeout=1
		data-cycle-speed=5000
		data-cycle-throttle-speed=true
		data-cycle-easing=linear
		data-cycle-slides=span
		>

			@foreach ($home_links as $home_link)
			   <span><a href='{{ $home_link->url }}' target='_blank' title='{{ $home_link->name }}'><img src='{{ url('') }}/{{ $home_link->image }}' ></a></span>		
			@endforeach

	</div> 
</div>
	