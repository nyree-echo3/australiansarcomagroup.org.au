@if(isset($home_news))
	 <div class="home-news">
       <h2>Latest News</h2>
	   <div class="container">
		  <div class="row">         
			 @foreach($home_news as $item)       	 
				  <div class="col-lg-4">
		               <div class="home-news-item">
						   <div class="home-news-item-box">
							  <div class="home-news-box">
								  <div class="home-news-box-line">
								  {{ date("M", strtotime($item->start_date)) }}
								  <br>
								  {{ date("Y", strtotime($item->start_date)) }}</div>
							  </div>
						   </div>

						   <div class="home-news-item-txt">
							   <h3>{{ $item->title }}</h3>							   
							   <a class="btn-home-news" href="{{ url('') }}/news/{{ $item->category->slug }}/{{ $item->slug }}" role="button">more</a>			           				       			   					 					   				  
						   </div>
					   </div>
					   
				  </div><!-- /.col-lg-4 -->
			 @endforeach 	

			</div>
	   </div>
	</div>
@endif