<div class="col-sm-3 blog-sidebar">
	<div class="sidebar-module">
		<h4>{{ $category[0]->name }}</h4>
		<ol class="list-group list-unstyled list-group-flush">
			{!! $side_nav !!}
		</ol>				
	</div>
	
	@if (strtolower($category[0]->slug) == "about-us" && isset($page) && $page->slug == "about-anzsa")
		<div class="sidebar-charity">
			The Australia and New Zealand Sarcoma Association is registered as a charity with the Australian Charities and Not-for-profits Commission ABN 73 132 759 525
			<div class="sidebar-charity-img">
			   <img src="{{ url('') }}/images/site/logo-acnc.png" title="Registered Charity" alt="Registered Charity">
			</div>
		</div>
	@endif
</div>