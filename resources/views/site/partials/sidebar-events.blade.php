<div class="col-sm-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>Events</h4>
        <ol class="navsidebar list-unstyled list-group-flush">
            @if($side_nav_mode=='manual')
                {!! $side_nav !!}
            @endif
            @if($side_nav_mode=='auto')
                @foreach ($side_nav as $item)
                    @if ($item->url == "events/asm-2020")
                       <li class='list-group-item'><a class="navsidebar" href="{{ url('') }}/asm-2020/welcome">{{ $item->name }}</a></li>
                    @elseif ($item->url == "events/agm-2020")
                       <li class='list-group-item'><a class="navsidebar" href="{{ url('') }}/agm-2020">{{ $item->name }}</a></li>   
                    @else
                       <li class='list-group-item'><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a></li>
                    @endif
                @endforeach
            @endif
        </ol>       
    </div>
</div>