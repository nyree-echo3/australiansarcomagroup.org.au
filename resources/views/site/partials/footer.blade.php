<footer class='footer'>
 <div class='footerContentWrapper'>
	 <div class='footerContent'>
        <div class="panelNav">
	    <div class="container-fluid">
		<div class="row">
       
           <div class="col-lg-5">
		      <div class="center-block">
			     <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-rev.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
			     <div class="footer-logo-old">
			        Formerly known as the Australasian<br>Sarcoma Study Group and the Australian<br>Sarcoma Group
			     </div>
			  </div>			  			 
		   </div>  
                	              		   		   
		   
		   <div class="col-lg-3 ">
			   <div class="center-block footer-address">
			   {!! $contact_details !!}	
			   </div>
		   </div>
		   
		   <div class="col-lg-4">
			   <div class="center-block">
			   @if ( $phone_number != "") <strong>T</strong> <a href="tel:{{ str_replace(' ', '', $phone_number) }}">{{ $phone_number }}</a><br> @endif 
			   @if ( $email != "") <strong>E</strong> <a href="mailto:{{ $email }}">{{ $email }}</a><br> @endif 
			   
			    <div class="footer-charity">
			       <div class="footer-charity-txt">
						The Australia and New Zealand<br>
						Sarcoma Association is registered<br>
						as a charity with the Australian<br>
						Charities and Not-for-profits<br>
						Commission ABN 73 132 759 525
					</div>
					
					<div class="footer-charity-img">
					   <img src="{{ url('') }}/images/site/logo-acnc.png" title="Registered Charity" alt="Registered Charity">
					</div>					
			   </div>
		   </div>
		  
		</div>				
  
	 </div> 	 	 		
 </div> 
	
 </div>
 
	
  </div>
  
    <div id="footer-txt"> 
		@if ( $company_name != "")<a href="{{ url('') }}">&copy; {{ date('Y') }} All rights reserved</a> |@endif 
		<a href="{{ url('') }}">ANZSA receives funding from the Australian Government through Cancer Australia</a> | 
		<a href="{{ url('') }}/pages/other/terms-and-conditions">Terms & Conditions</a> |
		<a href="{{ url('') }}/pages/other/privacy-policy" target="_blank">Privacy Policy</a> |
		<a href="{{ url('') }}/contact">Contact</a> |     
		<a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a> 

	</div>
</footer>