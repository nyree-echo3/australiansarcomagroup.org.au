<div class="header-search" id="divSearch">
	<form class="form-inline mt-2 mt-md-0" method="post" action="{{ url('results') }}">
        {{ csrf_field() }}
	    <div class="header-search-input">
		   <input name="query" type="textbox" placeholder="What are you looking for?" />
		</div>
		
		<button class="btn-search" type="submit"><i class='fas fa-search'></i> Search</button>
	</form>
</div>