 <div class="home-buttons">
	  <div class="row">  
	      <div class="col-6 col-lg-3 home-buttons-box">
		     <a href="{{ url('') }}/pages/research/our-sarcoma-research">
		        <div class="home-buttons-box1">
			       <h2>Research</h2>	
                   <img src="{{ url('') }}/images/site/homepage-icon-research.png" alt="Research" />	
                   <div class="home-buttons-more">ANZSA invests in high-quality sarcoma research. </div>							   
			    </div> 
			  </a>			   
		  </div><!-- /.col-lg-3 -->
		         		   	 
		  <div class="col-6 col-lg-3 home-buttons-box">
	         <a href="{{ url('') }}/pages/about-sarcoma/find-a-sarcoma-specialist">
		        <div class="home-buttons-box2">
			      <h2>Sarcoma Specialist</h2>
                  <img src="{{ url('') }}/images/site/homepage-icon-specialist.png" alt="About Us" />	
                  <div class="home-buttons-more">Find an ANZSA-approved sarcoma specialist from Australia and New Zealand</div>						   			   
			    </div>   			   
			 </a>
		  </div><!-- /.col-lg-3 -->								  				  
		
		  <div class="col-6 col-lg-3 home-buttons-box">
		     <a href="{{ url('') }}/pages/support-us">
		        <div class="home-buttons-box3">
			       <h2>Support Us</h2>	
                   <img src="{{ url('') }}/images/site/homepage-icon-supportus.png" alt="Support Us" />	
                   <div class="home-buttons-more">100% of your donations go towards crucial sarcoma research to save and improve lives. </div>						
			    </div> 
			 </a> 			   
		  </div><!-- /.col-lg-3 -->		
		  
		  <div class="col-6 col-lg-3 home-buttons-box">
		     <a href="{{ url('') }}/news">
		        <div class="home-buttons-box4">
			       <h2>Events & News</h2>	
                   <img src="{{ url('') }}/images/site/homepage-icon-eventsandnews.png" alt="Events & News" />
                   <div class="home-buttons-more">Keep up-to-date with the latest news from ANZSA. </div>								   
			    </div> 
			 </a>			   
		  </div><!-- /.col-lg-3 -->		
		  
	   </div>

</div>
