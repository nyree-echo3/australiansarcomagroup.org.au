<input type="hidden" id="hidMembersOnlyEvent" name="hidMembersOnlyEvent" value="{{ $events_item->members_only }}">
<input type="hidden" id="hidIsMemberLoggedIn" name="hidIsMemberLoggedIn" value="{{ $isMemberLoggedIn }}">
          	 
<div id="jQueryPopupDiv" style="display: none"></div>

@section('inline-scripts-events')
	<script src="{{ asset('js/site/events.js') }}"></script>
	<script>
	$(document).ready(function() {
		$(".btnBooking, .btnRegister").click(function(e) {
			btnBooking_onclick('{{ $events_item->slug }}', '{{ url('') }}');
			e.stopPropagation();
		});	
	});	
	</script>
@endsection