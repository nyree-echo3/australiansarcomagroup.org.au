@if ($events_item->id == 3)
	<style>
		.inside-page {height:auto!important;}
		.inside-page .carousel-item {height:auto!important;}
		.inside-page .carousel-item > img {height:auto!important; position: inherit!important; width:100%!important;}
		
		@media only screen and  (max-width: 480px) {	
		   .inside-page .carousel-item > img {margin-top: 0px!important;}
		   .inside-page .carousel-item { height: 130px!important;}
		}
		
		@media only screen and  (max-width: 991px) {	
		   .inside-page .carousel-item > img {margin-top: 78px;}
		}	
		
		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation:portrait) {
            /* For ipad portrait layouts only */
		   .inside-page .carousel-item > img {margin-top: 0px!important;}
		   .inside-page .carousel-item { height: 180px!important;}
		}
	</style>
@endif
<div class="col-sm-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>Annual Scientific Meeting 2020</h4>
        <ol class="navsidebar list-unstyled list-group-flush"> 
                <li class='list-group-item {{ (Request::url() == url('').'/'.$events_item->url ? "active" : "") }}'><a class="navsidebar" href="{{ url('').'/'.$events_item->url }}">Welcome</a></li>                      
                @foreach ($asm_pages as $item)
                    <li class='list-group-item {{ (Request::url() == url('').'/'.$item->url ? "active" : "") }}'><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->title }}</a></li>
                @endforeach
                <li class='list-group-item {{ (strpos(Request::url(), url('').'/asm-2020/abstracts') !== false ? "active" : "") }}'><a class="navsidebar" href="{{ url('')}}/asm-2020/abstracts">Abstracts</a></li>   
        </ol>                     
    </div>
    
    <!-- <input type="button" class="btnBooking" value="Register Now" /> -->
</div>