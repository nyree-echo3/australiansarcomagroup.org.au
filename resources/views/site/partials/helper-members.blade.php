<div class='navbar-members'>
   @guest("member")
      <!--<a href='{{ url('') }}/register'>Register</a> | -->
      <a href='{{ url('') }}/login'><i class="fas fa-arrow-right"></i> Members Login</a>  
   @endguest
   
   @auth("member")
      <a href='{{ url('') }}/members-portal'><i class="fas fa-arrow-right"></i> Members Portal</a>
      <!--<a href='{{ url('') }}/logout'>Logout</a>  -->
   @endauth
</div>