<div id="myCarousel" class="carousel slide" data-ride="carousel">
  @include('site/partials/helper-donation')            
  @include('site/partials/helper-members')

  <ol class="carousel-indicators">
    <?php $counter = 0; ?>
    @foreach($images as $image)        
       <li data-target="#myCarousel" data-slide-to="{{ $counter }}" class="{{ $counter == 0 ? 'active' : '' }}"></li>       
       <?php $counter++;  ?>
    @endforeach 
  </ol>
  <div class="carousel-inner">
    
        <?php $counter = 0; ?>
        @foreach($images as $image) 
            <?php $counter++;  ?>
               
			<div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
			  <img class="slide" src="{{ url('') }}/{{ $image->location }}" alt="{{ $image->title }}">
			  <div class="container">
				<div class="carousel-caption text-left">
			      <div class="carousel-caption-div">					  
					  @if ( $image->url != "")
				         <a href="{{ $image->url }}" target="_blank"  class="btnCarousel-a">
					     <h2>{{ $image->title }}</h1>
					     </a>
						 <div class="btnCarousel"><a href="{{ $image->url }}" target="_blank">Find Out More <i class="fas fa-arrow-right"></i></a></div> 
					  @else
					     <h2>{{ $image->title }}</h1>
					  @endif
				  </div>
				</div>
			  </div>
			</div>
        @endforeach
   
  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>