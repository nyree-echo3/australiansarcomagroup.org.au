<!-- Add social media sharing include -->
<!-- AddToAny BEGIN -->
<div id='share'>
<div class='a2a_kit a2a_default_style'>
<div class="share-space">
<a href="javascript:window.print()"><i class='fa fa-print'></i></a>
<a href="{{ url('') }}/contact"><i class='fa fa-envelope'></i></a>
<a id="bookmarkme" href="#" rel="sidebar" title="bookmark this page"><i class='fa fa-bookmark'></i></a>
</div>
<a class='a2a_button_facebook'></a>
<a class='a2a_button_twitter'></a>
<a class='a2a_button_google_plus'></a>
<a class='a2a_dd' href='http://www.addtoany.com/share_save'></a>
</div></div>
<script type='text/javascript'>
var a2a_config = a2a_config || {};
a2a_config.onclick = 1;
a2a_config.color_main = 'f8f8f8';
a2a_config.color_border = 'f8f8f8';
a2a_config.color_link_text = '333333';
a2a_config.color_link_text_hover = '1870dc';
</script>
<script type='text/javascript' src='//static.addtoany.com/menu/page.js'></script>
<!-- AddToAny END -->

<script type="text/javascript">
    $(function() {
        $('#bookmarkme').click(function() {
            if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
                window.sidebar.addPanel(document.title,window.location.href,'');
            } else if(window.external && ('AddFavorite' in window.external)) { // IE Favorite
                window.external.AddFavorite(location.href,document.title); 
            } else if(window.opera && window.print) { // Opera Hotlist
                this.title=document.title;
                return true;
            } else { // webkit - safari/chrome
                alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != - 1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
            }
        });
    });
</script>