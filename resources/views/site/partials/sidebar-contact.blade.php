<div class="col-sm-3 blog-sidebar">         
  <div class="sidebar-module">
  <!--<h4>Contact</h4>-->
	
	<div class="contact-details">
	   {!! $contact_details !!}	   
	</div>	
	
	<div class="contact-social">
        Follow us on
	    @if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif
	    @if ( $social_twitter != "") <a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a> @endif 
	</div>
  </div>          
</div>