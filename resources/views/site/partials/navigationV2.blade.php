<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark btco-hover-menu navbar-custom">   
    <button class="navbar-toggler custom-toggler hamburger hamburger--collapse hamburger--accessible js-hamburger" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="hamburger-box">
		<span class="hamburger-inner"></span>
	  </span>
	</button>
   
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <!--<li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
                <a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i> <span
                            class="sr-only">(current)</span></a>
            </li>-->
            {!! $navigation !!}
        </ul>        
    </div>
</nav>

@section('inline-scripts-navigation')
    <script type="text/javascript">
		  var $hamburger = $(".hamburger");
		  $hamburger.on("click", function(e) {
			$hamburger.toggleClass("is-active");
			// Do something else, like open/close menu
		  });
    </script>
@endsection