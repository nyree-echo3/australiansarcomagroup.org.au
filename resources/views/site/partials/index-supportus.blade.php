<div class="panel-supportus">
  <div class="home-supportus">
	<div class="container">
	   <div class="row">
		  <div class="col-lg-3">
			 <img src="{{ url('') }}/images/site/homepage-supportourresearch.png" title="Support Our Research" alt="Support Our Research"> 
		  </div>

		  <div class="col-lg-9">
			 {!! $home_supportus_text !!}

			 <div class="btn-home-supportus">
				<a href='{{ url('') }}/pages/support-us'>Learn More</a>   
			 </div>

		  </div>			  
	   </div>
	</div>
 </div>
</div>