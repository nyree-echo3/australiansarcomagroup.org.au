<!-- Header -->
<div class="header">
    <div class="header-logo">
        <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
        
    </div>    
    <div class="header-logo-old"><span>Formerly known as the Australasian Sarcoma Study Group<br>and the Australian Sarcoma Group</span>
</div>
    
    <div class="header-buttons">
       <a href="#" id="btnSearch"><i class='fas fa-search'></i></a>
       @if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif
	   @if ( $social_twitter != "") <a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a> @endif 
	</div>
   
    @include('site/partials/helper-search')      
</div>

@section('header-search-scripts')
   <script type="text/javascript">
      $(document).ready(function () {
		  $("#btnSearch").click(function () {
             $("#divSearch").fadeToggle(2000);
          });
	  });	  
   </script>
@endsection 
