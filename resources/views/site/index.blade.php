@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')

@include('site/partials/index-buttons')

<div class="container marketing divHome">

	<!-- Intro Text -->
	<div class="row featurette">
	  <div class="home-intro">
		<div class="container">
		   <div class="row">
			  <div class="col-lg-9">
				 <?php echo $home_intro_text; ?>

				 <div class="btn-home-intro">
					<a href='{{ url('') }}/pages/about-us'>Learn More</a>   
				 </div>

			  </div>

			  <div class="col-lg-3">
				 <img src="{{ url('') }}/images/site/hompeage-whoweare.png" title="Who we are" alt="Who we are"> 
			  </div>
		   </div>
	   </div>
	</div>

	</div>
	
</div>

@include('site/partials/index-news')
@include('site/partials/index-newsletter')
@include('site/partials/index-supportus')
@include('site/partials/index-links')

@endsection
