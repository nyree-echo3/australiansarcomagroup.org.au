<!DOCTYPE html>
<html>
<head>
	<style>
		h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
		h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
	</style>
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">

<p>Dear {{ $event_booking->first_name }},</p>

<p>Thank you for your registration. We are thankful that you can join us online over two days for the ANZSA ASM 2021.</p>

<p>You can view more details of the programs and international speakers <a href="{{ url('') }}/asm-2021/welcome">here.</a></p>

<h2>Registration Details</h2>
<table class="table">
	<tr>
		<th align="left" style="width:150px">Your Ticket:</th>
		<td align="left">{{ $ticket->name }}</td>
	</tr>

	@if ($event_payment)
		<tr>
			<th style="width:100px" align="left">Payment Method:</th>
			<td align="left">
				@if($event_payment->payment_method=='paypal')
					PayPal
				@endif
				@if($event_payment->payment_method=='bank-deposit')
					Bank Deposit
				@endif
			</td>
		</tr>

		<tr>
			<th align="left">Amount:</th>
			<td align="left">${{ number_format($event_payment->amount,2) }}</td>
		</tr>

		<tr>
			<th align="left">Status:</th>
			<td align="left">{{ ucfirst($event_payment->payment_status) }}</td>
		</tr>

		@if($event_payment->payment_method=='bank-deposit')
			<tr>
				<th align="left"></th>
				<td align="left">
					{!! $payment_direct_deposit_events !!}
					<p><b>Your registration will be activated once your payment is approved.</b></p>
				</td>
			</tr>
		@endif

		@if($event_payment->payment_method=='paypal')
			<tr>
				<th align="left">Transaction Number:</th>
				<td align="left">{{ $event_payment->payment_transaction_number }}</td>
			</tr>
			<tr>
				<th align="left">PayPal Payer ID:</th>
				<td align="left">{{ $event_payment->paypal_payer_id }}</td>
			</tr>
		@endif
	@endif
	<tr>
		<th align="left">&nbsp</th>
		<td align="left">&nbsp</td>
	</tr>
</table>

<p>Should you have any questions or problems with login in, please email <a href="mailto:contact@sarcoma.org.au">contact@sarcoma.org.au</a>.</p>

<p>Regards,<br>Australia and New Zealand Sarcoma Association (ANZSA)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>