<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Hi {{ $member->firstName }},</p>

<p>Thank you for renewing your membership!</p>

<h2>Member Details</h2>
<table class="table">
    <tr>
        <th align="left" style="width:150px">Membership Type :</th>
        <td align="left">{{ $member->type->name }}</td>
    </tr>
    <tr>
        <th align="left">Full Name :</th>
        <td align="left">{{ $member->full_name }}</td>
    </tr>
    <tr>
        <th align="left">Join Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateJoin)->format('d-m-Y') }}</td>
    </tr>
    <tr>
        <th align="left">Expire Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateExpire)->format('d-m-Y') }}</td>
    </tr>
</table>
@if($payment->payment_method=='bank-deposit')
    {!! $payment_direct_deposit_members !!}
    <p>Your account will be renewed once your payment is approved.</p>
@endif
@if($payment->payment_method=='paypal')
    <p>Your account has been renewed and ready to use. <a href="{{ url('login') }}">Please click here for login.</a></p>
@endif

<h2>Payment Details</h2>
<table class="table">
    <tr>
        <th style="width:100px" align="left">Method :</th>
        <td align="left">
            @if($payment->payment_method=='paypal')
                PayPal
            @endif
            @if($payment->payment_method=='bank-deposit')
                Bank Deposit
            @endif
        </td>
    </tr>

    <tr>
        <th align="left">Amount :</th>
        <td align="left">${{ number_format($payment->amount,2) }}</td>
    </tr>

    <tr>
        <th align="left">Status :</th>
        <td align="left">{{ ucfirst($payment->payment_status) }}</td>
    </tr>

    @if($payment->payment_method=='paypal')
        <tr>
            <th align="left">Transaction Number :</th>
            <td align="left">{{ $payment->payment_transaction_number }}</td>
        </tr>
        <tr>
            <th align="left">PayPal Payer ID :</th>
            <td align="left">{{ $payment->paypal_payer_id }}</td>
        </tr>
    @endif
    <tr>
        <th align="left">&nbsp</th>
        <td align="left">&nbsp</td>
    </tr>
</table>

<p>Kind regards,<br>
Australia and New Zealand Sarcoma Association (ANZSA)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
