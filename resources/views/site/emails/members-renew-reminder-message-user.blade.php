<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>
</head>
<body>
<img src="{{ config('app.url') }}/images/site/email-logo.png">

<p>Hi {{ $member->firstName }},</p>

<p>Your membership with us requires renewal soon.  Please visit our website to renew your membership.</p>

<p><a href='{{ config('app.url') }}/members-portal/renew'>{{ config('app.url') }}/members-portal/renew</a></p>

<p>Kind regards,<br>
Australia and New Zealand Sarcoma Association (ANZSA)</p>
<br>
<img src="{{ config('app.url') }}/images/site/email-thanks.png">
</body>
</html>
