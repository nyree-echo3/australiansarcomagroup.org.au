<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>	
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<h1>Website | Abstract Submission</h1>

<h2>Presenter Details</h2>
<table class="table">    
    <tr>
        <th align="left">Title :</th>
        <td align="left">{{ $abstract->title }}</td>
    </tr>
    <tr>
        <th align="left">First Name :</th>
        <td align="left">{{ $abstract->first_name }}</td>
    </tr>
    <tr>
        <th align="left">Last Name :</th>
        <td align="left">{{ $abstract->last_name }}</td>
    </tr>
    <tr>
        <th align="left">E-mail :</th>
        <td align="left">{{ $abstract->email }}</td>
    </tr>
</table>

<h2>Hospital/Institution</h2>
<table class="table">    
    <tr>
        <th align="left">Details :</th>
        <td align="left">{{ $abstract->hospital }}</td>
    </tr>
</table>

<h2>Abstract/Poster Submission</h2>
<table class="table">
    <tr>
        <th align="left">Title :</th>
        <td align="left">{{ $abstract->title }}</td>
    </tr>
    <tr>
        <th align="left">Is this a poster submission?</th>
        <td align="left">{{ $abstract->first_name }}</td>
    </tr>
    <tr>
        <th align="left">Is this a podium submission?</th>
        <td align="left">{{ $abstract->last_name }}</td>
    </tr>
    <tr>
        <th align="left">Abstract/Poster Title :</th>
        <td align="left">{{ $abstract->abstract_title }}</td>
    </tr>
    <tr>
        <th align="left">Author(s)/Presenter(s) :</th>
        <td align="left">{{ $abstract->authors }}</td>
    </tr>
    <tr>
        <th align="left">Institution(s) :</th>
        <td align="left">{{ $abstract->institutions }}</td>
    </tr>
    <tr>
        <th align="left">Abstract copy :</th>
        <td align="left">{{ $abstract->copy }}</td>
    </tr>
</table>

<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
