<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>	
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Dear {{ $member->firstName }},</p>
<p>Thank you, your ANZSA Membership Application has been received and will be considered by our Board of Directors as soon as possible.</p>
<p>We will be in touch with you to inform you of your membership status.</p>

<h2>Member Details</h2>
<table class="table">
    <tr>
        <th align="left" style="width:150px">Membership Type :</th>
        <td align="left">{{ $member->type->name }}</td>
    </tr>
    <tr>
        <th align="left">Full Name :</th>
        <td align="left">{{ $member->full_name }}</td>
    </tr>
    <tr>
        <th align="left">Join Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateJoin)->format('d-m-Y') }}</td>
    </tr>
    <tr>
        <th align="left">Expire Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateExpire)->format('d-m-Y') }}</td>
    </tr>
</table>

<p>Kind regards,<br>
Australia and New Zealand Sarcoma Association (ANZSA)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
