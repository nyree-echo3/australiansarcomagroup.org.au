<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">

@php
   switch ($member->type_id)  {
      case 1: 
      case 2:   
         $membership_type = "Ordinary"; break;
         
      case 3:   
         $membership_type = "Associate"; break;   
         
      case 4: 
      case 5:   
         $membership_type = "Corresponding"; break;   
   }
@endphp

<p>Dear {{ $member->firstName }},</p>
<p>Thank you for your payment of ${{$payment->amount}} for your ANZSA {{ $membership_type }} Membership.</p>
<p>Please find attached your receipt.</p>

<h2>Member Details</h2>
<table class="table">
    <tr>
        <th align="left" style="width:150px">Membership Type :</th>
        <td align="left">{{ $member->type->name }}</td>
    </tr>
    <tr>
        <th align="left">Full Name :</th>
        <td align="left">{{ $member->full_name }}</td>
    </tr>
    <tr>
        <th align="left">Join Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateJoin)->format('d-m-Y') }}</td>
    </tr>
    <tr>
        <th align="left">Expire Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateExpire)->format('d-m-Y') }}</td>
    </tr>
</table>

@if($payment->payment_method=='bank-deposit')
    {!! $payment_direct_deposit_members !!}
    <p>Your account will be activated once your payment is approved.</p>
@endif
@if($payment->payment_method=='paypal')
    <p>Your account has been activated and is ready to use. <a href="{{ url('login') }}">Please click here for login.</a></p>
@endif

<p>Kind regards,<br>
Australia and New Zealand Sarcoma Association (ANZSA)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
