<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>	
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">

<h1>Website | Contact Form</h1>

<h2>User Details</h2>
<table class="table">
    @foreach(json_decode($contact_message->data) as $field)
        <tr>
            <th style="text-align:left; width:20%">{{ str_replace("-", " ", ucfirst($field->field)) }} :</th>
            <td>{{ $field->value }}</td>
        </tr>
    @endforeach
</table>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
