<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<br><br>
<h1>Website | Event Registration</h1>
<p>The registration can be reviewed <a href='{{ url('') }}/dreamcms/events/registrations/{{ $event_booking->id }}/edit-registration'>here</a>.</p>

<h2>Attendee</h2>
<table class="table">    
    <tr>
        <th align="left">Full Name :</th>
        <td align="left">{{ $event_booking->title }} {{ $event_booking->first_name }} {{ $event_booking->last_name }} {{ ($event_booking->member_id != "" ? "(Member ID " . $event_booking->member_id . ")" : "") }}</td>
    </tr>
    <tr>
        <th align="left">Email :</th>
        <td align="left">{{ $event_booking->email_address }}</td>
    </tr>
    <tr>
        <th align="left">Mobile :</th>
        <td align="left">{{ $event_booking->phone_mobile }}</td>
    </tr>
    <tr>
        <th align="left">Role :</th>
        <td align="left">{{ $event_booking->designation }}</td>
    </tr>
    <tr>
        <th align="left">Company/Institution :</th>
        <td align="left">{{ $event_booking->company_name }}</td>
    </tr>
    <tr>
        <th align="left">Location :</th>
        <td align="left">{{ $event_booking->state }} {{ $event_booking->country }}</td>
    </tr>
</table>	

<h2>Event</h2>
<table class="table">
    <tr>
        <th align="left">Event :</th>
        <td align="left">{{ $event->title }}</td>
    </tr>
    <!--<tr>
        <th align="left">Date/Time :</th>
        <td align="left">
        	@if ($event->start_date != $event->end_date)
				{{date("D j M Y", strtotime($event->start_date))}} {{date("g:ia", strtotime($event->start_time))}} - {{date("D j M Y", strtotime($event->end_date))}} {{date("g:ia", strtotime($event->end_time))}}
			@else
				{{date("D j M Y", strtotime($event->start_date))}} - {{date("g:ia", strtotime($event->start_time))}} to {{date("g:ia", strtotime($event->end_time))}}
			@endif
        </td>
    </tr>-->
    <tr>
        <th align="left">Ticket :</th>
        <td align="left">{{ $ticket->name }} (${{ number_format($ticket->price, 2) }})</td>
    </tr>    
</table>

@if($event_payment)	
	<h2>Payment Details</h2>
	<table class="table">
		<tr>
			<th style="width:100px" align="left">Method :</th>
			<td align="left">
				@if($event_payment->payment_method=='paypal')
					PayPal
				@endif
				@if($event_payment->payment_method=='bank-deposit')
					Bank Deposit
				@endif
			</td>
		</tr>

		<tr>
			<th align="left">Amount :</th>
			<td align="left">${{ number_format($event_payment->amount,2) }}</td>
		</tr>

		<tr>
			<th align="left">Status :</th>
			<td align="left">{{ ucfirst($event_payment->payment_status) }}</td>
		</tr>

		@if($event_payment->payment_method=='paypal')
		<tr>
			<th align="left">Transaction Number :</th>
			<td align="left">{{ $event_payment->payment_transaction_number }}</td>
		</tr>
		<tr>
			<th align="left">PayPal Payer ID :</th>
			<td align="left">{{ $event_payment->paypal_payer_id }}</td>
		</tr>
		@endif
		<tr>
			<th align="left">&nbsp</th>
			<td align="left">&nbsp</td>
		</tr>
	</table>
	
	@if($event_payment->payment_method=='bank-deposit')
		<p><b>In order to active the registration, please change the status of the payment to "Completed" once the payment has been received.</b></p>
	@endif
@endif

<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
