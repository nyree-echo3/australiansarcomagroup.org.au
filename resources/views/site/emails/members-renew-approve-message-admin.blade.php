<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<br><br>
<h1>Website | Membership Renewal Payment Approved</h1>

<h2>Member Details</h2>
<table class="table">
    <tr>
        <th align="left" style="width:150px">Membership Type :</th>
        <td align="left">{{ $member->type->name }}</td>
    </tr>
    <tr>
        <th align="left">Full Name :</th>
        <td align="left">{{ $member->full_name }}</td>
    </tr>
    <tr>
        <th align="left">Join Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateJoin)->format('d-m-Y') }}</td>
    </tr>
    <tr>
        <th align="left">Expire Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateExpire)->format('d-m-Y') }}</td>
    </tr>
</table>

<p>User account has been activated and ready to use.</p>

<h2>Payment Details</h2>
<table class="table">
    <tr>
        <th style="width:100px" align="left">Method :</th>
        <td align="left">Bank Deposit</td>
    </tr>

    <tr>
        <th align="left">Amount :</th>
        <td align="left">${{ number_format($payment->amount,2) }}</td>
    </tr>

    <tr>
        <th align="left">Status :</th>
        <td align="left">Completed</td>
    </tr>
    <tr>
        <th align="left">&nbsp</th>
        <td align="left">&nbsp</td>
    </tr>
</table>

<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
