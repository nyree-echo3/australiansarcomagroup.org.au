<!DOCTYPE html>
<html>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Dear {{ $member->firstName }},</p>

<p>Thank you for your application for Associate membership. Your application was considered by the Board and I am delighted to advise that you have been awarded Associate member status.</p>

<p>
As an Associate member, you are entitled to the following benefits:
<ul>
<li>Attend ANZSA’s Annual Scientific Meeting (discounts for members) and other related workshops
<li>Access to CPD accredited events</li>
<li>Eligible to apply for ANZSA’s Sarcoma Research Grants Program</li>
<li>Access to ANZSA’s national sarcoma database</li>
<li>Access to ANZSA’s research support</li>
<li>Opportunities to collaborate and network</li>
<li>Contribute to sarcoma research and clinical trials</li>
<li>Eligible to apply to become an Ordinary Member upon achieving specialist recognition</li>
<li>Receive frequent ANZSA’s news updates and announcements</li>	
</ul>
</p>

<p>Associate members are required to pay an annual membership fee of AUD100. Your membership account now requires completion and payment by visiting the link below. </p>

<p><a href='{{ url('') }}/register/{{ $member->id }}'>{{ url('') }}/register/{{ $member->id }}</a></p>

<p>As an Associate member, we encourage you to continue playing an active role in the organisation. We value your voice and are open to queries, ideas and suggestions you may have. Feel free to write to us at <a href="mailto:contact@sarcoma.org.au">contact@sarcoma.org.au</a>.</p>

<p>I look forward to welcoming you personally and to working closely with you as we aim to continue improving outcomes for sarcoma and related tumours, patients and their families. </p>

<p>Sincerely,<br>
Adrian Cosenza<br>
Chair
</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
