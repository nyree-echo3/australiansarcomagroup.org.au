<!DOCTYPE html>
<html>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<br><br>
<h2>New membership renew</h2>
<h3>Member Details</h3>
<table class="table">
    <tr>
        <th align="left" style="width:150px">Membership Type :</th>
        <td align="left">{{ $member->type->name }}</td>
    </tr>
    <tr>
        <th align="left">Full Name :</th>
        <td align="left">{{ $member->full_name }}</td>
    </tr>
    <tr>
        <th align="left">Join Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateJoin)->format('d-m-Y') }}</td>
    </tr>
    <tr>
        <th align="left">Expire Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateExpire)->format('d-m-Y') }}</td>
    </tr>
</table>
@if($payment->payment_method=='bank-deposit')
    <p>In order to renew the user, please change the status of the renewal payment to "Completed" once the payment has been received.</p>
@endif
<h3>Payment Details</h3>
<table class="table">
    <tr>
        <th style="width:100px" align="left">Method :</th>
        <td align="left">
            @if($payment->payment_method=='paypal')
                PayPal
            @endif
            @if($payment->payment_method=='bank-deposit')
                Bank Deposit
            @endif
        </td>
    </tr>

    <tr>
        <th align="left">Amount :</th>
        <td align="left">${{ number_format($payment->amount,2) }}</td>
    </tr>

    <tr>
        <th align="left">Status :</th>
        <td align="left">{{ ucfirst($payment->payment_status) }}</td>
    </tr>

    @if($payment->payment_method=='paypal')
    <tr>
        <th align="left">Transaction Number :</th>
        <td align="left">{{ $payment->payment_transaction_number }}</td>
    </tr>
    <tr>
        <th align="left">PayPal Payer ID :</th>
        <td align="left">{{ $payment->paypal_payer_id }}</td>
    </tr>
    @endif
    <tr>
        <th align="left">&nbsp</th>
        <td align="left">&nbsp</td>
    </tr>
</table>

<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
