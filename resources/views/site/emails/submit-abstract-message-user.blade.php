<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>	
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Dear {{ $abstract->first_name }},</p>
<p>Thank you for your abstract submission. The ASM committee will review it and let you know the outcome shortly.</p>

<p>In the meantime, please <a href="{{ url('') }}/asm-2021/welcome">register</a> for the ANZSA ASM 2021. If you have any questions, please email <a href="mailto:contact@sarcoma.org.au">contact@sarcoma.org.au</a></p>

<p>Regards,<br>
Australia and New Zealand Sarcoma Association (ANZSA)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
