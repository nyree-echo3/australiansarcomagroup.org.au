<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>	
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Hi,</p>
@if($donation->payment_type=='regular')
   <p>Thank you for your continuous giving! Through your generosity, ANZSA dedicates 100% of your donations towards crucial sarcoma research and clinical trials to save and improve lives. </p>
   <p>All donations over $2 are tax deductible. Should you need a tax receipt, please do not hesitate to write to us at <a href="mailto:contact@sarcoma.org.au">contact@sarcoma.org.au</a>.</p>
@else
   <p>Thank you for your donation! Through your generosity, ANZSA dedicates 100% of your donations towards crucial sarcoma research and clinical trials to save and improve lives.</p>
   <p>All donations over $2 are tax deductible. Should you need a tax receipt (if donated via direct debit), please do not hesitate to write to us at <a href="mailto:contact@sarcoma.org.au">contact@sarcoma.org.au</a>.</p>
@endif

<h2>Your Details</h2>
<table class="table">    
    @foreach(json_decode($donation->data) as $field)
        <tr>
            <th style="width:20%" align="left">{!! $field->label !!} :</th>
            <td align="left">
            @if (trim(strip_tags($field->label)) == "Donation type") 
			   @if ($field->value == "once") 
				   Give Once
			   @else
				  Give Regularly
			   @endif               
			@else
			   {{ $field->value }}
			@endif	
            </td>
        </tr>
    @endforeach
</table>

<h2>Payment Details</h2>
<table class="table">
    <tr>
        <th style="width:20%" align="left">Type :</th>
        <td align="left">
        @if($donation->payment_type=='regular')
            Regular
        @endif
        @if($donation->payment_type=='once')
            Once
        @endif
        </td>
    </tr>

    <tr>
        <th style="width:20%" align="left">Method :</th>
        <td align="left">
            @if($donation->payment_method=='paypal')
                PayPal
            @endif
            @if($donation->payment_method=='bank-deposit')
                Bank Deposit
            @endif
        </td>
    </tr>

    <tr>
        <th style="width:20%" align="left">Amount :</th>
        <td align="left">{{ number_format($donation->amount,2) }}</td>
    </tr>

    <tr>
        <th style="width:20%" align="left">Status :</th>
        <td align="left">Completed</td>
    </tr>

    @if($donation->payment_method=='paypal' && $donation->payment_type=='once')
        <tr>
            <th style="width:20%" align="left">Transaction Number :</th>
            <td align="left">{{ $donation->payment_transaction_number }}</td>
        </tr>
    @endif

    @if($donation->payment_method=='paypal' && $donation->payment_type=='regular')
        <tr>
            <th style="width:20%" align="left">PayPal Profile ID :</th>
            <td align="left">{{ $donation->paypal_profile_id }}</td>
        </tr>
        <tr>
            <th style="width:20%" align="left">Next Billing Date :</th>
            <td align="left">{{ date("j M Y", strtotime($next_billing_date)) }}</td>
        </tr>
    @endif

    <tr>
        <th style="width:20%" align="left">Transaction Result :</th>
        <td align="left">{{ $donation->payment_transaction_result }}</td>
    </tr>
    <tr>
        <th style="width:20%" align="left">&nbsp</th>
        <td align="left">&nbsp</td>
    </tr>
</table>

@if($donation->payment_method=='bank-deposit')
    {!! $payment_direct_deposit !!}   
@endif

@if($donation->payment_method=='paypal' && $donation->payment_type=='regular')
<table class="table">
    <tr>
        <td><strong>If you want to cancel your regular donation, please click the link below.</strong></td>
    </tr>
    <tr>
        <td><a href="{{ url('donations/cancel-recurring-payment/'.$donation->id.'/'.$cancel_link_hash) }}" >{{ url('donation/cancel-recurring-payment/'.$donation->id.'/'.$cancel_link_hash) }}</a></td>
    </tr>
    <tr>
        <td>&nbsp</td>
    </tr>
</table>
@endif

<p>Kind regards,<br>
Australia and New Zealand Sarcoma Association (ANZSA)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
