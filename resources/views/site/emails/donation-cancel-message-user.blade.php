<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>	
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Hi,</p>
<p>Your regular donation has been cancelled! Here is a summary of your donation to us.</p>

<h2>Your Details</h2>
<table class="table">   
    @foreach(json_decode($donation->data) as $field)
        <tr>
            <th style="width:20%" align="left">{{ $field->label }} :</th>
            <td align="left">
            @if (trim(strip_tags($field->label)) == "Donation type") 
			   @if ($field->value == "once") 
				   Give Once
			   @else
				  Give Regularly
			   @endif               
			@else
			   {{ $field->value }}
			@endif	
            </td>
        </tr>
    @endforeach
</table>
	
<h2>Payment Details</h2>
<table class="table">  
    <tr>
        <th style="width:20%" align="left">Type :</th>
        <td align="left">
        @if($donation->payment_type=='regular')
            Regular
        @endif
        @if($donation->payment_type=='once')
            Once
        @endif
        </td>
    </tr>

    <tr>
        <th style="width:20%" align="left">Method :</th>
        <td align="left">
            @if($donation->payment_method=='paypal')
                PayPal
            @endif
            @if($donation->payment_method=='bank-deposit')
                Bank Deposit
            @endif
        </td>
    </tr>

    <tr>
        <th style="width:20%" align="left">Amount :</th>
        <td align="left">{{ number_format($donation->amount,2) }}</td>
    </tr>

    <tr>
        <th style="width:20%" align="left">Status :</th>
        <td align="left">Cancelled</td>
    </tr>

    @if($donation->payment_method=='paypal' && $donation->payment_type=='regular')
        <tr>
            <th style="width:20%" align="left">PayPal Profile ID :</th>
            <td align="left">{{ $donation->paypal_profile_id }}</td>
        </tr>
    @endif
    <tr>
        <th style="width:20%" align="left">&nbsp</th>
        <td align="left">&nbsp</td>
    </tr>
</table>

<p>Kind regards,<br>
Australia and New Zealand Sarcoma Association (ANZSA)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
