<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>	
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Hi,</p>
<p>Thank you for contacting ANZSA.</p>
<p>Should you wish to receive further ANZSA and sarcoma news, please sign up to our <a href="https://sarcoma.org.au/news/newsletters">newsletter</a> or follow us on <a href="https://www.facebook.com/ANZSarcoma">Facebook</a> and <a href="https://twitter.com/anzsarcoma">Twitter.</a></p>
<p>Kind regards,<br>
Australia and New Zealand Sarcoma Association (ANZSA)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
