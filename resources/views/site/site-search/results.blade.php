@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="blog-post search-results">
                <h1 class="blog-post-title">Site search</h1>

                <form method="post" action="{{ url('results') }}">
                    {{ csrf_field() }}
                    <div class="form-group col-md-12 site-search-form">
                        <div class="row">
                            <div class="col-sm-8">
                                <input type="text" name="query" class="form-control" placeholder="Search for..." value="{{ $query }}">
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn-checkout">Search</button>
                            </div>
                        </div>
                    </div>
                </form>

                @foreach($results as $result)
                    @php
                        if($result['type']=='page'){
                            $url = $result['item']->url;
                            $link = $result['item']->title;
                            $description = $result['item']->body_clean;
                        }

                        if($result['type']=='news'){
                            $url = $result['item']->url;
                            $link = $result['item']->title;
                            $description = $result['item']->body_clean;
                        }

                        if($result['type']=='project'){
                            $url = 'projects';
                            $link = $result['item']->name;
                            $description = $result['item']->summary;
                        }

                        if($result['type']=='team'){
                            $url = $result['item']->url;
                            $link = $result['item']->name;
                            $description = $result['item']->body_clean;
                        }

                        if($result['type']=='link'){
                            $url = $result['item']->url;
                            $link = $result['item']->name;
                            $description = $result['item']->url;
                        }
                    @endphp
                    <div class="row result">
                        <div class="col-12"><a href="{{ url($url) }}" class="result-link" {!! $result['type']=='link' ? ' target="_blank"' : null !!}>{{ $link }}</a></div>
                        <div class="col-12 result-description">{{ $description }}</div>
                        <div class="col-12">
                            <hr/>
                        </div>
                    </div>
                @endforeach
                {{ $results->links() }}
            </div>
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection

@section('scripts')
    <script src="{{ asset('components/mark.js/dist/jquery.mark.js') }}"></script>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".result-description").mark('{{ addslashes($query) }}');
        });
    </script>
@endsection
