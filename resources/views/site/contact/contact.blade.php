@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<style>
input[type="text"]::-ms-input-placeholder, input[type="email"]::-ms-input-placeholder { color: #000!important; }
</style>

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-contact')
                     
        <div class="col-sm-9 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Contact</h1>                 
            
               <p>We would love to hear from you! Please complete and submit the form below or write to us <a href="mailto:contact@sarcoma.org.au">contact@sarcoma.org.au</a>. </p>

               <div class="page-note"><p>Please note: ANZSA is unable to provide medical opinions on individual cases due to the complex nature of a sarcoma diagnosis. Please see your medical oncologist for appropriate discussion and consultation. For a list of sarcoma services in Australia and New Zealand, please click here to <a href="pages/about-sarcoma/find-a-sarcoma-specialist">find a sarcoma specialist</a>.</p></div>

               
                <form id="contact-form" method="post" action="{{ url('contact/save-message') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div id="contact-form-fields"></div>

                    <div class="form-row">
                        <div class="col-12 col-sm-10 g-recaptcha-container">
                            <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn-checkout">Send</button>
                </form>
            </div>
        </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection
@section('scripts')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#contact-form-fields').formRender({
                dataType: 'json',
                formData: {!! $form !!},
                notify: {
                    success: function(message) {

                        FormValidation.formValidation(
                            document.getElementById('contact-form'),
                            {
                                plugins: {
                                    declarative: new FormValidation.plugins.Declarative({
                                        html5Input: true,
                                    }),
                                    submitButton: new FormValidation.plugins.SubmitButton(),
                                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                    bootstrap: new FormValidation.plugins.Bootstrap(),
                                    icon: new FormValidation.plugins.Icon({
                                        valid: 'fa fa-check',
                                        invalid: 'fa fa-times',
                                        validating: 'fa fa-refresh',
                                    })
                                },
                            }
                        );

                    }
                }
            });

        });
    </script>
@endsection
