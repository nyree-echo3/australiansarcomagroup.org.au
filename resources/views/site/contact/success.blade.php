@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-contact')
                     
        <div class="col-sm-9 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Contact</h1>    
               <p>Thank you for contacting ANZSA.</p>
               <p>Should you wish to receive further ANZSA and sarcoma news, please sign up to our <a href="https://sarcoma.org.au/news/newsletters">newsletter</a> or follow us on <a href="https://www.facebook.com/ANZSarcoma" target="_blank">Facebook</a> and <a href="https://twitter.com/anzsarcoma" target="_blank">Twitter</a>.</p>
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection            
