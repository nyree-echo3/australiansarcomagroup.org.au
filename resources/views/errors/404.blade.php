<?php 
   // Set Meta Tags
   $meta_title_inner = "Page Not Found"; 
   $meta_keywords_inner = "Page Not Found"; 
   $meta_description_inner = "Page Not Found";  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row page-not-found">                
        <div class="col-sm-3 offset-sm-1 blog-sidebar">
           <i class="fas fa-exclamation-triangle"></i>          
		</div>
       
        <div class="col-sm-8 blog-main">
        
            <h1>Oops :(</h1>
			<h2>Sorry, the page you are looking for is missing!</h2>

			<p>The page has expired or may have been unpublished. Please try the following:</p>

			<ul>
				<li>Go to the <a href="{{ url('') }}">Home page</a>.</li>
			<li>Follow the links on the menu above.</li>
			<li>Check the URL and try again.</li>
			</ul>
       
        </div><!-- /.blog-main -->        
      </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection