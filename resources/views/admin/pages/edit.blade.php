@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/pages') }}"><i class="fas fa-file-alt"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/pages/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $page->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Title" value="{{ old('title',$page->title) }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">SEO Name *</label>
                                    <div class="col-sm-10">

                                        <div class="input-group">
                                            <input type="text" id="slug" name="slug" class="form-control"
                                                   value="{{ old('slug',$page->slug) }}" readonly>
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      data-target="#change-slug">Change SEO Name
                                              </button>
                                            </span>
                                        </div>

                                        @if ($errors->has('slug'))
                                            <small class="help-block">{{ $errors->first('slug') }}</small>
                                        @endif
                                    </div>
                                </div>

                                @php
                                    if(old('page_type')!=''){
                                        $page_type = old('page_type');
                                    }else{
                                        $page_type = $page->page_type;
                                    }

                                    if(old('category_id')!=''){
                                        $category_id = old('category_id');
                                    }else{
                                        $category_id = $page->category_id;
                                    }

                                    if(old('parent_page_id')!=''){
                                        $parent_page_id = old('parent_page_id');
                                    }else{
                                        $parent_page_id = $page->parent_page_id;
                                    }
                                @endphp
                                <div class="form-group inc-radio {{ ($errors->has('page_type')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Page Type *</label>
                                    <div class="col-sm-10">
                                        <input id="page_type_parent" type="radio" name="page_type" value="parent"
                                               class="minimal" {{ ($page_type == 'parent') ? ' checked' : '' }}>
                                        <label for="page_type_parent" class="radio-label">Parent</label>

                                        <input id="page_type_child" type="radio" name="page_type" value="child"
                                               class="minimal" {{ ($page_type == 'child') ? ' checked' : '' }}>
                                        <label for="page_type_child" class="radio-label">Child</label>

                                        @if ($errors->has('page_type'))
                                            <small class="help-block">{{ $errors->first('page_type') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                     id="category_selector"{!! ($page_type=='child') ? 'style="display: none"' : '' !!}>
                                    <label class="col-sm-2 control-label">Category *</label>

                                    <div class="col-sm-10">
                                        <select name="category_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"{{ ($category_id == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('parent_page_id')) ? ' has-error' : '' }}"
                                     id="page_selector" {!! ($page_type=='' || $page_type=='parent') ? 'style="display:none"' : '' !!}>
                                    <label class="col-sm-2 control-label">Parent Page</label>

                                    <div class="col-sm-10">
                                        <select name="parent_page_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($parent_pages as $parent_page)
                                                <option value="{{ $parent_page->id }}"{{ ($parent_page_id == $parent_page->id) ? ' selected="selected"' : '' }}>{{ $parent_page->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Title *</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" id="meta_title" name="meta_title" class="form-control"
                                                   placeholder="Meta Title"
                                                   value="{{ old('meta_title',$page->meta_title) }}" maxlength="56">
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      id="copy-title" name="copy-title">Copy From Title
                                              </button>
                                            </span>
                                        </div>
                                        <div id="c_count_meta_title"></div>
                                        @if ($errors->has('meta_title'))
                                            <small class="help-block">{{ $errors->first('meta_title') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ ($errors->has('meta_keywords')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Keywords *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="meta_keywords" name="meta_keywords"
                                                  placeholder="Meta Keywords">{{ old('meta_keywords',$page->meta_keywords) }}</textarea>
                                        @if ($errors->has('meta_keyword'))
                                            <small class="help-block">{{ $errors->first('meta_keywords') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="meta_description" name="meta_description"
                                                  placeholder="Meta Description">{{ old('meta_description',$page->meta_description) }}</textarea>
                                        <div id="c_count_meta_description"></div>
                                        @if ($errors->has('meta_description'))
                                            <small class="help-block">{{ $errors->first('meta_description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('body')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Body *</label>

                                    <div class="col-sm-10">
                                        <textarea id="body" name="body" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('body', $page->body) }}</textarea>
                                        @if ($errors->has('body'))
                                            <small class="help-block">{{ $errors->first('body') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('special_url')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Special URL</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">{{ env('APP_URL') }}/</span>
                                            <input type="text" name="special_url" class="form-control" value="{{ old('special_url', $page->special_url) }}">
                                        </div>
                                        @if ($errors->has('special_url'))
                                            <small class="help-block">{{ $errors->first('special_url') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                @php
                                    if(count($errors)>0){
                                       if(old('thumbnail')!=''){
                                        $thumbnail_image = old('thumbnail');
                                       }else{
                                        $thumbnail_image = '';
                                       }
                                    }else{
                                        $thumbnail_image = $page->thumbnail;
                                    }
                                @endphp
                                <div class="form-group {{ ($errors->has('thumbnail')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Thumbnail Image</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="thumbnail" name="thumbnail"
                                               value="{{ old('thumbnail',$thumbnail_image) }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if($thumbnail_image!=''){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Image
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if($thumbnail_image!='')
                                                <image src="{{ old('thumbnail',$thumbnail_image) }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('thumbnail'))
                                            <small class="help-block">{{ $errors->first('thumbnail') }}</small>
                                        @endif
                                    </div>                                
                               </div>
                               
                               <div class="form-group {{ ($errors->has('header')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Header Image</label>
                                <div class="col-sm-10">
                                    <input type="hidden" id="header" name="header" value="{{ old('header',$page->header_image) }}">
                                    <button id="header-popup" type="button" class="btn btn-info btn-sm">Upload Image
                                    </button>
                                    @php
                                        $class = ' invisible';
                                        if($page->header){
                                            $class = '';
                                        }
                                    @endphp
                                    <button id="remove-header" type="button" class="btn btn-danger btn-sm{{ $class }}">
                                        Remove Image
                                    </button>
                                    <br/><br/>
                                    <span id="added_header">
									@if($page->header)
                                            <image src="{{ env('APP_URL').$page->header }}"/>
                                        @endif
									</span>
                                    @if ($errors->has('header'))
                                        <small class="help-block">{{ $errors->first('header') }}</small>
                                    @endif
                                </div>
                            </div>
                            
                            </div>
                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $page->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : null }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/pages') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>                                                                   
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>                                                                
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                                    
                                <br><br>                             
                                <a href="{{ url('dreamcms/pages/'.$page->id.'/preview') }}" class="pull-right" target="_blank"><i class="far fa-eye"></i>&nbsp;&nbsp;Page Preview</a>    								
                            </div>
                            
                            <div class="box-footer">                                
                                                                                           
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal"
                           value="{{ old('slug',$page->slug) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('body');
            $(".select2").select2();

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            $('input[type="radio"].minimal').on('ifChecked', function (event) {

                if ($(this).val() == 'parent') {
                    $('#category_selector').show();
                    $('#page_selector').hide();
                }

                if ($(this).val() == 'child') {
                    $('#category_selector').hide();
                    $('#page_selector').show();
                }

            });

            var maxLengthTitle = 56;
            $('#meta_title').keyup(function () {
                var textlen = maxLengthTitle - $(this).val().length;
                $('#c_count_meta_title').text($(this).val().length + " characters | " + textlen + " characters left");
            });
            $('#meta_title').trigger("keyup", {which: 50});

            var maxLengthDescription = 250;
            $('#meta_description').keyup(function () {
                var textlen = maxLengthDescription - $(this).val().length;
                var words = $(this).val().trim().split(" ").length;
                $('#c_count_meta_description').text($(this).val().length + " characters | " + textlen + " characters left | " + words + " words");
            });
            $('#meta_description').trigger("keyup", {which: 50});

            $('#copy-title').click(function () {
                $('#meta_title').val($('#title').val().substr(0, maxLengthTitle));
                $('#meta_title').trigger("keyup", {which: 50});
            });

            $('#title').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });
			
			$("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#thumbnail').val('');
            });
			
			$("#header-popup").click(function () {
                openHeaderPopup();
            });

            $("#remove-header").click(function () {
                $('#added_header').html('');
                $('#remove-header').addClass('invisible')
                $('#header').val('');
            });

        });
		
		function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="{{ env('APP_URL') }}' + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#thumbnail').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="{{ env('APP_URL') }}' + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#thumbnail').val(evt.data.resizedUrl);
                    });
                }
            });
        }
		
		function openHeaderPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_header').html('<image src="{{ env('APP_URL') }}' + file.getUrl() + '">');
                        $('#remove-header').removeClass('invisible');
                        $('#header').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_header').html('<image src="{{ env('APP_URL') }}' + evt.data.resizedUrl + '">');
                        $('#remove-header').removeClass('invisible');
                        $('#header').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection