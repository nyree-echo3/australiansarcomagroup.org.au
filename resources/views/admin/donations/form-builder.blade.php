@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fas fa-donate"></i> {{ $display_name }}</a></li>
                <li class="active">Details</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $display_name }} Form Builder</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/donations/store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div id="fb-editor" class="box-body"></div>
                        </form>

                        <div class="box-footer">
                            <div id="control-buttons" class="btn-group pull-right">
                                <button id="save" type="button" class="btn btn-info">Save</button>
                                <a href="{{ url('dreamcms/donations') }}" id="cancel" type="button" class="btn btn-danger">Cancel</a>
                                <button id="clear" type="button" class="btn btn-warning">Clear</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/components/formBuilder/dist/form-builder.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            var options = {
                defaultFields : {!! $form !!},
                disableFields : ['autocomplete','button','file','hidden'],
                disabledAttrs: ['access'],
                showActionButtons: false,
            };
            var formBuilder = $('#fb-editor').formBuilder(options);

            $('#save').click(function(){
                $.ajax({
                    type: "POST",
                    url: '{{ url("dreamcms/donations/save-form") }}',
                    data:  {
                        'form':formBuilder.actions.getData('json')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('{{ $display_name }} form saved');
                        }

                        if(response.status=="fail"){
                            toastr.options = {"closeButton": true}
                            toastr.error(response.message);
                        }
                    }
                });
            });

            $('#clear').click(function(){
                formBuilder.actions.clearFields();
            });
        });
    </script>
@endsection