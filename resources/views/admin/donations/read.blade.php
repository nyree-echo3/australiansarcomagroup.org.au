@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/donations') }}"><i class="fa fa-envelope"></i> {{ $display_name }}</a>
                </li>
                <li class="active">View</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12">

                    <div class="box box-primary">
                        <div class="box-body">

                            <div class="table-responsive">
                                <table class="table">
                                    @foreach(json_decode($donation->data) as $field)
                                        <tr>
                                            <th style="width:20%">{{ strip_tags($field->label) }} :</th>
                                            <td>
                                               @if (trim(strip_tags($field->label)) == "Donation type") 
												   @if ($field->value == "once") 
													   Give Once
												   @else
													  Give Regularly
												   @endif               
												@else
												   {{ $field->value }}
												@endif	
                                            </td>
                                        </tr>
                                        @endforeach
                                        <!--<tr>
                                              <th style="width:20%">Amount :</th>
                                              <td>${{ $donation->amount }}</td>
                                          </tr>-->
                                </table>
                            </div>

                            <div class="table-responsive">
                                <h5><strong>&nbsp;&nbsp;PAYMENT</strong></h5>
                                <table class="table">
                                    <tr>
                                        <th style="width:20%">Type :</th>
                                        <td>
                                        @if($donation->payment_type=='regular')
                                            Regular
                                        @endif
                                        @if($donation->payment_type=='once')
                                            Once
                                        @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <th style="width:20%">Method :</th>
                                        <td>
                                            @if($donation->payment_method=='paypal')
                                                PayPal
                                            @endif
                                            @if($donation->payment_method=='bank-deposit')
                                                Bank Deposit
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <th style="width:20%">Status :</th>
                                        <td>
                                        @if($donation->payment_status=='completed')
                                            Completed
                                        @endif
                                        @if($donation->payment_status=='regular_payment_cancelled')
                                            Regular Payment Cancelled
                                        @endif
                                        </td>
                                    </tr>
                                    @if($donation->payment_method=='paypal' && $donation->payment_type=='regular')
                                    <tr>
                                        <th style="width:20%">PayPal Profile ID :</th>
                                        <td>{{ $donation->paypal_profile_id }}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th style="width:20%">Transaction Result :</th>
                                        <td>
                                            <span class="{{ ($donation->payment_transaction_result == 'Successful' ? 'text-success' : 'text-danger') }}">{!! ($donation->payment_transaction_result == 'Successful' ? '<i class="fas fa-check"></i> ' : '<i class="fas fa-times"></i> ') !!}{{ $donation->payment_transaction_result }}</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            @if($donation->payment_method=='paypal' && $donation->payment_type=='regular')
                            <div class="box-footer">
                                <button id="get-details" data-id="{{ $donation->id }}" type="button" class="btn btn-info">
                                    Get Details From PayPal
                                </button>

                                @if($donation->payment_status!='regular_payment_cancelled')
                                <a type="button" href="{{ url('dreamcms/donations/'.$donation->id.'/cancel-regular-payment') }}"
                                   class="btn btn-danger" data-toggle=confirmation data-title="Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes" data-btn-cancel-label="No">
                                    Cancel Regular Payment
                                </a>
                                @endif
                            </div>
                            @endif
                        </div>
                    </div>

                </div>
            </div>

            <div id="payment-details-container" class="row">
                <div class="col-sm-12">
                    <div class="box box-primary">
                        <div id="payment-details" class="box-body"></div>
                    </div>
                </div>
            </div>

        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('#get-details').click(function () {

                $('#get-details').attr("disabled", true);
                $('#payment-details-container').hide();

                if($("#get-details i").length == 0) {
                    $("#get-details").prepend("<i class='fas fa-spinner fa-spin'></i>");
                }

                $.ajax({
                    type: "POST",
                    url: '{{ url('dreamcms/donations/'.$donation->id.'/get-details') }}',
                    data: {
                        'id': {{ $donation->id }}
                    },
                    success: function (response) {

                        $("#get-details i").remove();
                        $('#get-details').attr("disabled", false);
                        $('#payment-details-container').show();

                        if (response.status == "success") {
                            $('#payment-details').html(response.table);
                        }

                        if (response.status == "fail") {
                            toastr.options = {"closeButton": true}
                            toastr.error('Error! Please try again.');
                        }
                    }
                });
            });
        });


    </script>
@endsection