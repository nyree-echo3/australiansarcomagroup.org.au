@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/news') }}"><i class="fas fa-donate"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/news/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $news->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Title"
                                               value="{{ old('title',$news->title) }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">SEO Name *</label>
                                    <div class="col-sm-10">

                                        <div class="input-group">
                                            <input type="text" id="slug" name="slug" class="form-control"
                                                   value="{{ old('slug',$news->slug) }}" readonly>
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      data-target="#change-slug">Change SEO Name
                                              </button>
                                            </span>
                                        </div>

                                        @if ($errors->has('slug'))
                                            <small class="help-block">{{ $errors->first('slug') }}</small>
                                        @endif
                                    </div>
                                </div>

                                @php
                                    if(old('category_id')!=''){
                                        $category_id = old('category_id');
                                    }else{
                                        $category_id = $news->category_id;
                                    }
                                @endphp
                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                     id="category_selector">
                                    <label class="col-sm-2 control-label">Category *</label>
                                    <div class="col-sm-10">
                                        <select name="category_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"{{ ($category_id == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Title *</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" id="meta_title" name="meta_title" class="form-control"
                                                   placeholder="Meta Title"
                                                   value="{{ old('meta_title',$news->meta_title) }}" maxlength="56">
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      id="copy-title" name="copy-title">Copy From Title
                                              </button>
                                            </span>
                                        </div>
                                        <div id="c_count_meta_title"></div>
                                        @if ($errors->has('meta_title'))
                                            <small class="help-block">{{ $errors->first('meta_title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_keywords')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Keywords *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="meta_keywords"
                                                  placeholder="Meta Keywords">{{ old('meta_keywords', $news->meta_keywords) }}</textarea>
                                        @if ($errors->has('meta_keyword'))
                                            <small class="help-block">{{ $errors->first('meta_keywords') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" id="meta_description"
                                                  name="meta_description" maxlength="250"
                                                  placeholder="Meta Description">{{ old('meta_description',$news->meta_description) }}</textarea>
                                        <div id="c_count_meta_description"></div>
                                        @if ($errors->has('meta_description'))
                                            <small class="help-block">{{ $errors->first('meta_description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('short_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Short Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="short_description"
                                                  placeholder="Short Description">{{ old('short_description',$news->short_description) }}</textarea>
                                        @if ($errors->has('short_description'))
                                            <small class="help-block">{{ $errors->first('short_description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('body')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Body *</label>

                                    <div class="col-sm-10">
                                        <textarea id="body" name="body" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('body',$news->body) }}</textarea>
                                        @if ($errors->has('body'))
                                            <small class="help-block">{{ $errors->first('body') }}</small>
                                        @endif
                                    </div>
                                </div>
                                @php
                                    if(count($errors)>0){
                                       if(old('thumbnail')!=''){
                                        $thumbnail_image = old('thumbnail');
                                       }else{
                                        $thumbnail_image = '';
                                       }
                                    }else{
                                        $thumbnail_image = $news->thumbnail;
                                    }
                                @endphp
                                <div class="form-group {{ ($errors->has('thumbnail')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Hero / RSS / Thumbnail Image</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="thumbnail" name="thumbnail"
                                               value="{{ old('thumbnail',$thumbnail_image) }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if($thumbnail_image!=''){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Image
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if($thumbnail_image!='')
                                                <image src="{{ old('thumbnail',$thumbnail_image) }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('thumbnail'))
                                            <small class="help-block">{{ $errors->first('thumbnail') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ ($errors->has('start_date')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Start Date *</label>
                                <div class="col-sm-10">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="start_date" type="text" class="form-control pull-right datepicker"
                                               value="{{ old('start_date', date("d/m/Y", strtotime($news->start_date))) }}">
                                    </div>
                                    @if ($errors->has('start_date'))
                                        <small class="help-block">{{ $errors->first('start_date') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ ($errors->has('archive_date')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Archive Date *</label>
                                <div class="col-sm-10">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="archive_date" type="text"
                                               class=" form-control pull-right datepicker"
                                               value="{{ old('archive_date', date("d/m/Y", strtotime($news->archive_date))) }}">
                                    </div>
                                    @if ($errors->has('archive_date'))
                                        <small class="help-block">{{ $errors->first('archive_date') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ ($errors->has('special_url')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Special URL</label>

                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{ env('APP_URL') }}/</span>
                                        <input type="text" name="special_url" class="form-control" value="{{ old('special_url', $news->special_url) }}">
                                    </div>
                                    @if ($errors->has('special_url'))
                                        <small class="help-block">{{ $errors->first('special_url') }}</small>
                                    @endif
                                </div>
                            </div>
                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $news->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status * </label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/news') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                                
                                <br><br>                             
                                <a href="{{ url('dreamcms/news/'.$news->id.'/preview') }}" class="pull-right" target="_blank"><i class="far fa-eye"></i>&nbsp;&nbsp;Page Preview</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal"
                           value="{{ old('slug',$news->slug) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            CKEDITOR.replace('body');
            CKEDITOR.replace('short_description');

            $(".select2").select2();

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            var maxLengthTitle = 56;
            $('#meta_title').keyup(function () {
                var textlen = maxLengthTitle - $(this).val().length;
                $('#c_count_meta_title').text($(this).val().length + " characters | " + textlen + " characters left");
            });
            $('#meta_title').trigger("keyup", {which: 50});

            var maxLengthDescription = 250;
            $('#meta_description').keyup(function () {
                var textlen = maxLengthDescription - $(this).val().length;
                var words = $(this).val().trim().split(" ").length;
                $('#c_count_meta_description').text($(this).val().length + " characters | " + textlen + " characters left | " + words + " words");
            });
            $('#meta_description').trigger("keyup", {which: 50});

            $('#copy-title').click(function () {
                $('#meta_title').val($('#title').val().substr(0, maxLengthTitle));
                $('#meta_title').trigger("keyup", {which: 50});
            });

            $("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#thumbnail').val('');
            });

            $('#title').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });
        });

        function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#thumbnail').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#thumbnail').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection 