@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-question-circle"></i> {{ $display_name }}</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/donations') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">

                            <div class="form-group col-xs-2">
                                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ $session['search'] }}">
                            </div>

                            <div class="form-group col-xs-3 filter-button">
                                <button type="submit" class="btn btn-info">Filter</button>
                                @if($is_filtered)
                                    <a href="{{ url('dreamcms/donations/forget') }}" type="submit" class="btn btn-danger">Remove</a>
                                @endif
                            </div>
                            
                            <div class="form-group col-xs-7 filter-button" style="text-align: right;">                                
                                <a href="{{ url('dreamcms/donations/export') }}" type="submit" class="btn btn-danger">Export to Excel</a>                                
                            </div>
                        </div>
                    </form>

                </div>
                <div class="box-body">
                    @if(count($donations))
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Amount</th>
                                <th>Type</th>
                                <th>Method</th>
                                <th>Date Time</th>
                                <th class="hd-text-right">Actions</th>
                            </tr>
                            @foreach($donations as $donation)
                                @php
                                    $properties = json_decode($donation->data);
                                    $name = '';

                                    foreach($properties as $property){
                                        if($property->field=='name'){
                                            $name = $property->value;
                                        }
                                        if($property->field=='name-last'){
                                            $last_name = $property->value;
                                        }
                                    }
                                @endphp
                                <tr>
                                    <td>{{ $name." ".$last_name }}</td>
                                    <td>${{ number_format($donation->amount,2) }}</td>
                                    <td>
                                    @if($donation->payment_type=='regular')
                                        Regular
                                        @if($donation->payment_status=='regular_payment_cancelled')
                                                <span class="cancelled-label">(Cancelled)</span>
                                        @endif
                                    @endif
                                    @if($donation->payment_type=='once')
                                        Once
                                    @endif
                                    </td>
                                    <td>
                                    @if($donation->payment_method=='paypal')
                                        PayPal
                                    @endif
                                    @if($donation->payment_method=='bank-deposit')
                                        Bank Deposit
                                    @endif
                                    </td>
                                    <td>{{ \Carbon\Carbon::parse($donation->created_at)->format('d-m-Y H:i:s') }}</td>
                                    <td>
                                        <div class="pull-right">
                                            @can('read-donation')
                                            <a href="{{ url('dreamcms/donations/'.$donation->id.'/read') }}" class="tool"><i class="far fa-eye"></i></a>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $donations->total() }} record</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $donations->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection