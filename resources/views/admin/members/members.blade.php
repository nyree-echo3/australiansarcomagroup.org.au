@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members') }}"><i class="fa fa-users"></i> {{ $display_name }}</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/members') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">

                            <div class="form-group col-xs-2">
                                <select name="type" class=" select2" style="width: 100%;">
                                    <option value="all" {{ $session['type'] == "" || $session['type']=="all" ? ' selected="selected"' : '' }}>
                                        All Types
                                    </option>
                                    @foreach($types as $type)
                                        <option value="{{ $type->id }}"{{ $session['type'] == $type->id ? ' selected="selected"' : '' }}>{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-xs-2">
                                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ $session['search'] }}">
                            </div>

                            <div class="form-group col-xs-3 filter-button">
                                <button type="submit" class="btn btn-info">Filter</button>
                                @if($is_filtered)
                                    <a href="{{ url('dreamcms/members/forget') }}" type="submit" class="btn btn-danger">Remove</a>
                                @endif
                            </div>
                            
                            <div class="form-group col-xs-5 filter-button" style="text-align: right;">                                
                                <a href="{{ url('dreamcms/members/export') }}" type="submit" class="btn btn-danger">Export to Excel</a>                                
                            </div>
                        </div>
                    </form>
                    
                </div>
                <div class="box-body">
                    @if(count($members))
                        <table class="table table-hover">
                            <tr>
                                <th>@sortablelink('id')</th>
                                <th>@sortablelink('firstName')</th>
                                <th>@sortablelink('lastName')</th>
                                <th>@sortablelink('email')</th>
                                <th>@sortablelink('typesort.suburb','Location')</th>
                                <th>@sortablelink('type_id')</th>
                                <th>@sortablelink('status')</th>
                                <th class="hd-text-right">Actions</th>
                            </tr>
                            @foreach($members as $member)
                                <tr>
                                    <td>{{ $member->id }}</td>
                                    <td>{{ $member->firstName }}</td>
                                    <td>{{ $member->lastName }}</td>
                                    <td>{{ $member->email }}</td>
                                    <td>{{ $member->suburb }} {{ $member->state }} {{ $member->postcode }}</td>
                                    <td>
                                    @if($member->type)
                                    {{ $member->type->name }}
                                    @endif
                                    </td>
                                    @php
                                    if ($member->status == "passive")  {
                                        $status = "Pending";
                                    } elseif ($member->status == "active")  {
                                        $status = "Approved";
                                    } else  {
                                        $status = ucfirst($member->status);
                                    }    
                                    @endphp                                 
                                    <td>{{ $status }}</td>
                                    <td>
                                        <div class="pull-right">
                                            @can('edit-members')
                                            <a href="{{ url('dreamcms/members/'.$member->id.'/show') }}" class="tool"><i class="far fa-eye"></i></a>
                                            <a href="{{ url('dreamcms/members/'.$member->id.'/edit') }}" class="tool" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                                            @endcan

                                            @can('delete-members')
                                            <a href="{{ url('dreamcms/members/'.$member->id.'/delete') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                           @endcan
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $members->total() }} record</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $members->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.member_status').change(function() {
                $.ajax({
                    type: "POST",
                    url: "members/"+$(this).data('id')+"/change-member-status",
                    data:  {
                        'status':$(this).prop('checked')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });

        });
    </script>
@endsection