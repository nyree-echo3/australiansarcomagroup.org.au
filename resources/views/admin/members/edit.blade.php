@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members') }}"><i class="fa fa-users"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" enctype="multipart/form-data" class="form-horizontal" action="{{ url('dreamcms/members/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $member->id }}">
                            <div class="box-body">
                                <h4>Membership Class</h4>

                                @php
                                    if(old('type_id')!=''){
                                        $type_id = old('type_id');
                                    }else{
                                        $type_id = $member->type_id;
                                    }
                                @endphp
                                <div class="form-group{{ ($errors->has('type_id')) ? ' has-error' : '' }}"
                                     id="type_selector">
                                    <label class="col-sm-2 control-label">Type</label>

                                    <div class="col-sm-10">
                                        <select name="type_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}"{{ ($type_id == $type->id) ? ' selected="selected"' : '' }}>{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <h4>Member Information</h4>


                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <select name="title" class="form-control select2" style="width: 100%;">
                                            <option value="Mr"{{ (old('title', $member->title) == "Mr") ? ' selected="selected"' : '' }}>
                                                Mr
                                            </option>
                                            <option value="Ms"{{ (old('title', $member->title) == "Ms") ? ' selected="selected"' : '' }}>
                                                Ms
                                            </option>
                                            <option value="Mrs"{{ (old('title', $member->title) == "Mrs") ? ' selected="selected"' : '' }}>
                                                Mrs
                                            </option>                                                                                        
                                            <option value="Dr"{{ (old('title', $member->title) == "Dr") ? ' selected="selected"' : '' }}>
                                                Dr
                                            </option>
                                            <option value="Assc. Professor"{{ (old('title', $member->title) == "Assc. Professor") ? ' selected="selected"' : '' }}>
                                                Assc. Professor
                                            </option>
                                            <option value="Professor"{{ (old('title', $member->title) == "Professor") ? ' selected="selected"' : '' }}>
                                                Professor
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('firstName')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">First Name *</label>

                                    <div class="col-sm-10">
                                        <input type="textbox" class="form-control" name="firstName"
                                               placeholder="First Name"
                                               value="{{ old('firstName',$member->firstName) }}">
                                        @if ($errors->has('firstName'))
                                            <small class="help-block">{{ $errors->first('firstName') }}</small>
                                        @endif
                                    </div>
                                </div>                                                                

                                <div class="form-group {{ ($errors->has('lastName')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Last Name *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="lastName" placeholder="Last Name"
                                               value="{{ old('lastName', $member->lastName) }}">
                                        @if ($errors->has('lastName'))
                                            <small class="help-block">{{ $errors->first('lastName') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('formerName')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Former Name</label>

                                    <div class="col-sm-10">
                                        <input type="textbox" class="form-control" name="formerName"
                                               placeholder="Former Name"
                                               value="{{ old('formerName',$member->formerName) }}">
                                        @if ($errors->has('formerName'))
                                            <small class="help-block">{{ $errors->first('formerName') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('dob')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Date of Birth *</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="dob" type="text"
                                                   class="form-control pull-right datepicker"
                                                   value="{{ date('d/m/Y' , strtotime(old('dob',$member->dob))) }}">
                                        </div>
                                        @if ($errors->has('dob'))
                                            <small class="help-block">{{ $errors->first('dob') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <h4>Member Contact Details</h4>                                

                                <div class="form-group {{ ($errors->has('address1')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address (Line 1) *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="address1" placeholder="Address (Line 1)"
                                               value="{{ old('address1',$member->address1) }}">
                                        @if ($errors->has('address1'))
                                            <small class="help-block">{{ $errors->first('address1') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('address2')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address (Line 2)</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="address2" placeholder="Address (Line 2)"
                                               value="{{ old('address2',$member->address2) }}">
                                        @if ($errors->has('address2'))
                                            <small class="help-block">{{ $errors->first('address2') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('suburb')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Suburb *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="suburb" placeholder="Suburb"
                                               value="{{ old('suburb',$member->suburb) }}">
                                        @if ($errors->has('suburb'))
                                            <small class="help-block">{{ $errors->first('suburb') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('state')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">State *</label>

                                    <div class="col-sm-10">
                                        <select name="state" class="form-control select2" style="width: 100%;">
                                            <option value="ACT"{{ (old('state', $member->state) == "ACT") ? ' selected="selected"' : '' }}>
                                                ACT
                                            </option>
                                            <option value="QLD"{{ (old('state', $member->state) == "QLD") ? ' selected="selected"' : '' }}>
                                                QLD
                                            </option>
                                            <option value="NSW"{{ (old('state', $member->state) == "NSW") ? ' selected="selected"' : '' }}>
                                                NSW
                                            </option>
                                            <option value="NT"{{ (old('state', $member->state) == "NT") ? ' selected="selected"' : '' }}>
                                                NT
                                            </option>
                                            <option value="SA"{{ (old('state', $member->state) == "SA") ? ' selected="selected"' : '' }}>
                                                SA
                                            </option>
                                            <option value="TAS"{{ (old('state', $member->state) == "TAS") ? ' selected="selected"' : '' }}>
                                                TAS
                                            </option>
                                            <option value="VIC"{{ (old('state', $member->state) == "VIC") ? ' selected="selected"' : '' }}>
                                                VIC
                                            </option>
                                            <option value="WA"{{ (old('state', $member->state) == "WA") ? ' selected="selected"' : '' }}>
                                                WA
                                            </option>
                                            <option value="NA"{{ (old('state', $member->state) == "NA") ? ' selected="selected"' : '' }}>
                                                Not Applicable
                                            </option>
                                        </select>
                                    </div>
                                </div>                                                                

                                <div class="form-group {{ ($errors->has('postcode')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Postcode *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="postcode" placeholder="Postcode"
                                               value="{{ old('postcode', $member->postcode) }}">
                                        @if ($errors->has('postcode'))
                                            <small class="help-block">{{ $errors->first('postcode') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('country')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Country *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="country" placeholder="Country"
                                               value="{{ old('country',$member->country) }}">
                                        @if ($errors->has('country'))
                                            <small class="help-block">{{ $errors->first('country') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('phoneLandline')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Phone (Landline)</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="phoneLandline" placeholder="Phone (Landline)"
                                               value="{{ old('phoneLandline', $member->phoneLandline) }}">
                                        @if ($errors->has('phoneLandline'))
                                            <small class="help-block">{{ $errors->first('phoneLandline') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('phoneMobile')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Mobile *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="phoneMobile" placeholder="mobile"
                                               value="{{ old('phoneMobile', $member->phoneMobile) }}">
                                        @if ($errors->has('phoneMobile'))
                                            <small class="help-block">{{ $errors->first('phoneMobile') }}</small>
                                        @endif
                                    </div>
                                </div>                                

                                <div class="form-group {{ ($errors->has('email')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Email *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="email" placeholder="email"
                                               value="{{ old('email', $member->email) }}">
                                        @if ($errors->has('email'))
                                            <small class="help-block">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                </div> 
                                
                                <h4>Qualifications Details</h4>
                                @if (sizeof($qualifications) > 0)
                                    @php 
                                       $counter = 0;
                                    @endphp
                                    
                                    <div id="divQualifications">                                    
                                    @foreach($qualifications as $qualification)
                                       @php                                         
                                          $counter++;
                                       @endphp
                                       <div class="form-group">
											<label class="col-sm-2 control-label">Qualification #{{ $counter }}</label>

											<div class="col-sm-10">
												<input class="form-control" name="qualification{{ $counter }}" placeholder="Qualification" value="{{ $qualification->qualification }}">	
												<input class="form-control" name="institution{{ $counter }}" placeholder="Institution" value="{{ $qualification->institution }}">	
												<input class="form-control" name="graduationYear{{ $counter }}" placeholder="Graduation Year" value="{{ $qualification->graduationYear }}">		
												<input type="hidden" name="qualificationId{{ $counter }}" value="{{ $qualification->id }}">												
											</div>
										</div>
                                    @endforeach
								    </div>
                                @endif
                                <input type="hidden" id="hidQualifications" name="hidQualifications" value="{{ sizeof($qualifications) }}">	
                                
                                <div class="form-group">
									<label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <a href="#" id="btnAddQualification">+ Add another qualification</a>
									</div>
								</div>
                               
                                @if (sizeof($awards) > 0)
                                    @php 
                                       $counter = 0;
                                    @endphp
                                    
                                    <div id="divAwards">    
                                    @foreach($awards as $award)
                                       @php                                         
                                          $counter++;
                                       @endphp
                                       <div class="form-group">
											<label class="col-sm-2 control-label">Honours & Awards #{{ $counter }}</label>

											<div class="col-sm-10">
												<input class="form-control" name="award{{ $counter }}" placeholder="Honours & Awards" value="{{ $award->award }}">
												<input type="hidden" name="awardId{{ $counter }}" value="{{ $award->id }}">																			
											</div>
										</div>
                                    @endforeach
								    </div>
                                @endif
                                <input type="hidden" id="hidAwards" name="hidAwards" value="{{ sizeof($awards) }}">
                                
                                <div class="form-group">
									<label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <a href="#" id="btnAddAwards">+ Add another honours & awards</a>
									</div>
								</div>
                                
                                <h4>Employment Details</h4>
                                
                                <div class="form-group {{ ($errors->has('occupation')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Discipline</label>

                                    <div class="col-sm-10">                                        
                                        <select class="form-control" name="occupation" required>
											<option value="">Choose your discipline </option>
											<option value="Academic" {{ (old('occupation', $member->occupation) == 'Academic' ? " selected" : "") }}>Academic</option>
											<option value="Administration" {{ (old('occupation', $member->occupation) == 'Administration' ? " selected" : "") }}>Administration</option>
											<option value="Biostatistician" {{ (old('occupation', $member->occupation) == 'Biostatistician' ? " selected" : "") }}>Biostatistician</option>
											<option value="Consumer" {{ (old('occupation', $member->occupation) == 'Consumer' ? " selected" : "") }}>Consumer</option>
											<option value="Dietetics" {{ (old('occupation', $member->occupation) == 'Dietetics' ? " selected" : "") }}>Dietetics</option>
                                            <option value="Exercise Physiology" {{ (old('occupation') == 'Exercise Physiology' ? " selected" : "") }}>Exercise Physiology</option>
											<option value="Fellow/Resident/Registrar" {{ (old('occupation', $member->occupation) == 'Fellow/Resident/Registrar' ? " selected" : "") }}>Fellow/Resident/Registrar</option>
											<option value="General Practitioner" {{ (old('occupation', $member->occupation) == 'General Practitioner' ? " selected" : "") }}>General Practitioner</option>
											<option value="Genetics" {{ (old('occupation', $member->occupation) == 'Genetics' ? " selected" : "") }}>Genetics</option>
											<option value="Haematologist" {{ (old('occupation', $member->occupation) == 'Haematologist' ? " selected" : "") }}>Haematologist</option>
											<option value="Medical Oncology (Adult)" {{ (old('occupation', $member->occupation) == 'Medical Oncology (Adult)' ? " selected" : "") }}>Medical Oncology (Adult)</option>
											<option value="Medical Oncology (Paediatric)" {{ (old('occupation', $member->occupation) == 'Medical Oncology (Paediatric)' ? " selected" : "") }}>Medical Oncology (Paediatric)</option>
											<option value="Nuclear Medicine" {{ (old('occupation', $member->occupation) == 'Nuclear Medicine' ? " selected" : "") }}>Nuclear Medicine</option>
											<option value="Nursing" {{ (old('occupation', $member->occupation) == 'Nursing' ? " selected" : "") }}>Nursing</option>
                                            <option value="Occupational Therapy" {{ (old('occupation') == 'Occupational Therapy' ? " selected" : "") }}>Occupational Therapy</option>
											<option value="Orthopaedic Surgery" {{ (old('occupation', $member->occupation) == 'Orthopaedic Surgery' ? " selected" : "") }}>Orthopaedic Surgery</option>
											<option value="Palliative Care" {{ (old('occupation', $member->occupation) == 'Palliative Care' ? " selected" : "") }}>Palliative Care</option>
											<option value="Pathology" {{ (old('occupation', $member->occupation) == 'Pathology' ? " selected" : "") }}>Pathology</option>
											<option value="Pharmacist" {{ (old('occupation', $member->occupation) == 'Pharmacist' ? " selected" : "") }}>Pharmacist</option>
											<option value="Physiotherapist" {{ (old('occupation', $member->occupation) == 'Physiotherapist' ? " selected" : "") }}>Physiotherapist</option>
											<option value="Plastic and Reconstructive Surgery" {{ (old('occupation', $member->occupation) == 'Plastic and Reconstructive Surgery' ? " selected" : "") }}>Plastic and Reconstructive Surgery</option>
											<option value="Psychiatry" {{ (old('occupation', $member->occupation) == 'Psychiatry' ? " selected" : "") }}>Psychiatry</option>
											<option value="Psychology" {{ (old('occupation', $member->occupation) == 'Psychology' ? " selected" : "") }}>Psychology</option>
											<option value="Radiation Oncology" {{ (old('occupation', $member->occupation) == 'Radiation Oncology' ? " selected" : "") }}>Radiation Oncology</option>
											<option value="Radiation Therapy" {{ (old('occupation', $member->occupation) == 'Radiation Therapy' ? " selected" : "") }}>Radiation Therapy</option>
											<option value="Radiology" {{ (old('occupation', $member->occupation) == 'Radiology' ? " selected" : "") }}>Radiology</option>
											<option value="Research" {{ (old('occupation', $member->occupation) == 'Research' ? " selected" : "") }}>Research</option>
                                            <option value="Scientist" {{ (old('occupation', $member->occupation) == 'Scientist' ? " selected" : "") }}>Scientist</option>
                                            <option value="Social Work" {{ (old('occupation', $member->occupation) == 'Social Work' ? " selected" : "") }}>Social Work</option>
                                            <option value="Speech Pathology" {{ (old('occupation') == 'Speech Pathology' ? " selected" : "") }}>Speech Pathology</option>
											<option value="Student" {{ (old('occupation', $member->occupation) == 'Student' ? " selected" : "") }}>Student</option>
											<option value="Surgical Oncology (General Surgeon)" {{ (old('occupation', $member->occupation) == 'Surgical Oncology (General Surgeon)' ? " selected" : "") }}>Surgical Oncology (General Surgeon)</option>
										</select>
                                              
                                        @if ($errors->has('occupation'))
                                            <small class="help-block">{{ $errors->first('occupation') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('companyName')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Employer</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="companyName" placeholder="Company Name"
                                               value="{{ old('companyName',$member->companyName) }}">
                                        @if ($errors->has('companyName'))
                                            <small class="help-block">{{ $errors->first('companyName') }}</small>
                                        @endif
                                    </div>
                                </div> 
                                
                                <div class="form-group{{ ($errors->has('employmentDateCommencement')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Date of Commencement *</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="employmentDateCommencement" type="text"
                                                   class="form-control pull-right datepicker"
                                                   value="{{ date('d/m/Y' , strtotime(old('employmentDateCommencement',$member->employmentDateCommencement))) }}">
                                        </div>
                                        @if ($errors->has('employmentDateCommencement'))
                                            <small class="help-block">{{ $errors->first('employmentDateCommencement') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('employmentAddress1')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address (Line 1) *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="employmentAddress1" placeholder="Address (Line 1)"
                                               value="{{ old('employmentAddress1',$member->employmentAddress1) }}">
                                        @if ($errors->has('employmentAddress1'))
                                            <small class="help-block">{{ $errors->first('employmentAddress1') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('employmentAddress2')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address (Line 2)</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="employmentAddress2" placeholder="Address (Line 2)"
                                               value="{{ old('employmentAddress2',$member->employmentAddress2) }}">
                                        @if ($errors->has('employmentAddress2'))
                                            <small class="help-block">{{ $errors->first('employmentAddress2') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('employmentSuburb')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Suburb *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="employmentSuburb" placeholder="Suburb"
                                               value="{{ old('employmentSuburb',$member->employmentSuburb) }}">
                                        @if ($errors->has('employmentSuburb'))
                                            <small class="help-block">{{ $errors->first('employmentSuburb') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('employmentState')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">State *</label>

                                    <div class="col-sm-10">
                                        <select name="employmentState" class="form-control select2" style="width: 100%;">
                                            <option value="ACT"{{ (old('employmentState', $member->employmentState) == "ACT") ? ' selected="selected"' : '' }}>
                                                ACT
                                            </option>
                                            <option value="QLD"{{ (old('employmentState', $member->employmentState) == "QLD") ? ' selected="selected"' : '' }}>
                                                QLD
                                            </option>
                                            <option value="NSW"{{ (old('employmentState', $member->employmentState) == "NSW") ? ' selected="selected"' : '' }}>
                                                NSW
                                            </option>
                                            <option value="NT"{{ (old('employmentState', $member->employmentState) == "NT") ? ' selected="selected"' : '' }}>
                                                NT
                                            </option>
                                            <option value="SA"{{ (old('employmentState', $member->employmentState) == "SA") ? ' selected="selected"' : '' }}>
                                                SA
                                            </option>
                                            <option value="TAS"{{ (old('employmentState', $member->employmentState) == "TAS") ? ' selected="selected"' : '' }}>
                                                TAS
                                            </option>
                                            <option value="VIC"{{ (old('employmentState', $member->employmentState) == "VIC") ? ' selected="selected"' : '' }}>
                                                VIC
                                            </option>
                                            <option value="WA"{{ (old('employmentState', $member->employmentState) == "WA") ? ' selected="selected"' : '' }}>
                                                WA
                                            </option>
                                            <option value="NA"{{ (old('employmentState', $member->employmentState) == "NA") ? ' selected="selected"' : '' }}>
                                                Not Applicable
                                            </option>
                                        </select>
                                    </div>
                                </div>                                                                

                                <div class="form-group {{ ($errors->has('employmentPostcode')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Postcode *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="employmentPostcode" placeholder="Postcode"
                                               value="{{ old('employmentPostcode', $member->employmentPostcode) }}">
                                        @if ($errors->has('employmentPostcode'))
                                            <small class="help-block">{{ $errors->first('employmentPostcode') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('employmentCountry')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Country *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="employmentCountry" placeholder="Country"
                                               value="{{ old('employmentCountry',$member->employmentCountry) }}">
                                        @if ($errors->has('employmentCountry'))
                                            <small class="help-block">{{ $errors->first('employmentCountry') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('employmentPhoneNumber')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Phone (Landline)</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="employmentPhoneNumber" placeholder="Phone (Landline)"
                                               value="{{ old('employmentPhoneNumber', $member->employmentPhoneNumber) }}">
                                        @if ($errors->has('employmentPhoneNumber'))
                                            <small class="help-block">{{ $errors->first('employmentPhoneNumber') }}</small>
                                        @endif
                                    </div>
                                </div>                                                     

                                <div class="form-group {{ ($errors->has('employmentEmail')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Email *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="employmentEmail" placeholder="email"
                                               value="{{ old('employmentEmail', $member->employmentEmail) }}">
                                        @if ($errors->has('employmentEmail'))
                                            <small class="help-block">{{ $errors->first('employmentEmail') }}</small>
                                        @endif
                                    </div>
                                </div>   
                                
                                <h4>Other Relevant Experience</h4>
                                
                                <div class="form-group {{ ($errors->has('otherExperience')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Other Relevant Experience </label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="otherExperience"
                                                  placeholder="Other experiences or contributions">{{ old('otherExperience', $member->otherExperience) }}</textarea>
                                        @if ($errors->has('otherExperience'))
                                            <small class="help-block">{{ $errors->first('otherExperience') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">CV File</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="cv_current" id="cv_current" value="{{ old('cv_current', $member->cvFile) }}">
                                        
                                        <div class="div-cvfile-upload">
                                           <div>+ Upload New CV</div>
                                           <div><input type="file" name="cv_file" id="cv_file"></div>
										</div>
                                       
                                       <div><a href="#" id="btnDeleteCV">- Delete CV</a></div>
                                        
                                        <div class="div-cvfile">
											@if ($member->cvFile != "")
											   <a href="{{ url('dreamcms/members/'.$member->id.'/download') }}" target="_blank"><i class="far fa-eye"></i> View CV - {{ $member->cvFile }}</a>								       
											@endif
                                        </div>	
                                        
                                        
                                    </div>
                                </div>                                                                
                                
                                <h4>Account Details</h4>                                                                                                            

                                <div class="form-group{{ ($errors->has('dateJoin')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Join Date *</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="dateJoin" type="text"
                                                   class="form-control pull-right datepicker"
                                                   value="{{ date('d/m/Y' , strtotime(old('dateJoin',$member->dateJoin))) }}">
                                        </div>
                                        @if ($errors->has('dateJoin'))
                                            <small class="help-block">{{ $errors->first('dateJoin') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('dateExpire')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Expiry Date *</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="dateExpire" type="text"
                                                   class="form-control pull-right datepicker"
                                                   value="{{ date('d/m/Y' , strtotime(old('dateExpire',$member->dateExpire))) }}">
                                        </div>
                                        @if ($errors->has('dateExpire'))
                                            <small class="help-block">{{ $errors->first('dateExpire') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('password')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Change Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="password" value="">
                                        <small class="help-block">Leave blank if you don't want to change the password
                                        </small>
                                        @if ($errors->has('password'))
                                            <small class="help-block">{{ $errors->first('password') }}</small>
                                        @endif
                                    </div>
                                </div>  
                                
                                <div class="form-group {{ ($errors->has('comments')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Comments </label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="comments"
                                                  placeholder="Comments">{{ old('otherExperience', $member->comments) }}</textarea>
                                        @if ($errors->has('comments'))
                                            <small class="help-block">{{ $errors->first('comments') }}</small>
                                        @endif
                                    </div>
                                </div>
                                                                                        
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Approval Status</label>
                                    <div class="col-sm-10">                                                                                  
                                            <select name="status" class="form-control select2" data-placeholder="All" style="width: 100%;">                                            
                                                <option value="passive"{{ ($member->status == 'passive') ? ' selected="selected"' : '' }}>Pending</option>                                            
                                                <option value="active"{{ ($member->status == 'active') ? ' selected="selected"' : '' }}>Approved</option>  
                                                <option value="inactive"{{ ($member->status == 'inactive') ? ' selected="selected"' : '' }}>Inactive</option>  
                                                <option value="rejected"{{ ($member->status == 'rejected') ? ' selected="selected"' : '' }}>Rejected</option>  
                                            </select>                                               
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ url('dreamcms/members') }}" class="btn btn-info pull-right"
                                       data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                       data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                       data-btn-cancel-label="No">Cancel</a>
                                    <button type="submit" class="btn btn-info pull-right" name="action"
                                            value="save_close">Save & Close
                                    </button>
                                    <button type="submit" class="btn btn-info pull-right" name="action" value="save">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$( "#btnAddQualification" ).click(function(e) {
                qualificationNum = parseInt($( "#hidQualifications" ).val()) + 1;
                $( "#hidQualifications" ).val(qualificationNum);

                html  = '';
                html += '<div class="form-group">';
                html += '<label class="col-sm-2 control-label">Qualification #' + qualificationNum + '</label>';
                html += '<div class="col-sm-10">';
                html += '<input type="text" class="form-control" name="qualification' + qualificationNum + '" placeholder="Qualification" value=""/>';
                html += '<input type="text" class="form-control" name="institution' + qualificationNum + '" placeholder="Institution" value=""/>';
                html += '<input type="text" class="form-control" name="graduationYear' + qualificationNum + '" placeholder="Graduation year" value=""/>';
                html += '</div>';
                html += '</div>';

                $( "#divQualifications" ).append(html);

                e.preventDefault();

            });
			
			$( "#btnAddAwards" ).click(function(e) {
                awardsNum = parseInt($( "#hidAwards" ).val()) + 1;
                $( "#hidAwards" ).val(awardsNum);

                html  = '';
                html += '<div class="form-group">';
                html += '<label class="col-sm-2 control-label">Honours & Awards #' + awardsNum + '</label>';
                html += '<div class="col-sm-10">';
                html += '<input type="text" class="form-control" name="award' + awardsNum + '" placeholder="Honours and Awards" value=""/>';
                html += '</div>';
                html += '</div>';

                $( "#divAwards" ).append(html);

                e.preventDefault();

            });
			
			$( "#btnDeleteCV" ).click(function(e) {
				
				$( ".div-cvfile" ).remove();
				$( "#cv_current" ).val('');
				$( "#cv_file" ).val('');
				
				e.preventDefault();
			});
			
			
        });
    </script>
@endsection