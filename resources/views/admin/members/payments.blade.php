@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/daterangepicker/daterangepicker.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members') }}"><i class="fa fa-users"></i> {{ $display_name }}</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/members/payments') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">

                            <div class="form-group col-xs-2">
                                <label>Type</label>
                                <select name="payment_type" class="select2" style="width: 100%;">
                                    <option value="all" {{ $session['payment_type'] == "" || $session['payment_type']=="all" ? ' selected="selected"' : '' }}>All</option>
                                    <option value="initial" {{ $session['payment_type']=="initial" ? ' selected="selected"' : '' }}>Initial</option>
                                    <option value="renew" {{ $session['payment_type']=="renew" ? ' selected="selected"' : '' }}>Renew</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-2">
                                <label>Method</label>
                                <select name="payment_method" class="select2" style="width: 100%;">
                                    <option value="all" {{ $session['payment_method'] == "" || $session['payment_method']=="all" ? ' selected="selected"' : '' }}>All</option>
                                    <option value="bank-deposit" {{ $session['payment_method']=="bank-deposit" ? ' selected="selected"' : '' }}>Bank Deposit</option>
                                    <option value="paypal" {{ $session['payment_method']=="paypal" ? ' selected="selected"' : '' }}>PayPal</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-2">
                                <label>Status</label>
                                <select name="payment_status" class="select2" style="width: 100%;">
                                    <option value="all" {{ $session['payment_status'] == "" || $session['payment_status']=="all" ? ' selected="selected"' : '' }}>All</option>
                                    <option value="pending" {{ $session['payment_status']=="pending" ? ' selected="selected"' : '' }}>Pending</option>
                                    <option value="completed" {{ $session['payment_status']=="completed" ? ' selected="selected"' : '' }}>Completed</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-3">
                                <label>Date Range</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="daterange" type="text" class="form-control pull-right" id="daterange" value="{{ $session['daterange'] }}">
                                </div>
                            </div>

                            <div class="form-group col-xs-2 filter-button-payments">
                                <button type="submit" class="btn btn-info">Filter</button>
                                @if($is_filtered)
                                    <a href="{{ url('dreamcms/members/payment-forget') }}" type="submit" class="btn btn-danger">Remove</a>
                                @endif
                            </div>
                            
                            <div class="form-group col-xs-1 filter-button" style="text-align: right;">                                
                                <a href="{{ url('dreamcms/members/payment-export') }}" type="submit" class="btn btn-danger">Export to Excel</a>                                
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box-body">
                    @if(count($payments))
                        <table class="table table-hover">
                            <tr>
                                <th>Member</th>
                                <th>@sortablelink('payment_type','Type')</th>
                                <th>@sortablelink('payment_method','Method')</th>
                                <th>@sortablelink('amount')</th>
                                <th>@sortablelink('payment_status','Status')</th>
                                <th>@sortablelink('created_at','Date / Time')</th>
                                <th class="hd-text-right">Actions</th>
                            </tr>
                            @foreach($payments as $payment)
                            <tr>
                                <td>{{ (isset($payment->member) ? $payment->member->full_name : "") }}</td>
                                <td>{{ ucfirst($payment->payment_type) }}</td>
                                <td>
                                @if($payment->payment_method=='paypal')
                                PayPal
                                @endif
                                @if($payment->payment_method=='bank-deposit')
                                Bank Deposit
                                @endif
                                </td>
                                <td>${{ $payment->amount }}</td>
                                <td>
                                @if($payment->payment_status=='completed')
                                <span class="label label-success">Completed</span>
                                @endif

                                @if($payment->payment_status=='pending')
                                <span class="label label-warning">Pending</span>
                                @endif
                                </td>
                                <td>{{ \Carbon\Carbon::parse($payment->created_at)->format('d-m-Y H:i:s') }}</td>
                                <td>
                                    <div class="pull-right">
                                        @can('edit-members')
                                        <a href="{{ url('dreamcms/members/'.$payment->id.'/edit-payment') }}" class="tool"><i class="fa fa-edit"></i></a>
                                        @endcan
                                        
                                        @can('delete-members')
                                            <a href="{{ url('dreamcms/members/'.$payment->id.'/delete-payment') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                           @endcan
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $payments->total() }} record</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $payments->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/daterangepicker/moment.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/daterangepicker/daterangepicker.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('#daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD/MM/YYYY',
                }
            });

            $('#daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });

            $('#daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
        });
    </script>
@endsection