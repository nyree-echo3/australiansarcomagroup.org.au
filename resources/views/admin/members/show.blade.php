@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members') }}"><i class="fa fa-envelope"></i> {{ $display_name }}</a>
                </li>
                <li class="active">View</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12">

                    <div class="box box-primary">
                        <div class="box-body">

                            <div class="table-responsive">
                                <h4>Membership Class</h4>
                                <table class="table table-members">
                                    <tr>
                                        <th>Type :</th>
                                        <td>{{ $member->type->name }}</td>
                                    </tr>                                   
                                </table>
                            </div>
                            
                            <div class="table-responsive">
                                <h4>Member Information</h4>
                                <table class="table table-members">
                                    <tr>
                                        <th>Title :</th>
                                        <td>{{ $member->title }}</td>
                                    </tr>
                                    <tr>
                                        <th>First Name :</th>
                                        <td>{{ $member->firstName }}</td>
                                    </tr>
                                    <tr>
                                        <th>Last Name :</th>
                                        <td>{{ $member->lastName }}</td>
                                    </tr>
                                    <tr>
                                        <th>Former Name :</th>
                                        <td>{{ $member->formerName }}</td>
                                    </tr>
                                    <tr>
                                        <th>Date of Birth :</th>
                                        <td>{{ \Carbon\Carbon::parse($member->dob)->format('d-m-Y') }}</td>
                                    </tr>
                                 </table>
                            </div>
                            
                            <div class="table-responsive">
                                <h4>Member Contact Details</h4>
                                <table class="table table-members">                                   
                                    <tr>
                                        <th>Address :</th>
                                        <td>
                                           {{ $member->address1 }}
                                           {!! ($member->address2 != "" ? "<br>" . $member->address2 : "") !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Suburb :</th>
                                        <td>{{ $member->suburb }}</td>
                                    </tr>
                                    <tr>
                                        <th>State :</th>
                                        <td>{{ $member->state }}</td>
                                    </tr>
                                    <tr>
                                        <th>Postcode :</th>
                                        <td>{{ $member->postcode }}</td>
                                    </tr>
                                    <tr>
                                        <th>Country :</th>
                                        <td>{{ $member->country }}</td>
                                    </tr>
                                    <tr>
                                        <th>Phone (Landline) :</th>
                                        <td>{{ $member->phoneLandline }}</td>
                                    </tr>
                                    <tr>
                                        <th>Mobile :</th>
                                        <td>{{ $member->phoneMobile }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email :</th>
                                        <td>{{ $member->email }}</td>
                                    </tr>
                                </table>
                            </div>
                            
                            <div class="table-responsive">
                                <h4>Qualifications Details</h4>
                                <table class="table table-members">   
                                    @if (sizeof($qualifications) > 0)
                                      @php                                        
                                         $counter = 0;
                                      @endphp
                                    
                                      @foreach($qualifications as $qualification)
                                          @php                                         
                                            $counter++;
                                          @endphp                                
										  <tr>
											<th>Qualification #{{ $counter }} :</th>
											<td>
										       {{ $qualification->qualification }}<br>
											   {{ $qualification->institution }}<br>
											   {{ $qualification->graduationYear }}
											</td>
										  </tr>
                                      @endforeach
                                   @endif
                                   
                                   @if (sizeof($awards) > 0)
                                      @php 
                                         $counter = 0;
                                      @endphp
                                    
                                      @foreach($awards as $award)
                                          @php                                         
                                            $counter++;
                                          @endphp                                
										  <tr>
											<th>Honours & Awards #{{ $counter }} :</th>
											<td>{{ $award->award }}</td>
										  </tr>
                                      @endforeach
                                   @endif
                                    
                                </table>
                            </div>
                            
                            <div class="table-responsive">
                                <h4>Employment Details</h4>
                                <table class="table table-members">                                   
                                    <tr>
                                        <th>Discipline :</th>
                                        <td>{{ $member->occupation }}</td>
                                    </tr>
                                    <tr>
                                        <th>Employer :</th>
                                        <td>{{ $member->companyName }}</td>
                                    </tr>
                                    <tr>
                                        <th>Address :</th>
                                        <td>
                                           {{ $member->employmentAddress1 }}
                                           {!! ($member->employmentAddress2 != "" ? "<br>" . $member->employmentAddress2 : "") !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Suburb :</th>
                                        <td>{{ $member->employmentSuburb }}</td>
                                    </tr>
                                    <tr>
                                        <th>State :</th>
                                        <td>{{ $member->employmentState }}</td>
                                    </tr>
                                    <tr>
                                        <th>Postcode :</th>
                                        <td>{{ $member->employmentPostcode }}</td>
                                    </tr>
                                    <tr>
                                        <th>Country :</th>
                                        <td>{{ $member->employmentCountry }}</td>
                                    </tr>
                                    <tr>
                                        <th>Phone (Landline) :</th>
                                        <td>{{ $member->employmentPhoneLandline }}</td>
                                    </tr>                                    
                                    <tr>
                                        <th>Email :</th>
                                        <td>{{ $member->employmentEmail }}</td>
                                    </tr>
                                </table>
                            </div>
                            
                            <div class="table-responsive">
                                <h4>Other Relevant Experience</h4>
                                <table class="table table-members">                                   
                                    <tr>
                                        <th>Relevant Experience :</th>
                                        <td>{{ $member->otherExperience }}</td>
                                    </tr>
                                    
                                    <tr>
                                        <th>CV File :</th>
                                        <td><a href="{{ url('dreamcms/members/'.$member->id.'/download') }}" target="_blank">{{ $member->cvFile }}</a></td>
                                    </tr>                                                                                                          
                                </table>
                            </div>                                                  
                           
                            <div class="table-responsive">
                                <h4>Account Details</h4>
                                <table class="table table-members">                                   
                                    <tr>
                                        <th style="width:20%">Join Date :</th>
                                        <td>{{ \Carbon\Carbon::parse($member->dateJoin)->format('d-m-Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:20%">Expiry Date :</th>
                                        <td>{{ \Carbon\Carbon::parse($member->dateExpire)->format('d-m-Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Comments :</th>
                                        <td>{{ $member->comments }}</td>
                                    </tr>    
                                </table>
                            </div>
                            @if($member->payments)
                            <h4>Payments</h4>
                            <table class="table table-hover">
                                <tr>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Method</th>
                                    <th>Status</th>
                                    <th>Date / Time</th>
                                    <th class="hd-text-right">Actions</th>
                                </tr>
                                @foreach($member->payments as $payment)
                                <tr>
                                    <td>${{ number_format($payment->amount, 2) }}</td>
                                    <td>{{ ucfirst($payment->payment_type) }}</td>
                                    <td>
                                    @if($payment->payment_method=='paypal')
                                        PayPal
                                    @endif
                                    @if($payment->payment_method=='bank-deposit')
                                        Bank Deposit
                                    @endif
                                    </td>
                                    <td>
                                        @if($payment->payment_status=='completed')
                                        <span class="label label-success">Completed</span>
                                        @endif

                                        @if($payment->payment_status=='pending')
                                        <span class="label label-warning">Pending</span>
                                        @endif
                                    <td>{{ \Carbon\Carbon::parse($payment->created_at)->format('d-m-Y H:i:s') }}</td>
                                    <td>
                                        <div class="pull-right">
                                            @can('edit-members')
                                                <a href="{{ url('dreamcms/members/'.$payment->id.'/edit-payment') }}" class="tool"><i class="fa fa-edit"></i></a>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection