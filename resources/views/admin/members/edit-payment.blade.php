@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members') }}"><i class="fa fa-envelope"></i> {{ $display_name }}</a>
                </li>
                <li class="active">Payment</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12">

                    <div class="box box-primary">
                        <div class="box-body">

                            <div class="table-responsive">
                                <h4>Payment Details</h4>
                                <form method="post" class="form-horizontal" action="{{ url('dreamcms/members/update-payment') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="id" value="{{ $payment->id }}">
                                    <table class="table">
                                        <tr>
                                            <th style="width:150px;">Member :</th>
                                            <td>{{ $payment->member->full_name }}</td>
                                        </tr>
                                        <tr>
                                            <th style="width:150px;">Amount :</th>
                                            <td>${{ number_format($payment->amount, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Type :</th>
                                            <td>{{ ucfirst($payment->payment_type) }}</td>
                                        </tr>
                                        @if($payment->payment_method=='bank-deposit')
                                            <tr>
                                                <th>Method :</th>
                                                <td>Bank Deposit</td>
                                            </tr>
                                            <tr>
                                                <th>Transaction Number :</th>
                                                <td>
                                                    <input class="form-control" name="payment_transaction_number" placeholder="Transaction Number"
                                                           value="{{ $payment->payment_transaction_number }}"
                                                           style="width: 40%;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Status :</th>
                                                <td>
                                                    <select name="payment_status" class="form-control select2" style="width: 40%;">
                                                        <option value="pending" {{ ($payment->payment_status == "pending") ? ' selected="selected"' : '' }}>Pending</option>
                                                        <option value="completed" {{ ($payment->payment_status == "completed") ? ' selected="selected"' : '' }}>Completed</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        @endif

                                        @if($payment->payment_method=='paypal')
                                            <tr>
                                                <th>Method :</th>
                                                <td>PayPal</td>
                                            </tr>
                                            <tr>
                                                <th>Transaction Number :</th>
                                                <td>{{ $payment->payment_transaction_number }}</td>
                                            </tr>
                                            <tr>
                                                <th>PayPal Payer ID :</th>
                                                <td>{{ $payment->paypal_payer_id }}</td>
                                            </tr>
                                            <tr>
                                                <th>Status :</th>
                                                <td>{{ ucfirst($payment->payment_status) }}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th>Date :</th>
                                            <td><input name="created_at" type="text"
                                                   class="form-control pull-right datepicker"
                                                   value="{{ date('d/m/Y' , strtotime(old('created_at',\Carbon\Carbon::parse($payment->created_at)->format('d-m-Y')))) }}">
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <th>Time :</th>
                                            <td>{{ \Carbon\Carbon::parse($payment->created_at)->format('H:i:s') }}                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <button type="submit" class="btn btn-info" name="action" value="save">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $(".select2").select2();
        });
    </script>
@endsection