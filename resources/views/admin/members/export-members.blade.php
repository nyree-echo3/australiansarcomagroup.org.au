 <table>
    <thead>
    <tr>
        <th><b>Members Export</b></th>
	</tr>      
      
    <tr>
    	<th><b><u>Report Creation:</u></b></th>
    	<th>{{ date('d M Y ga') }}</th>
    </tr>
     
    <tr>
        <th><b>Total Members:</b></th> 
        <th>{{ sizeof($members) }}</th>   	 
    </tr> 
      
    <tr>
        <th></th>
	</tr>              
       
    <tr>
        <th><b>Membership Class</b></th>
        
        <th><b>Member ID</b></th>
        <th><b>Title</b></th>
        <th><b>First Name</b></th>
        <th><b>Last Name</b></th>
        <th><b>Former Name</b></th>
        <th><b>Date of Birth</b></th>
        
        <th><b>Address 1</b></th>
        <th><b>Address 2</b></th>
        <th><b>Suburb</b></th>
        <th><b>State</b></th>
        <th><b>Postcode</b></th>
        <th><b>Country</b></th>
        <th><b>Phone</b></th>
        <th><b>Mobile</b></th>
        <th><b>Email</b></th>
        
        <th><b>Qualifications</b></th>
        <th><b>Honours/Awards</b></th>
        
        <th><b>Position</b></th>
        <th><b>Employer</b></th>
        <th><b>Date of Commencement</b></th>
        <th><b>Employment Address 1</b></th>
        <th><b>Employment Address 2</b></th>
        <th><b>Employment Suburb</b></th>
        <th><b>Employment State</b></th>
        <th><b>Employment Postcode</b></th>
        <th><b>Employment Country</b></th>
        <th><b>Employment Phone</b></th>
        <th><b>Employment Email</b></th>
        
        <th><b>Other Relevant Experience</b></th>
        
        <th><b>Join Date</b></th>
        <th><b>Expiry Date</b></th>
        <th><b>Status</b></th>
        
    </tr>
    </thead>
    <tbody>
    @foreach($members as $member)
        <tr>
            <td>{{ $member->type->name }}</td>
            
            <td>{{ $member->id }}</td>
            <td>{{ $member->title }}</td>
            <td>{{ $member->firstName }}</td>
            <td>{{ $member->lastName }}</td>
            <td>{{ $member->formerName }}</td>
            <td>{{ date('d/m/Y' , strtotime($member->dob)) }}</td>
                        
            <td>{{ $member->address1 }}</td>
            <td>{{ $member->address2 }}</td>
            <td>{{ $member->suburb }}</td>
            <td>{{ $member->state }}</td>
            <td>{{ $member->postcode }}</td>
            <td>{{ $member->country }}</td>
            <td>{{ $member->phoneLandline }}</td>
            <td>{{ $member->phoneMobile }}</td>
            <td>{{ $member->email }}</td>
            
            <td>
               @foreach ($member->qualifications as $qualification)
                  {{ $qualification->qualification }} - {{ $qualification->institution }} - {{ $qualification->graduationYear }}<br>
               @endforeach
            </td>
            
            <td>
               @foreach ($member->awards as $award)
                  {{ $award->award }} <br>
               @endforeach
            </td>
            
            <td>{{ $member->occupation }}</td>
            <td>{{ $member->companyName }}</td>
            <td>{{ ($member->employmentCommencementDate <> null ? date('d/m/Y' , strtotime($member->employmentCommencementDate)) : "") }}</td>
            <td>{{ $member->employmentAddress1 }}</td>
            <td>{{ $member->employmentAddress2 }}</td>
            <td>{{ $member->employmentSuburb }}</td>
            <td>{{ $member->employmentState }}</td>
            <td>{{ $member->employmentPostcode }}</td>
            <td>{{ $member->employmentCountry }}</td>
            <td>{{ $member->employmentPhoneNumber }}</td>
            <td>{{ $member->employmentEmail }}</td>
            
            <td>{{ $member->otherExperience }}</td>
            
            @php
               $status = "";
               switch ($member->status)  {
                  case "passive": $status = "Inactive"; break;
                  case "active": $status = "Approved"; break;
                  case "rejected": $status = "Rejected"; break;
               }
            @endphp
            
            <td>{{ date('d/m/Y' , strtotime($member->dateJoin)) }}</td>
            <td>{{ date('d/m/Y' , strtotime($member->dateExpire)) }}</td>
            <td>{{ $status }}</td>
        </tr>
    @endforeach

    </tbody>
</table>