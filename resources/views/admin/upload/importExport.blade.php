<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DreamCMS | Upload Member Data</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="{{ asset('/components/theme/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('/components/theme/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/admin/general.css') }}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">

<div class="login-box">       
    <div class="login-logo">       
        <div class="login-logo-img">
           <img src="{{ asset('/images/admin/logo.png') }}" alt="Echo3" title="Echo3" class='navbar-logo'>    
        </div>
       
        <a href="../../index2.html">Dream<b>CMS</b></a>
    </div>
    <div class="login-box-body">                                    
          <p class="login-box-msg">Upload Member Data</p>
                       
             <form action="{{ url('') }}/dreamcms/upload/importExcel" class="form-horizontal" method="post" enctype="multipart/form-data">
                @csrf 

                @if ($errors->any())
                    <div class="form-group">
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
							<ul>

								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach

							</ul>
						</div>
				    </div>                    
                @endif

                @if (Session::has('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif
                
                <div class="form-group">
                   <input type="file" name="import_file" />
				</div>
               
                <div class="form-group">
					<div class="row">
						<div class="col-xs-4">
						   <button class="btn btn-primary btn-block btn-flat">Import File</button>
						</div>
					</div>
				</div>
           
            </form>

          </div>
    </div>
</div>

<footer class="login-footer">
    <div>
    </div>
    Copyright &copy; 2011 - {{ date('Y') }} <span>|</span> DreamCMS V4.0 <span>|</span> <a href='https://www.echo3.com.au' target='_blank'>Echo3</a>
</footer>

<script src="{{ asset('/components/theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('/components/theme/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
</script>
</body>
</html>
	