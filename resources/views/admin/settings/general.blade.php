@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Settings</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                <li class="active">General</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">General Values</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/settings/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">

                                <div class="form-group {{ ($errors->has('company_name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Company Name</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="{{ $company_name->value }}">
                                        @if ($errors->has('company_name'))
                                            <small class="help-block">{{ $errors->first('company_name') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('email')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Contact Email</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email" placeholder="Contact Email" value="{{ $email->value }}">
                                        @if ($errors->has('email'))
                                            <small class="help-block">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('phone_number')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Phone Number</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone_number" placeholder="Phone Number" value="{{ $phone_number->value }}">
                                        @if ($errors->has('phone_number'))
                                            <small class="help-block">{{ $errors->first('phone_number') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('fax_number')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Fax Number</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="fax_number" placeholder="Fax Number" value="{{ $fax_number->value }}">
                                        @if ($errors->has('fax_number'))
                                            <small class="help-block">{{ $errors->first('fax_number') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('address')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="address" rows='5' placeholder="Address">{{ $address->value }}</textarea>
                                        @if ($errors->has('address'))
                                            <small class="help-block">{{ $errors->first('address') }}</small>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            
                            <div class="box-header with-border">
								<h3 class="box-title">Website</h3>
							</div>
                                                                                                  
                            <div class="box-body">

                                <div class="form-group{{ ($errors->has('live_date')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Go Live Date</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="live_date" type="text" class="form-control pull-right datepicker" value="{{ $live_date->value }}">
                                        </div>
                                        @if ($errors->has('live_date'))
                                            <small class="help-block">{{ $errors->first('live_date') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <!--<div class="form-group {{ ($errors->has('google_analytics')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Google Analytics</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="google_analytics" rows='10' placeholder="Google Analytics Script">{{ $google_analytics->value }}</textarea>
                                        @if ($errors->has('google_analytics'))
                                            <small class="help-block">{{ $errors->first('google_analytics') }}</small>
                                        @endif
                                    </div>
                                </div>-->
                                
							</div>
                                                                                                 
                            <div class="box-header with-border">
								<h3 class="box-title">Payment</h3>
							</div>
                                                                                                  
                            <div class="box-body">                               
                                <div class="form-group {{ ($errors->has('payment_direct_deposit')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Direct Deposit</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="payment_direct_deposit" rows='10' placeholder="Direct Deposit">{{ $payment_direct_deposit->value }}</textarea>
                                        @if ($errors->has('payment_direct_deposit'))
                                            <small class="help-block">{{ $errors->first('payment_direct_deposit') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('payment_direct_deposit_members')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Members Direct Deposit</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="payment_direct_deposit_members" rows='10' placeholder="Direct Deposit">{{ $payment_direct_deposit_members->value }}</textarea>
                                        @if ($errors->has('payment_direct_deposit_members'))
                                            <small class="help-block">{{ $errors->first('payment_direct_deposit_members') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('payment_direct_deposit_events')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Events Direct Deposit</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="payment_direct_deposit_events" rows='10' placeholder="Direct Deposit">{{ $payment_direct_deposit_events->value }}</textarea>
                                        @if ($errors->has('payment_direct_deposit_events'))
                                            <small class="help-block">{{ $errors->first('payment_direct_deposit_events') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
							</div>
                                                                                                  
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {  
			CKEDITOR.replace('payment_direct_deposit');
			CKEDITOR.replace('payment_direct_deposit_members');
			CKEDITOR.replace('payment_direct_deposit_events');

			$('.datepicker').datepicker({
              autoclose: true,
			  format: 'dd/mm/yyyy'
            });
        });
    </script>
@endsection