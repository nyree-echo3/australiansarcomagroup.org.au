@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/specialists') }}"><i class="fa fa-clipboard"></i> {{ $display_name }}</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/specialists/store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" id="title"
                                               placeholder="Title" value="{{ old('title') }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div> 
                                
                                <div class="form-group {{ ($errors->has('address1')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address (Line 1) *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="address1" placeholder="Address (Line 1)"
                                               value="{{ old('address1') }}">
                                        @if ($errors->has('address1'))
                                            <small class="help-block">{{ $errors->first('address1') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('address2')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address (Line 2)</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="address2" placeholder="Address (Line 2)"
                                               value="{{ old('address2') }}">
                                        @if ($errors->has('address2'))
                                            <small class="help-block">{{ $errors->first('address2') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('suburb')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Suburb *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="suburb" placeholder="Suburb"
                                               value="{{ old('suburb') }}">
                                        @if ($errors->has('suburb'))
                                            <small class="help-block">{{ $errors->first('suburb') }}</small>
                                        @endif
                                    </div>
                                </div>                                                                

                                <div class="form-group{{ ($errors->has('state')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">State</label>

                                    <div class="col-sm-10">
                                        <select id="state" name="state" class="form-control select2" style="width: 100%;">
                                            <option value=""{{ (old('state') == "") ? ' selected="selected"' : '' }}></option>
                                            <option value="ACT"{{ (old('state') == "ACT") ? ' selected="selected"' : '' }}>
                                                ACT
                                            </option>
                                            <option value="QLD"{{ (old('state') == "QLD") ? ' selected="selected"' : '' }}>
                                                QLD
                                            </option>
                                            <option value="NSW"{{ (old('state') == "NSW") ? ' selected="selected"' : '' }}>
                                                NSW
                                            </option>
                                            <option value="NT"{{ (old('state') == "NT") ? ' selected="selected"' : '' }}>
                                                NT
                                            </option>
                                            <option value="SA"{{ (old('state') == "SA") ? ' selected="selected"' : '' }}>
                                                SA
                                            </option>
                                            <option value="TAS"{{ (old('state') == "TAS") ? ' selected="selected"' : '' }}>
                                                TAS
                                            </option>
                                            <option value="VIC"{{ (old('state') == "VIC") ? ' selected="selected"' : '' }}>
                                                VIC
                                            </option>
                                            <option value="WA"{{ (old('state') == "WA") ? ' selected="selected"' : '' }}>
                                                WA
                                            </option>                                            
                                        </select>
                                        <input class="form-control" id="state_other" name="state_other" placeholder="State" value="{{ old('state_other') }}">
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('country')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Country *</label>

                                    <div class="col-sm-10">
                                        <select name="country" class="form-control select2" style="width: 100%;">
                                            <option value="Australia"{{ (old('country') == "Australia") ? ' selected="selected"' : '' }}>Australia</option>
                                            <option value="New Zealand"{{ (old('country') == "New Zealand") ? ' selected="selected"' : '' }}>New Zealand</option>
										</select>
                                        @if ($errors->has('country'))
                                            <small class="help-block">{{ $errors->first('country') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('postcode')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Postcode *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="postcode" placeholder="Postcode"
                                               value="{{ old('postcode') }}">
                                        @if ($errors->has('postcode'))
                                            <small class="help-block">{{ $errors->first('postcode') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('contact_name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Contact Name</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="contact_name" placeholder="Contact Name"
                                               value="{{ old('contact_name') }}">
                                        @if ($errors->has('contact_name'))
                                            <small class="help-block">{{ $errors->first('contact_name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('contact_email')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Contact Email</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="contact_email" placeholder="Contact Email"
                                               value="{{ old('contact_email') }}">
                                        @if ($errors->has('contact_email'))
                                            <small class="help-block">{{ $errors->first('contact_email') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('contact_phone')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Contact Phone</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="contact_phone" placeholder="Contact Phone"
                                               value="{{ old('contact_phone') }}">
                                        @if ($errors->has('contact_phone'))
                                            <small class="help-block">{{ $errors->first('contact_phone') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('contact_fax')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Contact Fax</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="contact_fax" placeholder="Contact Fax"
                                               value="{{ old('contact_fax') }}">
                                        @if ($errors->has('contact_fax'))
                                            <small class="help-block">{{ $errors->first('contact_fax') }}</small>
                                        @endif
                                    </div>
                                </div>
                                                               
                                <div class="form-group {{ ($errors->has('medical_oncologist')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Medical Oncologist</label>

									<div class="col-sm-10">
											<textarea id="medical_oncologist" name="medical_oncologist" rows="5" cols="80"  class="form-control textarea" >{{ old('medical_oncologist') }}</textarea>
										@if ($errors->has('medical_oncologist'))
											<small class="help-block">{{ $errors->first('medical_oncologist') }}</small>
										@endif
									</div>
                               </div>          
                               
                               <div class="form-group {{ ($errors->has('paediatric_medical_oncologist')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Paediatric Medical Oncologist</label>

									<div class="col-sm-10">
											<textarea id="paediatric_medical_oncologist" name="paediatric_medical_oncologist" rows="5" cols="80"  class="form-control textarea" >{{ old('paediatric_medical_oncologist') }}</textarea>
										@if ($errors->has('paediatric_medical_oncologist'))
											<small class="help-block">{{ $errors->first('paediatric_medical_oncologist') }}</small>
										@endif
									</div>
                               </div> 
                               
                               <div class="form-group {{ ($errors->has('adolescent_medical_oncologist')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Adolescent Medical Oncologist</label>

									<div class="col-sm-10">
											<textarea id="adolescent_medical_oncologist" name="adolescent_medical_oncologist" rows="5" cols="80"  class="form-control textarea" >{{ old('adolescent_medical_oncologist') }}</textarea>
										@if ($errors->has('adolescent_medical_oncologist'))
											<small class="help-block">{{ $errors->first('adolescent_medical_oncologist') }}</small>
										@endif
									</div>
                               </div>    
                                     
                               <div class="form-group {{ ($errors->has('radiation_oncologist')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Radiation Oncologist</label>

									<div class="col-sm-10">
											<textarea id="radiation_oncologist" name="radiation_oncologist" rows="5" cols="80"  class="form-control textarea" >{{ old('radiation_oncologist') }}</textarea>
										@if ($errors->has('radiation_oncologist'))
											<small class="help-block">{{ $errors->first('radiation_oncologist') }}</small>
										@endif
									</div>
                               </div> 
                                        
                               <div class="form-group {{ ($errors->has('surgical_oncologist')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Surgical Oncologist</label>

									<div class="col-sm-10">
											<textarea id="surgical_oncologist" name="surgical_oncologist" rows="5" cols="80"  class="form-control textarea" >{{ old('surgical_oncologist') }}</textarea>
										@if ($errors->has('surgical_oncologist'))
											<small class="help-block">{{ $errors->first('surgical_oncologist') }}</small>
										@endif
									</div>
                               </div> 
                                        
                               <div class="form-group {{ ($errors->has('orthopaedic_surgeon')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Orthopaedic Surgeon</label>

									<div class="col-sm-10">
											<textarea id="orthopaedic_surgeon" name="orthopaedic_surgeon" rows="5" cols="80"  class="form-control textarea" >{{ old('orthopaedic_surgeon') }}</textarea>
										@if ($errors->has('orthopaedic_surgeon'))
											<small class="help-block">{{ $errors->first('orthopaedic_surgeon') }}</small>
										@endif
									</div>
                               </div>    
                                     
                               <div class="form-group {{ ($errors->has('pathologist')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Pathologist</label>

									<div class="col-sm-10">
											<textarea id="pathologist" name="pathologist" rows="5" cols="80"  class="form-control textarea" >{{ old('pathologist') }}</textarea>
										@if ($errors->has('pathologist'))
											<small class="help-block">{{ $errors->first('pathologist') }}</small>
										@endif
									</div>
                               </div>
                                         
                               <div class="form-group {{ ($errors->has('radiologist')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Radiologist</label>

									<div class="col-sm-10">
											<textarea id="radiologist" name="radiologist" rows="5" cols="80"  class="form-control textarea" >{{ old('radiologist') }}</textarea>
										@if ($errors->has('radiologist'))
											<small class="help-block">{{ $errors->first('radiologist') }}</small>
										@endif
									</div>
                               </div>      
                                   
                               <div class="form-group {{ ($errors->has('sarcoma_nurse')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Sarcoma Nurse</label>

									<div class="col-sm-10">
											<textarea id="sarcoma_nurse" name="sarcoma_nurse" rows="5" cols="80"  class="form-control textarea" >{{ old('sarcoma_nurse') }}</textarea>
										@if ($errors->has('sarcoma_nurse'))
											<small class="help-block">{{ $errors->first('sarcoma_nurse') }}</small>
										@endif
									</div>
                               </div>   
                                      
                               <div class="form-group {{ ($errors->has('other')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Other</label>

									<div class="col-sm-10">
											<textarea id="other" name="other" rows="5" cols="80"  class="form-control textarea" >{{ old('other') }}</textarea>
										@if ($errors->has('other'))
											<small class="help-block">{{ $errors->first('other') }}</small>
										@endif
									</div>
                               </div>                   
                               
                                                                                                                                                                                                                                               
                               
                                @php
                                    $status = 'active';
                                    if(count($errors)>0){
                                       if(old('live')=='on'){
                                        $status = 'active';
                                       }else{
                                        $status = '';
                                       }
                                    }
                                @endphp
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status *</label>
                                    <div class="col-sm-10">
                                        <label>
                                            <input class="page_status" type="checkbox" data-toggle="toggle"
                                                   data-size="mini" name="live" {{ $status == 'active' ? ' checked' : null }}>
                                        </label>
                                    </div>
                                </div>								
                               
                                <div class="box-footer">
                                    <a href="{{ url('dreamcms/specialists') }}" class="btn btn-info pull-right">Cancel</a>
                                    <button type="submit" class="btn btn-info pull-right" name="action"
                                            value="save_close">Save & Close
                                    </button>
                                    <button type="submit" class="btn btn-info pull-right" name="action" value="save">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>    
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {   
			$('#state').on('change', function() {			
				$('#state_other').val($(this).val());
			});
			
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            
		});
				
    </script>
@endsection