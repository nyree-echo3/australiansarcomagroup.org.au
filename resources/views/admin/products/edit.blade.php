@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/products') }}"><i class="fas fa-gift"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/products/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $product->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name"
                                               placeholder="Title"
                                               value="{{ old('name',$product->name) }}">
                                        @if ($errors->has('name'))
                                            <small class="help-block">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>                                                                

                                <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">SEO Name *</label>
                                    <div class="col-sm-10">

                                        <div class="input-group">
                                            <input type="text" id="slug" name="slug" class="form-control"
                                                   value="{{ old('slug',$product->slug) }}" readonly>
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      data-target="#change-slug">Change SEO Name
                                              </button>
                                            </span>
                                        </div>

                                        @if ($errors->has('slug'))
                                            <small class="help-block">{{ $errors->first('slug') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('sku')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">SKU</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="sku" name="sku" placeholder="SKU" value="{{ old('sku',$product->sku) }}">
                                        @if ($errors->has('sku'))
                                            <small class="help-block">{{ $errors->first('sku') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('price')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Price *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="price" name="price" placeholder="Price" value="{{ old('price',$product->price) }}">
                                        @if ($errors->has('price'))
                                            <small class="help-block">{{ $errors->first('price') }}</small>
                                        @endif
                                    </div>
                                </div>

                                @php
                                    if(old('category_id')!=''){
                                        $category_id = old('category_id');
                                    }else{
                                        $category_id = $product->category_id;
                                    }
                                @endphp
                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                     id="category_selector">

                                    <label class="col-sm-2 control-label">Category *</label>
                                    <div class="col-sm-10">
                                        <select name="category_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"{{ ($category_id == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('ext_title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Title *</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <input type="text" id="ext_title" name="ext_title" class="form-control"
                                                   placeholder="Meta Title"
                                                   value="{{ old('ext_title',$product->ext_title) }}">
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      id="copy-title" name="copy-title">Copy From Title
                                              </button>
                                            </span>
                                        </div>
                                        <div id="c_count_ext_title"></div>
                                        @if ($errors->has('ext_title'))
                                            <small class="help-block">{{ $errors->first('ext_title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_keywords')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Keywords *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="meta_keywords"
                                                  placeholder="Meta Keywords">{{ old('meta_keywords', $product->meta_keywords) }}</textarea>
                                        @if ($errors->has('meta_keyword'))
                                            <small class="help-block">{{ $errors->first('meta_keywords') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('meta_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" id="meta_description"
                                                  name="meta_description" maxlength="250"
                                                  placeholder="Meta Description">{{ old('meta_description',$product->meta_description) }}</textarea>
                                        <div id="c_count_meta_description"></div>
                                        @if ($errors->has('meta_description'))
                                            <small class="help-block">{{ $errors->first('meta_description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('excerpt')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Short Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="excerpt"
                                                  placeholder="Short Description">{{ old('excerpt',$product->excerpt) }}</textarea>
                                        @if ($errors->has('excerpt'))
                                            <small class="help-block">{{ $errors->first('excerpt') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Description *</label>

                                    <div class="col-sm-10">
                                        <textarea id="description" name="description" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('description',$product->description) }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>
                                @php
                                    if(count($errors)>0){
                                       $imageCount = old('imageCount');
                                    }else{                                        
                                       $imageCount = count($product->images);
                                    }
                                @endphp
                                <div class="form-group {{ ($errors->has('thumbnail')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Images</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="imageCount" name="imageCount" value="{{ $imageCount }}">

                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload Images</button>
                                        @php
                                        $class = ' invisible';
                                        if(count($product->images) > 0){
                                            $class = '';
                                        }
                                        $i=0;
                                        @endphp
                                        <br/><br/>
                                        <span id="added_image">
										  @foreach($product->images as $image)
                                            <div id="imgBox{{ $i }}" class="image-box sortable{!! $image->delete == '1' ? ' invisible' : null !!}">
											<input id="imgId{{ $i }}" name="imgId{{ $i }}" data-id="{{ $i }}" type="hidden" data-size="mini" value="{{ $image->id }}">
											<input id="imgPosition{{ $i }}" name="imgPosition{{ $i }}" class="imgPosition" type="hidden" value="{{ $image->position }}">
											<input id="imgDelete{{ $i }}" name="imgDelete{{ $i }}" type="hidden" value="{{ $image->delete }}">
											<input id="imgType{{ $i }}" name="imgType{{ $i }}" type="hidden" value="{{ $image->type }}">

											<div class="image-box-toolbox">
											<a class="tool" title="Delete" href="#" onclick="image_delete({{ $i }}); return false;">
											<i class="fa fa-trash-alt"></i>
											</a>

											<div class="pull-right" style="margin-right: 10px">
											<div style="width: 33px; height: 22px;">											
											<input id="imgStatus{{ $i }}" name="imgStatus{{ $i }}" data-id="{{ $i }}" type="checkbox" data-size="mini" {{ ($image->status == 'active' || $image->status == 'on') ? ' checked' : null }}>
											
											</div>														
											</div>

											</div>

											<div class="image-box-img">
											<image class="image-item" src="{{ $image->location }}" />
											<input id="imgLocation{{ $i }}" name="imgLocation{{ $i }}" type="hidden" value="{{ $image->location }}">
											</div>

											</div>
                                            @php
                                            $i++;
                                            @endphp
										  @endforeach
                                        </span>

                                    </div>
                                </div>
                            </div>


                            @php
                            if(count($errors)>0){                               
                                $state = old('state');                               
                            }else{
                                $state = $product->state;
                            }                            
                            @endphp
                            <div class="form-group">
								<label class="col-sm-2 control-label">Status *</label>
								<div class="col-sm-10">
									<select name="state" class="form-control select2" style="width: 100%;">                                           
									   <option value="1"{{ ($state == 'draft') ? ' selected="selected"' : '' }}>Draft</option>
									   <option value="2"{{ ($state == 'inactive') ? ' selected="selected"' : '' }}>Inactive</option>                                            
									   <option value="3"{{ ($state == 'active') ? ' selected="selected"' : '' }}>Active</option>
									   <option value="4"{{ ($state == 'unavailable') ? ' selected="selected"' : '' }}>Unavailable</option>
									   <option value="5"{{ ($state == 'retired') ? ' selected="selected"' : '' }}>Retired</option>
									</select>
								</div>
							</div>

                            <div class="form-group {{ ($errors->has('special_url')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Special URL</label>

                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{ env('APP_URL') }}/</span>
                                        <input type="text" name="special_url" class="form-control" value="{{ old('special_url', $product->special_url) }}">
                                    </div>
                                    @if ($errors->has('special_url'))
                                        <small class="help-block">{{ $errors->first('special_url') }}</small>
                                    @endif
                                </div>
                            </div>
                                
                            <div class="box-footer">
                                <a href="{{ url('dreamcms/products') }}" class="btn btn-info pull-right">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal"
                           value="{{ old('slug',$product->slug) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            CKEDITOR.replace('description');
            CKEDITOR.replace('excerpt');

            $(".select2").select2();

            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            var maxLengthTitle = 56;
            $('#ext_title').keyup(function () {
                var textlen = maxLengthTitle - $(this).val().length;
                $('#c_count_ext_title').text($(this).val().length + " characters | " + textlen + " characters left");
            });
            $('#ext_title').trigger("keyup", {which: 50});

            var maxLengthDescription = 250;
            $('#meta_description').keyup(function () {
                var textlen = maxLengthDescription - $(this).val().length;
                var words = $(this).val().trim().split(" ").length;
                $('#c_count_meta_description').text($(this).val().length + " characters | " + textlen + " characters left | " + words + " words");
            });
            $('#meta_description').trigger("keyup", {which: 50});

            $('#copy-title').click(function () {
                $('#ext_title').val($('#name').val().substr(0, maxLengthTitle));
                $('#ext_title').trigger("keyup", {which: 50});
            });

            $("#image-popup").click(function () {
                openPopup();
            });

            $('#title').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });
        });

		$('#added_image').sortable({
			items: '.sortable',

			stop: function () {
				counter = 0;
				$('input.imgPosition').each(function() {
                   $(this).val(counter);
				   counter++;
                });
			}

		});

		for (i = 0; i < $('#imageCount').val(); i++) {
		    $('#imgStatus' + i).bootstrapToggle();
		}

        function openPopup() {
            imageCounter = $('#imageCount').val();

			CKFinder.popup( {
				chooseFiles: true,
				width: 800,
				height: 600,

				onInit: function( finder ) {

					finder.on( 'files:choose', function( evt ) {
						var files = evt.data.files;

						files.forEach( function( file, i ) {
							folder = file.get( 'folder' );

							strHtml  = '<div id="imgBox' + imageCounter + '" class="image-box sortable">';

							strHtml += '<input id="imgPosition' + imageCounter + '" name="imgPosition' + imageCounter + '" type="hidden" class="imgPosition" value="' + imageCounter + '">';
                            strHtml += '<input id="imgDelete' + imageCounter + '" name="imgDelete' + imageCounter + '" type="hidden" value="">';
                            strHtml += '<input id="imgType' + imageCounter + '" name="imgType' + imageCounter + '" type="hidden" value="new">';

							strHtml += '<div class="image-box-toolbox">';
							strHtml += '<a class="tool" title="Delete" href="#" onclick="image_delete(' + imageCounter + '); return false;">';
							strHtml += '<i class="fa fa-trash-alt"></i>';
							strHtml += '</a>';

							strHtml += '<div class="pull-right" style="margin-right: 10px">';
							strHtml += '<div style="width: 33px; height: 22px;">';
							strHtml += '<input id="imgStatus' + imageCounter + '" name="imgStatus' + imageCounter + '" data-id="' + imageCounter + '" type="checkbox" class="image_status" data-size="mini" checked>';
							strHtml += '</div>';
							strHtml += '</div>';

							strHtml += '</div>';

							strHtml += '<div class="image-box-img">';
							strHtml += '<image class="image-item" src="' + folder.getUrl() + file.get( 'name' ) + '">';
							strHtml += '<input id="imgLocation' + imageCounter + '" name="imgLocation' + imageCounter + '" type="hidden" value="' + folder.getUrl() + file.get( 'name' ) + '">';
							strHtml += '</div>';

							strHtml += '</div>';

							$('#added_image').append(strHtml);
                            $('#imgStatus' + imageCounter).bootstrapToggle();

							imageCounter++;

							$('#imageCount').val(imageCounter);
							$('#remove-image').removeClass('invisible');
						} );
					} );


				}

				} );

}

		function image_delete(id)  {
			//$('#imgBox' + id).remove();
			//$('#imageCount').val($('#imageCount').val() - 1);

			//if ($('#imageCount').val() == 0)  {
			//   $('#remove-image').addClass('invisible');
			//}

			$('#imgBox' + id).addClass('invisible');
			$('#imgDelete' + id).val("1");
		}
    </script>
@endsection 