@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="far fa-newspaper"></i> {{ $display_name }}</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/news') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">

                            <div class="form-group col-xs-2">
                                <select name="category" class=" select2" style="width: 100%;">
                                    <option value="all" {{ $session['category'] == "" || $session['category']=="all" ? ' selected="selected"' : '' }}>
                                        All Categories
                                    </option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"{{ $session['category'] == $category->id ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-xs-2">
                                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ $session['search'] }}">
                            </div>

                            <div class="form-group col-xs-3 filter-button">
                                <button type="submit" class="btn btn-info">Filter</button>
                                @if($is_filtered)
                                    <a href="{{ url('dreamcms/news/forget') }}" type="submit" class="btn btn-danger">Remove</a>
                                @endif
                            </div>
                        </div>
                    </form>
                    @can('add-news')
                    <div class="pull-right box-tools">
                        <a href="{{ url('dreamcms/news/add') }}" type="button" class="btn btn-info btn-sm"
                           data-widget="add">Add New
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    @endcan
                </div>
                <div class="box-body">
                    @if(count($news))
                        <table class="table table-hover">
                            <tr>
                                <th>@sortablelink('title')</th>
                                <th>@sortablelink('categorysort.name','Category')</th>
                                <th>@sortablelink('startDate', 'Start Date')</th>
                                <th>@sortablelink('status')</th>
                                <th class="hd-text-right">Actions</th>
                            </tr>
                            @foreach($news as $item)
                                <tr class="{{ date('Ymd' , strtotime(str_replace("/", "-", $item->archive_date))) < date('Ymd') ? 'news-archive-date' : '' }}"</tr>
                                    <td>{{ $item->title }}</td>
                                    <td>
                                    @if($item->category)
                                    {{ $item->category->name }}
                                    @endif
                                    </td>
                                    <td>{{ date('d/m/Y' , strtotime(str_replace("/", "-", $item->start_date))) }}</td>
                                    <td>
                                        <input id="news_{{ $item->id }}" data-id="{{ $item->id }}" class="news_status" type="checkbox" data-toggle="toggle" data-size="mini"{{ $item->status == 'active' ? ' checked' : null }}>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            @can('edit-news')
                                            <a href="{{ url('dreamcms/news/'.$item->id.'/edit') }}"
                                               class="tool" data-toggle="tooltip" title="Edit"><i
                                                        class="fa fa-edit"></i></a>
                                                        
                                            <a href="{{ url('dreamcms/news/'.$item->id.'/preview') }}" class="tool" data-toggle="tooltip" title="Preview" target="_blank"><i class="far fa-eye"></i></a>               
                                            @endcan

                                            @can('delete-news')
                                            <a href="{{ url('dreamcms/news/'.$item->id.'/delete') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                           @endcan
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $news->total() }} record</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $news->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.news_status').change(function() {
                $.ajax({
                    type: "POST",
                    url: "news/"+$(this).data('id')+"/change-status",
                    data:  {
                        'status':$(this).prop('checked')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });

        });
    </script>
@endsection