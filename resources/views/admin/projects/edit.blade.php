@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/projects') }}"><i class="fa fa-clipboard"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/projects/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $project->id }}">
                            <div class="box-body">
                               <div class="form-group{{ ($errors->has('name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Name *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name',$project->name) }}">
                                        @if ($errors->has('name'))
                                            <small class="help-block">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div> 
                                 
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ old('title',$project->title) }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>                                

                                @php
                                    if(old('category_id')!=''){
                                        $category_id = old('category_id');
                                    }else{
                                        $category_id = $project->category_id;
                                    }
                                @endphp
                                
                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                     id="category_selector">

                                    <label class="col-sm-2 control-label">Category *</label>
                                    <div class="col-sm-10">
                                        <select name="category_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"{{ ($category_id == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('summary')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Summary *</label>

									<div class="col-sm-10">
											<textarea id="summary" name="summary" rows="5" cols="80"  class="form-control textarea" >{{ old('summary',$project->summary) }}</textarea>
										@if ($errors->has('summary'))
											<small class="help-block">{{ $errors->first('summary') }}</small>
										@endif
									</div>
                               </div>
                               
                               <div class="form-group {{ ($errors->has('eligible_cohort')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Eligible Cohort *</label>

									<div class="col-sm-10">
											<textarea id="eligible_cohort" name="eligible_cohort" rows="5" cols="80"  class="form-control textarea">{{ old('eligible_cohort',$project->eligible_cohort) }}</textarea>
										@if ($errors->has('eligible_cohort'))
											<small class="help-block">{{ $errors->first('eligible_cohort') }}</small>
										@endif
									</div>
                               </div>
                               
                               <div class="form-group {{ ($errors->has('recruitment_status')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Recruitment Status *</label>

									<div class="col-sm-10">
											<input type="text" class="form-control" id="recruitment_status" name="recruitment_status" placeholder="Recruitment Status" value="{{ old('recruitment_status',$project->recruitment_status) }}">
										@if ($errors->has('recruitment_status'))
											<small class="help-block">{{ $errors->first('recruitment_status') }}</small>
										@endif
									</div>
                               </div>                                                              
                               
                               <div class="form-group {{ ($errors->has('phase')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Phase *</label>

									<div class="col-sm-10">
											<input type="text" class="form-control" id="phase" name="phase" placeholder="Phase" value="{{ old('phase',$project->phase) }}">
										@if ($errors->has('phase'))
											<small class="help-block">{{ $errors->first('phase') }}</small>
										@endif
									</div>
                               </div>
                               
                               <div class="form-group {{ ($errors->has('recruitment_target')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Recruitment Target *</label>

									<div class="col-sm-10">
											<input type="text" class="form-control" id="recruitment_target" name="recruitment_target" placeholder="Recruitment Target" value="{{ old('recruitment_target',$project->recruitment_target) }}">
										@if ($errors->has('recruitment_target'))
											<small class="help-block">{{ $errors->first('recruitment_target') }}</small>
										@endif
									</div>
                               </div>
                               
                               <div class="form-group {{ ($errors->has('expected_completion')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Expected Completion *</label>

									<div class="col-sm-10">
											<input type="text" class="form-control" id="expected_completion" name="expected_completion" placeholder="Expected Completion" value="{{ old('expected_completion',$project->expected_completion) }}">
										@if ($errors->has('expected_completion'))
											<small class="help-block">{{ $errors->first('expected_completion') }}</small>
										@endif
									</div>
                               </div>
                               
                               <div class="form-group {{ ($errors->has('participating_sites')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Participating Sites *</label>

									<div class="col-sm-10">
											<textarea id="participating_sites" name="participating_sites" rows="5" cols="80"  class="form-control textarea">{{ old('participating_sites',$project->participating_sites) }}</textarea>
										@if ($errors->has('participating_sites'))
											<small class="help-block">{{ $errors->first('participating_sites') }}</small>
										@endif
									</div>
                               </div>
                               
                               <!--<div class="form-group {{ ($errors->has('collaborators')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Collaborators *</label>

									<div class="col-sm-10">
											<textarea id="collaborators" name="collaborators" rows="5" cols="80"  class="form-control textarea">{{ old('collaborators',$project->collaborators) }}</textarea>
										@if ($errors->has('collaborators'))
											<small class="help-block">{{ $errors->first('collaborators') }}</small>
										@endif
									</div>
                               </div>-->
                               
                               <div class="form-group {{ ($errors->has('country_pi')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Country PI *</label>

									<div class="col-sm-10">
											<textarea id="country_pi" name="country_pi" rows="5" cols="80"  class="form-control textarea">{{ old('country_pi',$project->country_pi) }}</textarea>
										@if ($errors->has('country_pi'))
											<small class="help-block">{{ $errors->first('country_pi') }}</small>
										@endif
									</div>
                               </div>
                               
                               <div class="form-group {{ ($errors->has('co_sponsors')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Co-Sponsors *</label>

									<div class="col-sm-10">
											<textarea id="co_sponsors" name="co_sponsors" rows="5" cols="80"  class="form-control textarea">{{ old('co_sponsors',$project->co_sponsors) }}</textarea>
										@if ($errors->has('co_sponsors'))
											<small class="help-block">{{ $errors->first('co_sponsors') }}</small>
										@endif
									</div>
                               </div>
                               
                               <div class="form-group {{ ($errors->has('link')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Link</label>

									<div class="col-sm-10">
											<textarea id="link" name="link" rows="5" cols="80"  class="form-control textarea">{{ old('link',$project->link) }}</textarea>
										@if ($errors->has('link'))
											<small class="help-block">{{ $errors->first('link') }}</small>
										@endif
									</div>
                               </div>
                               
                               <div class="form-group {{ ($errors->has('publications')) ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Publications</label>

									<div class="col-sm-10">
											<textarea id="publications" name="publications" rows="5" cols="80"  class="form-control textarea">{{ old('publications',$project->publications) }}</textarea>
										@if ($errors->has('publications'))
											<small class="help-block">{{ $errors->first('publications') }}</small>
										@endif
									</div>
                               </div>



                                
                            @php
                            if(count($errors)>0){
                               if(old('live')=='on'){
                                $status = 'active';
                               }else{
                                $status = '';
                               }
                            }else{
                                $status = $project->status;
                            }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status * </label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/projects') }}" class="btn btn-info pull-right">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal"
                           value="{{ old('slug',$project->slug) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {           
            $(".select2").select2();
			CKEDITOR.replace('publications');

            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });            
		});
    </script>
@endsection 