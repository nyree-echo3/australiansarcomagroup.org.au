@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Abstracts</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/events/abstracts') }}">Abstracts</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Abstracts</h3>
                </div>
                <div class="box-body">
                    @if(count($abstracts))
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Abstract Title</th>
                                <th>Date / Time</th>
                                <th class="hd-text-right">Actions</th>
                            </tr>
                            @foreach($abstracts as $abstract)
                                <tr>
                                    <td>{{ $abstract->title.' '.$abstract->first_name.' '.$abstract->last_name }}</td>
                                    <td>{{ $abstract->abstract_title }}</td>
                                    <td>{{ $abstract->created_at }}</td>
                                    <td>
                                        <div class="pull-right">
                                            <a href="{{ url('dreamcms/events/'.$abstract->id.'/show-abstract') }}"
                                               class="tool" data-toggle="tooltip" title="Show">
                                                <i class="far fa-eye"></i></a>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection