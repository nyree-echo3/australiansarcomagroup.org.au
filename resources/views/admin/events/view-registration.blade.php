@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }} Registration</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/events') }}"><i class="far fa-calendar"></i> {{ $display_name }}</a>
                </li>
                <li class="active">Event Registration</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12">

                    <div class="box box-primary">
                        <div class="box-body">

                            <div class="table-responsive">
                                <h4>Registration Details</h4>
                                <form method="post" class="form-horizontal" action="{{ url('dreamcms/events/update-registration') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="id" value="{{ ($registration->payment ? $registration->payment->id : "") }}">
                                    <table class="table">
                                        <tr>
                                            <th style="width:150px;">Attendee :</th>
                                            <td>{{ $registration->title }} {{ $registration->first_name }} {{ $registration->last_name }} {{ ($registration->member_id != "" ? "(Member ID " . $registration->member_id . ")" : "") }}</td>
                                        </tr>
                                        <tr>
                                            <th style="width:150px;">Email</th>
                                            <td>{{ $registration->email_address }}</td>
                                        </tr>
                                        <tr>
                                            <th style="width:150px;">Mobile</th>
                                            <td>{{ $registration->phone_mobile }}</td>
                                        </tr>
                                        @if($registration->event->slug=='annual-scientific-meeting-2021')
                                            <tr>
                                                <th>Address1</th>
                                                <td>{{ $registration->address1 }}</td>
                                            </tr>
                                            <tr>
                                                <th>Address2</th>
                                                <td>{{ $registration->address1 }}</td>
                                            </tr>
                                            <tr>
                                                <th>Suburb</th>
                                                <td>{{ $registration->suburb }}</td>
                                            </tr>
                                            <tr>
                                                <th>Post Code</th>
                                                <td>{{ $registration->postcode }}</td>
                                            </tr>
                                            <tr>
                                                <th>Social Activity</th>
                                                <td>{{ $registration->social_activity }}</td>
                                            </tr>
                                            <tr>
                                                <th>Partner</th>
                                                <td>{{ $registration->partner }}</td>
                                            </tr>
                                            <tr>
                                                <th>Partner Name</th>
                                                <td>{{ $registration->partner_name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Partner Diet</th>
                                                <td>{{ $registration->partner_diet }}</td>
                                            </tr>
                                            <tr>
                                                <th>Breastfeeding / Childcare services</th>
                                                <td>{{ $registration->breastfeeding }}</td>
                                            </tr>
                                            <tr>
                                                <th>Delegate List</th>
                                                <td>{{ $registration->delegate_list }}</td>
                                            </tr>
                                            <tr>
                                                <th>Delegate Acknowledgment</th>
                                                <td>{{ $registration->delegate_ack }}</td>
                                            </tr>
                                            <tr>
                                                <th>Cancellation</th>
                                                <td>{{ $registration->cancellation }}</td>
                                            </tr>
                                            <tr>
                                                <th>Dietary</th>
                                                <td>{{ $registration->dietary }}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th style="width:150px;">Role</th>
                                            <td>{{ $registration->designation }}</td>
                                        </tr>
                                        <tr>
                                            <th style="width:150px;">Company/Institution</th>
                                            <td>{{ $registration->company_name }}</td>
                                        </tr>
                                        <tr>
                                            <th style="width:150px;">Location</th>
                                            <td>{{ $registration->state }} {{ $registration->country }}</td>
                                        </tr>

                                        <tr>
                                            <th style="width:150px;">Event :</th>
                                            <td>{{ $registration->event->title }}</td>
                                        </tr>
                                        <tr>
                                            <th style="width:150px;">Ticket :</th>
                                            <td>{{ $registration->ticket->name }} ${{ number_format($registration->ticket->price, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <th style="width:150px;">Abstracts Link :</th>
                                            <td><a target='_blank' href='{{ url('') }}/asm-2020/abstracts/{{ $registration->token}}'>{{ url('') }}/asm-2020/abstracts/{{ $registration->token}}</a></td>
                                        </tr>
									</table>
                                    
                                    @if ($registration->payment)
										<h4>Payment Details</h4>              
										<table class="table">    
											<tr>
												<th style="width:150px;">Amount :</th>
												<td>${{ number_format($registration->payment->amount, 2) }}</td>
											</tr>											

											@if($registration->payment->payment_method=='bank-deposit')
												<tr>
													<th>Method :</th>
													<td>Bank Deposit</td>
												</tr>
												<tr>
													<th>Transaction Number :</th>
													<td>
														<input class="form-control" name="payment_transaction_number" placeholder="Transaction Number"
															   value="{{ $registration->payment->payment_transaction_number }}"
															   style="width: 40%;">
													</td>
												</tr>
												<tr>
													<th>Status :</th>
													<td>
														<select name="payment_status" class="form-control select2" style="width: 40%;">
															<option value="pending" {{ ($registration->payment->payment_status == "pending") ? ' selected="selected"' : '' }}>Pending</option>
															<option value="completed" {{ ($registration->payment->payment_status == "completed") ? ' selected="selected"' : '' }}>Completed</option>
														</select>
													</td>
												</tr>
												
												<button type="submit" class="btn btn-info" name="action" value="save">Save</button>    
											@endif                                        

											@if($registration->payment->payment_method=='paypal')
												<tr>
													<th>Method :</th>
													<td>PayPal</td>
												</tr>
												<tr>
													<th>Transaction Number :</th>
													<td>{{ $registration->payment->payment_transaction_number }}</td>
												</tr>
												<tr>
													<th>PayPal Payer ID :</th>
													<td>{{ $registration->payment->paypal_payer_id }}</td>
												</tr>
												<tr>
													<th>Status :</th>
													<td>{{ ucfirst($registration->payment->payment_status) }}</td>
												</tr>
											@endif
											<tr>
												<th>Date :</th>
												<td>{{ \Carbon\Carbon::parse($registration->payment->created_at)->format('d-m-Y') }}</td>
											</tr>

											<tr>
												<th>Time :</th>
												<td>{{ \Carbon\Carbon::parse($registration->payment->created_at)->format('H:i:s') }}</td>
											</tr>
										</table>

										<a href="{{ url('dreamcms/events/registrations') }}" class="btn btn-info">Close</a>                               
                                    @else
									    <a href="{{ url('dreamcms/events/registrations') }}" class="btn btn-info">Close</a>
                                    @endif
                                                                        
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $(".select2").select2();
        });
    </script>
@endsection