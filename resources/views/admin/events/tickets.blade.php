<div class="box box-info box-info-ticket">
	<div class="box-header ticket-header">
		<div class="form-group ticket-group">
			<label class="col-sm-1 control-label">TICKETS</label>
			<div class="col-sm-11">			
				<button id="btnTicketAdd" type="button" class="btn btn-info btn-sm">Add New</button>			
			</div>
		</div>
	</div>	
</div>

<div class="form-group">	
	<div class="col-sm-6">
		<div class="input-group ticket-group">			
			<label class="control-label">Name *</label>
		</div>		
	</div>
	
	<div class="col-sm-3">
		<div class="input-group ticket-group">			
			<label class="control-label">Price $ *</label>
		</div>		
	</div>
	
	<div class="col-sm-1">
		<div class="input-group ticket-group ticket-group-center">			
			<label class="control-label">Members Only</label>
		</div>		
	</div>

	<div class="col-sm-1">
		<div class="input-group ticket-group ticket-group-center">
			<label class="control-label">Virtual</label>
		</div>
	</div>
	
	<div class="col-sm-1">
		<div class="input-group ticket-group">			
			<label class="control-label">Delete</label>
		</div>		
	</div>		
		
</div> 

<div id="ticketsWrapper">
							
	@php								   
	   $event_row = 0;	   					   
	@endphp

	@if (isset($event_tickets))  
		@foreach ($event_tickets as $event_ticket) 																	
			@php
			  $event_row++;
		   @endphp

			<div id="ticketRow{{ $event_row }}" class="{{ ($event_ticket->is_deleted == "true" ? "invisible" : "") }}">
				<div class="form-group">	
					<div class="col-sm-6">
						<div class="input-group ticket-group">			
							<input type="text" name="ticket_name{{ $event_row }}" class="form-control" value="{{ old('ticket_name' . $event_row, $event_ticket->name) }}" placeholder="Name">
							<input type="hidden" name="ticket_id{{ $event_row }}" value="{{ $event_ticket->id }}">
						</div>		
					</div>

					<div class="col-sm-3">
						<div class="input-group ticket-group">						
							<input type="text" name="ticket_price{{ $event_row }}" class="form-control money" value="{{ old('ticket_price', $event_ticket->price) }}" placeholder="0.00">
						</div>		
					</div>

					<div class="col-sm-1">
						<div class="input-group ticket-group">			
							<input class="page_status" type="checkbox" name="ticket_members_only{{ $event_row }}" {{ $event_ticket->members_only == 'active' ? ' checked' : '' }}>
						</div>		
					</div>

					<div class="col-sm-1">
						<div class="input-group ticket-group">
							<input class="page_status" type="checkbox" name="ticket_virtual{{ $event_row }}" {{ $event_ticket->type == 'virtual' ? ' checked' : '' }}>
						</div>
					</div>

					<div class="col-sm-1">
						<div class="input-group ticket-group">			
							<a href="#"
								   id="btnTicketDelete{{ $event_row }}" class="tool" data-toggle=confirmation data-title="Are you sure?"
								   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
								   data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
							<input type="hidden" id="ticket_is_deleted{{ $event_row }}" name="ticket_is_deleted{{ $event_row }}" value="{{ $event_ticket->is_deleted}}">
						</div>		
					</div>
				</div> 
			</div>		    
		@endforeach
	@endif
	
</div>	
<input type="hidden" id="event_tickets" name="event_tickets" value="{{$event_row }}">

@section('inline-scripts-tickets')
    @php
       $event_row = 0;
	@endphp

    @if (isset($event_tickets))
		@foreach ($event_tickets as $event_ticket) 																	
			@php
			  $event_row++;
			@endphp

			<script>
				$('#btnTicketDelete{{ $event_row }}').confirmation({
					onConfirm:function(e) {					
						btnDeleteTicket({{ $event_row }});		
						return false;
					},
					onCancel:function(e) {       
					   return false;	
					}

				});
			</script>

		@endforeach
    @endif
@endsection

