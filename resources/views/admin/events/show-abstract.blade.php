@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Abstracts</h1>
            <ol class="breadcrumb">
                <li class="active">Abstracts</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12">

                    <div class="box box-primary">
                        <div class="box-body">
                            
                            <div class="table-responsive">
                                <h4>Presenter Details</h4>
                                <table class="table table-members">
                                    <tr>
                                        <th>Title :</th>
                                        <td>{{ $abstract->title }}</td>
                                    </tr>
                                    <tr>
                                        <th>First Name :</th>
                                        <td>{{ $abstract->first_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Last Name :</th>
                                        <td>{{ $abstract->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email :</th>
                                        <td>{{ $abstract->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Mobile :</th>
                                        <td>{{ $abstract->mobile }}</td>
                                    </tr>
                                 </table>
                            </div>
                            
                            <div class="table-responsive">
                                <h4>Hospital/Institution</h4>
                                <table class="table table-members">                                   
                                    <tr>
                                        <th>Details :</th>
                                        <td>{{ $abstract->hospital }}</td>
                                    </tr>
                                </table>
                            </div>
                            
                            <div class="table-responsive">
                                <h4>Abstract/Poster Submission</h4>
                                <table class="table table-members">
                                    <tr>
                                        <th>Is this a poster submission?</th>
                                        <td>{{ $abstract->is_poster }}</td>
                                    </tr>
                                    <tr>
                                        <th>Is this a podium submission?</th>
                                        <td>{{ $abstract->is_podium }}</td>
                                    </tr>
                                    <tr>
                                        <th>Abstract/Poster Title :</th>
                                        <td>{{ $abstract->abstract_title }}</td>
                                    </tr>
                                    <tr>
                                        <th>Author(s)/Presenter(s) :</th>
                                        <td>{{ $abstract->authors }}</td>
                                    </tr>
                                    <tr>
                                        <th>Institutions :</th>
                                        <td>{{ $abstract->institutions }}</td>
                                    </tr>
                                    <tr>
                                        <th>Copy :</th>
                                        <td>{{ $abstract->copy }}</td>
                                    </tr>
                                    <tr>
                                        <th>Date / Time :</th>
                                        <td>{{ $abstract->created_at }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </div>
@endsection