@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/daterangepicker/daterangepicker.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }} Registrations</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/events') }}"><i class="far fa-calendar"></i> {{ $display_name }}</a></li>
                <li><a href="{{ url('dreamcms/events/registration') }}">Registration</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/events/registrations') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                           
                            <div class="form-group col-xs-2">
                                <label>Event</label>
                                <select name="event" class=" select2" style="width: 100%;">
                                    <option value="all" {{ $session['event_id'] == "" || $session['event_id']=="all" ? ' selected="selected"' : '' }}>
                                        All Events
                                    </option>
                                    @foreach($events as $event)
                                        <option value="{{ $event->id }}"{{ $session['event_id'] == $event->id ? ' selected="selected"' : '' }}>{{ $event->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-xs-2">
                                <label>Method</label>
                                <select name="payment_method" class="select2" style="width: 100%;">
                                    <option value="all" {{ $session['payment_method'] == "" || $session['payment_method']=="all" ? ' selected="selected"' : '' }}>All</option>
                                    <option value="bank-deposit" {{ $session['payment_method']=="bank-deposit" ? ' selected="selected"' : '' }}>Bank Deposit</option>
                                    <option value="paypal" {{ $session['payment_method']=="paypal" ? ' selected="selected"' : '' }}>PayPal</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-2">
                                <label>Status</label>
                                <select name="payment_status" class="select2" style="width: 100%;">
                                    <option value="all" {{ $session['payment_status'] == "" || $session['payment_status']=="all" ? ' selected="selected"' : '' }}>All</option>
                                    <option value="pending" {{ $session['payment_status']=="pending" ? ' selected="selected"' : '' }}>Pending</option>
                                    <option value="completed" {{ $session['payment_status']=="completed" ? ' selected="selected"' : '' }}>Completed</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-3">
                                <label>Date Range</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="daterange" type="text" class="form-control pull-right" id="daterange" value="{{ $session['daterange'] }}">
                                </div>
                            </div>

                            <!--<div class="form-group col-xs-2">
                                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ $session['search'] }}">
                            </div>-->

                            <div class="form-group col-xs-2 filter-button-payments">
                                <button type="submit" class="btn btn-info">Filter</button>
                                @if($is_filtered)
                                    <a href="{{ url('dreamcms/events/registrations-forget') }}" type="submit" class="btn btn-danger">Remove</a>
                                @endif
                            </div>
                            
                            <div class="form-group col-xs-1 filter-button" style="text-align: right;">                                
                                <a href="{{ url('dreamcms/events/registrations-export') }}" type="submit" class="btn btn-danger">Export to Excel</a><br><br>  
                                <a href="{{ url('dreamcms/events/registration/add') }}" type="button" class="btn btn-info btn-sm" data-widget="add">Add New <i class="fa fa-plus"></i></a>                              
                            </div>                                                      
                        </div>
                    </form>
                    
                </div>
                <div class="box-body">
                    @if(count($bookings))
                        <table class="table table-hover">
                            <tr>
                                <th>@sortablelink('title')</th>
                                <th>@sortablelink('firstName')</th>
                                <th>@sortablelink('lastName')</th>
                                <th>@sortablelink('email')</th>
                                <th>Member</th>
                                <th>@sortablelink('event')</th> 
                                <th>@sortablelink('ticket')</th> 
                                <th>@sortablelink('price')</th>
                                <th>@sortablelink('payment_method','Method')</th>                               
                                <th>@sortablelink('payment_status','Status')</th>
                                <th>@sortablelink('created_at','Date / Time')</th>
                                <th class="hd-text-right">Actions</th>
                            </tr>
                            @php
                               $bookingTotal = count($bookings);
                            @endphp
                               
                            @foreach($bookings as $booking)
                                @php
                                   $trStyle = "";
                                   if (($booking->payment && $booking->payment->payment_status == "pending"))  {
                                      $trStyle = "visibility:hidden; display:none; height:0;";
                                      $bookingTotal -= 1;
                                   }
                                @endphp
                                
                                <tr style="{{ $trStyle }}">
                                    <td>{{ $booking->title }}</td>
                                    <td>{{ $booking->first_name }}</td>
                                    <td>{{ $booking->last_name }}</td>
                                    <td>{{ $booking->email_address }}</td>
                                    <td>{{ ($booking->member_id != "" ? "Yes (ID " . $booking->member_id . ")" : "No") }}</td>                                    
                                    <td>
                                    @if($booking->event)
                                    {{ $booking->event->title }}
                                    @endif
                                    </td>
                                    <td>
                                    @if($booking->ticket)
                                    {{ $booking->ticket->name }}
                                    @endif
                                    </td>
                                    <td>
									@if($booking->ticket)
                                    ${{ number_format($booking->ticket->price, 2) }}
                                    @endif
									</td>
                                    <td>
                                    @if ($booking->payment)
										@if ($booking->payment->payment_method == 'paypal')
										    PayPal
										@endif
										@if ($booking->payment->payment_method == 'bank-deposit')
										    Bank Deposit
										@endif
									@else
									   @if ($booking->is_manual == "true")
									       Manual
									   @endif
									@endif
									</td>
                                    <td>
                                    @if ($booking->payment)
										@if($booking->payment->payment_status=='completed')
										<span class="label label-success">Completed</span>
										@endif

										@if($booking->payment->payment_status=='pending')
										<span class="label label-warning">Pending</span>
										@endif
									@endif
									</td>
                                    <td>
                                    @if ($booking->payment)
                                    {{ \Carbon\Carbon::parse($booking->payment->created_at)->format('d-m-Y H:i:s') }}
                                    @endif
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            @can('edit-events')   
												@if ($booking->is_manual == "true")  
												   <a href="{{ url('dreamcms/events/'.$booking->id.'/edit-registration') }}" class="tool" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
												@else                                       
												   <a href="{{ url('dreamcms/events/'.$booking->id.'/view-registration') }}" class="tool" data-toggle="tooltip" title="View"><i class="far fa-eye"></i></a>
												@endif
                                               <a href="{{ url('dreamcms/events/registration/'.$booking->id.'/delete') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                            @endcan                                            
                                        </div>
                                    </td>

                                </tr>                                
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $bookingTotal }} records</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $bookings->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/daterangepicker/moment.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/daterangepicker/daterangepicker.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$('#daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD/MM/YYYY',
                }
            });

            $('#daterange').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });

            $('#daterange').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });			           

        });
    </script>
@endsection