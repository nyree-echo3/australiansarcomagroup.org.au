 <table>
    <thead>
    <tr>
        <th><b>Events Registrations Export</b></th>
	</tr>      
      
    <tr>
    	<th><b><u>Report Creation:</u></b></th>
    	<th>{{ date('d M Y ga') }}</th>
    </tr>
     
    <tr>
        <th><b>Total Payments:</b></th> 
        <th>{{ sizeof($registrations) }}</th>   	 
    </tr> 
      
    <tr>
        <th></th>
	</tr>              
       
    <tr>        
        <th><b>Title</b></th>
        <th><b>First Name</b></th>
        <th><b>Last Name</b></th>
        <th><b>Email</b></th> 
        <th><b>Mobile</b></th>  
        <th><b>Role</b></th>   
        <th><b>Company/Institution</b></th>       
        <th><b>State</b></th>   
        <th><b>Country</b></th>   
        <th><b>Member ID</b></th>
        
        <th><b>Event</b></th>
        <th><b>Ticket</b></th>
        
        <th><b>Payment Amount</b></th>
        <th><b>Payment Method</b></th> 
        <th><b>Payment Transaction Number</b></th>        
        <th><b>Payment PayPal Payer ID</b></th> 
        <th><b>Payment Status</b></th>        
        <th><b>Payment Date/Time</b></th>        
    </tr>
    </thead>
    <tbody>
    @foreach($registrations as $item)
        @if (!$item->payment || ($item->payment && $item->payment->payment_status != "pending")) 
        <tr>           
            <td>{{ $item->title }}</td>   
            <td>{{ $item->first_name }}</td>   
            <td>{{ $item->last_name }}</td>   
            <td>{{ $item->email_address }}</td>
            <td>{{ $item->phone_mobile }}</td>
            <td>{{ $item->designation }}</td>
            <td>{{ $item->company_name }}</td>
            <td>{{ $item->state }}</td>   
            <td>{{ $item->country }}</td>   
            <td>{{ $item->member_id }}</td>
            
            <td>{{ $item->event->title }}</td>     
            <td>{{ $item->ticket->name }}</td>     
            
            <td>${{ $item->ticket->price }}</td>
            <td>
            @if ($item->payment)
            {{ ucwords(str_replace("-", " " , $item->payment->payment_method)) }}
            @else
               @if ($item->is_manual == "true" && $item->ticket->price > 0)
                   Manual
               @endif
            @endif                        
            </td>
            <td>
            @if ($item->payment)
            {{ ucwords(str_replace("-", " " , $item->payment->payment_transaction_number)) }}
            @endif                        
            </td>
            <td>
            @if ($item->payment)
            {{ ucwords(str_replace("-", " " , $item->payment->paypal_payer_id)) }}
            @endif                        
            </td>
            @if ($item->payment)
            {{ ucwords($item->payment->payment_status) }}
            @endif
            <td>            
            <td>
            @if ($item->payment)
            {{ date('d/m/Y H:i:s' , strtotime($item->payment->created_at)) }}
            @endif
            </td>                      
        </tr>
        @endif
    @endforeach

    </tbody>
</table>