@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/events') }}"><i class="far fa-calendar"></i> {{ $display_name }}</a></li>
                <li><a href="{{ url('dreamcms/events/registration') }}">Registration</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Item</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/events/store-registration') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('event_id')) ? ' has-error' : '' }}"
                                     id="category_selector">
                                    <label class="col-sm-2 control-label">Event *</label>

                                    <div class="col-sm-10{{ ($errors->has('event_id')) ? ' has-error' : '' }}">
                                        @if(count($events)>0)
                                            <select name="event_id" id="event_id" class="form-control select2" data-placeholder="All" style="width: 100%;" required>
                                                @foreach($events as $event)
                                                    <option value="{{ $event->id }}"{{ (old('event_id') == $event->id) ? ' selected="selected"' : '' }}>{{ $event->title }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <div class="callout callout-danger">
                                                <h4>No event found!</h4>
                                                <a href="{{ url('dreamcms/events/add') }}">Please click here to
                                                    add event</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('ticket_id')) ? ' has-error' : '' }}"
                                     id="category_selector">
                                    <label class="col-sm-2 control-label">Ticket *</label>

                                    <div class="col-sm-10{{ ($errors->has('ticket_id')) ? ' has-error' : '' }}">
                                        <select name="ticket_id" id="ticket_id" class="form-control select2" placeholder="None" style="width: 100%;" required>
											@if (count($tickets)>0)                                            
												@foreach($tickets as $ticket)
													<option value="{{ $ticket->id }}"{{ (old('ticket_id') == $ticket->id) ? ' selected="selected"' : '' }}>{{ $ticket->name }}</option>
												@endforeach                                                                                    
											@endif
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('member_id')) ? ' has-error' : '' }}"
                                     id="category_selector">
                                    <label class="col-sm-2 control-label">Member</label>

                                    <div class="col-sm-10{{ ($errors->has('member_id')) ? ' has-error' : '' }}">
                                        @if(count($members)>0)
                                            <select name="member_id" id="member_id" class="form-control select2" placeholder="None" style="width: 100%;">
                                                <option value=""{{ (old('member_id') == "") ? ' selected="selected"' : '' }}></option>
                                                @foreach($members as $member)
                                                    <option value="{{ $member->id }}"{{ (old('member_id') == $member->id) ? ' selected="selected"' : '' }}>{{ $member->title }} {{ $member->firstName }} {{ $member->lastName }}</option>
                                                @endforeach
                                            </select>                                        
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <select name="title" id="title" class="form-control select2" style="width: 100%;" required>
                                            <option value="Mr"{{ (old('title') == "Mr") ? ' selected="selected"' : '' }}>
                                                Mr
                                            </option>
                                            <option value="Mrs"{{ (old('title') == "Mrs") ? ' selected="selected"' : '' }}>
                                                Mrs
                                            </option>
                                            <option value="Misss"{{ (old('title') == "Miss") ? ' selected="selected"' : '' }}>
                                                Miss
                                            </option>
                                            <option value="Ms"{{ (old('title') == "Ms") ? ' selected="selected"' : '' }}>
                                                Ms
                                            </option>
                                            <option value="Dr"{{ (old('title') == "Dr") ? ' selected="selected"' : '' }}>
                                                Dr
                                            </option>
                                        </select>
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>         
                                
                                <div class="form-group{{ ($errors->has('first_name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">First Name *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="first_name" id="first_name"
                                               placeholder="First Name" value="{{ old('first_name') }}" required>
                                        @if ($errors->has('first_name'))
                                            <small class="help-block">{{ $errors->first('first_name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('last_name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Last Name *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="last_name" id="last_name"
                                               placeholder="Last Name" value="{{ old('last_name') }}" required>
                                        @if ($errors->has('last_name'))
                                            <small class="help-block">{{ $errors->first('last_name') }}</small>
                                        @endif
                                    </div>
                                </div> 
                                
                                <div class="form-group{{ ($errors->has('designation')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Role *</label>

                                    <div class="col-sm-10">                                       
                                        <select class="form-control" name="designation" id="designation" required>
												<option value="">Your role</option>
												<option value="Medical oncologist" {{ (old('designation') == "Medical oncologist") ? ' selected="selected"' : '' }}>Medical oncologist</option>
												<option value="Surgical oncologist" {{ (old('designation') == "Surgical oncologist") ? ' selected="selected"' : '' }}>Surgical oncologist</option>
												<option value="Radiation oncologist" {{ (old('designation') == "Radiation oncologist") ? ' selected="selected"' : '' }}>Radiation oncologist</option>
												<option value="Paediatric oncologist" {{ (old('designation') == "Paediatric oncologist") ? ' selected="selected"' : '' }}>Paediatric oncologist</option>
												<option value="AYA oncologist" {{ (old('designation') == "AYA oncologist") ? ' selected="selected"' : '' }}>AYA oncologist</option>
												<option value="Orthopaedic surgeon" {{ (old('designation') == "Orthopaedic surgeon") ? ' selected="selected"' : '' }}>Orthopaedic surgeon</option>
												<option value="Plastic surgeon" {{ (old('designation') == "Plastic surgeon") ? ' selected="selected"' : '' }}>Plastic surgeon</option>
												<option value="Vascular surgeon" {{ (old('designation') == "Vascular surgeon") ? ' selected="selected"' : '' }}>Vascular surgeon</option>
												<option value="Pathologist" {{ (old('designation') == "Pathologist") ? ' selected="selected"' : '' }}>Pathologist</option>
												<option value="Allied health" {{ (old('designation') == "Allied health") ? ' selected="selected"' : '' }}>Allied health</option>
												<option value="Nurse" {{ (old('designation') == "Nurse") ? ' selected="selected"' : '' }}>Nurse</option>
												<option value="Research scientist" {{ (old('designation') == "Research scientist") ? ' selected="selected"' : '' }}>Research scientist</option>
												<option value="Student" {{ (old('designation') == "Student") ? ' selected="selected"' : '' }}>Student</option>
												<option value="Consumer" {{ (old('designation') == "Consumer") ? ' selected="selected"' : '' }}>Consumer</option>
												<option value="Other" {{ (old('designation') == "Other") ? ' selected="selected"' : '' }}>Other</option>
											</select>
                                        @if ($errors->has('designation'))
                                            <small class="help-block">{{ $errors->first('designation') }}</small>
                                        @endif
                                    </div>
                                </div> 
                                
                                <div class="form-group{{ ($errors->has('company_name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Company/Institution *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="company_name" id="company_name"
                                               placeholder="Company/Institution" value="{{ old('company_name') }}" required>
                                        @if ($errors->has('company_name'))
                                            <small class="help-block">{{ $errors->first('company_name') }}</small>
                                        @endif
                                    </div>
                                </div>   
                                
                                <div class="form-group{{ ($errors->has('state')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Location *</label>

                                    <div class="col-sm-10">
                                        <select name="state" class="form-control select2" style="width: 100%;" required>
                                            <option value="ACT"{{ (old('state') == "ACT") ? ' selected="selected"' : '' }}>
                                                ACT
                                            </option>
                                            <option value="QLD"{{ (old('state') == "QLD") ? ' selected="selected"' : '' }}>
                                                QLD
                                            </option>
                                            <option value="NSW"{{ (old('state') == "NSW") ? ' selected="selected"' : '' }}>
                                                NSW
                                            </option>
                                            <option value="NT"{{ (old('state') == "NT") ? ' selected="selected"' : '' }}>
                                                NT
                                            </option>
                                            <option value="SA"{{ (old('state') == "SA") ? ' selected="selected"' : '' }}>
                                                SA
                                            </option>
                                            <option value="TAS"{{ (old('state') == "TAS") ? ' selected="selected"' : '' }}>
                                                TAS
                                            </option>
                                            <option value="VIC"{{ (old('state') == "VIC") ? ' selected="selected"' : '' }}>
                                                VIC
                                            </option>
                                            <option value="WA"{{ (old('state') == "WA") ? ' selected="selected"' : '' }}>
                                                WA
                                            </option>
                                            <option value="NA"{{ (old('state') == "NA") ? ' selected="selected"' : '' }}>
                                                Not Applicable
                                            </option>
                                        </select>
                                        <select name="country" id="country" class="form-control select2" style="width: 100%;" required></select>                                    
                                    </div>
                                </div>   
                                
                                <div class="form-group{{ ($errors->has('email_address')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Email Address *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email_address" id="email_address"
                                               placeholder="Email Address" value="{{ old('email_address') }}" required>
                                        @if ($errors->has('email_address'))
                                            <small class="help-block">{{ $errors->first('email_address') }}</small>
                                        @endif
                                    </div>
                                </div>  
                                
                                <div class="form-group{{ ($errors->has('phone_mobile')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Mobile</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone_mobile" id="phone_mobile"
                                               placeholder="Mobile" value="{{ old('phone_mobile') }}">
                                        @if ($errors->has('phone_mobile'))
                                            <small class="help-block">{{ $errors->first('phone_mobile') }}</small>
                                        @endif
                                    </div>
                                </div>                                                                                                                                                                                                                                                  

                                <div class="box-footer">
                                    <a href="{{ url('dreamcms/events/registrations') }}" class="btn btn-info pull-right"
                                       data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                       data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                       data-btn-cancel-label="No">Cancel</a>
                                    <button type="submit" class="btn btn-info pull-right" name="action"
                                            value="save_close">Save & Close
                                    </button>
                                    <button type="submit" class="btn btn-info pull-right" name="action" value="save">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>    
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>    
    <script src="{{ asset('/components/moment/min/moment.min.js') }}"></script> 
    <script src="{{ asset('/components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script> 
    <script src="{{ asset('/components/jquery-mask-plugin/dist/jquery.mask.js') }}"></script>
    <script src="{{ asset('/components/intl-tel-input/build/js/intlTelInput-jquery.js') }}"></script>   
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
			var countryData = window.intlTelInputGlobals.getCountryData();

            for (var i = 0; i < countryData.length; i++) {
                var country = countryData[i];
                $('#country').append($('<option>', {
                    value: country.name,
                    text: country.name,
                    'data-iso':country.iso2
                }));               
            }
			$("#country").val("Australia");
			
			$("#event_id").on("change", function() {
				$.ajax({
                    type: "POST",
                    url: $(this).val()+"/get-tickets",                    
                    success: function (response) {
                        if (response.status=="success"){                          
						   tickets = response.tickets;
						   for (var i = 0; i < tickets.length; i++) {
								var ticket = tickets[i];
								$('#ticket_id').append($('<option>', {
									value: ticket.id,
									text: ticket.name + " ($" + ticket.price + ")"
								}));               
                           }
                        }
                    }
                });
			});	
			
			$("#member_id").on("change", function() {
				$.ajax({
                    type: "POST",
                    url: $(this).val()+"/get-member",                    
                    success: function (response) {
                        if (response.status=="success"){                          
						   member = response.member;
							
						   $('#title').val(member.title);		
						   $('#first_name').val(member.firstName);	
						   $('#last_name').val(member.lastName);								  
						   $('#company_name').val(member.companyName);
						   $('#state').val(member.state);
						   $('#country').val(member.country);	
						   $('#email_address').val(member.email);	
						   $('#phone_mobile').val(member.phoneMobile);		
                        }
                    }
                });
			});	
        });
    </script>
@endsection