![Echo3](https://www.echo3.com.au/images/logo.png)

# DreamCMS

## Create a New DreamCMS 4.0 Website on Local Webhost

1.  Create a Bit Bucket public Repository.  Ensure that the Private Repository checkbox is not checked.  https://bitbucket.org
2.  On your local computer, create a new Laravel project folder in the Code folder of the Homestead development environment.
3.  Create a new Dreamweaver Site for the new website.
4.  Use a Git Bash command window from the Homestead development environment location (Homestead folder).  
    *   Start the Virtual Machine using the following two commands
        *  vagrant up
        *  VAGRANT_PREFER_SYSTEM_BIN=1 vagrant ssh
    * Go to the new website project folder.
    * Clone the Bit Bucket Repository using the command
        *  git clone git@bitbucket.org:nyree-echo3/websitename.com.au.git  
5.  Using Git, pull latest version of DreamCMS 4.0 from BitBucket to local computer.
6.  Copy all files from the DreamCMS 4.0 Laravel project to the new folder created in step 2.  **Do not copy the .git folder**
7.  Using DreamWeaver’s Git window, add all files to an Initial Commit with a comment.
8.  Using DreamWeaver’s Git window, Git Pull the Initial Commit.
9.  Add a Sites entry to the Homstead \Homstead.yaml file.
10. Add a new IP address mapping to the hosts files on the local computer.
11. Setup a new database for the new project.
12. Update the database connection details in the .env file for the new Laravel project.
13. Use a Git Bash command window from the Homestead development environment location (Homestead folder).  Run the following command.
    *  vagrant provision
14. Use a Git Bash command window from the Homestead development environment location
    *   Start the Virtual Machine using the following two commands    
        *  vagrant up
        *  VAGRANT_PREFER_SYSTEM_BIN=1 vagrant ssh
    *   Start the Virtual Machine using the following two commands
        *  composer install
        *  bower install
        *  php artisan migrate –seed
        *  php artisan key:generate
        *  php artisan cache:forget spatie.permission.cache
        *  php artisan set:permissions
15. Create the initial user administration permissions for the website by opening the following page in the browser.  Need to uncomment the code for the setPerssions function in the app\Console\Commands\SetPermissions.php file.
    *  websitename.test\dreamcms\set-permissions
                
## Setup DreamCMS 4.0 Website on live Webhost

1.  Setup a new database on the webhost.
2.  Setup a new database user on the webhost and give the following database permissions :
    *   SELECT    
    *   INSERT    
    *   UPDATE    
    *   DELETE    
3.  Create a new folder called web under the public_html folder.
4.  Create a SSH account.
5.  Using SSH, run the following commands
    *   cd ~
    *   wget https://nodejs.org/dist/v0.10.15/node-v0.10.15-linux-x64.tar.gz    
    *   tar -xvf node-v0.10.15-linux-x64.tar.gz    
    *   mv node-v0.10.15-linux-x64 nodejs    
    *   mkdir ~/bin
    *   cp nodejs/bin/node ~/bin    
    *   cd ~/bin
    *   ln -s ../nodejs/lib/node_modules/npm/bin/npm-cli.js npm    
    *   node --version
    *   npm --version
    *   npm config set strict-ssl=false
    *   npm config set ca=""
    *   npm install npm -g --ca=null
    *   npm install -g bower
6.  Using SSH, clone the Bit Bucket Repository using the command in the public_html/web folder.  
    *   git clone https://nyree-echo3:Echoecho3@bitbucket.org/nyree-echo3/website.com.au .
7.  Using the web host Control Panel, set the PHP version to 7.2
8.  Create the .env file and update the database connection details.
9.  Using SSH, run the following commands 
     *   composer install
**NOTE  **
Permission issue.  After the first error if you comment out the "getModelForGuard" function in /vendor/konekt/acl/src/Support/helpers.php and run "composer update".
     *   bower install
     *   php artisan migrate –seed (OR setup database using MySQL Export/Import)
10.  Upload /public/media folder using FTP.

## Go Live DreamCMS 4.0 Website

1.  Create .htaccess file in public_html file with the following lines of code.  
    <IfModule mod_rewrite.c>
    RewriteEngine On  
    RewriteRule ^(.*)$ web/public/$1 [L]    
    </IfModule>
2.  Setup a new database user on the webhost and give the following database permissions :
    *   Firefox on desktop
    *   Chrome on desktop
    *   Microsoft Edge on desktop
    *   Apple iPad in landscape
    *   Apple iPad in portrait
    *   Apple iPhone in portrait
    *   Apple iPhone in landscape
3.  Check website has the following logo files:
    *   web/public/images/apple-icon.png
    *   web/public/images/favicon.ico
    *   web/public/images/site/logo.png
    *   web/public/images/site/email-logo-header.gif
    *   web/public/images/site/email-logo-footer.gif
    *   web/public/images/site/og-logo.jpg
    *   web/public/images/site/rss.jpg
4.  In DreamCMS – Settings – General page update the following values: 
    *   Go Live Date
    *   Google Analytics
5.  Add permanent redirect rules for old website in the Routes file.  
    public_html/web/routes/web.php 
6.  Update APP_URL in the Environment file. 
    public_html/web/.env 
7.  Update email details in the Environment file.  Test the Contact Form on the website to ensure the emails are delivered.
    public_html/web/.env 
8.  For the SSL, please follow instructions at https://echo3media.freshdesk.com/a/solutions/.  Update the HTTPS setting to true in the Environment file at public_html/web/.env  
9.  For the sitemap, setup a Cron Job using CPanel as follows 
```
* * * * * cd /home2/folder-of-the-project/public_html/web && /usr/local/bin/ea-php70 artisan schedule:run >> /dev/null 2>&1
(* * * * * Runs once per minute)
```
