<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('category_id');
            $table->string('title');
            $table->string('slug');
            $table->text('meta_title');
            $table->text('meta_keywords');
            $table->text('meta_description');
            $table->text('short_description');
            $table->text('body');
			$table->string('thumbnail')->nullable();
            $table->enum('status', ['active','passive'])->default('passive');
			$table->enum('members_only', ['active','passive'])->default('passive');
            $table->date('start_date');
            $table->date('end_date');
			$table->time('start_time');
            $table->time('end_time');
			$table->enum('is_deleted', ['true','false'])->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
