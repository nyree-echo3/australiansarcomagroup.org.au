<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEventsBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events_bookings', function (Blueprint $table) {			 
			$table->string('phone_mobile')->nullable()->after('email_address');
			$table->string('designation')->nullable()->after('phone_mobile');
			$table->string('company_name')->nullable()->after('designation');
			$table->string('state')->nullable()->after('company_name');
			$table->string('country')->nullable()->after('state');
		});	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
