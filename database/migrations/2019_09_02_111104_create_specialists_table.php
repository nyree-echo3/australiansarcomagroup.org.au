<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialists', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');           
            $table->string('title');
			
			$table->string('address1');
			$table->string('address2')->nullable();
			$table->string('suburb');
			$table->string('state')->nullable();
			$table->string('postcode');
			$table->string('country');
			
			$table->string('contact_name')->nullable();
			$table->string('contact_email')->nullable();
			$table->string('contact_phone')->nullable();
			$table->string('contact_fax')->nullable();
			
			$table->text('medical_oncologist')->nullable();
			$table->text('paediatric_medical_oncologist')->nullable();
			$table->text('adolescent_medical_oncologist')->nullable();
			$table->text('radiation_oncologist')->nullable();
			$table->text('surgical_oncologist')->nullable();
			$table->text('orthopaedic_surgeon')->nullable();
			$table->text('pathologist')->nullable();
			$table->text('radiologist')->nullable();
			$table->text('sarcoma_nurse')->nullable();
			$table->text('other')->nullable();
			
            $table->enum('status', ['active','passive'])->default('passive');   
			$table->integer('position');
			$table->enum('is_deleted', ['true','false'])->default('false');
            $table->timestamps();		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialists');
    }
}
