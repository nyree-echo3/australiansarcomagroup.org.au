<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToEventBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events_bookings', function (Blueprint $table) {
            $table->string('address1')->nullable()->after('last_name');
            $table->string('address2')->nullable()->after('address1');
            $table->string('suburb')->nullable()->after('address2');
            $table->string('postcode')->nullable()->after('suburb');
            $table->string('social_activity')->nullable()->after('postcode');
            $table->string('partner')->nullable()->after('social_activity');
            $table->string('partner_name')->nullable()->after('partner');
            $table->text('partner_diet')->nullable()->after('partner_name');
            $table->string('breastfeeding')->nullable()->after('partner_diet');
            $table->string('delegate_list')->nullable()->after('breastfeeding');
            $table->string('delegate_ack')->nullable()->after('delegate_list');
            $table->string('cancellation')->nullable()->after('delegate_ack');
            $table->string('dietary')->nullable()->after('cancellation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_bookings', function (Blueprint $table) {
            //
        });
    }
}
