<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');            
            $table->text('message')->nullable();
            $table->text('data')->nullable();
            $table->decimal('amount', 8, 2);    
			$table->string('payment_type')->nullable();
			$table->enum('payment_status', ['pending','completed','cancelled','regular_payment_cancelled'])->default('pending');
			$table->string('payment_transaction_number')->nullable();
			$table->string('payment_transaction_result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donations');
    }
}
