<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('members', function (Blueprint $table) {
            $table->dropColumn(['newsletters']);
        });
		
        Schema::table('members', function (Blueprint $table) {
            $table->text('formerName')->nullable()->after('lastName');
			$table->date('dob')->nullable()->after('formerName');
			$table->string('country')->nullable()->after('postcode');
			$table->date('employmentDateCommencement')->nullable()->after('email');
			
			$table->string('employmentAddress1')->nullable()->after('employmentDateCommencement');
			$table->string('employmentAddress2')->nullable()->after('employmentAddress1');
			$table->string('employmentSuburb')->nullable()->after('employmentAddress2');
			$table->string('employmentState')->nullable()->after('employmentSuburb');
			$table->string('employmentPostcode')->nullable()->after('employmentState');
			$table->string('employmentCountry')->nullable()->after('employmentPostcode');
			$table->string('employmentPhoneNumber')->nullable()->after('employmentCountry');
			$table->string('employmentEmail')->nullable()->after('employmentPhoneNumber');
			$table->text('otherExperience')->nullable()->after('employmentEmail');
			$table->text('comments')->nullable()->after('otherExperience');			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
