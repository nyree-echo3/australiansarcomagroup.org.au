<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('category_id')->nullable();            
            $table->string('name');                      
            $table->text('description')->nullable();    
			$table->string('url')->nullable();   
            $table->string('image')->nullable();
            $table->enum('status', ['active','passive'])->default('passive');
            $table->integer('position');
			$table->enum('is_deleted', ['true','false'])->default('false');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
